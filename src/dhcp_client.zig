const std = @import("std");
const log = std.log;
const mem = std.mem;
const ell = @import("ell.zig");
const c = @cImport({
    @cInclude("linux/if_arp.h");
});

const EllError = error{
    EllInitFailed,
};

pub fn main() !void {
    const argv = std.os.argv;
    var mac: [6]u8 = undefined;
    var debug_flag = "[DHCP] ".*;

    if (argv.len < 2) {
        std.debug.print("Usage: {s} <interface index>\n", .{argv[0]});
        std.os.exit(0);
    }

    const ifindex = try std.fmt.parseInt(u32, mem.span(argv[1]), 0);

    if (!ell.l_net_get_mac_address(ifindex, &mac)) {
        std.debug.print("Unable to get address from interface {d}\n", .{ifindex});
        std.os.exit(0);
    }

    if (!ell.l_main_init())
        return EllError.EllInitFailed;
    defer _ = ell.l_main_exit();

    var client = ell.l_dhcp_client_new(ifindex);
    defer ell.l_dhcp_client_destroy(client);

    _ = ell.l_dhcp_client_set_address(client, c.ARPHRD_ETHER, &mac, 6);
    _ = ell.l_dhcp_client_set_event_handler(client, event_handler, null, null);
    _ = ell.l_dhcp_client_set_debug(client, do_debug, &debug_flag, null, ell.L_LOG_DEBUG);
    _ = ell.l_dhcp_client_start(client);

    _ = ell.l_main_run_with_signal(signal_handler, null);
}

pub fn event_handler(
    client: ?*ell.struct_l_dhcp_client,
    event: ell.enum_l_dhcp_client_event,
    user_data: ?*anyopaque,
) callconv(.C) void {
    _ = event;
    _ = user_data;

    const lease = ell.l_dhcp_client_get_lease(client);
    const ip = ell.l_dhcp_lease_get_address(lease);
    const netmask = ell.l_dhcp_lease_get_netmask(lease);
    const gw = ell.l_dhcp_lease_get_gateway(lease);
    const lifetime = ell.l_dhcp_lease_get_lifetime(lease);
    const dns = ell.l_dhcp_lease_get_dns(lease);

    defer {
        ell.l_free(gw);
        ell.l_free(netmask);
        ell.l_free(ip);
    }

    log.info("Lease Obtained:", .{});
    log.info("\tIP: {s}, Netmask: {s}, Gateway: {s}", .{ ip, netmask, gw });
    log.info("Lifetime: {d} seconds", .{lifetime});

    if (dns != null) {
        const dns_concat = ell.l_strjoinv(dns, ',');
        log.info("DNS List: {s}", .{dns_concat});
        ell.l_free(dns_concat);
        ell.l_strfreev(dns);
    } else {
        log.info("No DNS information obtained", .{});
    }
}

pub fn do_debug(str: [*c]const u8, user_data: ?*anyopaque) callconv(.C) void {
    const prefix = user_data;
    if (prefix) |p| {
        var c_str = @ptrCast([*:0]u8, p);
        log.info("{s}{s}", .{ c_str, str });
    } else {
        log.info("{s}", .{str});
    }
}

pub fn signal_handler(signo: u32, user_data: ?*anyopaque) callconv(.C) void {
    _ = user_data;
    switch (signo) {
        std.os.SIG.INT, std.os.SIG.TERM => {
            log.info("Terminate", .{});
            _ = ell.l_main_quit();
        },
        else => {},
    }
}
