pub const __builtin_bswap16 = @import("std").zig.c_builtins.__builtin_bswap16;
pub const __builtin_bswap32 = @import("std").zig.c_builtins.__builtin_bswap32;
pub const __builtin_bswap64 = @import("std").zig.c_builtins.__builtin_bswap64;
pub const __builtin_signbit = @import("std").zig.c_builtins.__builtin_signbit;
pub const __builtin_signbitf = @import("std").zig.c_builtins.__builtin_signbitf;
pub const __builtin_popcount = @import("std").zig.c_builtins.__builtin_popcount;
pub const __builtin_ctz = @import("std").zig.c_builtins.__builtin_ctz;
pub const __builtin_clz = @import("std").zig.c_builtins.__builtin_clz;
pub const __builtin_sqrt = @import("std").zig.c_builtins.__builtin_sqrt;
pub const __builtin_sqrtf = @import("std").zig.c_builtins.__builtin_sqrtf;
pub const __builtin_sin = @import("std").zig.c_builtins.__builtin_sin;
pub const __builtin_sinf = @import("std").zig.c_builtins.__builtin_sinf;
pub const __builtin_cos = @import("std").zig.c_builtins.__builtin_cos;
pub const __builtin_cosf = @import("std").zig.c_builtins.__builtin_cosf;
pub const __builtin_exp = @import("std").zig.c_builtins.__builtin_exp;
pub const __builtin_expf = @import("std").zig.c_builtins.__builtin_expf;
pub const __builtin_exp2 = @import("std").zig.c_builtins.__builtin_exp2;
pub const __builtin_exp2f = @import("std").zig.c_builtins.__builtin_exp2f;
pub const __builtin_log = @import("std").zig.c_builtins.__builtin_log;
pub const __builtin_logf = @import("std").zig.c_builtins.__builtin_logf;
pub const __builtin_log2 = @import("std").zig.c_builtins.__builtin_log2;
pub const __builtin_log2f = @import("std").zig.c_builtins.__builtin_log2f;
pub const __builtin_log10 = @import("std").zig.c_builtins.__builtin_log10;
pub const __builtin_log10f = @import("std").zig.c_builtins.__builtin_log10f;
pub const __builtin_abs = @import("std").zig.c_builtins.__builtin_abs;
pub const __builtin_fabs = @import("std").zig.c_builtins.__builtin_fabs;
pub const __builtin_fabsf = @import("std").zig.c_builtins.__builtin_fabsf;
pub const __builtin_floor = @import("std").zig.c_builtins.__builtin_floor;
pub const __builtin_floorf = @import("std").zig.c_builtins.__builtin_floorf;
pub const __builtin_ceil = @import("std").zig.c_builtins.__builtin_ceil;
pub const __builtin_ceilf = @import("std").zig.c_builtins.__builtin_ceilf;
pub const __builtin_trunc = @import("std").zig.c_builtins.__builtin_trunc;
pub const __builtin_truncf = @import("std").zig.c_builtins.__builtin_truncf;
pub const __builtin_round = @import("std").zig.c_builtins.__builtin_round;
pub const __builtin_roundf = @import("std").zig.c_builtins.__builtin_roundf;
pub const __builtin_strlen = @import("std").zig.c_builtins.__builtin_strlen;
pub const __builtin_strcmp = @import("std").zig.c_builtins.__builtin_strcmp;
pub const __builtin_object_size = @import("std").zig.c_builtins.__builtin_object_size;
pub const __builtin___memset_chk = @import("std").zig.c_builtins.__builtin___memset_chk;
pub const __builtin_memset = @import("std").zig.c_builtins.__builtin_memset;
pub const __builtin___memcpy_chk = @import("std").zig.c_builtins.__builtin___memcpy_chk;
pub const __builtin_memcpy = @import("std").zig.c_builtins.__builtin_memcpy;
pub const __builtin_expect = @import("std").zig.c_builtins.__builtin_expect;
pub const __builtin_nanf = @import("std").zig.c_builtins.__builtin_nanf;
pub const __builtin_huge_valf = @import("std").zig.c_builtins.__builtin_huge_valf;
pub const __builtin_inff = @import("std").zig.c_builtins.__builtin_inff;
pub const __builtin_isnan = @import("std").zig.c_builtins.__builtin_isnan;
pub const __builtin_isinf = @import("std").zig.c_builtins.__builtin_isinf;
pub const __builtin_isinf_sign = @import("std").zig.c_builtins.__builtin_isinf_sign;
pub const __has_builtin = @import("std").zig.c_builtins.__has_builtin;
pub const __builtin_assume = @import("std").zig.c_builtins.__builtin_assume;
pub const __builtin_unreachable = @import("std").zig.c_builtins.__builtin_unreachable;
pub const __builtin_constant_p = @import("std").zig.c_builtins.__builtin_constant_p;
pub const __builtin_mul_overflow = @import("std").zig.c_builtins.__builtin_mul_overflow;
pub extern fn memcpy(__dest: ?*anyopaque, __src: ?*const anyopaque, __n: c_ulong) ?*anyopaque;
pub extern fn memmove(__dest: ?*anyopaque, __src: ?*const anyopaque, __n: c_ulong) ?*anyopaque;
pub extern fn memccpy(__dest: ?*anyopaque, __src: ?*const anyopaque, __c: c_int, __n: c_ulong) ?*anyopaque;
pub extern fn memset(__s: ?*anyopaque, __c: c_int, __n: c_ulong) ?*anyopaque;
pub extern fn memcmp(__s1: ?*const anyopaque, __s2: ?*const anyopaque, __n: c_ulong) c_int;
pub extern fn __memcmpeq(__s1: ?*const anyopaque, __s2: ?*const anyopaque, __n: usize) c_int;
pub extern fn memchr(__s: ?*const anyopaque, __c: c_int, __n: c_ulong) ?*anyopaque;
pub extern fn strcpy(__dest: [*c]u8, __src: [*c]const u8) [*c]u8;
pub extern fn strncpy(__dest: [*c]u8, __src: [*c]const u8, __n: c_ulong) [*c]u8;
pub extern fn strcat(__dest: [*c]u8, __src: [*c]const u8) [*c]u8;
pub extern fn strncat(__dest: [*c]u8, __src: [*c]const u8, __n: c_ulong) [*c]u8;
pub extern fn strcmp(__s1: [*c]const u8, __s2: [*c]const u8) c_int;
pub extern fn strncmp(__s1: [*c]const u8, __s2: [*c]const u8, __n: c_ulong) c_int;
pub extern fn strcoll(__s1: [*c]const u8, __s2: [*c]const u8) c_int;
pub extern fn strxfrm(__dest: [*c]u8, __src: [*c]const u8, __n: c_ulong) c_ulong;
pub const struct___locale_data = opaque {};
pub const struct___locale_struct = extern struct {
    __locales: [13]?*struct___locale_data,
    __ctype_b: [*c]const c_ushort,
    __ctype_tolower: [*c]const c_int,
    __ctype_toupper: [*c]const c_int,
    __names: [13][*c]const u8,
};
pub const __locale_t = [*c]struct___locale_struct;
pub const locale_t = __locale_t;
pub extern fn strcoll_l(__s1: [*c]const u8, __s2: [*c]const u8, __l: locale_t) c_int;
pub extern fn strxfrm_l(__dest: [*c]u8, __src: [*c]const u8, __n: usize, __l: locale_t) usize;
pub extern fn strdup(__s: [*c]const u8) [*c]u8;
pub extern fn strndup(__string: [*c]const u8, __n: c_ulong) [*c]u8;
pub extern fn strchr(__s: [*c]const u8, __c: c_int) [*c]u8;
pub extern fn strrchr(__s: [*c]const u8, __c: c_int) [*c]u8;
pub extern fn strcspn(__s: [*c]const u8, __reject: [*c]const u8) c_ulong;
pub extern fn strspn(__s: [*c]const u8, __accept: [*c]const u8) c_ulong;
pub extern fn strpbrk(__s: [*c]const u8, __accept: [*c]const u8) [*c]u8;
pub extern fn strstr(__haystack: [*c]const u8, __needle: [*c]const u8) [*c]u8;
pub extern fn strtok(__s: [*c]u8, __delim: [*c]const u8) [*c]u8;
pub extern fn __strtok_r(noalias __s: [*c]u8, noalias __delim: [*c]const u8, noalias __save_ptr: [*c][*c]u8) [*c]u8;
pub extern fn strtok_r(noalias __s: [*c]u8, noalias __delim: [*c]const u8, noalias __save_ptr: [*c][*c]u8) [*c]u8;
pub extern fn strlen(__s: [*c]const u8) c_ulong;
pub extern fn strnlen(__string: [*c]const u8, __maxlen: usize) usize;
pub extern fn strerror(__errnum: c_int) [*c]u8;
pub extern fn strerror_r(__errnum: c_int, __buf: [*c]u8, __buflen: usize) c_int;
pub extern fn strerror_l(__errnum: c_int, __l: locale_t) [*c]u8;
pub extern fn bcmp(__s1: ?*const anyopaque, __s2: ?*const anyopaque, __n: c_ulong) c_int;
pub extern fn bcopy(__src: ?*const anyopaque, __dest: ?*anyopaque, __n: usize) void;
pub extern fn bzero(__s: ?*anyopaque, __n: c_ulong) void;
pub extern fn index(__s: [*c]const u8, __c: c_int) [*c]u8;
pub extern fn rindex(__s: [*c]const u8, __c: c_int) [*c]u8;
pub extern fn ffs(__i: c_int) c_int;
pub extern fn ffsl(__l: c_long) c_int;
pub extern fn ffsll(__ll: c_longlong) c_int;
pub extern fn strcasecmp(__s1: [*c]const u8, __s2: [*c]const u8) c_int;
pub extern fn strncasecmp(__s1: [*c]const u8, __s2: [*c]const u8, __n: c_ulong) c_int;
pub extern fn strcasecmp_l(__s1: [*c]const u8, __s2: [*c]const u8, __loc: locale_t) c_int;
pub extern fn strncasecmp_l(__s1: [*c]const u8, __s2: [*c]const u8, __n: usize, __loc: locale_t) c_int;
pub extern fn explicit_bzero(__s: ?*anyopaque, __n: usize) void;
pub extern fn strsep(noalias __stringp: [*c][*c]u8, noalias __delim: [*c]const u8) [*c]u8;
pub extern fn strsignal(__sig: c_int) [*c]u8;
pub extern fn __stpcpy(noalias __dest: [*c]u8, noalias __src: [*c]const u8) [*c]u8;
pub extern fn stpcpy(__dest: [*c]u8, __src: [*c]const u8) [*c]u8;
pub extern fn __stpncpy(noalias __dest: [*c]u8, noalias __src: [*c]const u8, __n: usize) [*c]u8;
pub extern fn stpncpy(__dest: [*c]u8, __src: [*c]const u8, __n: c_ulong) [*c]u8;
pub const struct___va_list_tag = extern struct {
    gp_offset: c_uint,
    fp_offset: c_uint,
    overflow_arg_area: ?*anyopaque,
    reg_save_area: ?*anyopaque,
};
pub const __builtin_va_list = [1]struct___va_list_tag;
pub const va_list = __builtin_va_list;
pub const __gnuc_va_list = __builtin_va_list;
pub const __u_char = u8;
pub const __u_short = c_ushort;
pub const __u_int = c_uint;
pub const __u_long = c_ulong;
pub const __int8_t = i8;
pub const __uint8_t = u8;
pub const __int16_t = c_short;
pub const __uint16_t = c_ushort;
pub const __int32_t = c_int;
pub const __uint32_t = c_uint;
pub const __int64_t = c_long;
pub const __uint64_t = c_ulong;
pub const __int_least8_t = __int8_t;
pub const __uint_least8_t = __uint8_t;
pub const __int_least16_t = __int16_t;
pub const __uint_least16_t = __uint16_t;
pub const __int_least32_t = __int32_t;
pub const __uint_least32_t = __uint32_t;
pub const __int_least64_t = __int64_t;
pub const __uint_least64_t = __uint64_t;
pub const __quad_t = c_long;
pub const __u_quad_t = c_ulong;
pub const __intmax_t = c_long;
pub const __uintmax_t = c_ulong;
pub const __dev_t = c_ulong;
pub const __uid_t = c_uint;
pub const __gid_t = c_uint;
pub const __ino_t = c_ulong;
pub const __ino64_t = c_ulong;
pub const __mode_t = c_uint;
pub const __nlink_t = c_ulong;
pub const __off_t = c_long;
pub const __off64_t = c_long;
pub const __pid_t = c_int;
pub const __fsid_t = extern struct {
    __val: [2]c_int,
};
pub const __clock_t = c_long;
pub const __rlim_t = c_ulong;
pub const __rlim64_t = c_ulong;
pub const __id_t = c_uint;
pub const __time_t = c_long;
pub const __useconds_t = c_uint;
pub const __suseconds_t = c_long;
pub const __suseconds64_t = c_long;
pub const __daddr_t = c_int;
pub const __key_t = c_int;
pub const __clockid_t = c_int;
pub const __timer_t = ?*anyopaque;
pub const __blksize_t = c_long;
pub const __blkcnt_t = c_long;
pub const __blkcnt64_t = c_long;
pub const __fsblkcnt_t = c_ulong;
pub const __fsblkcnt64_t = c_ulong;
pub const __fsfilcnt_t = c_ulong;
pub const __fsfilcnt64_t = c_ulong;
pub const __fsword_t = c_long;
pub const __ssize_t = c_long;
pub const __syscall_slong_t = c_long;
pub const __syscall_ulong_t = c_ulong;
pub const __loff_t = __off64_t;
pub const __caddr_t = [*c]u8;
pub const __intptr_t = c_long;
pub const __socklen_t = c_uint;
pub const __sig_atomic_t = c_int;
pub const int_least8_t = __int_least8_t;
pub const int_least16_t = __int_least16_t;
pub const int_least32_t = __int_least32_t;
pub const int_least64_t = __int_least64_t;
pub const uint_least8_t = __uint_least8_t;
pub const uint_least16_t = __uint_least16_t;
pub const uint_least32_t = __uint_least32_t;
pub const uint_least64_t = __uint_least64_t;
pub const int_fast8_t = i8;
pub const int_fast16_t = c_long;
pub const int_fast32_t = c_long;
pub const int_fast64_t = c_long;
pub const uint_fast8_t = u8;
pub const uint_fast16_t = c_ulong;
pub const uint_fast32_t = c_ulong;
pub const uint_fast64_t = c_ulong;
pub const intmax_t = __intmax_t;
pub const uintmax_t = __uintmax_t;
pub const __gwchar_t = c_int;
pub const imaxdiv_t = extern struct {
    quot: c_long,
    rem: c_long,
};
pub extern fn imaxabs(__n: intmax_t) intmax_t;
pub extern fn imaxdiv(__numer: intmax_t, __denom: intmax_t) imaxdiv_t;
pub extern fn strtoimax(noalias __nptr: [*c]const u8, noalias __endptr: [*c][*c]u8, __base: c_int) intmax_t;
pub extern fn strtoumax(noalias __nptr: [*c]const u8, noalias __endptr: [*c][*c]u8, __base: c_int) uintmax_t;
pub extern fn wcstoimax(noalias __nptr: [*c]const __gwchar_t, noalias __endptr: [*c][*c]__gwchar_t, __base: c_int) intmax_t;
pub extern fn wcstoumax(noalias __nptr: [*c]const __gwchar_t, noalias __endptr: [*c][*c]__gwchar_t, __base: c_int) uintmax_t;
pub fn __bswap_16(arg___bsx: __uint16_t) callconv(.C) __uint16_t {
    var __bsx = arg___bsx;
    return @bitCast(__uint16_t, @truncate(c_short, ((@bitCast(c_int, @as(c_uint, __bsx)) >> @intCast(@import("std").math.Log2Int(c_int), 8)) & @as(c_int, 255)) | ((@bitCast(c_int, @as(c_uint, __bsx)) & @as(c_int, 255)) << @intCast(@import("std").math.Log2Int(c_int), 8))));
}
pub fn __bswap_32(arg___bsx: __uint32_t) callconv(.C) __uint32_t {
    var __bsx = arg___bsx;
    return ((((__bsx & @as(c_uint, 4278190080)) >> @intCast(@import("std").math.Log2Int(c_uint), 24)) | ((__bsx & @as(c_uint, 16711680)) >> @intCast(@import("std").math.Log2Int(c_uint), 8))) | ((__bsx & @as(c_uint, 65280)) << @intCast(@import("std").math.Log2Int(c_uint), 8))) | ((__bsx & @as(c_uint, 255)) << @intCast(@import("std").math.Log2Int(c_uint), 24));
}
pub fn __bswap_64(arg___bsx: __uint64_t) callconv(.C) __uint64_t {
    var __bsx = arg___bsx;
    return @bitCast(__uint64_t, @truncate(c_ulong, ((((((((@bitCast(c_ulonglong, @as(c_ulonglong, __bsx)) & @as(c_ulonglong, 18374686479671623680)) >> @intCast(@import("std").math.Log2Int(c_ulonglong), 56)) | ((@bitCast(c_ulonglong, @as(c_ulonglong, __bsx)) & @as(c_ulonglong, 71776119061217280)) >> @intCast(@import("std").math.Log2Int(c_ulonglong), 40))) | ((@bitCast(c_ulonglong, @as(c_ulonglong, __bsx)) & @as(c_ulonglong, 280375465082880)) >> @intCast(@import("std").math.Log2Int(c_ulonglong), 24))) | ((@bitCast(c_ulonglong, @as(c_ulonglong, __bsx)) & @as(c_ulonglong, 1095216660480)) >> @intCast(@import("std").math.Log2Int(c_ulonglong), 8))) | ((@bitCast(c_ulonglong, @as(c_ulonglong, __bsx)) & @as(c_ulonglong, 4278190080)) << @intCast(@import("std").math.Log2Int(c_ulonglong), 8))) | ((@bitCast(c_ulonglong, @as(c_ulonglong, __bsx)) & @as(c_ulonglong, 16711680)) << @intCast(@import("std").math.Log2Int(c_ulonglong), 24))) | ((@bitCast(c_ulonglong, @as(c_ulonglong, __bsx)) & @as(c_ulonglong, 65280)) << @intCast(@import("std").math.Log2Int(c_ulonglong), 40))) | ((@bitCast(c_ulonglong, @as(c_ulonglong, __bsx)) & @as(c_ulonglong, 255)) << @intCast(@import("std").math.Log2Int(c_ulonglong), 56))));
}
pub fn __uint16_identity(arg___x: __uint16_t) callconv(.C) __uint16_t {
    var __x = arg___x;
    return __x;
}
pub fn __uint32_identity(arg___x: __uint32_t) callconv(.C) __uint32_t {
    var __x = arg___x;
    return __x;
}
pub fn __uint64_identity(arg___x: __uint64_t) callconv(.C) __uint64_t {
    var __x = arg___x;
    return __x;
}
pub const u_char = __u_char;
pub const u_short = __u_short;
pub const u_int = __u_int;
pub const u_long = __u_long;
pub const quad_t = __quad_t;
pub const u_quad_t = __u_quad_t;
pub const fsid_t = __fsid_t;
pub const loff_t = __loff_t;
pub const ino_t = __ino_t;
pub const dev_t = __dev_t;
pub const gid_t = __gid_t;
pub const mode_t = __mode_t;
pub const nlink_t = __nlink_t;
pub const uid_t = __uid_t;
pub const off_t = __off_t;
pub const pid_t = __pid_t;
pub const id_t = __id_t;
pub const daddr_t = __daddr_t;
pub const caddr_t = __caddr_t;
pub const key_t = __key_t;
pub const clock_t = __clock_t;
pub const clockid_t = __clockid_t;
pub const time_t = __time_t;
pub const timer_t = __timer_t;
pub const ulong = c_ulong;
pub const ushort = c_ushort;
pub const uint = c_uint;
pub const u_int8_t = __uint8_t;
pub const u_int16_t = __uint16_t;
pub const u_int32_t = __uint32_t;
pub const u_int64_t = __uint64_t;
pub const register_t = c_long;
pub const __sigset_t = extern struct {
    __val: [16]c_ulong,
};
pub const sigset_t = __sigset_t;
pub const struct_timeval = extern struct {
    tv_sec: __time_t,
    tv_usec: __suseconds_t,
};
pub const struct_timespec = extern struct {
    tv_sec: __time_t,
    tv_nsec: __syscall_slong_t,
};
pub const suseconds_t = __suseconds_t;
pub const __fd_mask = c_long;
pub const fd_set = extern struct {
    __fds_bits: [16]__fd_mask,
};
pub const fd_mask = __fd_mask;
pub extern fn select(__nfds: c_int, noalias __readfds: [*c]fd_set, noalias __writefds: [*c]fd_set, noalias __exceptfds: [*c]fd_set, noalias __timeout: [*c]struct_timeval) c_int;
pub extern fn pselect(__nfds: c_int, noalias __readfds: [*c]fd_set, noalias __writefds: [*c]fd_set, noalias __exceptfds: [*c]fd_set, noalias __timeout: [*c]const struct_timespec, noalias __sigmask: [*c]const __sigset_t) c_int;
pub const blksize_t = __blksize_t;
pub const blkcnt_t = __blkcnt_t;
pub const fsblkcnt_t = __fsblkcnt_t;
pub const fsfilcnt_t = __fsfilcnt_t;
const struct_unnamed_1 = extern struct {
    __low: c_uint,
    __high: c_uint,
};
pub const __atomic_wide_counter = extern union {
    __value64: c_ulonglong,
    __value32: struct_unnamed_1,
};
pub const struct___pthread_internal_list = extern struct {
    __prev: [*c]struct___pthread_internal_list,
    __next: [*c]struct___pthread_internal_list,
};
pub const __pthread_list_t = struct___pthread_internal_list;
pub const struct___pthread_internal_slist = extern struct {
    __next: [*c]struct___pthread_internal_slist,
};
pub const __pthread_slist_t = struct___pthread_internal_slist;
pub const struct___pthread_mutex_s = extern struct {
    __lock: c_int,
    __count: c_uint,
    __owner: c_int,
    __nusers: c_uint,
    __kind: c_int,
    __spins: c_short,
    __elision: c_short,
    __list: __pthread_list_t,
};
pub const struct___pthread_rwlock_arch_t = extern struct {
    __readers: c_uint,
    __writers: c_uint,
    __wrphase_futex: c_uint,
    __writers_futex: c_uint,
    __pad3: c_uint,
    __pad4: c_uint,
    __cur_writer: c_int,
    __shared: c_int,
    __rwelision: i8,
    __pad1: [7]u8,
    __pad2: c_ulong,
    __flags: c_uint,
};
pub const struct___pthread_cond_s = extern struct {
    __wseq: __atomic_wide_counter,
    __g1_start: __atomic_wide_counter,
    __g_refs: [2]c_uint,
    __g_size: [2]c_uint,
    __g1_orig_size: c_uint,
    __wrefs: c_uint,
    __g_signals: [2]c_uint,
};
pub const __tss_t = c_uint;
pub const __thrd_t = c_ulong;
pub const __once_flag = extern struct {
    __data: c_int,
};
pub const pthread_t = c_ulong;
pub const pthread_mutexattr_t = extern union {
    __size: [4]u8,
    __align: c_int,
};
pub const pthread_condattr_t = extern union {
    __size: [4]u8,
    __align: c_int,
};
pub const pthread_key_t = c_uint;
pub const pthread_once_t = c_int;
pub const union_pthread_attr_t = extern union {
    __size: [56]u8,
    __align: c_long,
};
pub const pthread_attr_t = union_pthread_attr_t;
pub const pthread_mutex_t = extern union {
    __data: struct___pthread_mutex_s,
    __size: [40]u8,
    __align: c_long,
};
pub const pthread_cond_t = extern union {
    __data: struct___pthread_cond_s,
    __size: [48]u8,
    __align: c_longlong,
};
pub const pthread_rwlock_t = extern union {
    __data: struct___pthread_rwlock_arch_t,
    __size: [56]u8,
    __align: c_long,
};
pub const pthread_rwlockattr_t = extern union {
    __size: [8]u8,
    __align: c_long,
};
pub const pthread_spinlock_t = c_int;
pub const pthread_barrier_t = extern union {
    __size: [32]u8,
    __align: c_long,
};
pub const pthread_barrierattr_t = extern union {
    __size: [4]u8,
    __align: c_int,
};
pub const struct_iovec = extern struct {
    iov_base: ?*anyopaque,
    iov_len: usize,
};
pub extern fn readv(__fd: c_int, __iovec: [*c]const struct_iovec, __count: c_int) isize;
pub extern fn writev(__fd: c_int, __iovec: [*c]const struct_iovec, __count: c_int) isize;
pub extern fn preadv(__fd: c_int, __iovec: [*c]const struct_iovec, __count: c_int, __offset: __off_t) isize;
pub extern fn pwritev(__fd: c_int, __iovec: [*c]const struct_iovec, __count: c_int, __offset: __off_t) isize;
pub fn l_get_u8(arg_ptr: ?*const anyopaque) callconv(.C) u8 {
    const ptr = arg_ptr;
    return @ptrCast([*c]const u8, @alignCast(@import("std").meta.alignment([*c]const u8), ptr)).*;
}
pub fn l_put_u8(arg_val: u8, arg_ptr: ?*anyopaque) callconv(.C) void {
    var val = arg_val;
    const ptr = arg_ptr;
    @ptrCast([*c]u8, @alignCast(@import("std").meta.alignment([*c]u8), ptr)).* = val;
}
pub fn l_get_u16(arg_ptr: ?*const anyopaque) callconv(.C) u16 {
    const ptr = arg_ptr;
    return blk: {
        const struct_unnamed_2 = extern struct {
            __v: @TypeOf(@ptrCast([*c]const u16, @alignCast(@import("std").meta.alignment([*c]const u16), ptr)).*) align(1),
        };
        _ = @TypeOf(struct_unnamed_2);
        var __p: [*c]struct_unnamed_2 = @intToPtr([*c]struct_unnamed_2, @ptrToInt(@ptrCast([*c]const u16, @alignCast(@import("std").meta.alignment([*c]const u16), ptr))));
        break :blk __p.*.__v;
    };
}
pub fn l_put_u16(arg_val: u16, arg_ptr: ?*anyopaque) callconv(.C) void {
    var val = arg_val;
    const ptr = arg_ptr;
    while (true) {
        const struct_unnamed_3 = extern struct {
            __v: @TypeOf(@ptrCast([*c]u16, @alignCast(@import("std").meta.alignment([*c]u16), ptr)).*) align(1),
        };
        _ = @TypeOf(struct_unnamed_3);
        var __p: [*c]struct_unnamed_3 = @ptrCast([*c]struct_unnamed_3, @alignCast(@import("std").meta.alignment([*c]struct_unnamed_3), @ptrCast([*c]u16, @alignCast(@import("std").meta.alignment([*c]u16), ptr))));
        __p.*.__v = val;
        if (!false) break;
    }
}
pub fn l_get_u32(arg_ptr: ?*const anyopaque) callconv(.C) u32 {
    const ptr = arg_ptr;
    return blk: {
        const struct_unnamed_4 = extern struct {
            __v: @TypeOf(@ptrCast([*c]const u32, @alignCast(@import("std").meta.alignment([*c]const u32), ptr)).*) align(1),
        };
        _ = @TypeOf(struct_unnamed_4);
        var __p: [*c]struct_unnamed_4 = @intToPtr([*c]struct_unnamed_4, @ptrToInt(@ptrCast([*c]const u32, @alignCast(@import("std").meta.alignment([*c]const u32), ptr))));
        break :blk __p.*.__v;
    };
}
pub fn l_put_u32(arg_val: u32, arg_ptr: ?*anyopaque) callconv(.C) void {
    var val = arg_val;
    const ptr = arg_ptr;
    while (true) {
        const struct_unnamed_5 = extern struct {
            __v: @TypeOf(@ptrCast([*c]u32, @alignCast(@import("std").meta.alignment([*c]u32), ptr)).*) align(1),
        };
        _ = @TypeOf(struct_unnamed_5);
        var __p: [*c]struct_unnamed_5 = @ptrCast([*c]struct_unnamed_5, @alignCast(@import("std").meta.alignment([*c]struct_unnamed_5), @ptrCast([*c]u32, @alignCast(@import("std").meta.alignment([*c]u32), ptr))));
        __p.*.__v = val;
        if (!false) break;
    }
}
pub fn l_get_u64(arg_ptr: ?*const anyopaque) callconv(.C) u64 {
    const ptr = arg_ptr;
    return blk: {
        const struct_unnamed_6 = extern struct {
            __v: @TypeOf(@ptrCast([*c]const u64, @alignCast(@import("std").meta.alignment([*c]const u64), ptr)).*) align(1),
        };
        _ = @TypeOf(struct_unnamed_6);
        var __p: [*c]struct_unnamed_6 = @intToPtr([*c]struct_unnamed_6, @ptrToInt(@ptrCast([*c]const u64, @alignCast(@import("std").meta.alignment([*c]const u64), ptr))));
        break :blk __p.*.__v;
    };
}
pub fn l_put_u64(arg_val: u64, arg_ptr: ?*anyopaque) callconv(.C) void {
    var val = arg_val;
    const ptr = arg_ptr;
    while (true) {
        const struct_unnamed_7 = extern struct {
            __v: @TypeOf(@ptrCast([*c]u64, @alignCast(@import("std").meta.alignment([*c]u64), ptr)).*) align(1),
        };
        _ = @TypeOf(struct_unnamed_7);
        var __p: [*c]struct_unnamed_7 = @ptrCast([*c]struct_unnamed_7, @alignCast(@import("std").meta.alignment([*c]struct_unnamed_7), @ptrCast([*c]u64, @alignCast(@import("std").meta.alignment([*c]u64), ptr))));
        __p.*.__v = val;
        if (!false) break;
    }
}
pub fn l_get_s16(arg_ptr: ?*const anyopaque) callconv(.C) i16 {
    const ptr = arg_ptr;
    return blk: {
        const struct_unnamed_8 = extern struct {
            __v: @TypeOf(@ptrCast([*c]const i16, @alignCast(@import("std").meta.alignment([*c]const i16), ptr)).*) align(1),
        };
        _ = @TypeOf(struct_unnamed_8);
        var __p: [*c]struct_unnamed_8 = @intToPtr([*c]struct_unnamed_8, @ptrToInt(@ptrCast([*c]const i16, @alignCast(@import("std").meta.alignment([*c]const i16), ptr))));
        break :blk __p.*.__v;
    };
}
pub fn l_get_s32(arg_ptr: ?*const anyopaque) callconv(.C) i32 {
    const ptr = arg_ptr;
    return blk: {
        const struct_unnamed_9 = extern struct {
            __v: @TypeOf(@ptrCast([*c]const i32, @alignCast(@import("std").meta.alignment([*c]const i32), ptr)).*) align(1),
        };
        _ = @TypeOf(struct_unnamed_9);
        var __p: [*c]struct_unnamed_9 = @intToPtr([*c]struct_unnamed_9, @ptrToInt(@ptrCast([*c]const i32, @alignCast(@import("std").meta.alignment([*c]const i32), ptr))));
        break :blk __p.*.__v;
    };
}
pub fn l_get_s64(arg_ptr: ?*const anyopaque) callconv(.C) i64 {
    const ptr = arg_ptr;
    return blk: {
        const struct_unnamed_10 = extern struct {
            __v: @TypeOf(@ptrCast([*c]const i64, @alignCast(@import("std").meta.alignment([*c]const i64), ptr)).*) align(1),
        };
        _ = @TypeOf(struct_unnamed_10);
        var __p: [*c]struct_unnamed_10 = @intToPtr([*c]struct_unnamed_10, @ptrToInt(@ptrCast([*c]const i64, @alignCast(@import("std").meta.alignment([*c]const i64), ptr))));
        break :blk __p.*.__v;
    };
}
pub fn l_get_le16(arg_ptr: ?*const anyopaque) callconv(.C) u16 {
    const ptr = arg_ptr;
    return blk: {
        const struct_unnamed_11 = extern struct {
            __v: @TypeOf(@ptrCast([*c]const u16, @alignCast(@import("std").meta.alignment([*c]const u16), ptr)).*) align(1),
        };
        _ = @TypeOf(struct_unnamed_11);
        var __p: [*c]struct_unnamed_11 = @intToPtr([*c]struct_unnamed_11, @ptrToInt(@ptrCast([*c]const u16, @alignCast(@import("std").meta.alignment([*c]const u16), ptr))));
        break :blk __p.*.__v;
    };
}
pub fn l_get_be16(arg_ptr: ?*const anyopaque) callconv(.C) u16 {
    const ptr = arg_ptr;
    return __bswap_16(blk: {
        const struct_unnamed_12 = extern struct {
            __v: @TypeOf(@ptrCast([*c]const u16, @alignCast(@import("std").meta.alignment([*c]const u16), ptr)).*) align(1),
        };
        _ = @TypeOf(struct_unnamed_12);
        var __p: [*c]struct_unnamed_12 = @intToPtr([*c]struct_unnamed_12, @ptrToInt(@ptrCast([*c]const u16, @alignCast(@import("std").meta.alignment([*c]const u16), ptr))));
        break :blk __p.*.__v;
    });
}
pub fn l_get_le32(arg_ptr: ?*const anyopaque) callconv(.C) u32 {
    const ptr = arg_ptr;
    return blk: {
        const struct_unnamed_13 = extern struct {
            __v: @TypeOf(@ptrCast([*c]const u32, @alignCast(@import("std").meta.alignment([*c]const u32), ptr)).*) align(1),
        };
        _ = @TypeOf(struct_unnamed_13);
        var __p: [*c]struct_unnamed_13 = @intToPtr([*c]struct_unnamed_13, @ptrToInt(@ptrCast([*c]const u32, @alignCast(@import("std").meta.alignment([*c]const u32), ptr))));
        break :blk __p.*.__v;
    };
}
pub fn l_get_be32(arg_ptr: ?*const anyopaque) callconv(.C) u32 {
    const ptr = arg_ptr;
    return __bswap_32(blk: {
        const struct_unnamed_14 = extern struct {
            __v: @TypeOf(@ptrCast([*c]const u32, @alignCast(@import("std").meta.alignment([*c]const u32), ptr)).*) align(1),
        };
        _ = @TypeOf(struct_unnamed_14);
        var __p: [*c]struct_unnamed_14 = @intToPtr([*c]struct_unnamed_14, @ptrToInt(@ptrCast([*c]const u32, @alignCast(@import("std").meta.alignment([*c]const u32), ptr))));
        break :blk __p.*.__v;
    });
}
pub fn l_get_le64(arg_ptr: ?*const anyopaque) callconv(.C) u64 {
    const ptr = arg_ptr;
    return blk: {
        const struct_unnamed_15 = extern struct {
            __v: @TypeOf(@ptrCast([*c]const u64, @alignCast(@import("std").meta.alignment([*c]const u64), ptr)).*) align(1),
        };
        _ = @TypeOf(struct_unnamed_15);
        var __p: [*c]struct_unnamed_15 = @intToPtr([*c]struct_unnamed_15, @ptrToInt(@ptrCast([*c]const u64, @alignCast(@import("std").meta.alignment([*c]const u64), ptr))));
        break :blk __p.*.__v;
    };
}
pub fn l_get_be64(arg_ptr: ?*const anyopaque) callconv(.C) u64 {
    const ptr = arg_ptr;
    return __bswap_64(blk: {
        const struct_unnamed_16 = extern struct {
            __v: @TypeOf(@ptrCast([*c]const u64, @alignCast(@import("std").meta.alignment([*c]const u64), ptr)).*) align(1),
        };
        _ = @TypeOf(struct_unnamed_16);
        var __p: [*c]struct_unnamed_16 = @intToPtr([*c]struct_unnamed_16, @ptrToInt(@ptrCast([*c]const u64, @alignCast(@import("std").meta.alignment([*c]const u64), ptr))));
        break :blk __p.*.__v;
    });
}
pub fn l_put_le16(arg_val: u16, arg_ptr: ?*anyopaque) callconv(.C) void {
    var val = arg_val;
    const ptr = arg_ptr;
    while (true) {
        const struct_unnamed_17 = extern struct {
            __v: @TypeOf(@ptrCast([*c]u16, @alignCast(@import("std").meta.alignment([*c]u16), ptr)).*) align(1),
        };
        _ = @TypeOf(struct_unnamed_17);
        var __p: [*c]struct_unnamed_17 = @ptrCast([*c]struct_unnamed_17, @alignCast(@import("std").meta.alignment([*c]struct_unnamed_17), @ptrCast([*c]u16, @alignCast(@import("std").meta.alignment([*c]u16), ptr))));
        __p.*.__v = val;
        if (!false) break;
    }
}
pub fn l_put_be16(arg_val: u16, arg_ptr: ?*const anyopaque) callconv(.C) void {
    var val = arg_val;
    const ptr = arg_ptr;
    while (true) {
        const struct_unnamed_18 = extern struct {
            __v: @TypeOf(@intToPtr([*c]u16, @ptrToInt(ptr)).*) align(1),
        };
        _ = @TypeOf(struct_unnamed_18);
        var __p: [*c]struct_unnamed_18 = @ptrCast([*c]struct_unnamed_18, @alignCast(@import("std").meta.alignment([*c]struct_unnamed_18), @intToPtr([*c]u16, @ptrToInt(ptr))));
        __p.*.__v = __bswap_16(val);
        if (!false) break;
    }
}
pub fn l_put_le32(arg_val: u32, arg_ptr: ?*anyopaque) callconv(.C) void {
    var val = arg_val;
    const ptr = arg_ptr;
    while (true) {
        const struct_unnamed_19 = extern struct {
            __v: @TypeOf(@ptrCast([*c]u32, @alignCast(@import("std").meta.alignment([*c]u32), ptr)).*) align(1),
        };
        _ = @TypeOf(struct_unnamed_19);
        var __p: [*c]struct_unnamed_19 = @ptrCast([*c]struct_unnamed_19, @alignCast(@import("std").meta.alignment([*c]struct_unnamed_19), @ptrCast([*c]u32, @alignCast(@import("std").meta.alignment([*c]u32), ptr))));
        __p.*.__v = val;
        if (!false) break;
    }
}
pub fn l_put_be32(arg_val: u32, arg_ptr: ?*anyopaque) callconv(.C) void {
    var val = arg_val;
    const ptr = arg_ptr;
    while (true) {
        const struct_unnamed_20 = extern struct {
            __v: @TypeOf(@ptrCast([*c]u32, @alignCast(@import("std").meta.alignment([*c]u32), ptr)).*) align(1),
        };
        _ = @TypeOf(struct_unnamed_20);
        var __p: [*c]struct_unnamed_20 = @ptrCast([*c]struct_unnamed_20, @alignCast(@import("std").meta.alignment([*c]struct_unnamed_20), @ptrCast([*c]u32, @alignCast(@import("std").meta.alignment([*c]u32), ptr))));
        __p.*.__v = __bswap_32(val);
        if (!false) break;
    }
}
pub fn l_put_le64(arg_val: u64, arg_ptr: ?*anyopaque) callconv(.C) void {
    var val = arg_val;
    const ptr = arg_ptr;
    while (true) {
        const struct_unnamed_21 = extern struct {
            __v: @TypeOf(@ptrCast([*c]u64, @alignCast(@import("std").meta.alignment([*c]u64), ptr)).*) align(1),
        };
        _ = @TypeOf(struct_unnamed_21);
        var __p: [*c]struct_unnamed_21 = @ptrCast([*c]struct_unnamed_21, @alignCast(@import("std").meta.alignment([*c]struct_unnamed_21), @ptrCast([*c]u64, @alignCast(@import("std").meta.alignment([*c]u64), ptr))));
        __p.*.__v = val;
        if (!false) break;
    }
}
pub fn l_put_be64(arg_val: u64, arg_ptr: ?*anyopaque) callconv(.C) void {
    var val = arg_val;
    const ptr = arg_ptr;
    while (true) {
        const struct_unnamed_22 = extern struct {
            __v: @TypeOf(@ptrCast([*c]u64, @alignCast(@import("std").meta.alignment([*c]u64), ptr)).*) align(1),
        };
        _ = @TypeOf(struct_unnamed_22);
        var __p: [*c]struct_unnamed_22 = @ptrCast([*c]struct_unnamed_22, @alignCast(@import("std").meta.alignment([*c]struct_unnamed_22), @ptrCast([*c]u64, @alignCast(@import("std").meta.alignment([*c]u64), ptr))));
        __p.*.__v = __bswap_64(val);
        if (!false) break;
    }
}
pub extern fn l_malloc(size: usize) ?*anyopaque;
pub extern fn l_memdup(mem: ?*const anyopaque, size: usize) ?*anyopaque;
pub extern fn l_free(ptr: ?*anyopaque) void;
pub inline fn l_free_cleanup(arg_p: ?*anyopaque) void {
    var p = arg_p;
    l_free(@ptrCast([*c]?*anyopaque, @alignCast(@import("std").meta.alignment([*c]?*anyopaque), p)).*);
}
pub extern fn l_realloc(mem: ?*anyopaque, size: usize) ?*anyopaque;
pub fn auto_free(arg_a: ?*anyopaque) callconv(.C) void {
    var a = arg_a;
    var p: [*c]?*anyopaque = @ptrCast([*c]?*anyopaque, @alignCast(@import("std").meta.alignment([*c]?*anyopaque), a));
    l_free(p.*);
}
pub extern fn l_strdup(str: [*c]const u8) [*c]u8;
pub extern fn l_strndup(str: [*c]const u8, max: usize) [*c]u8;
pub extern fn l_strdup_printf(format: [*c]const u8, ...) [*c]u8;
pub extern fn l_strdup_vprintf(format: [*c]const u8, args: [*c]struct___va_list_tag) [*c]u8;
pub extern fn l_strlcpy(dst: [*c]u8, src: [*c]const u8, len: usize) usize;
pub extern fn l_str_has_prefix(str: [*c]const u8, prefix: [*c]const u8) bool;
pub extern fn l_str_has_suffix(str: [*c]const u8, suffix: [*c]const u8) bool;
pub extern fn l_streq0(a: [*c]const u8, b: [*c]const u8) bool;
pub extern fn l_util_hexstring(buf: ?*const anyopaque, len: usize) [*c]u8;
pub extern fn l_util_hexstring_upper(buf: ?*const anyopaque, len: usize) [*c]u8;
pub extern fn l_util_from_hexstring(str: [*c]const u8, out_len: [*c]usize) [*c]u8;
pub const l_util_hexdump_func_t = ?*const fn ([*c]const u8, ?*anyopaque) callconv(.C) void;
pub extern fn l_util_hexdump(in: bool, buf: ?*const anyopaque, len: usize, function: l_util_hexdump_func_t, user_data: ?*anyopaque) void;
pub extern fn l_util_hexdump_two(in: bool, buf1: ?*const anyopaque, len1: usize, buf2: ?*const anyopaque, len2: usize, function: l_util_hexdump_func_t, user_data: ?*anyopaque) void;
pub extern fn l_util_hexdumpv(in: bool, iov: [*c]const struct_iovec, n_iov: usize, function: l_util_hexdump_func_t, user_data: ?*anyopaque) void;
pub extern fn l_util_debug(function: l_util_hexdump_func_t, user_data: ?*anyopaque, format: [*c]const u8, ...) void;
pub extern fn l_util_get_debugfs_path() [*c]const u8;
pub fn l_secure_memcmp(arg_a: ?*const anyopaque, arg_b: ?*const anyopaque, arg_size: usize) callconv(.C) c_int {
    var a = arg_a;
    var b = arg_b;
    var size = arg_size;
    var aa: [*c]const volatile u8 = @ptrCast([*c]const volatile u8, @alignCast(@import("std").meta.alignment([*c]const volatile u8), a));
    var bb: [*c]const volatile u8 = @ptrCast([*c]const volatile u8, @alignCast(@import("std").meta.alignment([*c]const volatile u8), b));
    var res: c_int = 0;
    var diff: c_int = undefined;
    var mask: c_int = undefined;
    if (size > @bitCast(c_ulong, @as(c_long, @as(c_int, 0)))) {
        while (true) {
            size -%= 1;
            diff = @bitCast(c_int, @as(c_uint, aa[size])) - @bitCast(c_int, @as(c_uint, bb[size]));
            mask = ((diff - @as(c_int, 1)) & ~diff) >> @intCast(@import("std").math.Log2Int(c_int), 8);
            res = (res & mask) | diff;
            if (!(size != @bitCast(c_ulong, @as(c_long, @as(c_int, 0))))) break;
        }
    }
    return res;
}
pub extern fn l_memeq(field: ?*const anyopaque, size: usize, byte: u8) bool;
pub extern fn l_secure_memeq(field: ?*const anyopaque, size: usize, byte: u8) bool;
pub fn l_memeqzero(arg_field: ?*const anyopaque, arg_size: usize) callconv(.C) bool {
    var field = arg_field;
    var size = arg_size;
    return l_memeq(field, size, @bitCast(u8, @truncate(i8, @as(c_int, 0))));
}
pub fn l_secure_select(arg_select_left: bool, arg_left: ?*const anyopaque, arg_right: ?*const anyopaque, arg_out: ?*anyopaque, arg_len: usize) callconv(.C) void {
    var select_left = arg_select_left;
    var left = arg_left;
    var right = arg_right;
    var out = arg_out;
    var len = arg_len;
    var l: [*c]const u8 = @ptrCast([*c]const u8, @alignCast(@import("std").meta.alignment([*c]const u8), left));
    var r: [*c]const u8 = @ptrCast([*c]const u8, @alignCast(@import("std").meta.alignment([*c]const u8), right));
    var o: [*c]u8 = @ptrCast([*c]u8, @alignCast(@import("std").meta.alignment([*c]u8), out));
    var mask: u8 = @bitCast(u8, @truncate(i8, -@as(c_int, @boolToInt(!!select_left))));
    var i: usize = undefined;
    {
        i = 0;
        while (i < len) : (i +%= 1) {
            o[i] = @bitCast(u8, @truncate(i8, @bitCast(c_int, @as(c_uint, r[i])) ^ ((@bitCast(c_int, @as(c_uint, l[i])) ^ @bitCast(c_int, @as(c_uint, r[i]))) & @bitCast(c_int, @as(c_uint, mask)))));
        }
    }
}
pub const l_test_func_t = ?*const fn (?*const anyopaque) callconv(.C) void;
pub extern fn l_test_init(argc: [*c]c_int, argv: [*c][*c][*c]u8) void;
pub extern fn l_test_run() c_int;
pub extern fn l_test_add(name: [*c]const u8, function: l_test_func_t, test_data: ?*const anyopaque) void;
pub extern fn l_strfreev(strlist: [*c][*c]u8) void;
pub extern fn l_strsplit(str: [*c]const u8, sep: u8) [*c][*c]u8;
pub extern fn l_strsplit_set(str: [*c]const u8, separators: [*c]const u8) [*c][*c]u8;
pub extern fn l_strjoinv(str_array: [*c][*c]u8, delim: u8) [*c]u8;
pub extern fn l_strv_new() [*c][*c]u8;
pub extern fn l_strv_free(str_array: [*c][*c]u8) void;
pub inline fn l_strv_free_cleanup(arg_p: ?*anyopaque) void {
    var p = arg_p;
    l_strv_free(@ptrCast([*c][*c]u8, @alignCast(@import("std").meta.alignment([*c][*c]u8), @ptrCast([*c]?*anyopaque, @alignCast(@import("std").meta.alignment([*c]?*anyopaque), p)).*)));
}
pub extern fn l_strv_length(str_array: [*c][*c]u8) c_uint;
pub extern fn l_strv_contains(str_array: [*c][*c]u8, item: [*c]const u8) bool;
pub extern fn l_strv_append(str_array: [*c][*c]u8, str: [*c]const u8) [*c][*c]u8;
pub extern fn l_strv_append_printf(str_array: [*c][*c]u8, format: [*c]const u8, ...) [*c][*c]u8;
pub extern fn l_strv_append_vprintf(str_array: [*c][*c]u8, format: [*c]const u8, args: [*c]struct___va_list_tag) [*c][*c]u8;
pub extern fn l_strv_copy(str_array: [*c][*c]u8) [*c][*c]u8;
pub extern fn l_strv_eq(a: [*c][*c]u8, b: [*c][*c]u8) bool;
pub const _Float32 = f32;
pub const _Float64 = f64;
pub const _Float32x = f64;
pub const _Float64x = c_longdouble;
pub const wchar_t = c_int;
pub const wint_t = c_uint;
const union_unnamed_23 = extern union {
    __wch: c_uint,
    __wchb: [4]u8,
};
pub const __mbstate_t = extern struct {
    __count: c_int,
    __value: union_unnamed_23,
};
pub const mbstate_t = __mbstate_t;
pub const struct__IO_FILE = opaque {};
pub const __FILE = struct__IO_FILE;
pub const FILE = struct__IO_FILE;
pub const struct_tm = opaque {};
pub extern fn wcscpy(noalias __dest: [*c]wchar_t, noalias __src: [*c]const wchar_t) [*c]wchar_t;
pub extern fn wcsncpy(noalias __dest: [*c]wchar_t, noalias __src: [*c]const wchar_t, __n: usize) [*c]wchar_t;
pub extern fn wcscat(noalias __dest: [*c]wchar_t, noalias __src: [*c]const wchar_t) [*c]wchar_t;
pub extern fn wcsncat(noalias __dest: [*c]wchar_t, noalias __src: [*c]const wchar_t, __n: usize) [*c]wchar_t;
pub extern fn wcscmp(__s1: [*c]const c_int, __s2: [*c]const c_int) c_int;
pub extern fn wcsncmp(__s1: [*c]const c_int, __s2: [*c]const c_int, __n: c_ulong) c_int;
pub extern fn wcscasecmp(__s1: [*c]const wchar_t, __s2: [*c]const wchar_t) c_int;
pub extern fn wcsncasecmp(__s1: [*c]const wchar_t, __s2: [*c]const wchar_t, __n: usize) c_int;
pub extern fn wcscasecmp_l(__s1: [*c]const wchar_t, __s2: [*c]const wchar_t, __loc: locale_t) c_int;
pub extern fn wcsncasecmp_l(__s1: [*c]const wchar_t, __s2: [*c]const wchar_t, __n: usize, __loc: locale_t) c_int;
pub extern fn wcscoll(__s1: [*c]const wchar_t, __s2: [*c]const wchar_t) c_int;
pub extern fn wcsxfrm(noalias __s1: [*c]wchar_t, noalias __s2: [*c]const wchar_t, __n: usize) usize;
pub extern fn wcscoll_l(__s1: [*c]const wchar_t, __s2: [*c]const wchar_t, __loc: locale_t) c_int;
pub extern fn wcsxfrm_l(__s1: [*c]wchar_t, __s2: [*c]const wchar_t, __n: usize, __loc: locale_t) usize;
pub extern fn wcsdup(__s: [*c]const wchar_t) [*c]wchar_t;
pub extern fn wcschr(__wcs: [*c]const c_int, __wc: c_int) [*c]c_int;
pub extern fn wcsrchr(__wcs: [*c]const wchar_t, __wc: wchar_t) [*c]wchar_t;
pub extern fn wcscspn(__wcs: [*c]const wchar_t, __reject: [*c]const wchar_t) usize;
pub extern fn wcsspn(__wcs: [*c]const wchar_t, __accept: [*c]const wchar_t) usize;
pub extern fn wcspbrk(__wcs: [*c]const wchar_t, __accept: [*c]const wchar_t) [*c]wchar_t;
pub extern fn wcsstr(__haystack: [*c]const wchar_t, __needle: [*c]const wchar_t) [*c]wchar_t;
pub extern fn wcstok(noalias __s: [*c]wchar_t, noalias __delim: [*c]const wchar_t, noalias __ptr: [*c][*c]wchar_t) [*c]wchar_t;
pub extern fn wcslen(__s: [*c]const c_int) c_ulong;
pub extern fn wcsnlen(__s: [*c]const wchar_t, __maxlen: usize) usize;
pub extern fn wmemchr(__s: [*c]const c_int, __c: c_int, __n: c_ulong) [*c]c_int;
pub extern fn wmemcmp(__s1: [*c]const c_int, __s2: [*c]const c_int, __n: c_ulong) c_int;
pub extern fn wmemcpy(__s1: [*c]c_int, __s2: [*c]const c_int, __n: c_ulong) [*c]c_int;
pub extern fn wmemmove(__s1: [*c]c_int, __s2: [*c]const c_int, __n: c_ulong) [*c]c_int;
pub extern fn wmemset(__s: [*c]wchar_t, __c: wchar_t, __n: usize) [*c]wchar_t;
pub extern fn btowc(__c: c_int) wint_t;
pub extern fn wctob(__c: wint_t) c_int;
pub extern fn mbsinit(__ps: [*c]const mbstate_t) c_int;
pub extern fn mbrtowc(noalias __pwc: [*c]wchar_t, noalias __s: [*c]const u8, __n: usize, noalias __p: [*c]mbstate_t) usize;
pub extern fn wcrtomb(noalias __s: [*c]u8, __wc: wchar_t, noalias __ps: [*c]mbstate_t) usize;
pub extern fn __mbrlen(noalias __s: [*c]const u8, __n: usize, noalias __ps: [*c]mbstate_t) usize;
pub extern fn mbrlen(noalias __s: [*c]const u8, __n: usize, noalias __ps: [*c]mbstate_t) usize;
pub extern fn mbsrtowcs(noalias __dst: [*c]wchar_t, noalias __src: [*c][*c]const u8, __len: usize, noalias __ps: [*c]mbstate_t) usize;
pub extern fn wcsrtombs(noalias __dst: [*c]u8, noalias __src: [*c][*c]const wchar_t, __len: usize, noalias __ps: [*c]mbstate_t) usize;
pub extern fn mbsnrtowcs(noalias __dst: [*c]wchar_t, noalias __src: [*c][*c]const u8, __nmc: usize, __len: usize, noalias __ps: [*c]mbstate_t) usize;
pub extern fn wcsnrtombs(noalias __dst: [*c]u8, noalias __src: [*c][*c]const wchar_t, __nwc: usize, __len: usize, noalias __ps: [*c]mbstate_t) usize;
pub extern fn wcstod(noalias __nptr: [*c]const wchar_t, noalias __endptr: [*c][*c]wchar_t) f64;
pub extern fn wcstof(noalias __nptr: [*c]const wchar_t, noalias __endptr: [*c][*c]wchar_t) f32;
pub extern fn wcstold(noalias __nptr: [*c]const wchar_t, noalias __endptr: [*c][*c]wchar_t) c_longdouble;
pub extern fn wcstol(noalias __nptr: [*c]const wchar_t, noalias __endptr: [*c][*c]wchar_t, __base: c_int) c_long;
pub extern fn wcstoul(noalias __nptr: [*c]const wchar_t, noalias __endptr: [*c][*c]wchar_t, __base: c_int) c_ulong;
pub extern fn wcstoll(noalias __nptr: [*c]const wchar_t, noalias __endptr: [*c][*c]wchar_t, __base: c_int) c_longlong;
pub extern fn wcstoull(noalias __nptr: [*c]const wchar_t, noalias __endptr: [*c][*c]wchar_t, __base: c_int) c_ulonglong;
pub extern fn wcpcpy(noalias __dest: [*c]wchar_t, noalias __src: [*c]const wchar_t) [*c]wchar_t;
pub extern fn wcpncpy(noalias __dest: [*c]wchar_t, noalias __src: [*c]const wchar_t, __n: usize) [*c]wchar_t;
pub extern fn open_wmemstream(__bufloc: [*c][*c]wchar_t, __sizeloc: [*c]usize) ?*__FILE;
pub extern fn fwide(__fp: ?*__FILE, __mode: c_int) c_int;
pub extern fn fwprintf(noalias __stream: ?*__FILE, noalias __format: [*c]const wchar_t, ...) c_int;
pub extern fn wprintf(noalias __format: [*c]const wchar_t, ...) c_int;
pub extern fn swprintf(noalias __s: [*c]wchar_t, __n: usize, noalias __format: [*c]const wchar_t, ...) c_int;
pub extern fn vfwprintf(noalias __s: ?*__FILE, noalias __format: [*c]const wchar_t, __arg: [*c]struct___va_list_tag) c_int;
pub extern fn vwprintf(noalias __format: [*c]const wchar_t, __arg: [*c]struct___va_list_tag) c_int;
pub extern fn vswprintf(noalias __s: [*c]wchar_t, __n: usize, noalias __format: [*c]const wchar_t, __arg: [*c]struct___va_list_tag) c_int;
pub extern fn fwscanf(noalias __stream: ?*__FILE, noalias __format: [*c]const wchar_t, ...) c_int;
pub extern fn wscanf(noalias __format: [*c]const wchar_t, ...) c_int;
pub extern fn swscanf(noalias __s: [*c]const wchar_t, noalias __format: [*c]const wchar_t, ...) c_int;
pub extern fn vfwscanf(noalias __s: ?*__FILE, noalias __format: [*c]const wchar_t, __arg: [*c]struct___va_list_tag) c_int;
pub extern fn vwscanf(noalias __format: [*c]const wchar_t, __arg: [*c]struct___va_list_tag) c_int;
pub extern fn vswscanf(noalias __s: [*c]const wchar_t, noalias __format: [*c]const wchar_t, __arg: [*c]struct___va_list_tag) c_int;
pub extern fn fgetwc(__stream: ?*__FILE) wint_t;
pub extern fn getwc(__stream: ?*__FILE) wint_t;
pub extern fn getwchar() wint_t;
pub extern fn fputwc(__wc: wchar_t, __stream: ?*__FILE) wint_t;
pub extern fn putwc(__wc: wchar_t, __stream: ?*__FILE) wint_t;
pub extern fn putwchar(__wc: wchar_t) wint_t;
pub extern fn fgetws(noalias __ws: [*c]wchar_t, __n: c_int, noalias __stream: ?*__FILE) [*c]wchar_t;
pub extern fn fputws(noalias __ws: [*c]const wchar_t, noalias __stream: ?*__FILE) c_int;
pub extern fn ungetwc(__wc: wint_t, __stream: ?*__FILE) wint_t;
pub extern fn wcsftime(noalias __s: [*c]wchar_t, __maxsize: usize, noalias __format: [*c]const wchar_t, noalias __tp: ?*const struct_tm) usize;
pub extern var l_ascii_table: [*c]u8;
pub const L_ASCII_CNTRL: c_int = 128;
pub const L_ASCII_PRINT: c_int = 64;
pub const L_ASCII_PUNCT: c_int = 32;
pub const L_ASCII_SPACE: c_int = 16;
pub const L_ASCII_XDIGIT: c_int = 8;
pub const L_ASCII_UPPER: c_int = 4;
pub const L_ASCII_LOWER: c_int = 2;
pub const L_ASCII_DIGIT: c_int = 1;
pub const L_ASCII_ALPHA: c_int = 6;
pub const L_ASCII_ALNUM: c_int = 7;
pub const L_ASCII_GRAPH: c_int = 39;
pub const enum_l_ascii = c_uint;
pub inline fn l_ascii_isblank(arg_c: u8) bool {
    var c = arg_c;
    if ((@bitCast(c_int, @as(c_uint, c)) == @as(c_int, ' ')) or (@bitCast(c_int, @as(c_uint, c)) == @as(c_int, '\t'))) return @as(c_int, 1) != 0;
    return @as(c_int, 0) != 0;
}
pub inline fn l_ascii_isascii(arg_c: c_int) bool {
    var c = arg_c;
    if (c <= @as(c_int, 127)) return @as(c_int, 1) != 0;
    return @as(c_int, 0) != 0;
}
pub extern fn l_utf8_validate(src: [*c]const u8, len: usize, end: [*c][*c]const u8) bool;
pub extern fn l_utf8_strlen(str: [*c]const u8) usize;
pub extern fn l_utf8_get_codepoint(str: [*c]const u8, len: usize, cp: [*c]wchar_t) c_int;
pub extern fn l_utf8_from_wchar(c: wchar_t, out_buf: [*c]u8) usize;
pub extern fn l_utf8_from_utf16(utf16: ?*const anyopaque, utf16_size: isize) [*c]u8;
pub extern fn l_utf8_to_utf16(utf8: [*c]const u8, out_size: [*c]usize) ?*anyopaque;
pub extern fn l_utf8_from_ucs2be(ucs2be: ?*const anyopaque, ucs2be_size: isize) [*c]u8;
pub extern fn l_utf8_to_ucs2be(utf8: [*c]const u8, out_size: [*c]usize) ?*anyopaque;
pub const l_queue_foreach_func_t = ?*const fn (?*anyopaque, ?*anyopaque) callconv(.C) void;
pub const l_queue_destroy_func_t = ?*const fn (?*anyopaque) callconv(.C) void;
pub const l_queue_compare_func_t = ?*const fn (?*const anyopaque, ?*const anyopaque, ?*anyopaque) callconv(.C) c_int;
pub const l_queue_match_func_t = ?*const fn (?*const anyopaque, ?*const anyopaque) callconv(.C) bool;
pub const l_queue_remove_func_t = ?*const fn (?*anyopaque, ?*anyopaque) callconv(.C) bool;
pub const struct_l_queue = opaque {};
pub const struct_l_queue_entry = extern struct {
    data: ?*anyopaque,
    next: [*c]struct_l_queue_entry,
};
pub extern fn l_queue_new() ?*struct_l_queue;
pub extern fn l_queue_destroy(queue: ?*struct_l_queue, destroy: l_queue_destroy_func_t) void;
pub extern fn l_queue_clear(queue: ?*struct_l_queue, destroy: l_queue_destroy_func_t) void;
pub extern fn l_queue_push_tail(queue: ?*struct_l_queue, data: ?*anyopaque) bool;
pub extern fn l_queue_push_head(queue: ?*struct_l_queue, data: ?*anyopaque) bool;
pub extern fn l_queue_pop_head(queue: ?*struct_l_queue) ?*anyopaque;
pub extern fn l_queue_peek_head(queue: ?*struct_l_queue) ?*anyopaque;
pub extern fn l_queue_peek_tail(queue: ?*struct_l_queue) ?*anyopaque;
pub extern fn l_queue_insert(queue: ?*struct_l_queue, data: ?*anyopaque, function: l_queue_compare_func_t, user_data: ?*anyopaque) bool;
pub extern fn l_queue_find(queue: ?*struct_l_queue, function: l_queue_match_func_t, user_data: ?*const anyopaque) ?*anyopaque;
pub extern fn l_queue_remove(queue: ?*struct_l_queue, data: ?*anyopaque) bool;
pub extern fn l_queue_remove_if(queue: ?*struct_l_queue, function: l_queue_match_func_t, user_data: ?*const anyopaque) ?*anyopaque;
pub extern fn l_queue_reverse(queue: ?*struct_l_queue) bool;
pub extern fn l_queue_foreach(queue: ?*struct_l_queue, function: l_queue_foreach_func_t, user_data: ?*anyopaque) void;
pub extern fn l_queue_foreach_remove(queue: ?*struct_l_queue, function: l_queue_remove_func_t, user_data: ?*anyopaque) c_uint;
pub extern fn l_queue_length(queue: ?*struct_l_queue) c_uint;
pub extern fn l_queue_isempty(queue: ?*struct_l_queue) bool;
pub extern fn l_queue_get_entries(queue: ?*const struct_l_queue) [*c]const struct_l_queue_entry;
pub const l_hashmap_foreach_func_t = ?*const fn (?*const anyopaque, ?*anyopaque, ?*anyopaque) callconv(.C) void;
pub const l_hashmap_destroy_func_t = ?*const fn (?*anyopaque) callconv(.C) void;
pub const l_hashmap_hash_func_t = ?*const fn (?*const anyopaque) callconv(.C) c_uint;
pub const l_hashmap_compare_func_t = ?*const fn (?*const anyopaque, ?*const anyopaque) callconv(.C) c_int;
pub const l_hashmap_key_new_func_t = ?*const fn (?*const anyopaque) callconv(.C) ?*anyopaque;
pub const l_hashmap_key_free_func_t = ?*const fn (?*anyopaque) callconv(.C) void;
pub const l_hashmap_remove_func_t = ?*const fn (?*const anyopaque, ?*anyopaque, ?*anyopaque) callconv(.C) bool;
pub const struct_l_hashmap = opaque {};
pub extern fn l_str_hash(p: ?*const anyopaque) c_uint;
pub extern fn l_hashmap_new() ?*struct_l_hashmap;
pub extern fn l_hashmap_string_new() ?*struct_l_hashmap;
pub extern fn l_hashmap_set_hash_function(hashmap: ?*struct_l_hashmap, func: l_hashmap_hash_func_t) bool;
pub extern fn l_hashmap_set_compare_function(hashmap: ?*struct_l_hashmap, func: l_hashmap_compare_func_t) bool;
pub extern fn l_hashmap_set_key_copy_function(hashmap: ?*struct_l_hashmap, func: l_hashmap_key_new_func_t) bool;
pub extern fn l_hashmap_set_key_free_function(hashmap: ?*struct_l_hashmap, func: l_hashmap_key_free_func_t) bool;
pub extern fn l_hashmap_destroy(hashmap: ?*struct_l_hashmap, destroy: l_hashmap_destroy_func_t) void;
pub extern fn l_hashmap_insert(hashmap: ?*struct_l_hashmap, key: ?*const anyopaque, value: ?*anyopaque) bool;
pub extern fn l_hashmap_replace(hashmap: ?*struct_l_hashmap, key: ?*const anyopaque, value: ?*anyopaque, old_value: [*c]?*anyopaque) bool;
pub extern fn l_hashmap_remove(hashmap: ?*struct_l_hashmap, key: ?*const anyopaque) ?*anyopaque;
pub extern fn l_hashmap_lookup(hashmap: ?*struct_l_hashmap, key: ?*const anyopaque) ?*anyopaque;
pub extern fn l_hashmap_foreach(hashmap: ?*struct_l_hashmap, function: l_hashmap_foreach_func_t, user_data: ?*anyopaque) void;
pub extern fn l_hashmap_foreach_remove(hashmap: ?*struct_l_hashmap, function: l_hashmap_remove_func_t, user_data: ?*anyopaque) c_uint;
pub extern fn l_hashmap_size(hashmap: ?*struct_l_hashmap) c_uint;
pub extern fn l_hashmap_isempty(hashmap: ?*struct_l_hashmap) bool;
pub const struct_l_string = opaque {};
pub extern fn l_string_new(initial_length: usize) ?*struct_l_string;
pub extern fn l_string_free(string: ?*struct_l_string) void;
pub inline fn l_string_free_cleanup(arg_p: ?*anyopaque) void {
    var p = arg_p;
    l_string_free(@ptrCast(?*struct_l_string, @ptrCast([*c]?*anyopaque, @alignCast(@import("std").meta.alignment([*c]?*anyopaque), p)).*));
}
pub extern fn l_string_unwrap(string: ?*struct_l_string) [*c]u8;
pub extern fn l_string_append(dest: ?*struct_l_string, src: [*c]const u8) ?*struct_l_string;
pub extern fn l_string_append_c(dest: ?*struct_l_string, c: u8) ?*struct_l_string;
pub extern fn l_string_append_fixed(dest: ?*struct_l_string, src: [*c]const u8, max: usize) ?*struct_l_string;
pub extern fn l_string_append_vprintf(dest: ?*struct_l_string, format: [*c]const u8, args: [*c]struct___va_list_tag) void;
pub extern fn l_string_append_printf(dest: ?*struct_l_string, format: [*c]const u8, ...) void;
pub extern fn l_string_truncate(string: ?*struct_l_string, new_size: usize) ?*struct_l_string;
pub extern fn l_string_length(string: ?*struct_l_string) c_uint;
pub extern fn l_parse_args(args: [*c]const u8, out_n_args: [*c]c_int) [*c][*c]u8;
pub extern fn l_main_init() bool;
pub extern fn l_main_prepare() c_int;
pub extern fn l_main_iterate(timeout: c_int) void;
pub extern fn l_main_run() c_int;
pub extern fn l_main_exit() bool;
pub extern fn l_main_quit() bool;
pub const l_main_signal_cb_t = ?*const fn (u32, ?*anyopaque) callconv(.C) void;
pub extern fn l_main_run_with_signal(callback: l_main_signal_cb_t, user_data: ?*anyopaque) c_int;
pub extern fn l_main_get_epoll_fd() c_int;
pub const struct_l_idle = opaque {};
pub const l_idle_notify_cb_t = ?*const fn (?*struct_l_idle, ?*anyopaque) callconv(.C) void;
pub const l_idle_oneshot_cb_t = ?*const fn (?*anyopaque) callconv(.C) void;
pub const l_idle_destroy_cb_t = ?*const fn (?*anyopaque) callconv(.C) void;
pub extern fn l_idle_create(callback: l_idle_notify_cb_t, user_data: ?*anyopaque, destroy: l_idle_destroy_cb_t) ?*struct_l_idle;
pub extern fn l_idle_remove(idle: ?*struct_l_idle) void;
pub extern fn l_idle_oneshot(callback: l_idle_oneshot_cb_t, user_data: ?*anyopaque, destroy: l_idle_destroy_cb_t) bool;
pub const struct_l_signal = opaque {};
pub const l_signal_notify_cb_t = ?*const fn (?*anyopaque) callconv(.C) void;
pub const l_signal_destroy_cb_t = ?*const fn (?*anyopaque) callconv(.C) void;
pub extern fn l_signal_create(signo: u32, callback: l_signal_notify_cb_t, user_data: ?*anyopaque, destroy: l_signal_destroy_cb_t) ?*struct_l_signal;
pub extern fn l_signal_remove(signal: ?*struct_l_signal) void;
pub const struct_l_timeout = opaque {};
pub const l_timeout_notify_cb_t = ?*const fn (?*struct_l_timeout, ?*anyopaque) callconv(.C) void;
pub const l_timeout_destroy_cb_t = ?*const fn (?*anyopaque) callconv(.C) void;
pub extern fn l_timeout_create(seconds: c_uint, callback: l_timeout_notify_cb_t, user_data: ?*anyopaque, destroy: l_timeout_destroy_cb_t) ?*struct_l_timeout;
pub extern fn l_timeout_create_ms(milliseconds: u64, callback: l_timeout_notify_cb_t, user_data: ?*anyopaque, destroy: l_timeout_destroy_cb_t) ?*struct_l_timeout;
pub extern fn l_timeout_modify(timeout: ?*struct_l_timeout, seconds: c_uint) void;
pub extern fn l_timeout_modify_ms(timeout: ?*struct_l_timeout, milliseconds: u64) void;
pub extern fn l_timeout_remove(timeout: ?*struct_l_timeout) void;
pub extern fn l_timeout_set_callback(timeout: ?*struct_l_timeout, callback: l_timeout_notify_cb_t, user_data: ?*anyopaque, destroy: l_timeout_destroy_cb_t) void;
pub const struct_l_io = opaque {};
pub const l_io_debug_cb_t = ?*const fn ([*c]const u8, ?*anyopaque) callconv(.C) void;
pub const l_io_read_cb_t = ?*const fn (?*struct_l_io, ?*anyopaque) callconv(.C) bool;
pub const l_io_write_cb_t = ?*const fn (?*struct_l_io, ?*anyopaque) callconv(.C) bool;
pub const l_io_disconnect_cb_t = ?*const fn (?*struct_l_io, ?*anyopaque) callconv(.C) void;
pub const l_io_destroy_cb_t = ?*const fn (?*anyopaque) callconv(.C) void;
pub extern fn l_io_new(fd: c_int) ?*struct_l_io;
pub extern fn l_io_destroy(io: ?*struct_l_io) void;
pub extern fn l_io_get_fd(io: ?*struct_l_io) c_int;
pub extern fn l_io_set_close_on_destroy(io: ?*struct_l_io, do_close: bool) bool;
pub extern fn l_io_set_read_handler(io: ?*struct_l_io, callback: l_io_read_cb_t, user_data: ?*anyopaque, destroy: l_io_destroy_cb_t) bool;
pub extern fn l_io_set_write_handler(io: ?*struct_l_io, callback: l_io_write_cb_t, user_data: ?*anyopaque, destroy: l_io_destroy_cb_t) bool;
pub extern fn l_io_set_disconnect_handler(io: ?*struct_l_io, callback: l_io_disconnect_cb_t, user_data: ?*anyopaque, destroy: l_io_destroy_cb_t) bool;
pub extern fn l_io_set_debug(io: ?*struct_l_io, callback: l_io_debug_cb_t, user_data: ?*anyopaque, destroy: l_io_destroy_cb_t) bool;
pub const div_t = extern struct {
    quot: c_int,
    rem: c_int,
};
pub const ldiv_t = extern struct {
    quot: c_long,
    rem: c_long,
};
pub const lldiv_t = extern struct {
    quot: c_longlong,
    rem: c_longlong,
};
pub extern fn __ctype_get_mb_cur_max() usize;
pub extern fn atof(__nptr: [*c]const u8) f64;
pub extern fn atoi(__nptr: [*c]const u8) c_int;
pub extern fn atol(__nptr: [*c]const u8) c_long;
pub extern fn atoll(__nptr: [*c]const u8) c_longlong;
pub extern fn strtod(__nptr: [*c]const u8, __endptr: [*c][*c]u8) f64;
pub extern fn strtof(__nptr: [*c]const u8, __endptr: [*c][*c]u8) f32;
pub extern fn strtold(__nptr: [*c]const u8, __endptr: [*c][*c]u8) c_longdouble;
pub extern fn strtol(__nptr: [*c]const u8, __endptr: [*c][*c]u8, __base: c_int) c_long;
pub extern fn strtoul(__nptr: [*c]const u8, __endptr: [*c][*c]u8, __base: c_int) c_ulong;
pub extern fn strtoq(noalias __nptr: [*c]const u8, noalias __endptr: [*c][*c]u8, __base: c_int) c_longlong;
pub extern fn strtouq(noalias __nptr: [*c]const u8, noalias __endptr: [*c][*c]u8, __base: c_int) c_ulonglong;
pub extern fn strtoll(__nptr: [*c]const u8, __endptr: [*c][*c]u8, __base: c_int) c_longlong;
pub extern fn strtoull(__nptr: [*c]const u8, __endptr: [*c][*c]u8, __base: c_int) c_ulonglong;
pub extern fn l64a(__n: c_long) [*c]u8;
pub extern fn a64l(__s: [*c]const u8) c_long;
pub extern fn random() c_long;
pub extern fn srandom(__seed: c_uint) void;
pub extern fn initstate(__seed: c_uint, __statebuf: [*c]u8, __statelen: usize) [*c]u8;
pub extern fn setstate(__statebuf: [*c]u8) [*c]u8;
pub const struct_random_data = extern struct {
    fptr: [*c]i32,
    rptr: [*c]i32,
    state: [*c]i32,
    rand_type: c_int,
    rand_deg: c_int,
    rand_sep: c_int,
    end_ptr: [*c]i32,
};
pub extern fn random_r(noalias __buf: [*c]struct_random_data, noalias __result: [*c]i32) c_int;
pub extern fn srandom_r(__seed: c_uint, __buf: [*c]struct_random_data) c_int;
pub extern fn initstate_r(__seed: c_uint, noalias __statebuf: [*c]u8, __statelen: usize, noalias __buf: [*c]struct_random_data) c_int;
pub extern fn setstate_r(noalias __statebuf: [*c]u8, noalias __buf: [*c]struct_random_data) c_int;
pub extern fn rand() c_int;
pub extern fn srand(__seed: c_uint) void;
pub extern fn rand_r(__seed: [*c]c_uint) c_int;
pub extern fn drand48() f64;
pub extern fn erand48(__xsubi: [*c]c_ushort) f64;
pub extern fn lrand48() c_long;
pub extern fn nrand48(__xsubi: [*c]c_ushort) c_long;
pub extern fn mrand48() c_long;
pub extern fn jrand48(__xsubi: [*c]c_ushort) c_long;
pub extern fn srand48(__seedval: c_long) void;
pub extern fn seed48(__seed16v: [*c]c_ushort) [*c]c_ushort;
pub extern fn lcong48(__param: [*c]c_ushort) void;
pub const struct_drand48_data = extern struct {
    __x: [3]c_ushort,
    __old_x: [3]c_ushort,
    __c: c_ushort,
    __init: c_ushort,
    __a: c_ulonglong,
};
pub extern fn drand48_r(noalias __buffer: [*c]struct_drand48_data, noalias __result: [*c]f64) c_int;
pub extern fn erand48_r(__xsubi: [*c]c_ushort, noalias __buffer: [*c]struct_drand48_data, noalias __result: [*c]f64) c_int;
pub extern fn lrand48_r(noalias __buffer: [*c]struct_drand48_data, noalias __result: [*c]c_long) c_int;
pub extern fn nrand48_r(__xsubi: [*c]c_ushort, noalias __buffer: [*c]struct_drand48_data, noalias __result: [*c]c_long) c_int;
pub extern fn mrand48_r(noalias __buffer: [*c]struct_drand48_data, noalias __result: [*c]c_long) c_int;
pub extern fn jrand48_r(__xsubi: [*c]c_ushort, noalias __buffer: [*c]struct_drand48_data, noalias __result: [*c]c_long) c_int;
pub extern fn srand48_r(__seedval: c_long, __buffer: [*c]struct_drand48_data) c_int;
pub extern fn seed48_r(__seed16v: [*c]c_ushort, __buffer: [*c]struct_drand48_data) c_int;
pub extern fn lcong48_r(__param: [*c]c_ushort, __buffer: [*c]struct_drand48_data) c_int;
pub extern fn arc4random() __uint32_t;
pub extern fn arc4random_buf(__buf: ?*anyopaque, __size: usize) void;
pub extern fn arc4random_uniform(__upper_bound: __uint32_t) __uint32_t;
pub extern fn malloc(__size: c_ulong) ?*anyopaque;
pub extern fn calloc(__nmemb: c_ulong, __size: c_ulong) ?*anyopaque;
pub extern fn realloc(__ptr: ?*anyopaque, __size: c_ulong) ?*anyopaque;
pub extern fn free(__ptr: ?*anyopaque) void;
pub extern fn reallocarray(__ptr: ?*anyopaque, __nmemb: usize, __size: usize) ?*anyopaque;
pub extern fn alloca(__size: c_ulong) ?*anyopaque;
pub extern fn valloc(__size: usize) ?*anyopaque;
pub extern fn posix_memalign(__memptr: [*c]?*anyopaque, __alignment: usize, __size: usize) c_int;
pub extern fn aligned_alloc(__alignment: c_ulong, __size: c_ulong) ?*anyopaque;
pub extern fn abort() noreturn;
pub extern fn atexit(__func: ?*const fn () callconv(.C) void) c_int;
pub extern fn at_quick_exit(__func: ?*const fn () callconv(.C) void) c_int;
pub extern fn on_exit(__func: ?*const fn (c_int, ?*anyopaque) callconv(.C) void, __arg: ?*anyopaque) c_int;
pub extern fn exit(__status: c_int) noreturn;
pub extern fn quick_exit(__status: c_int) noreturn;
pub extern fn _Exit(__status: c_int) noreturn;
pub extern fn getenv(__name: [*c]const u8) [*c]u8;
pub extern fn putenv(__string: [*c]u8) c_int;
pub extern fn setenv(__name: [*c]const u8, __value: [*c]const u8, __replace: c_int) c_int;
pub extern fn unsetenv(__name: [*c]const u8) c_int;
pub extern fn clearenv() c_int;
pub extern fn mktemp(__template: [*c]u8) [*c]u8;
pub extern fn mkstemp(__template: [*c]u8) c_int;
pub extern fn mkstemps(__template: [*c]u8, __suffixlen: c_int) c_int;
pub extern fn mkdtemp(__template: [*c]u8) [*c]u8;
pub extern fn system(__command: [*c]const u8) c_int;
pub extern fn realpath(noalias __name: [*c]const u8, noalias __resolved: [*c]u8) [*c]u8;
pub const __compar_fn_t = ?*const fn (?*const anyopaque, ?*const anyopaque) callconv(.C) c_int;
pub extern fn bsearch(__key: ?*const anyopaque, __base: ?*const anyopaque, __nmemb: usize, __size: usize, __compar: __compar_fn_t) ?*anyopaque;
pub extern fn qsort(__base: ?*anyopaque, __nmemb: usize, __size: usize, __compar: __compar_fn_t) void;
pub extern fn abs(__x: c_int) c_int;
pub extern fn labs(__x: c_long) c_long;
pub extern fn llabs(__x: c_longlong) c_longlong;
pub extern fn div(__numer: c_int, __denom: c_int) div_t;
pub extern fn ldiv(__numer: c_long, __denom: c_long) ldiv_t;
pub extern fn lldiv(__numer: c_longlong, __denom: c_longlong) lldiv_t;
pub extern fn ecvt(__value: f64, __ndigit: c_int, noalias __decpt: [*c]c_int, noalias __sign: [*c]c_int) [*c]u8;
pub extern fn fcvt(__value: f64, __ndigit: c_int, noalias __decpt: [*c]c_int, noalias __sign: [*c]c_int) [*c]u8;
pub extern fn gcvt(__value: f64, __ndigit: c_int, __buf: [*c]u8) [*c]u8;
pub extern fn qecvt(__value: c_longdouble, __ndigit: c_int, noalias __decpt: [*c]c_int, noalias __sign: [*c]c_int) [*c]u8;
pub extern fn qfcvt(__value: c_longdouble, __ndigit: c_int, noalias __decpt: [*c]c_int, noalias __sign: [*c]c_int) [*c]u8;
pub extern fn qgcvt(__value: c_longdouble, __ndigit: c_int, __buf: [*c]u8) [*c]u8;
pub extern fn ecvt_r(__value: f64, __ndigit: c_int, noalias __decpt: [*c]c_int, noalias __sign: [*c]c_int, noalias __buf: [*c]u8, __len: usize) c_int;
pub extern fn fcvt_r(__value: f64, __ndigit: c_int, noalias __decpt: [*c]c_int, noalias __sign: [*c]c_int, noalias __buf: [*c]u8, __len: usize) c_int;
pub extern fn qecvt_r(__value: c_longdouble, __ndigit: c_int, noalias __decpt: [*c]c_int, noalias __sign: [*c]c_int, noalias __buf: [*c]u8, __len: usize) c_int;
pub extern fn qfcvt_r(__value: c_longdouble, __ndigit: c_int, noalias __decpt: [*c]c_int, noalias __sign: [*c]c_int, noalias __buf: [*c]u8, __len: usize) c_int;
pub extern fn mblen(__s: [*c]const u8, __n: usize) c_int;
pub extern fn mbtowc(noalias __pwc: [*c]wchar_t, noalias __s: [*c]const u8, __n: usize) c_int;
pub extern fn wctomb(__s: [*c]u8, __wchar: wchar_t) c_int;
pub extern fn mbstowcs(noalias __pwcs: [*c]wchar_t, noalias __s: [*c]const u8, __n: usize) usize;
pub extern fn wcstombs(noalias __s: [*c]u8, noalias __pwcs: [*c]const wchar_t, __n: usize) usize;
pub extern fn rpmatch(__response: [*c]const u8) c_int;
pub extern fn getsubopt(noalias __optionp: [*c][*c]u8, noalias __tokens: [*c]const [*c]u8, noalias __valuep: [*c][*c]u8) c_int;
pub extern fn getloadavg(__loadavg: [*c]f64, __nelem: c_int) c_int;
pub const l_ringbuf_tracing_func_t = ?*const fn (?*const anyopaque, usize, ?*anyopaque) callconv(.C) void;
pub const struct_l_ringbuf = opaque {};
pub extern fn l_ringbuf_new(size: usize) ?*struct_l_ringbuf;
pub extern fn l_ringbuf_free(ringbuf: ?*struct_l_ringbuf) void;
pub extern fn l_ringbuf_set_input_tracing(ringbuf: ?*struct_l_ringbuf, callback: l_ringbuf_tracing_func_t, user_data: ?*anyopaque) bool;
pub extern fn l_ringbuf_capacity(ringbuf: ?*struct_l_ringbuf) usize;
pub extern fn l_ringbuf_len(ringbuf: ?*struct_l_ringbuf) usize;
pub extern fn l_ringbuf_drain(ringbuf: ?*struct_l_ringbuf, count: usize) usize;
pub extern fn l_ringbuf_peek(ringbuf: ?*struct_l_ringbuf, offset: usize, len_nowrap: [*c]usize) ?*anyopaque;
pub extern fn l_ringbuf_write(ringbuf: ?*struct_l_ringbuf, fd: c_int) isize;
pub extern fn l_ringbuf_avail(ringbuf: ?*struct_l_ringbuf) usize;
pub extern fn l_ringbuf_printf(ringbuf: ?*struct_l_ringbuf, format: [*c]const u8, ...) c_int;
pub extern fn l_ringbuf_vprintf(ringbuf: ?*struct_l_ringbuf, format: [*c]const u8, ap: [*c]struct___va_list_tag) c_int;
pub extern fn l_ringbuf_read(ringbuf: ?*struct_l_ringbuf, fd: c_int) isize;
pub extern fn l_ringbuf_append(ringbuf: ?*struct_l_ringbuf, data: ?*const anyopaque, len: usize) isize;
pub const l_log_func_t = ?*const fn (c_int, [*c]const u8, [*c]const u8, [*c]const u8, [*c]const u8, [*c]struct___va_list_tag) callconv(.C) void;
pub extern fn l_log_set_ident(ident: [*c]const u8) void;
pub extern fn l_log_set_handler(function: l_log_func_t) void;
pub extern fn l_log_set_null() void;
pub extern fn l_log_set_stderr() void;
pub extern fn l_log_set_syslog() void;
pub extern fn l_log_set_journal() void;
pub extern fn l_log_with_location(priority: c_int, file: [*c]const u8, line: [*c]const u8, func: [*c]const u8, format: [*c]const u8, ...) void;
pub const struct_l_debug_desc = extern struct {
    file: [*c]const u8,
    func: [*c]const u8,
    flags: c_uint,
};
pub extern fn l_debug_enable_full(pattern: [*c]const u8, start: [*c]struct_l_debug_desc, end: [*c]struct_l_debug_desc) void;
pub extern fn l_debug_add_section(start: [*c]struct_l_debug_desc, end: [*c]struct_l_debug_desc) void;
pub extern fn l_debug_disable() void;
pub const struct_l_checksum = opaque {};
pub const L_CHECKSUM_NONE: c_int = 0;
pub const L_CHECKSUM_MD4: c_int = 1;
pub const L_CHECKSUM_MD5: c_int = 2;
pub const L_CHECKSUM_SHA1: c_int = 3;
pub const L_CHECKSUM_SHA224: c_int = 4;
pub const L_CHECKSUM_SHA256: c_int = 5;
pub const L_CHECKSUM_SHA384: c_int = 6;
pub const L_CHECKSUM_SHA512: c_int = 7;
pub const enum_l_checksum_type = c_uint;
pub extern fn l_checksum_new(@"type": enum_l_checksum_type) ?*struct_l_checksum;
pub extern fn l_checksum_new_cmac_aes(key: ?*const anyopaque, key_len: usize) ?*struct_l_checksum;
pub extern fn l_checksum_new_hmac(@"type": enum_l_checksum_type, key: ?*const anyopaque, key_len: usize) ?*struct_l_checksum;
pub extern fn l_checksum_clone(checksum: ?*struct_l_checksum) ?*struct_l_checksum;
pub extern fn l_checksum_free(checksum: ?*struct_l_checksum) void;
pub extern fn l_checksum_reset(checksum: ?*struct_l_checksum) void;
pub extern fn l_checksum_update(checksum: ?*struct_l_checksum, data: ?*const anyopaque, len: usize) bool;
pub extern fn l_checksum_updatev(checksum: ?*struct_l_checksum, iov: [*c]const struct_iovec, iov_len: usize) bool;
pub extern fn l_checksum_get_digest(checksum: ?*struct_l_checksum, digest: ?*anyopaque, len: usize) isize;
pub extern fn l_checksum_get_string(checksum: ?*struct_l_checksum) [*c]u8;
pub extern fn l_checksum_is_supported(@"type": enum_l_checksum_type, check_hmac: bool) bool;
pub extern fn l_checksum_cmac_aes_supported() bool;
pub extern fn l_checksum_digest_length(@"type": enum_l_checksum_type) isize;
pub const ptrdiff_t = c_long;
pub const max_align_t = extern struct {
    __clang_max_align_nonce1: c_longlong align(8),
    __clang_max_align_nonce2: c_longdouble align(16),
};
pub const struct_l_settings = opaque {};
pub const l_settings_debug_cb_t = ?*const fn ([*c]const u8, ?*anyopaque) callconv(.C) void;
pub const l_settings_destroy_cb_t = ?*const fn (?*anyopaque) callconv(.C) void;
pub extern fn l_settings_new() ?*struct_l_settings;
pub extern fn l_settings_clone(settings: ?*const struct_l_settings) ?*struct_l_settings;
pub extern fn l_settings_free(settings: ?*struct_l_settings) void;
pub inline fn l_settings_free_cleanup(arg_p: ?*anyopaque) void {
    var p = arg_p;
    l_settings_free(@ptrCast(?*struct_l_settings, @ptrCast([*c]?*anyopaque, @alignCast(@import("std").meta.alignment([*c]?*anyopaque), p)).*));
}
pub extern fn l_settings_load_from_data(settings: ?*struct_l_settings, data: [*c]const u8, len: usize) bool;
pub extern fn l_settings_to_data(settings: ?*const struct_l_settings, len: [*c]usize) [*c]u8;
pub extern fn l_settings_load_from_file(settings: ?*struct_l_settings, filename: [*c]const u8) bool;
pub extern fn l_settings_set_debug(settings: ?*struct_l_settings, callback: l_settings_debug_cb_t, user_data: ?*anyopaque, destroy: l_settings_destroy_cb_t) bool;
pub extern fn l_settings_get_groups(settings: ?*const struct_l_settings) [*c][*c]u8;
pub extern fn l_settings_get_keys(settings: ?*const struct_l_settings, group_name: [*c]const u8) [*c][*c]u8;
pub extern fn l_settings_add_group(settings: ?*struct_l_settings, group_name: [*c]const u8) bool;
pub extern fn l_settings_has_group(settings: ?*const struct_l_settings, group_name: [*c]const u8) bool;
pub extern fn l_settings_has_key(settings: ?*const struct_l_settings, group_name: [*c]const u8, key: [*c]const u8) bool;
pub extern fn l_settings_get_value(settings: ?*const struct_l_settings, group_name: [*c]const u8, key: [*c]const u8) [*c]const u8;
pub extern fn l_settings_set_value(settings: ?*struct_l_settings, group_name: [*c]const u8, key: [*c]const u8, value: [*c]const u8) bool;
pub extern fn l_settings_get_bool(settings: ?*const struct_l_settings, group_name: [*c]const u8, key: [*c]const u8, out: [*c]bool) bool;
pub extern fn l_settings_set_bool(settings: ?*struct_l_settings, group_name: [*c]const u8, key: [*c]const u8, in: bool) bool;
pub extern fn l_settings_get_int(settings: ?*const struct_l_settings, group_name: [*c]const u8, key: [*c]const u8, out: [*c]c_int) bool;
pub extern fn l_settings_set_int(settings: ?*struct_l_settings, group_name: [*c]const u8, key: [*c]const u8, in: c_int) bool;
pub extern fn l_settings_get_uint(settings: ?*const struct_l_settings, group_name: [*c]const u8, key: [*c]const u8, out: [*c]c_uint) bool;
pub extern fn l_settings_set_uint(settings: ?*struct_l_settings, group_name: [*c]const u8, key: [*c]const u8, in: c_uint) bool;
pub extern fn l_settings_get_int64(settings: ?*const struct_l_settings, group_name: [*c]const u8, key: [*c]const u8, out: [*c]i64) bool;
pub extern fn l_settings_set_int64(settings: ?*struct_l_settings, group_name: [*c]const u8, key: [*c]const u8, in: i64) bool;
pub extern fn l_settings_get_uint64(settings: ?*const struct_l_settings, group_name: [*c]const u8, key: [*c]const u8, out: [*c]u64) bool;
pub extern fn l_settings_set_uint64(settings: ?*struct_l_settings, group_name: [*c]const u8, key: [*c]const u8, in: u64) bool;
pub extern fn l_settings_get_string(settings: ?*const struct_l_settings, group_name: [*c]const u8, key: [*c]const u8) [*c]u8;
pub extern fn l_settings_set_string(settings: ?*struct_l_settings, group_name: [*c]const u8, key: [*c]const u8, value: [*c]const u8) bool;
pub extern fn l_settings_get_string_list(settings: ?*const struct_l_settings, group_name: [*c]const u8, key: [*c]const u8, delimiter: u8) [*c][*c]u8;
pub extern fn l_settings_set_string_list(settings: ?*struct_l_settings, group_name: [*c]const u8, key: [*c]const u8, list: [*c][*c]u8, delimiter: u8) bool;
pub extern fn l_settings_get_double(settings: ?*const struct_l_settings, group_name: [*c]const u8, key: [*c]const u8, out: [*c]f64) bool;
pub extern fn l_settings_set_double(settings: ?*struct_l_settings, group_name: [*c]const u8, key: [*c]const u8, in: f64) bool;
pub extern fn l_settings_get_float(settings: ?*const struct_l_settings, group_name: [*c]const u8, key: [*c]const u8, out: [*c]f32) bool;
pub extern fn l_settings_set_float(settings: ?*struct_l_settings, group_name: [*c]const u8, key: [*c]const u8, in: f32) bool;
pub extern fn l_settings_get_bytes(settings: ?*const struct_l_settings, group_name: [*c]const u8, key: [*c]const u8, out_len: [*c]usize) [*c]u8;
pub extern fn l_settings_set_bytes(settings: ?*struct_l_settings, group_name: [*c]const u8, key: [*c]const u8, value: [*c]const u8, value_len: usize) bool;
pub extern fn l_settings_remove_key(settings: ?*struct_l_settings, group_name: [*c]const u8, key: [*c]const u8) bool;
pub extern fn l_settings_remove_group(settings: ?*struct_l_settings, group_name: [*c]const u8) bool;
pub extern fn l_settings_remove_embedded_groups(settings: ?*struct_l_settings) bool;
pub extern fn l_settings_get_embedded_groups(settings: ?*struct_l_settings) [*c][*c]u8;
pub extern fn l_settings_has_embedded_group(settings: ?*struct_l_settings, group: [*c]const u8) bool;
pub extern fn l_settings_get_embedded_value(settings: ?*struct_l_settings, group_name: [*c]const u8, out_type: [*c][*c]const u8) [*c]const u8;
pub const struct_l_hwdb = opaque {};
pub extern fn l_hwdb_new(pathname: [*c]const u8) ?*struct_l_hwdb;
pub extern fn l_hwdb_new_default() ?*struct_l_hwdb;
pub extern fn l_hwdb_ref(hwdb: ?*struct_l_hwdb) ?*struct_l_hwdb;
pub extern fn l_hwdb_unref(hwdb: ?*struct_l_hwdb) void;
pub const struct_l_hwdb_entry = extern struct {
    key: [*c]const u8,
    value: [*c]const u8,
    next: [*c]struct_l_hwdb_entry,
};
pub extern fn l_hwdb_lookup(hwdb: ?*struct_l_hwdb, format: [*c]const u8, ...) [*c]struct_l_hwdb_entry;
pub extern fn l_hwdb_lookup_valist(hwdb: ?*struct_l_hwdb, format: [*c]const u8, args: [*c]struct___va_list_tag) [*c]struct_l_hwdb_entry;
pub extern fn l_hwdb_lookup_free(entries: [*c]struct_l_hwdb_entry) void;
pub const l_hwdb_foreach_func_t = ?*const fn ([*c]const u8, [*c]struct_l_hwdb_entry, ?*anyopaque) callconv(.C) void;
pub extern fn l_hwdb_foreach(hwdb: ?*struct_l_hwdb, func: l_hwdb_foreach_func_t, user_data: ?*anyopaque) bool;
pub const struct_l_cipher = opaque {};
pub const L_CIPHER_AES: c_int = 0;
pub const L_CIPHER_AES_CBC: c_int = 1;
pub const L_CIPHER_AES_CTR: c_int = 2;
pub const L_CIPHER_ARC4: c_int = 3;
pub const L_CIPHER_DES: c_int = 4;
pub const L_CIPHER_DES_CBC: c_int = 5;
pub const L_CIPHER_DES3_EDE_CBC: c_int = 6;
pub const L_CIPHER_RC2_CBC: c_int = 7;
pub const enum_l_cipher_type = c_uint;
pub extern fn l_cipher_new(@"type": enum_l_cipher_type, key: ?*const anyopaque, key_length: usize) ?*struct_l_cipher;
pub extern fn l_cipher_free(cipher: ?*struct_l_cipher) void;
pub extern fn l_cipher_encrypt(cipher: ?*struct_l_cipher, in: ?*const anyopaque, out: ?*anyopaque, len: usize) bool;
pub extern fn l_cipher_encryptv(cipher: ?*struct_l_cipher, in: [*c]const struct_iovec, in_cnt: usize, out: [*c]const struct_iovec, out_cnt: usize) bool;
pub extern fn l_cipher_decrypt(cipher: ?*struct_l_cipher, in: ?*const anyopaque, out: ?*anyopaque, len: usize) bool;
pub extern fn l_cipher_decryptv(cipher: ?*struct_l_cipher, in: [*c]const struct_iovec, in_cnt: usize, out: [*c]const struct_iovec, out_cnt: usize) bool;
pub extern fn l_cipher_set_iv(cipher: ?*struct_l_cipher, iv: [*c]const u8, iv_length: usize) bool;
pub const struct_l_aead_cipher = opaque {};
pub const L_AEAD_CIPHER_AES_CCM: c_int = 0;
pub const L_AEAD_CIPHER_AES_GCM: c_int = 1;
pub const enum_l_aead_cipher_type = c_uint;
pub extern fn l_aead_cipher_new(@"type": enum_l_aead_cipher_type, key: ?*const anyopaque, key_length: usize, tag_length: usize) ?*struct_l_aead_cipher;
pub extern fn l_aead_cipher_free(cipher: ?*struct_l_aead_cipher) void;
pub extern fn l_aead_cipher_encrypt(cipher: ?*struct_l_aead_cipher, in: ?*const anyopaque, in_len: usize, ad: ?*const anyopaque, ad_len: usize, nonce: ?*const anyopaque, nonce_len: usize, out: ?*anyopaque, out_len: usize) bool;
pub extern fn l_aead_cipher_decrypt(cipher: ?*struct_l_aead_cipher, in: ?*const anyopaque, in_len: usize, ad: ?*const anyopaque, ad_len: usize, nonce: ?*const anyopaque, nonce_len: usize, out: ?*anyopaque, out_len: usize) bool;
pub extern fn l_cipher_is_supported(@"type": enum_l_cipher_type) bool;
pub extern fn l_aead_cipher_is_supported(@"type": enum_l_aead_cipher_type) bool;
pub extern fn l_getrandom(buf: ?*anyopaque, len: usize) bool;
pub extern fn l_getrandom_is_supported() bool;
pub extern fn l_getrandom_uint32() u32;
pub const l_uintset_foreach_func_t = ?*const fn (u32, ?*anyopaque) callconv(.C) void;
pub const struct_l_uintset = opaque {};
pub extern fn l_uintset_new_from_range(min: u32, max: u32) ?*struct_l_uintset;
pub extern fn l_uintset_new(size: c_uint) ?*struct_l_uintset;
pub extern fn l_uintset_free(set: ?*struct_l_uintset) void;
pub inline fn l_uintset_free_cleanup(arg_p: ?*anyopaque) void {
    var p = arg_p;
    l_uintset_free(@ptrCast(?*struct_l_uintset, @ptrCast([*c]?*anyopaque, @alignCast(@import("std").meta.alignment([*c]?*anyopaque), p)).*));
}
pub extern fn l_uintset_contains(set: ?*struct_l_uintset, number: u32) bool;
pub extern fn l_uintset_take(set: ?*struct_l_uintset, number: u32) bool;
pub extern fn l_uintset_put(set: ?*struct_l_uintset, number: u32) bool;
pub extern fn l_uintset_get_min(set: ?*struct_l_uintset) u32;
pub extern fn l_uintset_get_max(set: ?*struct_l_uintset) u32;
pub extern fn l_uintset_find_max(set: ?*struct_l_uintset) u32;
pub extern fn l_uintset_find_min(set: ?*struct_l_uintset) u32;
pub extern fn l_uintset_find_unused_min(set: ?*struct_l_uintset) u32;
pub extern fn l_uintset_find_unused(set: ?*struct_l_uintset, start: u32) u32;
pub extern fn l_uintset_foreach(set: ?*const struct_l_uintset, function: l_uintset_foreach_func_t, user_data: ?*anyopaque) void;
pub extern fn l_uintset_clone(original: ?*const struct_l_uintset) ?*struct_l_uintset;
pub extern fn l_uintset_intersect(set_a: ?*const struct_l_uintset, set_b: ?*const struct_l_uintset) ?*struct_l_uintset;
pub extern fn l_uintset_subtract(set_a: ?*const struct_l_uintset, set_b: ?*const struct_l_uintset) ?*struct_l_uintset;
pub extern fn l_uintset_isempty(set: ?*const struct_l_uintset) bool;
pub extern fn l_uintset_size(set: ?*const struct_l_uintset) u32;
pub extern fn l_base64_decode(in: [*c]const u8, in_len: usize, n_written: [*c]usize) [*c]u8;
pub extern fn l_base64_encode(in: [*c]const u8, in_len: usize, columns: c_int) [*c]u8;
pub const struct_l_key = opaque {};
pub const struct_l_cert = opaque {};
pub const struct_l_certchain = opaque {};
pub extern fn l_pem_load_buffer(buf: ?*const anyopaque, buf_len: usize, type_label: [*c][*c]u8, out_len: [*c]usize) [*c]u8;
pub extern fn l_pem_load_file(filename: [*c]const u8, type_label: [*c][*c]u8, len: [*c]usize) [*c]u8;
pub extern fn l_pem_load_certificate_chain(filename: [*c]const u8) ?*struct_l_certchain;
pub extern fn l_pem_load_certificate_chain_from_data(buf: ?*const anyopaque, len: usize) ?*struct_l_certchain;
pub extern fn l_pem_load_certificate_list(filename: [*c]const u8) ?*struct_l_queue;
pub extern fn l_pem_load_certificate_list_from_data(buf: ?*const anyopaque, len: usize) ?*struct_l_queue;
pub extern fn l_pem_load_private_key(filename: [*c]const u8, passphrase: [*c]const u8, encrypted: [*c]bool) ?*struct_l_key;
pub extern fn l_pem_load_private_key_from_data(buf: ?*const anyopaque, len: usize, passphrase: [*c]const u8, encrypted: [*c]bool) ?*struct_l_key;
pub const L_TLS_V10: c_int = 769;
pub const L_TLS_V11: c_int = 770;
pub const L_TLS_V12: c_int = 771;
pub const L_TLS_V13: c_int = 772;
pub const enum_l_tls_version = c_uint;
pub const struct_l_tls = opaque {};
pub const TLS_ALERT_CLOSE_NOTIFY: c_int = 0;
pub const TLS_ALERT_UNEXPECTED_MESSAGE: c_int = 10;
pub const TLS_ALERT_BAD_RECORD_MAC: c_int = 20;
pub const TLS_ALERT_DECRYPT_FAIL_RESERVED: c_int = 21;
pub const TLS_ALERT_RECORD_OVERFLOW: c_int = 22;
pub const TLS_ALERT_DECOMPRESS_FAIL: c_int = 30;
pub const TLS_ALERT_HANDSHAKE_FAIL: c_int = 40;
pub const TLS_ALERT_NO_CERT_RESERVED: c_int = 41;
pub const TLS_ALERT_BAD_CERT: c_int = 42;
pub const TLS_ALERT_UNSUPPORTED_CERT: c_int = 43;
pub const TLS_ALERT_CERT_REVOKED: c_int = 44;
pub const TLS_ALERT_CERT_EXPIRED: c_int = 45;
pub const TLS_ALERT_CERT_UNKNOWN: c_int = 46;
pub const TLS_ALERT_ILLEGAL_PARAM: c_int = 47;
pub const TLS_ALERT_UNKNOWN_CA: c_int = 48;
pub const TLS_ALERT_ACCESS_DENIED: c_int = 49;
pub const TLS_ALERT_DECODE_ERROR: c_int = 50;
pub const TLS_ALERT_DECRYPT_ERROR: c_int = 51;
pub const TLS_ALERT_EXPORT_RES_RESERVED: c_int = 60;
pub const TLS_ALERT_PROTOCOL_VERSION: c_int = 70;
pub const TLS_ALERT_INSUFFICIENT_SECURITY: c_int = 71;
pub const TLS_ALERT_INTERNAL_ERROR: c_int = 80;
pub const TLS_ALERT_USER_CANCELED: c_int = 90;
pub const TLS_ALERT_NO_RENEGOTIATION: c_int = 100;
pub const TLS_ALERT_UNSUPPORTED_EXTENSION: c_int = 110;
pub const enum_l_tls_alert_desc = c_uint;
pub const l_tls_write_cb_t = ?*const fn ([*c]const u8, usize, ?*anyopaque) callconv(.C) void;
pub const l_tls_ready_cb_t = ?*const fn ([*c]const u8, ?*anyopaque) callconv(.C) void;
pub const l_tls_disconnect_cb_t = ?*const fn (enum_l_tls_alert_desc, bool, ?*anyopaque) callconv(.C) void;
pub const l_tls_debug_cb_t = ?*const fn ([*c]const u8, ?*anyopaque) callconv(.C) void;
pub const l_tls_destroy_cb_t = ?*const fn (?*anyopaque) callconv(.C) void;
pub const l_tls_session_update_cb_t = ?*const fn (?*anyopaque) callconv(.C) void;
pub extern fn l_tls_new(server: bool, app_data_handler: l_tls_write_cb_t, tx_handler: l_tls_write_cb_t, ready_handler: l_tls_ready_cb_t, disconnect_handler: l_tls_disconnect_cb_t, user_data: ?*anyopaque) ?*struct_l_tls;
pub extern fn l_tls_free(tls: ?*struct_l_tls) void;
pub extern fn l_tls_start(tls: ?*struct_l_tls) bool;
pub extern fn l_tls_close(tls: ?*struct_l_tls) void;
pub extern fn l_tls_reset(tls: ?*struct_l_tls) void;
pub extern fn l_tls_write(tls: ?*struct_l_tls, data: [*c]const u8, len: usize) void;
pub extern fn l_tls_handle_rx(tls: ?*struct_l_tls, data: [*c]const u8, len: usize) void;
pub extern fn l_tls_set_cacert(tls: ?*struct_l_tls, ca_certs: ?*struct_l_queue) bool;
pub extern fn l_tls_set_auth_data(tls: ?*struct_l_tls, certchain: ?*struct_l_certchain, priv_key: ?*struct_l_key) bool;
pub extern fn l_tls_set_version_range(tls: ?*struct_l_tls, min_version: enum_l_tls_version, max_version: enum_l_tls_version) void;
pub extern fn l_tls_set_domain_mask(tls: ?*struct_l_tls, mask: [*c][*c]u8) void;
pub extern fn l_tls_set_session_cache(tls: ?*struct_l_tls, settings: ?*struct_l_settings, group_prefix: [*c]const u8, lifetime: u64, max_sessions: c_uint, update_cb: l_tls_session_update_cb_t, user_data: ?*anyopaque) void;
pub extern fn l_tls_alert_to_str(desc: enum_l_tls_alert_desc) [*c]const u8;
pub extern fn l_tls_prf_get_bytes(tls: ?*struct_l_tls, use_master_secret: bool, label: [*c]const u8, buf: [*c]u8, len: usize) bool;
pub extern fn l_tls_set_debug(tls: ?*struct_l_tls, function: l_tls_debug_cb_t, user_data: ?*anyopaque, destroy: l_tls_destroy_cb_t) bool;
pub extern fn l_tls_set_cert_dump_path(tls: ?*struct_l_tls, path: [*c]const u8) bool;
pub const L_UUID_VERSION_1_TIME: c_int = 1;
pub const L_UUID_VERSION_2_DCE: c_int = 2;
pub const L_UUID_VERSION_3_MD5: c_int = 3;
pub const L_UUID_VERSION_4_RANDOM: c_int = 4;
pub const L_UUID_VERSION_5_SHA1: c_int = 5;
pub const enum_l_uuid_version = c_uint;
pub extern const L_UUID_NAMESPACE_DNS: [*c]const u8;
pub extern const L_UUID_NAMESPACE_URL: [*c]const u8;
pub extern const L_UUID_NAMESPACE_OID: [*c]const u8;
pub extern const L_UUID_NAMESPACE_X500: [*c]const u8;
pub extern fn l_uuid_v3(nsid: [*c]const u8, name: ?*const anyopaque, name_size: usize, out_uuid: [*c]u8) bool;
pub extern fn l_uuid_v4(out_uuid: [*c]u8) bool;
pub extern fn l_uuid_v5(nsid: [*c]const u8, name: ?*const anyopaque, name_size: usize, out_uuid: [*c]u8) bool;
pub extern fn l_uuid_is_valid(uuid: [*c]const u8) bool;
pub extern fn l_uuid_get_version(uuid: [*c]const u8) enum_l_uuid_version;
pub extern fn l_uuid_to_string(uuid: [*c]const u8, dest: [*c]u8, dest_size: usize) bool;
pub extern fn l_uuid_from_string(src: [*c]const u8, uuid: [*c]u8) bool;
pub const struct_l_keyring = opaque {};
pub const L_KEY_FEATURE_DH: c_int = 1;
pub const L_KEY_FEATURE_RESTRICT: c_int = 2;
pub const L_KEY_FEATURE_CRYPTO: c_int = 4;
pub const enum_l_key_feature = c_uint;
pub const L_KEY_RAW: c_int = 0;
pub const L_KEY_RSA: c_int = 1;
pub const L_KEY_ECC: c_int = 2;
pub const enum_l_key_type = c_uint;
pub const L_KEYRING_RESTRICT_ASYM: c_int = 0;
pub const L_KEYRING_RESTRICT_ASYM_CHAIN: c_int = 1;
pub const enum_l_keyring_restriction = c_uint;
pub const L_KEY_RSA_PKCS1_V1_5: c_int = 0;
pub const L_KEY_RSA_RAW: c_int = 1;
pub const L_KEY_ECDSA_X962: c_int = 2;
pub const enum_l_key_cipher_type = c_uint;
pub extern fn l_key_new(@"type": enum_l_key_type, payload: ?*const anyopaque, payload_length: usize) ?*struct_l_key;
pub extern fn l_key_free(key: ?*struct_l_key) void;
pub extern fn l_key_free_norevoke(key: ?*struct_l_key) void;
pub extern fn l_key_update(key: ?*struct_l_key, payload: ?*const anyopaque, len: usize) bool;
pub extern fn l_key_extract(key: ?*struct_l_key, payload: ?*anyopaque, len: [*c]usize) bool;
pub extern fn l_key_get_payload_size(key: ?*struct_l_key) isize;
pub extern fn l_key_get_info(key: ?*struct_l_key, cipher: enum_l_key_cipher_type, checksum: enum_l_checksum_type, bits: [*c]usize, out_public: [*c]bool) bool;
pub extern fn l_key_generate_dh_private(prime_buf: ?*const anyopaque, prime_len: usize) ?*struct_l_key;
pub extern fn l_key_compute_dh_public(generator: ?*struct_l_key, private_key: ?*struct_l_key, prime: ?*struct_l_key, payload: ?*anyopaque, len: [*c]usize) bool;
pub extern fn l_key_compute_dh_secret(other_public: ?*struct_l_key, private_key: ?*struct_l_key, prime: ?*struct_l_key, payload: ?*anyopaque, len: [*c]usize) bool;
pub extern fn l_key_validate_dh_payload(payload: ?*const anyopaque, len: usize, prime_buf: ?*const anyopaque, prime_len: usize) bool;
pub extern fn l_key_encrypt(key: ?*struct_l_key, cipher: enum_l_key_cipher_type, checksum: enum_l_checksum_type, in: ?*const anyopaque, out: ?*anyopaque, len_in: usize, len_out: usize) isize;
pub extern fn l_key_decrypt(key: ?*struct_l_key, cipher: enum_l_key_cipher_type, checksum: enum_l_checksum_type, in: ?*const anyopaque, out: ?*anyopaque, len_in: usize, len_out: usize) isize;
pub extern fn l_key_sign(key: ?*struct_l_key, cipher: enum_l_key_cipher_type, checksum: enum_l_checksum_type, in: ?*const anyopaque, out: ?*anyopaque, len_in: usize, len_out: usize) isize;
pub extern fn l_key_verify(key: ?*struct_l_key, cipher: enum_l_key_cipher_type, checksum: enum_l_checksum_type, data: ?*const anyopaque, sig: ?*const anyopaque, len_data: usize, len_sig: usize) bool;
pub extern fn l_keyring_new() ?*struct_l_keyring;
pub extern fn l_keyring_restrict(keyring: ?*struct_l_keyring, res: enum_l_keyring_restriction, trust: ?*const struct_l_keyring) bool;
pub extern fn l_keyring_free(keyring: ?*struct_l_keyring) void;
pub inline fn l_keyring_free_cleanup(arg_p: ?*anyopaque) void {
    var p = arg_p;
    l_keyring_free(@ptrCast(?*struct_l_keyring, @ptrCast([*c]?*anyopaque, @alignCast(@import("std").meta.alignment([*c]?*anyopaque), p)).*));
}
pub extern fn l_keyring_free_norevoke(keyring: ?*struct_l_keyring) void;
pub inline fn l_keyring_free_norevoke_cleanup(arg_p: ?*anyopaque) void {
    var p = arg_p;
    l_keyring_free_norevoke(@ptrCast(?*struct_l_keyring, @ptrCast([*c]?*anyopaque, @alignCast(@import("std").meta.alignment([*c]?*anyopaque), p)).*));
}
pub extern fn l_keyring_link(keyring: ?*struct_l_keyring, key: ?*const struct_l_key) bool;
pub extern fn l_keyring_unlink(keyring: ?*struct_l_keyring, key: ?*const struct_l_key) bool;
pub extern fn l_keyring_link_nested(keyring: ?*struct_l_keyring, nested: ?*const struct_l_keyring) bool;
pub extern fn l_keyring_unlink_nested(keyring: ?*struct_l_keyring, nested: ?*const struct_l_keyring) bool;
pub extern fn l_key_is_supported(features: u32) bool;
pub extern fn l_file_get_contents(filename: [*c]const u8, out_len: [*c]usize) ?*anyopaque;
pub const struct_l_dir_watch = opaque {};
pub const L_DIR_WATCH_EVENT_CREATED: c_int = 0;
pub const L_DIR_WATCH_EVENT_REMOVED: c_int = 1;
pub const L_DIR_WATCH_EVENT_MODIFIED: c_int = 2;
pub const L_DIR_WATCH_EVENT_ACCESSED: c_int = 3;
pub const L_DIR_WATCH_EVENT_ATTRIB: c_int = 4;
pub const enum_l_dir_watch_event = c_uint;
pub const l_dir_watch_event_func_t = ?*const fn ([*c]const u8, enum_l_dir_watch_event, ?*anyopaque) callconv(.C) void;
pub const l_dir_watch_destroy_func_t = ?*const fn (?*anyopaque) callconv(.C) void;
pub extern fn l_dir_watch_new(pathname: [*c]const u8, function: l_dir_watch_event_func_t, user_data: ?*anyopaque, destroy: l_dir_watch_destroy_func_t) ?*struct_l_dir_watch;
pub extern fn l_dir_watch_destroy(watch: ?*struct_l_dir_watch) void;
pub const struct_in_addr = opaque {};
pub const struct_in6_addr = opaque {};
pub extern fn l_net_get_mac_address(ifindex: u32, out_addr: [*c]u8) bool;
pub extern fn l_net_get_name(ifindex: u32) [*c]u8;
pub extern fn l_net_hostname_is_root(hostname: [*c]const u8) bool;
pub extern fn l_net_hostname_is_localhost(hostname: [*c]const u8) bool;
pub extern fn l_net_get_address(ifindex: c_int, out: ?*struct_in_addr) bool;
pub extern fn l_net_get_link_local_address(ifindex: c_int, out: ?*struct_in6_addr) bool;
pub fn l_net_prefix_matches(arg_a: ?*const anyopaque, arg_b: ?*const anyopaque, arg_prefix_len: u8) callconv(.C) bool {
    var a = arg_a;
    var b = arg_b;
    var prefix_len = arg_prefix_len;
    var bytes: u8 = @bitCast(u8, @truncate(i8, @divTrunc(@bitCast(c_int, @as(c_uint, prefix_len)), @as(c_int, 8))));
    var bits: u8 = @bitCast(u8, @truncate(i8, @bitCast(c_int, @as(c_uint, prefix_len)) & @as(c_int, 7)));
    var left: u8 = @ptrCast([*c]const u8, @alignCast(@import("std").meta.alignment([*c]const u8), a))[bytes];
    var right: u8 = @ptrCast([*c]const u8, @alignCast(@import("std").meta.alignment([*c]const u8), b))[bytes];
    if (memcmp(a, b, @bitCast(c_ulong, @as(c_ulong, bytes))) != 0) return @as(c_int, 0) != 0;
    return !(bits != 0) or ((@bitCast(c_uint, @bitCast(c_int, @as(c_uint, left)) ^ @bitCast(c_int, @as(c_uint, right))) & (@as(c_uint, 65280) >> @intCast(@import("std").math.Log2Int(c_uint), @bitCast(c_int, @as(c_uint, bits))))) == @bitCast(c_uint, @as(c_int, 0)));
}
pub const l_netlink_debug_func_t = ?*const fn ([*c]const u8, ?*anyopaque) callconv(.C) void;
pub const l_netlink_command_func_t = ?*const fn (c_int, u16, ?*const anyopaque, u32, ?*anyopaque) callconv(.C) void;
pub const l_netlink_notify_func_t = ?*const fn (u16, ?*const anyopaque, u32, ?*anyopaque) callconv(.C) void;
pub const l_netlink_destroy_func_t = ?*const fn (?*anyopaque) callconv(.C) void;
pub const struct_l_netlink = opaque {};
pub extern fn l_netlink_new(protocol: c_int) ?*struct_l_netlink;
pub extern fn l_netlink_destroy(netlink: ?*struct_l_netlink) void;
pub extern fn l_netlink_send(netlink: ?*struct_l_netlink, @"type": u16, flags: u16, data: ?*const anyopaque, len: u32, function: l_netlink_command_func_t, user_data: ?*anyopaque, destroy: l_netlink_destroy_func_t) c_uint;
pub extern fn l_netlink_cancel(netlink: ?*struct_l_netlink, id: c_uint) bool;
pub extern fn l_netlink_register(netlink: ?*struct_l_netlink, group: u32, function: l_netlink_notify_func_t, user_data: ?*anyopaque, destroy: l_netlink_destroy_func_t) c_uint;
pub extern fn l_netlink_unregister(netlink: ?*struct_l_netlink, id: c_uint) bool;
pub extern fn l_netlink_set_debug(netlink: ?*struct_l_netlink, function: l_netlink_debug_func_t, user_data: ?*anyopaque, destroy: l_netlink_destroy_func_t) bool;
pub const struct_l_genl = opaque {};
pub const struct_l_genl_family_info = opaque {};
pub const struct_l_genl_family = opaque {};
pub const struct_l_genl_msg = opaque {};
pub const l_genl_destroy_func_t = ?*const fn (?*anyopaque) callconv(.C) void;
pub const l_genl_debug_func_t = ?*const fn ([*c]const u8, ?*anyopaque) callconv(.C) void;
pub const l_genl_msg_func_t = ?*const fn (?*struct_l_genl_msg, ?*anyopaque) callconv(.C) void;
pub const l_genl_discover_func_t = ?*const fn (?*const struct_l_genl_family_info, ?*anyopaque) callconv(.C) void;
pub const l_genl_vanished_func_t = ?*const fn ([*c]const u8, ?*anyopaque) callconv(.C) void;
pub extern fn l_genl_new() ?*struct_l_genl;
pub extern fn l_genl_ref(genl: ?*struct_l_genl) ?*struct_l_genl;
pub extern fn l_genl_unref(genl: ?*struct_l_genl) void;
pub extern fn l_genl_set_debug(genl: ?*struct_l_genl, callback: l_genl_debug_func_t, user_data: ?*anyopaque, destroy: l_genl_destroy_func_t) bool;
pub extern fn l_genl_discover_families(genl: ?*struct_l_genl, cb: l_genl_discover_func_t, user_data: ?*anyopaque, destroy: l_genl_destroy_func_t) bool;
pub extern fn l_genl_add_unicast_watch(genl: ?*struct_l_genl, family: [*c]const u8, handler: l_genl_msg_func_t, user_data: ?*anyopaque, destroy: l_genl_destroy_func_t) c_uint;
pub extern fn l_genl_remove_unicast_watch(genl: ?*struct_l_genl, id: c_uint) bool;
pub extern fn l_genl_add_family_watch(genl: ?*struct_l_genl, name: [*c]const u8, appeared_func: l_genl_discover_func_t, vanished_func: l_genl_vanished_func_t, user_data: ?*anyopaque, destroy: l_genl_destroy_func_t) c_uint;
pub extern fn l_genl_remove_family_watch(genl: ?*struct_l_genl, id: c_uint) bool;
pub extern fn l_genl_request_family(genl: ?*struct_l_genl, name: [*c]const u8, appeared_func: l_genl_discover_func_t, user_data: ?*anyopaque, destroy: l_genl_destroy_func_t) bool;
pub const struct_l_genl_attr = extern struct {
    data: ?*const anyopaque,
    len: u32,
    next_data: ?*const anyopaque,
    next_len: u32,
};
pub extern fn l_genl_msg_new(cmd: u8) ?*struct_l_genl_msg;
pub extern fn l_genl_msg_new_sized(cmd: u8, size: u32) ?*struct_l_genl_msg;
pub extern fn l_genl_msg_new_from_data(data: ?*const anyopaque, size: usize) ?*struct_l_genl_msg;
pub extern fn l_genl_msg_to_data(msg: ?*struct_l_genl_msg, @"type": u16, flags: u16, seq: u32, pid: u32, out_size: [*c]usize) ?*const anyopaque;
pub extern fn l_genl_msg_ref(msg: ?*struct_l_genl_msg) ?*struct_l_genl_msg;
pub extern fn l_genl_msg_unref(msg: ?*struct_l_genl_msg) void;
pub extern fn l_genl_msg_get_command(msg: ?*struct_l_genl_msg) u8;
pub extern fn l_genl_msg_get_version(msg: ?*struct_l_genl_msg) u8;
pub extern fn l_genl_msg_get_error(msg: ?*struct_l_genl_msg) c_int;
pub extern fn l_genl_msg_get_extended_error(msg: ?*struct_l_genl_msg) [*c]const u8;
pub extern fn l_genl_msg_append_attr(msg: ?*struct_l_genl_msg, @"type": u16, len: u16, data: ?*const anyopaque) bool;
pub extern fn l_genl_msg_append_attrv(msg: ?*struct_l_genl_msg, @"type": u16, iov: [*c]const struct_iovec, iov_len: usize) bool;
pub extern fn l_genl_msg_enter_nested(msg: ?*struct_l_genl_msg, @"type": u16) bool;
pub extern fn l_genl_msg_leave_nested(msg: ?*struct_l_genl_msg) bool;
pub extern fn l_genl_attr_init(attr: [*c]struct_l_genl_attr, msg: ?*struct_l_genl_msg) bool;
pub extern fn l_genl_attr_next(attr: [*c]struct_l_genl_attr, @"type": [*c]u16, len: [*c]u16, data: [*c]?*const anyopaque) bool;
pub extern fn l_genl_attr_recurse(attr: [*c]const struct_l_genl_attr, nested: [*c]struct_l_genl_attr) bool;
pub extern fn l_genl_family_info_has_group(info: ?*const struct_l_genl_family_info, group: [*c]const u8) bool;
pub extern fn l_genl_family_info_can_send(info: ?*const struct_l_genl_family_info, cmd: u8) bool;
pub extern fn l_genl_family_info_can_dump(info: ?*const struct_l_genl_family_info, cmd: u8) bool;
pub extern fn l_genl_family_info_get_groups(info: ?*const struct_l_genl_family_info) [*c][*c]u8;
pub extern fn l_genl_family_info_get_id(info: ?*const struct_l_genl_family_info) u32;
pub extern fn l_genl_family_info_get_name(info: ?*const struct_l_genl_family_info) [*c]const u8;
pub extern fn l_genl_family_info_get_version(info: ?*const struct_l_genl_family_info) u32;
pub extern fn l_genl_family_new(genl: ?*struct_l_genl, name: [*c]const u8) ?*struct_l_genl_family;
pub extern fn l_genl_family_free(family: ?*struct_l_genl_family) void;
pub extern fn l_genl_family_get_info(family: ?*struct_l_genl_family) ?*const struct_l_genl_family_info;
pub extern fn l_genl_family_get_genl(family: ?*struct_l_genl_family) ?*struct_l_genl;
pub extern fn l_genl_family_send(family: ?*struct_l_genl_family, msg: ?*struct_l_genl_msg, callback: l_genl_msg_func_t, user_data: ?*anyopaque, destroy: l_genl_destroy_func_t) c_uint;
pub extern fn l_genl_family_dump(family: ?*struct_l_genl_family, msg: ?*struct_l_genl_msg, callback: l_genl_msg_func_t, user_data: ?*anyopaque, destroy: l_genl_destroy_func_t) c_uint;
pub extern fn l_genl_family_cancel(family: ?*struct_l_genl_family, id: c_uint) bool;
pub extern fn l_genl_family_request_sent(family: ?*struct_l_genl_family, id: c_uint) bool;
pub extern fn l_genl_family_register(family: ?*struct_l_genl_family, group: [*c]const u8, callback: l_genl_msg_func_t, user_data: ?*anyopaque, destroy: l_genl_destroy_func_t) c_uint;
pub extern fn l_genl_family_unregister(family: ?*struct_l_genl_family, id: c_uint) bool;
pub const __s8 = i8;
pub const __u8 = u8;
pub const __s16 = c_short;
pub const __u16 = c_ushort;
pub const __s32 = c_int;
pub const __u32 = c_uint;
pub const __s64 = c_longlong;
pub const __u64 = c_ulonglong;
pub const __kernel_fd_set = extern struct {
    fds_bits: [16]c_ulong,
};
pub const __kernel_sighandler_t = ?*const fn (c_int) callconv(.C) void;
pub const __kernel_key_t = c_int;
pub const __kernel_mqd_t = c_int;
pub const __kernel_old_uid_t = c_ushort;
pub const __kernel_old_gid_t = c_ushort;
pub const __kernel_old_dev_t = c_ulong;
pub const __kernel_long_t = c_long;
pub const __kernel_ulong_t = c_ulong;
pub const __kernel_ino_t = __kernel_ulong_t;
pub const __kernel_mode_t = c_uint;
pub const __kernel_pid_t = c_int;
pub const __kernel_ipc_pid_t = c_int;
pub const __kernel_uid_t = c_uint;
pub const __kernel_gid_t = c_uint;
pub const __kernel_suseconds_t = __kernel_long_t;
pub const __kernel_daddr_t = c_int;
pub const __kernel_uid32_t = c_uint;
pub const __kernel_gid32_t = c_uint;
pub const __kernel_size_t = __kernel_ulong_t;
pub const __kernel_ssize_t = __kernel_long_t;
pub const __kernel_ptrdiff_t = __kernel_long_t;
pub const __kernel_fsid_t = extern struct {
    val: [2]c_int,
};
pub const __kernel_off_t = __kernel_long_t;
pub const __kernel_loff_t = c_longlong;
pub const __kernel_old_time_t = __kernel_long_t;
pub const __kernel_time_t = __kernel_long_t;
pub const __kernel_time64_t = c_longlong;
pub const __kernel_clock_t = __kernel_long_t;
pub const __kernel_timer_t = c_int;
pub const __kernel_clockid_t = c_int;
pub const __kernel_caddr_t = [*c]u8;
pub const __kernel_uid16_t = c_ushort;
pub const __kernel_gid16_t = c_ushort;
pub const __le16 = __u16;
pub const __be16 = __u16;
pub const __le32 = __u32;
pub const __be32 = __u32;
pub const __le64 = __u64;
pub const __be64 = __u64;
pub const __sum16 = __u16;
pub const __wsum = __u32;
pub const __poll_t = c_uint;
pub const __kernel_sa_family_t = c_ushort;
const struct_unnamed_25 = extern struct {
    ss_family: __kernel_sa_family_t,
    __data: [126]u8,
};
const union_unnamed_24 = extern union {
    unnamed_0: struct_unnamed_25,
    __align: ?*anyopaque,
};
pub const struct___kernel_sockaddr_storage = extern struct {
    unnamed_0: union_unnamed_24,
};
pub const struct_sockaddr_nl = extern struct {
    nl_family: __kernel_sa_family_t,
    nl_pad: c_ushort,
    nl_pid: __u32,
    nl_groups: __u32,
};
pub const struct_nlmsghdr = extern struct {
    nlmsg_len: __u32,
    nlmsg_type: __u16,
    nlmsg_flags: __u16,
    nlmsg_seq: __u32,
    nlmsg_pid: __u32,
};
pub const struct_nlmsgerr = extern struct {
    @"error": c_int,
    msg: struct_nlmsghdr,
};
pub const NLMSGERR_ATTR_UNUSED: c_int = 0;
pub const NLMSGERR_ATTR_MSG: c_int = 1;
pub const NLMSGERR_ATTR_OFFS: c_int = 2;
pub const NLMSGERR_ATTR_COOKIE: c_int = 3;
pub const NLMSGERR_ATTR_POLICY: c_int = 4;
pub const __NLMSGERR_ATTR_MAX: c_int = 5;
pub const NLMSGERR_ATTR_MAX: c_int = 4;
pub const enum_nlmsgerr_attrs = c_uint;
pub const struct_nl_pktinfo = extern struct {
    group: __u32,
};
pub const struct_nl_mmap_req = extern struct {
    nm_block_size: c_uint,
    nm_block_nr: c_uint,
    nm_frame_size: c_uint,
    nm_frame_nr: c_uint,
};
pub const struct_nl_mmap_hdr = extern struct {
    nm_status: c_uint,
    nm_len: c_uint,
    nm_group: __u32,
    nm_pid: __u32,
    nm_uid: __u32,
    nm_gid: __u32,
};
pub const NL_MMAP_STATUS_UNUSED: c_int = 0;
pub const NL_MMAP_STATUS_RESERVED: c_int = 1;
pub const NL_MMAP_STATUS_VALID: c_int = 2;
pub const NL_MMAP_STATUS_COPY: c_int = 3;
pub const NL_MMAP_STATUS_SKIP: c_int = 4;
pub const enum_nl_mmap_status = c_uint;
pub const NETLINK_UNCONNECTED: c_int = 0;
pub const NETLINK_CONNECTED: c_int = 1;
const enum_unnamed_26 = c_uint;
pub const struct_nlattr = extern struct {
    nla_len: __u16,
    nla_type: __u16,
};
pub const struct_nla_bitfield32 = extern struct {
    value: __u32,
    selector: __u32,
};
pub const NL_ATTR_TYPE_INVALID: c_int = 0;
pub const NL_ATTR_TYPE_FLAG: c_int = 1;
pub const NL_ATTR_TYPE_U8: c_int = 2;
pub const NL_ATTR_TYPE_U16: c_int = 3;
pub const NL_ATTR_TYPE_U32: c_int = 4;
pub const NL_ATTR_TYPE_U64: c_int = 5;
pub const NL_ATTR_TYPE_S8: c_int = 6;
pub const NL_ATTR_TYPE_S16: c_int = 7;
pub const NL_ATTR_TYPE_S32: c_int = 8;
pub const NL_ATTR_TYPE_S64: c_int = 9;
pub const NL_ATTR_TYPE_BINARY: c_int = 10;
pub const NL_ATTR_TYPE_STRING: c_int = 11;
pub const NL_ATTR_TYPE_NUL_STRING: c_int = 12;
pub const NL_ATTR_TYPE_NESTED: c_int = 13;
pub const NL_ATTR_TYPE_NESTED_ARRAY: c_int = 14;
pub const NL_ATTR_TYPE_BITFIELD32: c_int = 15;
pub const enum_netlink_attribute_type = c_uint;
pub const NL_POLICY_TYPE_ATTR_UNSPEC: c_int = 0;
pub const NL_POLICY_TYPE_ATTR_TYPE: c_int = 1;
pub const NL_POLICY_TYPE_ATTR_MIN_VALUE_S: c_int = 2;
pub const NL_POLICY_TYPE_ATTR_MAX_VALUE_S: c_int = 3;
pub const NL_POLICY_TYPE_ATTR_MIN_VALUE_U: c_int = 4;
pub const NL_POLICY_TYPE_ATTR_MAX_VALUE_U: c_int = 5;
pub const NL_POLICY_TYPE_ATTR_MIN_LENGTH: c_int = 6;
pub const NL_POLICY_TYPE_ATTR_MAX_LENGTH: c_int = 7;
pub const NL_POLICY_TYPE_ATTR_POLICY_IDX: c_int = 8;
pub const NL_POLICY_TYPE_ATTR_POLICY_MAXTYPE: c_int = 9;
pub const NL_POLICY_TYPE_ATTR_BITFIELD32_MASK: c_int = 10;
pub const NL_POLICY_TYPE_ATTR_PAD: c_int = 11;
pub const NL_POLICY_TYPE_ATTR_MASK: c_int = 12;
pub const __NL_POLICY_TYPE_ATTR_MAX: c_int = 13;
pub const NL_POLICY_TYPE_ATTR_MAX: c_int = 12;
pub const enum_netlink_policy_type_attr = c_uint;
pub const struct_rtnl_link_stats = extern struct {
    rx_packets: __u32,
    tx_packets: __u32,
    rx_bytes: __u32,
    tx_bytes: __u32,
    rx_errors: __u32,
    tx_errors: __u32,
    rx_dropped: __u32,
    tx_dropped: __u32,
    multicast: __u32,
    collisions: __u32,
    rx_length_errors: __u32,
    rx_over_errors: __u32,
    rx_crc_errors: __u32,
    rx_frame_errors: __u32,
    rx_fifo_errors: __u32,
    rx_missed_errors: __u32,
    tx_aborted_errors: __u32,
    tx_carrier_errors: __u32,
    tx_fifo_errors: __u32,
    tx_heartbeat_errors: __u32,
    tx_window_errors: __u32,
    rx_compressed: __u32,
    tx_compressed: __u32,
    rx_nohandler: __u32,
};
pub const struct_rtnl_link_stats64 = extern struct {
    rx_packets: __u64,
    tx_packets: __u64,
    rx_bytes: __u64,
    tx_bytes: __u64,
    rx_errors: __u64,
    tx_errors: __u64,
    rx_dropped: __u64,
    tx_dropped: __u64,
    multicast: __u64,
    collisions: __u64,
    rx_length_errors: __u64,
    rx_over_errors: __u64,
    rx_crc_errors: __u64,
    rx_frame_errors: __u64,
    rx_fifo_errors: __u64,
    rx_missed_errors: __u64,
    tx_aborted_errors: __u64,
    tx_carrier_errors: __u64,
    tx_fifo_errors: __u64,
    tx_heartbeat_errors: __u64,
    tx_window_errors: __u64,
    rx_compressed: __u64,
    tx_compressed: __u64,
    rx_nohandler: __u64,
};
pub const struct_rtnl_hw_stats64 = extern struct {
    rx_packets: __u64,
    tx_packets: __u64,
    rx_bytes: __u64,
    tx_bytes: __u64,
    rx_errors: __u64,
    tx_errors: __u64,
    rx_dropped: __u64,
    tx_dropped: __u64,
    multicast: __u64,
};
pub const struct_rtnl_link_ifmap = extern struct {
    mem_start: __u64,
    mem_end: __u64,
    base_addr: __u64,
    irq: __u16,
    dma: __u8,
    port: __u8,
};
pub const IFLA_UNSPEC: c_int = 0;
pub const IFLA_ADDRESS: c_int = 1;
pub const IFLA_BROADCAST: c_int = 2;
pub const IFLA_IFNAME: c_int = 3;
pub const IFLA_MTU: c_int = 4;
pub const IFLA_LINK: c_int = 5;
pub const IFLA_QDISC: c_int = 6;
pub const IFLA_STATS: c_int = 7;
pub const IFLA_COST: c_int = 8;
pub const IFLA_PRIORITY: c_int = 9;
pub const IFLA_MASTER: c_int = 10;
pub const IFLA_WIRELESS: c_int = 11;
pub const IFLA_PROTINFO: c_int = 12;
pub const IFLA_TXQLEN: c_int = 13;
pub const IFLA_MAP: c_int = 14;
pub const IFLA_WEIGHT: c_int = 15;
pub const IFLA_OPERSTATE: c_int = 16;
pub const IFLA_LINKMODE: c_int = 17;
pub const IFLA_LINKINFO: c_int = 18;
pub const IFLA_NET_NS_PID: c_int = 19;
pub const IFLA_IFALIAS: c_int = 20;
pub const IFLA_NUM_VF: c_int = 21;
pub const IFLA_VFINFO_LIST: c_int = 22;
pub const IFLA_STATS64: c_int = 23;
pub const IFLA_VF_PORTS: c_int = 24;
pub const IFLA_PORT_SELF: c_int = 25;
pub const IFLA_AF_SPEC: c_int = 26;
pub const IFLA_GROUP: c_int = 27;
pub const IFLA_NET_NS_FD: c_int = 28;
pub const IFLA_EXT_MASK: c_int = 29;
pub const IFLA_PROMISCUITY: c_int = 30;
pub const IFLA_NUM_TX_QUEUES: c_int = 31;
pub const IFLA_NUM_RX_QUEUES: c_int = 32;
pub const IFLA_CARRIER: c_int = 33;
pub const IFLA_PHYS_PORT_ID: c_int = 34;
pub const IFLA_CARRIER_CHANGES: c_int = 35;
pub const IFLA_PHYS_SWITCH_ID: c_int = 36;
pub const IFLA_LINK_NETNSID: c_int = 37;
pub const IFLA_PHYS_PORT_NAME: c_int = 38;
pub const IFLA_PROTO_DOWN: c_int = 39;
pub const IFLA_GSO_MAX_SEGS: c_int = 40;
pub const IFLA_GSO_MAX_SIZE: c_int = 41;
pub const IFLA_PAD: c_int = 42;
pub const IFLA_XDP: c_int = 43;
pub const IFLA_EVENT: c_int = 44;
pub const IFLA_NEW_NETNSID: c_int = 45;
pub const IFLA_IF_NETNSID: c_int = 46;
pub const IFLA_TARGET_NETNSID: c_int = 46;
pub const IFLA_CARRIER_UP_COUNT: c_int = 47;
pub const IFLA_CARRIER_DOWN_COUNT: c_int = 48;
pub const IFLA_NEW_IFINDEX: c_int = 49;
pub const IFLA_MIN_MTU: c_int = 50;
pub const IFLA_MAX_MTU: c_int = 51;
pub const IFLA_PROP_LIST: c_int = 52;
pub const IFLA_ALT_IFNAME: c_int = 53;
pub const IFLA_PERM_ADDRESS: c_int = 54;
pub const IFLA_PROTO_DOWN_REASON: c_int = 55;
pub const IFLA_PARENT_DEV_NAME: c_int = 56;
pub const IFLA_PARENT_DEV_BUS_NAME: c_int = 57;
pub const IFLA_GRO_MAX_SIZE: c_int = 58;
pub const __IFLA_MAX: c_int = 59;
const enum_unnamed_27 = c_uint;
pub const IFLA_PROTO_DOWN_REASON_UNSPEC: c_int = 0;
pub const IFLA_PROTO_DOWN_REASON_MASK: c_int = 1;
pub const IFLA_PROTO_DOWN_REASON_VALUE: c_int = 2;
pub const __IFLA_PROTO_DOWN_REASON_CNT: c_int = 3;
pub const IFLA_PROTO_DOWN_REASON_MAX: c_int = 2;
const enum_unnamed_28 = c_uint;
pub const IFLA_INET_UNSPEC: c_int = 0;
pub const IFLA_INET_CONF: c_int = 1;
pub const __IFLA_INET_MAX: c_int = 2;
const enum_unnamed_29 = c_uint;
pub const IFLA_INET6_UNSPEC: c_int = 0;
pub const IFLA_INET6_FLAGS: c_int = 1;
pub const IFLA_INET6_CONF: c_int = 2;
pub const IFLA_INET6_STATS: c_int = 3;
pub const IFLA_INET6_MCAST: c_int = 4;
pub const IFLA_INET6_CACHEINFO: c_int = 5;
pub const IFLA_INET6_ICMP6STATS: c_int = 6;
pub const IFLA_INET6_TOKEN: c_int = 7;
pub const IFLA_INET6_ADDR_GEN_MODE: c_int = 8;
pub const IFLA_INET6_RA_MTU: c_int = 9;
pub const __IFLA_INET6_MAX: c_int = 10;
const enum_unnamed_30 = c_uint;
pub const IN6_ADDR_GEN_MODE_EUI64: c_int = 0;
pub const IN6_ADDR_GEN_MODE_NONE: c_int = 1;
pub const IN6_ADDR_GEN_MODE_STABLE_PRIVACY: c_int = 2;
pub const IN6_ADDR_GEN_MODE_RANDOM: c_int = 3;
pub const enum_in6_addr_gen_mode = c_uint;
pub const IFLA_BR_UNSPEC: c_int = 0;
pub const IFLA_BR_FORWARD_DELAY: c_int = 1;
pub const IFLA_BR_HELLO_TIME: c_int = 2;
pub const IFLA_BR_MAX_AGE: c_int = 3;
pub const IFLA_BR_AGEING_TIME: c_int = 4;
pub const IFLA_BR_STP_STATE: c_int = 5;
pub const IFLA_BR_PRIORITY: c_int = 6;
pub const IFLA_BR_VLAN_FILTERING: c_int = 7;
pub const IFLA_BR_VLAN_PROTOCOL: c_int = 8;
pub const IFLA_BR_GROUP_FWD_MASK: c_int = 9;
pub const IFLA_BR_ROOT_ID: c_int = 10;
pub const IFLA_BR_BRIDGE_ID: c_int = 11;
pub const IFLA_BR_ROOT_PORT: c_int = 12;
pub const IFLA_BR_ROOT_PATH_COST: c_int = 13;
pub const IFLA_BR_TOPOLOGY_CHANGE: c_int = 14;
pub const IFLA_BR_TOPOLOGY_CHANGE_DETECTED: c_int = 15;
pub const IFLA_BR_HELLO_TIMER: c_int = 16;
pub const IFLA_BR_TCN_TIMER: c_int = 17;
pub const IFLA_BR_TOPOLOGY_CHANGE_TIMER: c_int = 18;
pub const IFLA_BR_GC_TIMER: c_int = 19;
pub const IFLA_BR_GROUP_ADDR: c_int = 20;
pub const IFLA_BR_FDB_FLUSH: c_int = 21;
pub const IFLA_BR_MCAST_ROUTER: c_int = 22;
pub const IFLA_BR_MCAST_SNOOPING: c_int = 23;
pub const IFLA_BR_MCAST_QUERY_USE_IFADDR: c_int = 24;
pub const IFLA_BR_MCAST_QUERIER: c_int = 25;
pub const IFLA_BR_MCAST_HASH_ELASTICITY: c_int = 26;
pub const IFLA_BR_MCAST_HASH_MAX: c_int = 27;
pub const IFLA_BR_MCAST_LAST_MEMBER_CNT: c_int = 28;
pub const IFLA_BR_MCAST_STARTUP_QUERY_CNT: c_int = 29;
pub const IFLA_BR_MCAST_LAST_MEMBER_INTVL: c_int = 30;
pub const IFLA_BR_MCAST_MEMBERSHIP_INTVL: c_int = 31;
pub const IFLA_BR_MCAST_QUERIER_INTVL: c_int = 32;
pub const IFLA_BR_MCAST_QUERY_INTVL: c_int = 33;
pub const IFLA_BR_MCAST_QUERY_RESPONSE_INTVL: c_int = 34;
pub const IFLA_BR_MCAST_STARTUP_QUERY_INTVL: c_int = 35;
pub const IFLA_BR_NF_CALL_IPTABLES: c_int = 36;
pub const IFLA_BR_NF_CALL_IP6TABLES: c_int = 37;
pub const IFLA_BR_NF_CALL_ARPTABLES: c_int = 38;
pub const IFLA_BR_VLAN_DEFAULT_PVID: c_int = 39;
pub const IFLA_BR_PAD: c_int = 40;
pub const IFLA_BR_VLAN_STATS_ENABLED: c_int = 41;
pub const IFLA_BR_MCAST_STATS_ENABLED: c_int = 42;
pub const IFLA_BR_MCAST_IGMP_VERSION: c_int = 43;
pub const IFLA_BR_MCAST_MLD_VERSION: c_int = 44;
pub const IFLA_BR_VLAN_STATS_PER_PORT: c_int = 45;
pub const IFLA_BR_MULTI_BOOLOPT: c_int = 46;
pub const IFLA_BR_MCAST_QUERIER_STATE: c_int = 47;
pub const __IFLA_BR_MAX: c_int = 48;
const enum_unnamed_31 = c_uint;
pub const struct_ifla_bridge_id = extern struct {
    prio: [2]__u8,
    addr: [6]__u8,
};
pub const BRIDGE_MODE_UNSPEC: c_int = 0;
pub const BRIDGE_MODE_HAIRPIN: c_int = 1;
const enum_unnamed_32 = c_uint;
pub const IFLA_BRPORT_UNSPEC: c_int = 0;
pub const IFLA_BRPORT_STATE: c_int = 1;
pub const IFLA_BRPORT_PRIORITY: c_int = 2;
pub const IFLA_BRPORT_COST: c_int = 3;
pub const IFLA_BRPORT_MODE: c_int = 4;
pub const IFLA_BRPORT_GUARD: c_int = 5;
pub const IFLA_BRPORT_PROTECT: c_int = 6;
pub const IFLA_BRPORT_FAST_LEAVE: c_int = 7;
pub const IFLA_BRPORT_LEARNING: c_int = 8;
pub const IFLA_BRPORT_UNICAST_FLOOD: c_int = 9;
pub const IFLA_BRPORT_PROXYARP: c_int = 10;
pub const IFLA_BRPORT_LEARNING_SYNC: c_int = 11;
pub const IFLA_BRPORT_PROXYARP_WIFI: c_int = 12;
pub const IFLA_BRPORT_ROOT_ID: c_int = 13;
pub const IFLA_BRPORT_BRIDGE_ID: c_int = 14;
pub const IFLA_BRPORT_DESIGNATED_PORT: c_int = 15;
pub const IFLA_BRPORT_DESIGNATED_COST: c_int = 16;
pub const IFLA_BRPORT_ID: c_int = 17;
pub const IFLA_BRPORT_NO: c_int = 18;
pub const IFLA_BRPORT_TOPOLOGY_CHANGE_ACK: c_int = 19;
pub const IFLA_BRPORT_CONFIG_PENDING: c_int = 20;
pub const IFLA_BRPORT_MESSAGE_AGE_TIMER: c_int = 21;
pub const IFLA_BRPORT_FORWARD_DELAY_TIMER: c_int = 22;
pub const IFLA_BRPORT_HOLD_TIMER: c_int = 23;
pub const IFLA_BRPORT_FLUSH: c_int = 24;
pub const IFLA_BRPORT_MULTICAST_ROUTER: c_int = 25;
pub const IFLA_BRPORT_PAD: c_int = 26;
pub const IFLA_BRPORT_MCAST_FLOOD: c_int = 27;
pub const IFLA_BRPORT_MCAST_TO_UCAST: c_int = 28;
pub const IFLA_BRPORT_VLAN_TUNNEL: c_int = 29;
pub const IFLA_BRPORT_BCAST_FLOOD: c_int = 30;
pub const IFLA_BRPORT_GROUP_FWD_MASK: c_int = 31;
pub const IFLA_BRPORT_NEIGH_SUPPRESS: c_int = 32;
pub const IFLA_BRPORT_ISOLATED: c_int = 33;
pub const IFLA_BRPORT_BACKUP_PORT: c_int = 34;
pub const IFLA_BRPORT_MRP_RING_OPEN: c_int = 35;
pub const IFLA_BRPORT_MRP_IN_OPEN: c_int = 36;
pub const IFLA_BRPORT_MCAST_EHT_HOSTS_LIMIT: c_int = 37;
pub const IFLA_BRPORT_MCAST_EHT_HOSTS_CNT: c_int = 38;
pub const IFLA_BRPORT_LOCKED: c_int = 39;
pub const __IFLA_BRPORT_MAX: c_int = 40;
const enum_unnamed_33 = c_uint;
pub const struct_ifla_cacheinfo = extern struct {
    max_reasm_len: __u32,
    tstamp: __u32,
    reachable_time: __u32,
    retrans_time: __u32,
};
pub const IFLA_INFO_UNSPEC: c_int = 0;
pub const IFLA_INFO_KIND: c_int = 1;
pub const IFLA_INFO_DATA: c_int = 2;
pub const IFLA_INFO_XSTATS: c_int = 3;
pub const IFLA_INFO_SLAVE_KIND: c_int = 4;
pub const IFLA_INFO_SLAVE_DATA: c_int = 5;
pub const __IFLA_INFO_MAX: c_int = 6;
const enum_unnamed_34 = c_uint;
pub const IFLA_VLAN_UNSPEC: c_int = 0;
pub const IFLA_VLAN_ID: c_int = 1;
pub const IFLA_VLAN_FLAGS: c_int = 2;
pub const IFLA_VLAN_EGRESS_QOS: c_int = 3;
pub const IFLA_VLAN_INGRESS_QOS: c_int = 4;
pub const IFLA_VLAN_PROTOCOL: c_int = 5;
pub const __IFLA_VLAN_MAX: c_int = 6;
const enum_unnamed_35 = c_uint;
pub const struct_ifla_vlan_flags = extern struct {
    flags: __u32,
    mask: __u32,
};
pub const IFLA_VLAN_QOS_UNSPEC: c_int = 0;
pub const IFLA_VLAN_QOS_MAPPING: c_int = 1;
pub const __IFLA_VLAN_QOS_MAX: c_int = 2;
const enum_unnamed_36 = c_uint;
pub const struct_ifla_vlan_qos_mapping = extern struct {
    from: __u32,
    to: __u32,
};
pub const IFLA_MACVLAN_UNSPEC: c_int = 0;
pub const IFLA_MACVLAN_MODE: c_int = 1;
pub const IFLA_MACVLAN_FLAGS: c_int = 2;
pub const IFLA_MACVLAN_MACADDR_MODE: c_int = 3;
pub const IFLA_MACVLAN_MACADDR: c_int = 4;
pub const IFLA_MACVLAN_MACADDR_DATA: c_int = 5;
pub const IFLA_MACVLAN_MACADDR_COUNT: c_int = 6;
pub const IFLA_MACVLAN_BC_QUEUE_LEN: c_int = 7;
pub const IFLA_MACVLAN_BC_QUEUE_LEN_USED: c_int = 8;
pub const __IFLA_MACVLAN_MAX: c_int = 9;
const enum_unnamed_37 = c_uint;
pub const MACVLAN_MODE_PRIVATE: c_int = 1;
pub const MACVLAN_MODE_VEPA: c_int = 2;
pub const MACVLAN_MODE_BRIDGE: c_int = 4;
pub const MACVLAN_MODE_PASSTHRU: c_int = 8;
pub const MACVLAN_MODE_SOURCE: c_int = 16;
pub const enum_macvlan_mode = c_uint;
pub const MACVLAN_MACADDR_ADD: c_int = 0;
pub const MACVLAN_MACADDR_DEL: c_int = 1;
pub const MACVLAN_MACADDR_FLUSH: c_int = 2;
pub const MACVLAN_MACADDR_SET: c_int = 3;
pub const enum_macvlan_macaddr_mode = c_uint;
pub const IFLA_VRF_UNSPEC: c_int = 0;
pub const IFLA_VRF_TABLE: c_int = 1;
pub const __IFLA_VRF_MAX: c_int = 2;
const enum_unnamed_38 = c_uint;
pub const IFLA_VRF_PORT_UNSPEC: c_int = 0;
pub const IFLA_VRF_PORT_TABLE: c_int = 1;
pub const __IFLA_VRF_PORT_MAX: c_int = 2;
const enum_unnamed_39 = c_uint;
pub const IFLA_MACSEC_UNSPEC: c_int = 0;
pub const IFLA_MACSEC_SCI: c_int = 1;
pub const IFLA_MACSEC_PORT: c_int = 2;
pub const IFLA_MACSEC_ICV_LEN: c_int = 3;
pub const IFLA_MACSEC_CIPHER_SUITE: c_int = 4;
pub const IFLA_MACSEC_WINDOW: c_int = 5;
pub const IFLA_MACSEC_ENCODING_SA: c_int = 6;
pub const IFLA_MACSEC_ENCRYPT: c_int = 7;
pub const IFLA_MACSEC_PROTECT: c_int = 8;
pub const IFLA_MACSEC_INC_SCI: c_int = 9;
pub const IFLA_MACSEC_ES: c_int = 10;
pub const IFLA_MACSEC_SCB: c_int = 11;
pub const IFLA_MACSEC_REPLAY_PROTECT: c_int = 12;
pub const IFLA_MACSEC_VALIDATION: c_int = 13;
pub const IFLA_MACSEC_PAD: c_int = 14;
pub const IFLA_MACSEC_OFFLOAD: c_int = 15;
pub const __IFLA_MACSEC_MAX: c_int = 16;
const enum_unnamed_40 = c_uint;
pub const IFLA_XFRM_UNSPEC: c_int = 0;
pub const IFLA_XFRM_LINK: c_int = 1;
pub const IFLA_XFRM_IF_ID: c_int = 2;
pub const __IFLA_XFRM_MAX: c_int = 3;
const enum_unnamed_41 = c_uint;
pub const MACSEC_VALIDATE_DISABLED: c_int = 0;
pub const MACSEC_VALIDATE_CHECK: c_int = 1;
pub const MACSEC_VALIDATE_STRICT: c_int = 2;
pub const __MACSEC_VALIDATE_END: c_int = 3;
pub const MACSEC_VALIDATE_MAX: c_int = 2;
pub const enum_macsec_validation_type = c_uint;
pub const MACSEC_OFFLOAD_OFF: c_int = 0;
pub const MACSEC_OFFLOAD_PHY: c_int = 1;
pub const MACSEC_OFFLOAD_MAC: c_int = 2;
pub const __MACSEC_OFFLOAD_END: c_int = 3;
pub const MACSEC_OFFLOAD_MAX: c_int = 2;
pub const enum_macsec_offload = c_uint;
pub const IFLA_IPVLAN_UNSPEC: c_int = 0;
pub const IFLA_IPVLAN_MODE: c_int = 1;
pub const IFLA_IPVLAN_FLAGS: c_int = 2;
pub const __IFLA_IPVLAN_MAX: c_int = 3;
const enum_unnamed_42 = c_uint;
pub const IPVLAN_MODE_L2: c_int = 0;
pub const IPVLAN_MODE_L3: c_int = 1;
pub const IPVLAN_MODE_L3S: c_int = 2;
pub const IPVLAN_MODE_MAX: c_int = 3;
pub const enum_ipvlan_mode = c_uint;
pub const struct_tunnel_msg = extern struct {
    family: __u8,
    flags: __u8,
    reserved2: __u16,
    ifindex: __u32,
};
pub const VNIFILTER_ENTRY_STATS_UNSPEC: c_int = 0;
pub const VNIFILTER_ENTRY_STATS_RX_BYTES: c_int = 1;
pub const VNIFILTER_ENTRY_STATS_RX_PKTS: c_int = 2;
pub const VNIFILTER_ENTRY_STATS_RX_DROPS: c_int = 3;
pub const VNIFILTER_ENTRY_STATS_RX_ERRORS: c_int = 4;
pub const VNIFILTER_ENTRY_STATS_TX_BYTES: c_int = 5;
pub const VNIFILTER_ENTRY_STATS_TX_PKTS: c_int = 6;
pub const VNIFILTER_ENTRY_STATS_TX_DROPS: c_int = 7;
pub const VNIFILTER_ENTRY_STATS_TX_ERRORS: c_int = 8;
pub const VNIFILTER_ENTRY_STATS_PAD: c_int = 9;
pub const __VNIFILTER_ENTRY_STATS_MAX: c_int = 10;
const enum_unnamed_43 = c_uint;
pub const VXLAN_VNIFILTER_ENTRY_UNSPEC: c_int = 0;
pub const VXLAN_VNIFILTER_ENTRY_START: c_int = 1;
pub const VXLAN_VNIFILTER_ENTRY_END: c_int = 2;
pub const VXLAN_VNIFILTER_ENTRY_GROUP: c_int = 3;
pub const VXLAN_VNIFILTER_ENTRY_GROUP6: c_int = 4;
pub const VXLAN_VNIFILTER_ENTRY_STATS: c_int = 5;
pub const __VXLAN_VNIFILTER_ENTRY_MAX: c_int = 6;
const enum_unnamed_44 = c_uint;
pub const VXLAN_VNIFILTER_UNSPEC: c_int = 0;
pub const VXLAN_VNIFILTER_ENTRY: c_int = 1;
pub const __VXLAN_VNIFILTER_MAX: c_int = 2;
const enum_unnamed_45 = c_uint;
pub const IFLA_VXLAN_UNSPEC: c_int = 0;
pub const IFLA_VXLAN_ID: c_int = 1;
pub const IFLA_VXLAN_GROUP: c_int = 2;
pub const IFLA_VXLAN_LINK: c_int = 3;
pub const IFLA_VXLAN_LOCAL: c_int = 4;
pub const IFLA_VXLAN_TTL: c_int = 5;
pub const IFLA_VXLAN_TOS: c_int = 6;
pub const IFLA_VXLAN_LEARNING: c_int = 7;
pub const IFLA_VXLAN_AGEING: c_int = 8;
pub const IFLA_VXLAN_LIMIT: c_int = 9;
pub const IFLA_VXLAN_PORT_RANGE: c_int = 10;
pub const IFLA_VXLAN_PROXY: c_int = 11;
pub const IFLA_VXLAN_RSC: c_int = 12;
pub const IFLA_VXLAN_L2MISS: c_int = 13;
pub const IFLA_VXLAN_L3MISS: c_int = 14;
pub const IFLA_VXLAN_PORT: c_int = 15;
pub const IFLA_VXLAN_GROUP6: c_int = 16;
pub const IFLA_VXLAN_LOCAL6: c_int = 17;
pub const IFLA_VXLAN_UDP_CSUM: c_int = 18;
pub const IFLA_VXLAN_UDP_ZERO_CSUM6_TX: c_int = 19;
pub const IFLA_VXLAN_UDP_ZERO_CSUM6_RX: c_int = 20;
pub const IFLA_VXLAN_REMCSUM_TX: c_int = 21;
pub const IFLA_VXLAN_REMCSUM_RX: c_int = 22;
pub const IFLA_VXLAN_GBP: c_int = 23;
pub const IFLA_VXLAN_REMCSUM_NOPARTIAL: c_int = 24;
pub const IFLA_VXLAN_COLLECT_METADATA: c_int = 25;
pub const IFLA_VXLAN_LABEL: c_int = 26;
pub const IFLA_VXLAN_GPE: c_int = 27;
pub const IFLA_VXLAN_TTL_INHERIT: c_int = 28;
pub const IFLA_VXLAN_DF: c_int = 29;
pub const IFLA_VXLAN_VNIFILTER: c_int = 30;
pub const __IFLA_VXLAN_MAX: c_int = 31;
const enum_unnamed_46 = c_uint;
pub const struct_ifla_vxlan_port_range = extern struct {
    low: __be16,
    high: __be16,
};
pub const VXLAN_DF_UNSET: c_int = 0;
pub const VXLAN_DF_SET: c_int = 1;
pub const VXLAN_DF_INHERIT: c_int = 2;
pub const __VXLAN_DF_END: c_int = 3;
pub const VXLAN_DF_MAX: c_int = 2;
pub const enum_ifla_vxlan_df = c_uint;
pub const IFLA_GENEVE_UNSPEC: c_int = 0;
pub const IFLA_GENEVE_ID: c_int = 1;
pub const IFLA_GENEVE_REMOTE: c_int = 2;
pub const IFLA_GENEVE_TTL: c_int = 3;
pub const IFLA_GENEVE_TOS: c_int = 4;
pub const IFLA_GENEVE_PORT: c_int = 5;
pub const IFLA_GENEVE_COLLECT_METADATA: c_int = 6;
pub const IFLA_GENEVE_REMOTE6: c_int = 7;
pub const IFLA_GENEVE_UDP_CSUM: c_int = 8;
pub const IFLA_GENEVE_UDP_ZERO_CSUM6_TX: c_int = 9;
pub const IFLA_GENEVE_UDP_ZERO_CSUM6_RX: c_int = 10;
pub const IFLA_GENEVE_LABEL: c_int = 11;
pub const IFLA_GENEVE_TTL_INHERIT: c_int = 12;
pub const IFLA_GENEVE_DF: c_int = 13;
pub const IFLA_GENEVE_INNER_PROTO_INHERIT: c_int = 14;
pub const __IFLA_GENEVE_MAX: c_int = 15;
const enum_unnamed_47 = c_uint;
pub const GENEVE_DF_UNSET: c_int = 0;
pub const GENEVE_DF_SET: c_int = 1;
pub const GENEVE_DF_INHERIT: c_int = 2;
pub const __GENEVE_DF_END: c_int = 3;
pub const GENEVE_DF_MAX: c_int = 2;
pub const enum_ifla_geneve_df = c_uint;
pub const IFLA_BAREUDP_UNSPEC: c_int = 0;
pub const IFLA_BAREUDP_PORT: c_int = 1;
pub const IFLA_BAREUDP_ETHERTYPE: c_int = 2;
pub const IFLA_BAREUDP_SRCPORT_MIN: c_int = 3;
pub const IFLA_BAREUDP_MULTIPROTO_MODE: c_int = 4;
pub const __IFLA_BAREUDP_MAX: c_int = 5;
const enum_unnamed_48 = c_uint;
pub const IFLA_PPP_UNSPEC: c_int = 0;
pub const IFLA_PPP_DEV_FD: c_int = 1;
pub const __IFLA_PPP_MAX: c_int = 2;
const enum_unnamed_49 = c_uint;
pub const GTP_ROLE_GGSN: c_int = 0;
pub const GTP_ROLE_SGSN: c_int = 1;
pub const enum_ifla_gtp_role = c_uint;
pub const IFLA_GTP_UNSPEC: c_int = 0;
pub const IFLA_GTP_FD0: c_int = 1;
pub const IFLA_GTP_FD1: c_int = 2;
pub const IFLA_GTP_PDP_HASHSIZE: c_int = 3;
pub const IFLA_GTP_ROLE: c_int = 4;
pub const IFLA_GTP_CREATE_SOCKETS: c_int = 5;
pub const IFLA_GTP_RESTART_COUNT: c_int = 6;
pub const __IFLA_GTP_MAX: c_int = 7;
const enum_unnamed_50 = c_uint;
pub const IFLA_BOND_UNSPEC: c_int = 0;
pub const IFLA_BOND_MODE: c_int = 1;
pub const IFLA_BOND_ACTIVE_SLAVE: c_int = 2;
pub const IFLA_BOND_MIIMON: c_int = 3;
pub const IFLA_BOND_UPDELAY: c_int = 4;
pub const IFLA_BOND_DOWNDELAY: c_int = 5;
pub const IFLA_BOND_USE_CARRIER: c_int = 6;
pub const IFLA_BOND_ARP_INTERVAL: c_int = 7;
pub const IFLA_BOND_ARP_IP_TARGET: c_int = 8;
pub const IFLA_BOND_ARP_VALIDATE: c_int = 9;
pub const IFLA_BOND_ARP_ALL_TARGETS: c_int = 10;
pub const IFLA_BOND_PRIMARY: c_int = 11;
pub const IFLA_BOND_PRIMARY_RESELECT: c_int = 12;
pub const IFLA_BOND_FAIL_OVER_MAC: c_int = 13;
pub const IFLA_BOND_XMIT_HASH_POLICY: c_int = 14;
pub const IFLA_BOND_RESEND_IGMP: c_int = 15;
pub const IFLA_BOND_NUM_PEER_NOTIF: c_int = 16;
pub const IFLA_BOND_ALL_SLAVES_ACTIVE: c_int = 17;
pub const IFLA_BOND_MIN_LINKS: c_int = 18;
pub const IFLA_BOND_LP_INTERVAL: c_int = 19;
pub const IFLA_BOND_PACKETS_PER_SLAVE: c_int = 20;
pub const IFLA_BOND_AD_LACP_RATE: c_int = 21;
pub const IFLA_BOND_AD_SELECT: c_int = 22;
pub const IFLA_BOND_AD_INFO: c_int = 23;
pub const IFLA_BOND_AD_ACTOR_SYS_PRIO: c_int = 24;
pub const IFLA_BOND_AD_USER_PORT_KEY: c_int = 25;
pub const IFLA_BOND_AD_ACTOR_SYSTEM: c_int = 26;
pub const IFLA_BOND_TLB_DYNAMIC_LB: c_int = 27;
pub const IFLA_BOND_PEER_NOTIF_DELAY: c_int = 28;
pub const IFLA_BOND_AD_LACP_ACTIVE: c_int = 29;
pub const IFLA_BOND_MISSED_MAX: c_int = 30;
pub const IFLA_BOND_NS_IP6_TARGET: c_int = 31;
pub const __IFLA_BOND_MAX: c_int = 32;
const enum_unnamed_51 = c_uint;
pub const IFLA_BOND_AD_INFO_UNSPEC: c_int = 0;
pub const IFLA_BOND_AD_INFO_AGGREGATOR: c_int = 1;
pub const IFLA_BOND_AD_INFO_NUM_PORTS: c_int = 2;
pub const IFLA_BOND_AD_INFO_ACTOR_KEY: c_int = 3;
pub const IFLA_BOND_AD_INFO_PARTNER_KEY: c_int = 4;
pub const IFLA_BOND_AD_INFO_PARTNER_MAC: c_int = 5;
pub const __IFLA_BOND_AD_INFO_MAX: c_int = 6;
const enum_unnamed_52 = c_uint;
pub const IFLA_BOND_SLAVE_UNSPEC: c_int = 0;
pub const IFLA_BOND_SLAVE_STATE: c_int = 1;
pub const IFLA_BOND_SLAVE_MII_STATUS: c_int = 2;
pub const IFLA_BOND_SLAVE_LINK_FAILURE_COUNT: c_int = 3;
pub const IFLA_BOND_SLAVE_PERM_HWADDR: c_int = 4;
pub const IFLA_BOND_SLAVE_QUEUE_ID: c_int = 5;
pub const IFLA_BOND_SLAVE_AD_AGGREGATOR_ID: c_int = 6;
pub const IFLA_BOND_SLAVE_AD_ACTOR_OPER_PORT_STATE: c_int = 7;
pub const IFLA_BOND_SLAVE_AD_PARTNER_OPER_PORT_STATE: c_int = 8;
pub const __IFLA_BOND_SLAVE_MAX: c_int = 9;
const enum_unnamed_53 = c_uint;
pub const IFLA_VF_INFO_UNSPEC: c_int = 0;
pub const IFLA_VF_INFO: c_int = 1;
pub const __IFLA_VF_INFO_MAX: c_int = 2;
const enum_unnamed_54 = c_uint;
pub const IFLA_VF_UNSPEC: c_int = 0;
pub const IFLA_VF_MAC: c_int = 1;
pub const IFLA_VF_VLAN: c_int = 2;
pub const IFLA_VF_TX_RATE: c_int = 3;
pub const IFLA_VF_SPOOFCHK: c_int = 4;
pub const IFLA_VF_LINK_STATE: c_int = 5;
pub const IFLA_VF_RATE: c_int = 6;
pub const IFLA_VF_RSS_QUERY_EN: c_int = 7;
pub const IFLA_VF_STATS: c_int = 8;
pub const IFLA_VF_TRUST: c_int = 9;
pub const IFLA_VF_IB_NODE_GUID: c_int = 10;
pub const IFLA_VF_IB_PORT_GUID: c_int = 11;
pub const IFLA_VF_VLAN_LIST: c_int = 12;
pub const IFLA_VF_BROADCAST: c_int = 13;
pub const __IFLA_VF_MAX: c_int = 14;
const enum_unnamed_55 = c_uint;
pub const struct_ifla_vf_mac = extern struct {
    vf: __u32,
    mac: [32]__u8,
};
pub const struct_ifla_vf_broadcast = extern struct {
    broadcast: [32]__u8,
};
pub const struct_ifla_vf_vlan = extern struct {
    vf: __u32,
    vlan: __u32,
    qos: __u32,
};
pub const IFLA_VF_VLAN_INFO_UNSPEC: c_int = 0;
pub const IFLA_VF_VLAN_INFO: c_int = 1;
pub const __IFLA_VF_VLAN_INFO_MAX: c_int = 2;
const enum_unnamed_56 = c_uint;
pub const struct_ifla_vf_vlan_info = extern struct {
    vf: __u32,
    vlan: __u32,
    qos: __u32,
    vlan_proto: __be16,
};
pub const struct_ifla_vf_tx_rate = extern struct {
    vf: __u32,
    rate: __u32,
};
pub const struct_ifla_vf_rate = extern struct {
    vf: __u32,
    min_tx_rate: __u32,
    max_tx_rate: __u32,
};
pub const struct_ifla_vf_spoofchk = extern struct {
    vf: __u32,
    setting: __u32,
};
pub const struct_ifla_vf_guid = extern struct {
    vf: __u32,
    guid: __u64,
};
pub const IFLA_VF_LINK_STATE_AUTO: c_int = 0;
pub const IFLA_VF_LINK_STATE_ENABLE: c_int = 1;
pub const IFLA_VF_LINK_STATE_DISABLE: c_int = 2;
pub const __IFLA_VF_LINK_STATE_MAX: c_int = 3;
const enum_unnamed_57 = c_uint;
pub const struct_ifla_vf_link_state = extern struct {
    vf: __u32,
    link_state: __u32,
};
pub const struct_ifla_vf_rss_query_en = extern struct {
    vf: __u32,
    setting: __u32,
};
pub const IFLA_VF_STATS_RX_PACKETS: c_int = 0;
pub const IFLA_VF_STATS_TX_PACKETS: c_int = 1;
pub const IFLA_VF_STATS_RX_BYTES: c_int = 2;
pub const IFLA_VF_STATS_TX_BYTES: c_int = 3;
pub const IFLA_VF_STATS_BROADCAST: c_int = 4;
pub const IFLA_VF_STATS_MULTICAST: c_int = 5;
pub const IFLA_VF_STATS_PAD: c_int = 6;
pub const IFLA_VF_STATS_RX_DROPPED: c_int = 7;
pub const IFLA_VF_STATS_TX_DROPPED: c_int = 8;
pub const __IFLA_VF_STATS_MAX: c_int = 9;
const enum_unnamed_58 = c_uint;
pub const struct_ifla_vf_trust = extern struct {
    vf: __u32,
    setting: __u32,
};
pub const IFLA_VF_PORT_UNSPEC: c_int = 0;
pub const IFLA_VF_PORT: c_int = 1;
pub const __IFLA_VF_PORT_MAX: c_int = 2;
const enum_unnamed_59 = c_uint;
pub const IFLA_PORT_UNSPEC: c_int = 0;
pub const IFLA_PORT_VF: c_int = 1;
pub const IFLA_PORT_PROFILE: c_int = 2;
pub const IFLA_PORT_VSI_TYPE: c_int = 3;
pub const IFLA_PORT_INSTANCE_UUID: c_int = 4;
pub const IFLA_PORT_HOST_UUID: c_int = 5;
pub const IFLA_PORT_REQUEST: c_int = 6;
pub const IFLA_PORT_RESPONSE: c_int = 7;
pub const __IFLA_PORT_MAX: c_int = 8;
const enum_unnamed_60 = c_uint;
pub const PORT_REQUEST_PREASSOCIATE: c_int = 0;
pub const PORT_REQUEST_PREASSOCIATE_RR: c_int = 1;
pub const PORT_REQUEST_ASSOCIATE: c_int = 2;
pub const PORT_REQUEST_DISASSOCIATE: c_int = 3;
const enum_unnamed_61 = c_uint;
pub const PORT_VDP_RESPONSE_SUCCESS: c_int = 0;
pub const PORT_VDP_RESPONSE_INVALID_FORMAT: c_int = 1;
pub const PORT_VDP_RESPONSE_INSUFFICIENT_RESOURCES: c_int = 2;
pub const PORT_VDP_RESPONSE_UNUSED_VTID: c_int = 3;
pub const PORT_VDP_RESPONSE_VTID_VIOLATION: c_int = 4;
pub const PORT_VDP_RESPONSE_VTID_VERSION_VIOALTION: c_int = 5;
pub const PORT_VDP_RESPONSE_OUT_OF_SYNC: c_int = 6;
pub const PORT_PROFILE_RESPONSE_SUCCESS: c_int = 256;
pub const PORT_PROFILE_RESPONSE_INPROGRESS: c_int = 257;
pub const PORT_PROFILE_RESPONSE_INVALID: c_int = 258;
pub const PORT_PROFILE_RESPONSE_BADSTATE: c_int = 259;
pub const PORT_PROFILE_RESPONSE_INSUFFICIENT_RESOURCES: c_int = 260;
pub const PORT_PROFILE_RESPONSE_ERROR: c_int = 261;
const enum_unnamed_62 = c_uint;
pub const struct_ifla_port_vsi = extern struct {
    vsi_mgr_id: __u8,
    vsi_type_id: [3]__u8,
    vsi_type_version: __u8,
    pad: [3]__u8,
};
pub const IFLA_IPOIB_UNSPEC: c_int = 0;
pub const IFLA_IPOIB_PKEY: c_int = 1;
pub const IFLA_IPOIB_MODE: c_int = 2;
pub const IFLA_IPOIB_UMCAST: c_int = 3;
pub const __IFLA_IPOIB_MAX: c_int = 4;
const enum_unnamed_63 = c_uint;
pub const IPOIB_MODE_DATAGRAM: c_int = 0;
pub const IPOIB_MODE_CONNECTED: c_int = 1;
const enum_unnamed_64 = c_uint;
pub const HSR_PROTOCOL_HSR: c_int = 0;
pub const HSR_PROTOCOL_PRP: c_int = 1;
pub const HSR_PROTOCOL_MAX: c_int = 2;
const enum_unnamed_65 = c_uint;
pub const IFLA_HSR_UNSPEC: c_int = 0;
pub const IFLA_HSR_SLAVE1: c_int = 1;
pub const IFLA_HSR_SLAVE2: c_int = 2;
pub const IFLA_HSR_MULTICAST_SPEC: c_int = 3;
pub const IFLA_HSR_SUPERVISION_ADDR: c_int = 4;
pub const IFLA_HSR_SEQ_NR: c_int = 5;
pub const IFLA_HSR_VERSION: c_int = 6;
pub const IFLA_HSR_PROTOCOL: c_int = 7;
pub const __IFLA_HSR_MAX: c_int = 8;
const enum_unnamed_66 = c_uint;
pub const struct_if_stats_msg = extern struct {
    family: __u8,
    pad1: __u8,
    pad2: __u16,
    ifindex: __u32,
    filter_mask: __u32,
};
pub const IFLA_STATS_UNSPEC: c_int = 0;
pub const IFLA_STATS_LINK_64: c_int = 1;
pub const IFLA_STATS_LINK_XSTATS: c_int = 2;
pub const IFLA_STATS_LINK_XSTATS_SLAVE: c_int = 3;
pub const IFLA_STATS_LINK_OFFLOAD_XSTATS: c_int = 4;
pub const IFLA_STATS_AF_SPEC: c_int = 5;
pub const __IFLA_STATS_MAX: c_int = 6;
const enum_unnamed_67 = c_uint;
pub const IFLA_STATS_GETSET_UNSPEC: c_int = 0;
pub const IFLA_STATS_GET_FILTERS: c_int = 1;
pub const IFLA_STATS_SET_OFFLOAD_XSTATS_L3_STATS: c_int = 2;
pub const __IFLA_STATS_GETSET_MAX: c_int = 3;
const enum_unnamed_68 = c_uint;
pub const LINK_XSTATS_TYPE_UNSPEC: c_int = 0;
pub const LINK_XSTATS_TYPE_BRIDGE: c_int = 1;
pub const LINK_XSTATS_TYPE_BOND: c_int = 2;
pub const __LINK_XSTATS_TYPE_MAX: c_int = 3;
const enum_unnamed_69 = c_uint;
pub const IFLA_OFFLOAD_XSTATS_UNSPEC: c_int = 0;
pub const IFLA_OFFLOAD_XSTATS_CPU_HIT: c_int = 1;
pub const IFLA_OFFLOAD_XSTATS_HW_S_INFO: c_int = 2;
pub const IFLA_OFFLOAD_XSTATS_L3_STATS: c_int = 3;
pub const __IFLA_OFFLOAD_XSTATS_MAX: c_int = 4;
const enum_unnamed_70 = c_uint;
pub const IFLA_OFFLOAD_XSTATS_HW_S_INFO_UNSPEC: c_int = 0;
pub const IFLA_OFFLOAD_XSTATS_HW_S_INFO_REQUEST: c_int = 1;
pub const IFLA_OFFLOAD_XSTATS_HW_S_INFO_USED: c_int = 2;
pub const __IFLA_OFFLOAD_XSTATS_HW_S_INFO_MAX: c_int = 3;
const enum_unnamed_71 = c_uint;
pub const XDP_ATTACHED_NONE: c_int = 0;
pub const XDP_ATTACHED_DRV: c_int = 1;
pub const XDP_ATTACHED_SKB: c_int = 2;
pub const XDP_ATTACHED_HW: c_int = 3;
pub const XDP_ATTACHED_MULTI: c_int = 4;
const enum_unnamed_72 = c_uint;
pub const IFLA_XDP_UNSPEC: c_int = 0;
pub const IFLA_XDP_FD: c_int = 1;
pub const IFLA_XDP_ATTACHED: c_int = 2;
pub const IFLA_XDP_FLAGS: c_int = 3;
pub const IFLA_XDP_PROG_ID: c_int = 4;
pub const IFLA_XDP_DRV_PROG_ID: c_int = 5;
pub const IFLA_XDP_SKB_PROG_ID: c_int = 6;
pub const IFLA_XDP_HW_PROG_ID: c_int = 7;
pub const IFLA_XDP_EXPECTED_FD: c_int = 8;
pub const __IFLA_XDP_MAX: c_int = 9;
const enum_unnamed_73 = c_uint;
pub const IFLA_EVENT_NONE: c_int = 0;
pub const IFLA_EVENT_REBOOT: c_int = 1;
pub const IFLA_EVENT_FEATURES: c_int = 2;
pub const IFLA_EVENT_BONDING_FAILOVER: c_int = 3;
pub const IFLA_EVENT_NOTIFY_PEERS: c_int = 4;
pub const IFLA_EVENT_IGMP_RESEND: c_int = 5;
pub const IFLA_EVENT_BONDING_OPTIONS: c_int = 6;
const enum_unnamed_74 = c_uint;
pub const IFLA_TUN_UNSPEC: c_int = 0;
pub const IFLA_TUN_OWNER: c_int = 1;
pub const IFLA_TUN_GROUP: c_int = 2;
pub const IFLA_TUN_TYPE: c_int = 3;
pub const IFLA_TUN_PI: c_int = 4;
pub const IFLA_TUN_VNET_HDR: c_int = 5;
pub const IFLA_TUN_PERSIST: c_int = 6;
pub const IFLA_TUN_MULTI_QUEUE: c_int = 7;
pub const IFLA_TUN_NUM_QUEUES: c_int = 8;
pub const IFLA_TUN_NUM_DISABLED_QUEUES: c_int = 9;
pub const __IFLA_TUN_MAX: c_int = 10;
const enum_unnamed_75 = c_uint;
pub const IFLA_RMNET_UNSPEC: c_int = 0;
pub const IFLA_RMNET_MUX_ID: c_int = 1;
pub const IFLA_RMNET_FLAGS: c_int = 2;
pub const __IFLA_RMNET_MAX: c_int = 3;
const enum_unnamed_76 = c_uint;
pub const struct_ifla_rmnet_flags = extern struct {
    flags: __u32,
    mask: __u32,
};
pub const IFLA_MCTP_UNSPEC: c_int = 0;
pub const IFLA_MCTP_NET: c_int = 1;
pub const __IFLA_MCTP_MAX: c_int = 2;
const enum_unnamed_77 = c_uint;
pub const struct_ifaddrmsg = extern struct {
    ifa_family: __u8,
    ifa_prefixlen: __u8,
    ifa_flags: __u8,
    ifa_scope: __u8,
    ifa_index: __u32,
};
pub const IFA_UNSPEC: c_int = 0;
pub const IFA_ADDRESS: c_int = 1;
pub const IFA_LOCAL: c_int = 2;
pub const IFA_LABEL: c_int = 3;
pub const IFA_BROADCAST: c_int = 4;
pub const IFA_ANYCAST: c_int = 5;
pub const IFA_CACHEINFO: c_int = 6;
pub const IFA_MULTICAST: c_int = 7;
pub const IFA_FLAGS: c_int = 8;
pub const IFA_RT_PRIORITY: c_int = 9;
pub const IFA_TARGET_NETNSID: c_int = 10;
pub const IFA_PROTO: c_int = 11;
pub const __IFA_MAX: c_int = 12;
const enum_unnamed_78 = c_uint;
pub const struct_ifa_cacheinfo = extern struct {
    ifa_prefered: __u32,
    ifa_valid: __u32,
    cstamp: __u32,
    tstamp: __u32,
};
pub const struct_ndmsg = extern struct {
    ndm_family: __u8,
    ndm_pad1: __u8,
    ndm_pad2: __u16,
    ndm_ifindex: __s32,
    ndm_state: __u16,
    ndm_flags: __u8,
    ndm_type: __u8,
};
pub const NDA_UNSPEC: c_int = 0;
pub const NDA_DST: c_int = 1;
pub const NDA_LLADDR: c_int = 2;
pub const NDA_CACHEINFO: c_int = 3;
pub const NDA_PROBES: c_int = 4;
pub const NDA_VLAN: c_int = 5;
pub const NDA_PORT: c_int = 6;
pub const NDA_VNI: c_int = 7;
pub const NDA_IFINDEX: c_int = 8;
pub const NDA_MASTER: c_int = 9;
pub const NDA_LINK_NETNSID: c_int = 10;
pub const NDA_SRC_VNI: c_int = 11;
pub const NDA_PROTOCOL: c_int = 12;
pub const NDA_NH_ID: c_int = 13;
pub const NDA_FDB_EXT_ATTRS: c_int = 14;
pub const NDA_FLAGS_EXT: c_int = 15;
pub const __NDA_MAX: c_int = 16;
const enum_unnamed_79 = c_uint;
pub const struct_nda_cacheinfo = extern struct {
    ndm_confirmed: __u32,
    ndm_used: __u32,
    ndm_updated: __u32,
    ndm_refcnt: __u32,
};
pub const struct_ndt_stats = extern struct {
    ndts_allocs: __u64,
    ndts_destroys: __u64,
    ndts_hash_grows: __u64,
    ndts_res_failed: __u64,
    ndts_lookups: __u64,
    ndts_hits: __u64,
    ndts_rcv_probes_mcast: __u64,
    ndts_rcv_probes_ucast: __u64,
    ndts_periodic_gc_runs: __u64,
    ndts_forced_gc_runs: __u64,
    ndts_table_fulls: __u64,
};
pub const NDTPA_UNSPEC: c_int = 0;
pub const NDTPA_IFINDEX: c_int = 1;
pub const NDTPA_REFCNT: c_int = 2;
pub const NDTPA_REACHABLE_TIME: c_int = 3;
pub const NDTPA_BASE_REACHABLE_TIME: c_int = 4;
pub const NDTPA_RETRANS_TIME: c_int = 5;
pub const NDTPA_GC_STALETIME: c_int = 6;
pub const NDTPA_DELAY_PROBE_TIME: c_int = 7;
pub const NDTPA_QUEUE_LEN: c_int = 8;
pub const NDTPA_APP_PROBES: c_int = 9;
pub const NDTPA_UCAST_PROBES: c_int = 10;
pub const NDTPA_MCAST_PROBES: c_int = 11;
pub const NDTPA_ANYCAST_DELAY: c_int = 12;
pub const NDTPA_PROXY_DELAY: c_int = 13;
pub const NDTPA_PROXY_QLEN: c_int = 14;
pub const NDTPA_LOCKTIME: c_int = 15;
pub const NDTPA_QUEUE_LENBYTES: c_int = 16;
pub const NDTPA_MCAST_REPROBES: c_int = 17;
pub const NDTPA_PAD: c_int = 18;
pub const __NDTPA_MAX: c_int = 19;
const enum_unnamed_80 = c_uint;
pub const struct_ndtmsg = extern struct {
    ndtm_family: __u8,
    ndtm_pad1: __u8,
    ndtm_pad2: __u16,
};
pub const struct_ndt_config = extern struct {
    ndtc_key_len: __u16,
    ndtc_entry_size: __u16,
    ndtc_entries: __u32,
    ndtc_last_flush: __u32,
    ndtc_last_rand: __u32,
    ndtc_hash_rnd: __u32,
    ndtc_hash_mask: __u32,
    ndtc_hash_chain_gc: __u32,
    ndtc_proxy_qlen: __u32,
};
pub const NDTA_UNSPEC: c_int = 0;
pub const NDTA_NAME: c_int = 1;
pub const NDTA_THRESH1: c_int = 2;
pub const NDTA_THRESH2: c_int = 3;
pub const NDTA_THRESH3: c_int = 4;
pub const NDTA_CONFIG: c_int = 5;
pub const NDTA_PARMS: c_int = 6;
pub const NDTA_STATS: c_int = 7;
pub const NDTA_GC_INTERVAL: c_int = 8;
pub const NDTA_PAD: c_int = 9;
pub const __NDTA_MAX: c_int = 10;
const enum_unnamed_81 = c_uint;
pub const FDB_NOTIFY_BIT: c_int = 1;
pub const FDB_NOTIFY_INACTIVE_BIT: c_int = 2;
const enum_unnamed_82 = c_uint;
pub const NFEA_UNSPEC: c_int = 0;
pub const NFEA_ACTIVITY_NOTIFY: c_int = 1;
pub const NFEA_DONT_REFRESH: c_int = 2;
pub const __NFEA_MAX: c_int = 3;
const enum_unnamed_83 = c_uint;
pub const RTM_BASE: c_int = 16;
pub const RTM_NEWLINK: c_int = 16;
pub const RTM_DELLINK: c_int = 17;
pub const RTM_GETLINK: c_int = 18;
pub const RTM_SETLINK: c_int = 19;
pub const RTM_NEWADDR: c_int = 20;
pub const RTM_DELADDR: c_int = 21;
pub const RTM_GETADDR: c_int = 22;
pub const RTM_NEWROUTE: c_int = 24;
pub const RTM_DELROUTE: c_int = 25;
pub const RTM_GETROUTE: c_int = 26;
pub const RTM_NEWNEIGH: c_int = 28;
pub const RTM_DELNEIGH: c_int = 29;
pub const RTM_GETNEIGH: c_int = 30;
pub const RTM_NEWRULE: c_int = 32;
pub const RTM_DELRULE: c_int = 33;
pub const RTM_GETRULE: c_int = 34;
pub const RTM_NEWQDISC: c_int = 36;
pub const RTM_DELQDISC: c_int = 37;
pub const RTM_GETQDISC: c_int = 38;
pub const RTM_NEWTCLASS: c_int = 40;
pub const RTM_DELTCLASS: c_int = 41;
pub const RTM_GETTCLASS: c_int = 42;
pub const RTM_NEWTFILTER: c_int = 44;
pub const RTM_DELTFILTER: c_int = 45;
pub const RTM_GETTFILTER: c_int = 46;
pub const RTM_NEWACTION: c_int = 48;
pub const RTM_DELACTION: c_int = 49;
pub const RTM_GETACTION: c_int = 50;
pub const RTM_NEWPREFIX: c_int = 52;
pub const RTM_GETMULTICAST: c_int = 58;
pub const RTM_GETANYCAST: c_int = 62;
pub const RTM_NEWNEIGHTBL: c_int = 64;
pub const RTM_GETNEIGHTBL: c_int = 66;
pub const RTM_SETNEIGHTBL: c_int = 67;
pub const RTM_NEWNDUSEROPT: c_int = 68;
pub const RTM_NEWADDRLABEL: c_int = 72;
pub const RTM_DELADDRLABEL: c_int = 73;
pub const RTM_GETADDRLABEL: c_int = 74;
pub const RTM_GETDCB: c_int = 78;
pub const RTM_SETDCB: c_int = 79;
pub const RTM_NEWNETCONF: c_int = 80;
pub const RTM_DELNETCONF: c_int = 81;
pub const RTM_GETNETCONF: c_int = 82;
pub const RTM_NEWMDB: c_int = 84;
pub const RTM_DELMDB: c_int = 85;
pub const RTM_GETMDB: c_int = 86;
pub const RTM_NEWNSID: c_int = 88;
pub const RTM_DELNSID: c_int = 89;
pub const RTM_GETNSID: c_int = 90;
pub const RTM_NEWSTATS: c_int = 92;
pub const RTM_GETSTATS: c_int = 94;
pub const RTM_SETSTATS: c_int = 95;
pub const RTM_NEWCACHEREPORT: c_int = 96;
pub const RTM_NEWCHAIN: c_int = 100;
pub const RTM_DELCHAIN: c_int = 101;
pub const RTM_GETCHAIN: c_int = 102;
pub const RTM_NEWNEXTHOP: c_int = 104;
pub const RTM_DELNEXTHOP: c_int = 105;
pub const RTM_GETNEXTHOP: c_int = 106;
pub const RTM_NEWLINKPROP: c_int = 108;
pub const RTM_DELLINKPROP: c_int = 109;
pub const RTM_GETLINKPROP: c_int = 110;
pub const RTM_NEWVLAN: c_int = 112;
pub const RTM_DELVLAN: c_int = 113;
pub const RTM_GETVLAN: c_int = 114;
pub const RTM_NEWNEXTHOPBUCKET: c_int = 116;
pub const RTM_DELNEXTHOPBUCKET: c_int = 117;
pub const RTM_GETNEXTHOPBUCKET: c_int = 118;
pub const RTM_NEWTUNNEL: c_int = 120;
pub const RTM_DELTUNNEL: c_int = 121;
pub const RTM_GETTUNNEL: c_int = 122;
pub const __RTM_MAX: c_int = 123;
const enum_unnamed_84 = c_uint;
pub const struct_rtattr = extern struct {
    rta_len: c_ushort,
    rta_type: c_ushort,
};
pub const struct_rtmsg = extern struct {
    rtm_family: u8,
    rtm_dst_len: u8,
    rtm_src_len: u8,
    rtm_tos: u8,
    rtm_table: u8,
    rtm_protocol: u8,
    rtm_scope: u8,
    rtm_type: u8,
    rtm_flags: c_uint,
};
pub const RTN_UNSPEC: c_int = 0;
pub const RTN_UNICAST: c_int = 1;
pub const RTN_LOCAL: c_int = 2;
pub const RTN_BROADCAST: c_int = 3;
pub const RTN_ANYCAST: c_int = 4;
pub const RTN_MULTICAST: c_int = 5;
pub const RTN_BLACKHOLE: c_int = 6;
pub const RTN_UNREACHABLE: c_int = 7;
pub const RTN_PROHIBIT: c_int = 8;
pub const RTN_THROW: c_int = 9;
pub const RTN_NAT: c_int = 10;
pub const RTN_XRESOLVE: c_int = 11;
pub const __RTN_MAX: c_int = 12;
const enum_unnamed_85 = c_uint;
pub const RT_SCOPE_UNIVERSE: c_int = 0;
pub const RT_SCOPE_SITE: c_int = 200;
pub const RT_SCOPE_LINK: c_int = 253;
pub const RT_SCOPE_HOST: c_int = 254;
pub const RT_SCOPE_NOWHERE: c_int = 255;
pub const enum_rt_scope_t = c_uint;
pub const RT_TABLE_UNSPEC: c_int = 0;
pub const RT_TABLE_COMPAT: c_int = 252;
pub const RT_TABLE_DEFAULT: c_int = 253;
pub const RT_TABLE_MAIN: c_int = 254;
pub const RT_TABLE_LOCAL: c_int = 255;
pub const RT_TABLE_MAX: c_uint = 4294967295;
pub const enum_rt_class_t = c_uint;
pub const RTA_UNSPEC: c_int = 0;
pub const RTA_DST: c_int = 1;
pub const RTA_SRC: c_int = 2;
pub const RTA_IIF: c_int = 3;
pub const RTA_OIF: c_int = 4;
pub const RTA_GATEWAY: c_int = 5;
pub const RTA_PRIORITY: c_int = 6;
pub const RTA_PREFSRC: c_int = 7;
pub const RTA_METRICS: c_int = 8;
pub const RTA_MULTIPATH: c_int = 9;
pub const RTA_PROTOINFO: c_int = 10;
pub const RTA_FLOW: c_int = 11;
pub const RTA_CACHEINFO: c_int = 12;
pub const RTA_SESSION: c_int = 13;
pub const RTA_MP_ALGO: c_int = 14;
pub const RTA_TABLE: c_int = 15;
pub const RTA_MARK: c_int = 16;
pub const RTA_MFC_STATS: c_int = 17;
pub const RTA_VIA: c_int = 18;
pub const RTA_NEWDST: c_int = 19;
pub const RTA_PREF: c_int = 20;
pub const RTA_ENCAP_TYPE: c_int = 21;
pub const RTA_ENCAP: c_int = 22;
pub const RTA_EXPIRES: c_int = 23;
pub const RTA_PAD: c_int = 24;
pub const RTA_UID: c_int = 25;
pub const RTA_TTL_PROPAGATE: c_int = 26;
pub const RTA_IP_PROTO: c_int = 27;
pub const RTA_SPORT: c_int = 28;
pub const RTA_DPORT: c_int = 29;
pub const RTA_NH_ID: c_int = 30;
pub const __RTA_MAX: c_int = 31;
pub const enum_rtattr_type_t = c_uint;
pub const struct_rtnexthop = extern struct {
    rtnh_len: c_ushort,
    rtnh_flags: u8,
    rtnh_hops: u8,
    rtnh_ifindex: c_int,
};
pub const struct_rtvia = extern struct {
    rtvia_family: __kernel_sa_family_t align(2),
    pub fn rtvia_addr(self: anytype) @import("std").zig.c_translation.FlexibleArrayType(@TypeOf(self), __u8) {
        const Intermediate = @import("std").zig.c_translation.FlexibleArrayType(@TypeOf(self), u8);
        const ReturnType = @import("std").zig.c_translation.FlexibleArrayType(@TypeOf(self), __u8);
        return @ptrCast(ReturnType, @alignCast(@alignOf(__u8), @ptrCast(Intermediate, self) + 2));
    }
};
pub const struct_rta_cacheinfo = extern struct {
    rta_clntref: __u32,
    rta_lastuse: __u32,
    rta_expires: __s32,
    rta_error: __u32,
    rta_used: __u32,
    rta_id: __u32,
    rta_ts: __u32,
    rta_tsage: __u32,
};
pub const RTAX_UNSPEC: c_int = 0;
pub const RTAX_LOCK: c_int = 1;
pub const RTAX_MTU: c_int = 2;
pub const RTAX_WINDOW: c_int = 3;
pub const RTAX_RTT: c_int = 4;
pub const RTAX_RTTVAR: c_int = 5;
pub const RTAX_SSTHRESH: c_int = 6;
pub const RTAX_CWND: c_int = 7;
pub const RTAX_ADVMSS: c_int = 8;
pub const RTAX_REORDERING: c_int = 9;
pub const RTAX_HOPLIMIT: c_int = 10;
pub const RTAX_INITCWND: c_int = 11;
pub const RTAX_FEATURES: c_int = 12;
pub const RTAX_RTO_MIN: c_int = 13;
pub const RTAX_INITRWND: c_int = 14;
pub const RTAX_QUICKACK: c_int = 15;
pub const RTAX_CC_ALGO: c_int = 16;
pub const RTAX_FASTOPEN_NO_COOKIE: c_int = 17;
pub const __RTAX_MAX: c_int = 18;
const enum_unnamed_86 = c_uint;
const struct_unnamed_88 = extern struct {
    sport: __u16,
    dport: __u16,
};
const struct_unnamed_89 = extern struct {
    type: __u8,
    code: __u8,
    ident: __u16,
};
const union_unnamed_87 = extern union {
    ports: struct_unnamed_88,
    icmpt: struct_unnamed_89,
    spi: __u32,
};
pub const struct_rta_session = extern struct {
    proto: __u8,
    pad1: __u8,
    pad2: __u16,
    u: union_unnamed_87,
};
pub const struct_rta_mfc_stats = extern struct {
    mfcs_packets: __u64,
    mfcs_bytes: __u64,
    mfcs_wrong_if: __u64,
};
pub const struct_rtgenmsg = extern struct {
    rtgen_family: u8,
};
pub const struct_ifinfomsg = extern struct {
    ifi_family: u8,
    __ifi_pad: u8,
    ifi_type: c_ushort,
    ifi_index: c_int,
    ifi_flags: c_uint,
    ifi_change: c_uint,
};
pub const struct_prefixmsg = extern struct {
    prefix_family: u8,
    prefix_pad1: u8,
    prefix_pad2: c_ushort,
    prefix_ifindex: c_int,
    prefix_type: u8,
    prefix_len: u8,
    prefix_flags: u8,
    prefix_pad3: u8,
};
pub const PREFIX_UNSPEC: c_int = 0;
pub const PREFIX_ADDRESS: c_int = 1;
pub const PREFIX_CACHEINFO: c_int = 2;
pub const __PREFIX_MAX: c_int = 3;
const enum_unnamed_90 = c_uint;
pub const struct_prefix_cacheinfo = extern struct {
    preferred_time: __u32,
    valid_time: __u32,
};
pub const struct_tcmsg = extern struct {
    tcm_family: u8,
    tcm__pad1: u8,
    tcm__pad2: c_ushort,
    tcm_ifindex: c_int,
    tcm_handle: __u32,
    tcm_parent: __u32,
    tcm_info: __u32,
};
pub const TCA_UNSPEC: c_int = 0;
pub const TCA_KIND: c_int = 1;
pub const TCA_OPTIONS: c_int = 2;
pub const TCA_STATS: c_int = 3;
pub const TCA_XSTATS: c_int = 4;
pub const TCA_RATE: c_int = 5;
pub const TCA_FCNT: c_int = 6;
pub const TCA_STATS2: c_int = 7;
pub const TCA_STAB: c_int = 8;
pub const TCA_PAD: c_int = 9;
pub const TCA_DUMP_INVISIBLE: c_int = 10;
pub const TCA_CHAIN: c_int = 11;
pub const TCA_HW_OFFLOAD: c_int = 12;
pub const TCA_INGRESS_BLOCK: c_int = 13;
pub const TCA_EGRESS_BLOCK: c_int = 14;
pub const TCA_DUMP_FLAGS: c_int = 15;
pub const __TCA_MAX: c_int = 16;
const enum_unnamed_91 = c_uint;
pub const struct_nduseroptmsg = extern struct {
    nduseropt_family: u8,
    nduseropt_pad1: u8,
    nduseropt_opts_len: c_ushort,
    nduseropt_ifindex: c_int,
    nduseropt_icmp_type: __u8,
    nduseropt_icmp_code: __u8,
    nduseropt_pad2: c_ushort,
    nduseropt_pad3: c_uint,
};
pub const NDUSEROPT_UNSPEC: c_int = 0;
pub const NDUSEROPT_SRCADDR: c_int = 1;
pub const __NDUSEROPT_MAX: c_int = 2;
const enum_unnamed_92 = c_uint;
pub const RTNLGRP_NONE: c_int = 0;
pub const RTNLGRP_LINK: c_int = 1;
pub const RTNLGRP_NOTIFY: c_int = 2;
pub const RTNLGRP_NEIGH: c_int = 3;
pub const RTNLGRP_TC: c_int = 4;
pub const RTNLGRP_IPV4_IFADDR: c_int = 5;
pub const RTNLGRP_IPV4_MROUTE: c_int = 6;
pub const RTNLGRP_IPV4_ROUTE: c_int = 7;
pub const RTNLGRP_IPV4_RULE: c_int = 8;
pub const RTNLGRP_IPV6_IFADDR: c_int = 9;
pub const RTNLGRP_IPV6_MROUTE: c_int = 10;
pub const RTNLGRP_IPV6_ROUTE: c_int = 11;
pub const RTNLGRP_IPV6_IFINFO: c_int = 12;
pub const RTNLGRP_DECnet_IFADDR: c_int = 13;
pub const RTNLGRP_NOP2: c_int = 14;
pub const RTNLGRP_DECnet_ROUTE: c_int = 15;
pub const RTNLGRP_DECnet_RULE: c_int = 16;
pub const RTNLGRP_NOP4: c_int = 17;
pub const RTNLGRP_IPV6_PREFIX: c_int = 18;
pub const RTNLGRP_IPV6_RULE: c_int = 19;
pub const RTNLGRP_ND_USEROPT: c_int = 20;
pub const RTNLGRP_PHONET_IFADDR: c_int = 21;
pub const RTNLGRP_PHONET_ROUTE: c_int = 22;
pub const RTNLGRP_DCB: c_int = 23;
pub const RTNLGRP_IPV4_NETCONF: c_int = 24;
pub const RTNLGRP_IPV6_NETCONF: c_int = 25;
pub const RTNLGRP_MDB: c_int = 26;
pub const RTNLGRP_MPLS_ROUTE: c_int = 27;
pub const RTNLGRP_NSID: c_int = 28;
pub const RTNLGRP_MPLS_NETCONF: c_int = 29;
pub const RTNLGRP_IPV4_MROUTE_R: c_int = 30;
pub const RTNLGRP_IPV6_MROUTE_R: c_int = 31;
pub const RTNLGRP_NEXTHOP: c_int = 32;
pub const RTNLGRP_BRVLAN: c_int = 33;
pub const RTNLGRP_MCTP_IFADDR: c_int = 34;
pub const RTNLGRP_TUNNEL: c_int = 35;
pub const RTNLGRP_STATS: c_int = 36;
pub const __RTNLGRP_MAX: c_int = 37;
pub const enum_rtnetlink_groups = c_uint;
pub const struct_tcamsg = extern struct {
    tca_family: u8,
    tca__pad1: u8,
    tca__pad2: c_ushort,
};
pub const TCA_ROOT_UNSPEC: c_int = 0;
pub const TCA_ROOT_TAB: c_int = 1;
pub const TCA_ROOT_FLAGS: c_int = 2;
pub const TCA_ROOT_COUNT: c_int = 3;
pub const TCA_ROOT_TIME_DELTA: c_int = 4;
pub const __TCA_ROOT_MAX: c_int = 5;
const enum_unnamed_93 = c_uint;
pub const struct_l_rtnl_address = opaque {};
pub const struct_l_rtnl_route = opaque {};
pub const l_rtnl_neighbor_get_cb_t = ?*const fn (c_int, [*c]const u8, usize, ?*anyopaque) callconv(.C) void;
pub extern fn l_rtnl_address_new(ip: [*c]const u8, prefix_len: u8) ?*struct_l_rtnl_address;
pub extern fn l_rtnl_address_clone(orig: ?*const struct_l_rtnl_address) ?*struct_l_rtnl_address;
pub extern fn l_rtnl_address_free(addr: ?*struct_l_rtnl_address) void;
pub inline fn l_rtnl_address_free_cleanup(arg_p: ?*anyopaque) void {
    var p = arg_p;
    l_rtnl_address_free(@ptrCast(?*struct_l_rtnl_address, @ptrCast([*c]?*anyopaque, @alignCast(@import("std").meta.alignment([*c]?*anyopaque), p)).*));
}
pub extern fn l_rtnl_address_get_address(addr: ?*const struct_l_rtnl_address, out_buf: [*c]u8) bool;
pub extern fn l_rtnl_address_get_in_addr(addr: ?*const struct_l_rtnl_address) ?*const anyopaque;
pub extern fn l_rtnl_address_get_family(addr: ?*const struct_l_rtnl_address) u8;
pub extern fn l_rtnl_address_get_prefix_length(addr: ?*const struct_l_rtnl_address) u8;
pub extern fn l_rtnl_address_get_broadcast(addr: ?*const struct_l_rtnl_address, out_buf: [*c]u8) bool;
pub extern fn l_rtnl_address_set_broadcast(addr: ?*struct_l_rtnl_address, broadcast: [*c]const u8) bool;
pub extern fn l_rtnl_address_get_label(addr: ?*const struct_l_rtnl_address) [*c]const u8;
pub extern fn l_rtnl_address_set_label(addr: ?*struct_l_rtnl_address, label: [*c]const u8) bool;
pub extern fn l_rtnl_address_set_noprefixroute(addr: ?*struct_l_rtnl_address, noprefixroute: bool) bool;
pub extern fn l_rtnl_address_get_valid_lifetime(addr: ?*const struct_l_rtnl_address) u32;
pub extern fn l_rtnl_address_get_preferred_lifetime(addr: ?*const struct_l_rtnl_address) u32;
pub extern fn l_rtnl_address_set_lifetimes(addr: ?*struct_l_rtnl_address, preferred_lifetime: u32, valid_lifetime: u32) bool;
pub extern fn l_rtnl_address_get_expiry(addr: ?*const struct_l_rtnl_address, preferred_expiry_time: [*c]u64, valid_expiry_time: [*c]u64) bool;
pub extern fn l_rtnl_address_set_expiry(addr: ?*struct_l_rtnl_address, preferred_expiry_time: u64, valid_expiry_time: u64) bool;
pub extern fn l_rtnl_address_set_scope(addr: ?*struct_l_rtnl_address, scope: u8) bool;
pub extern fn l_rtnl_route_new_gateway(gw: [*c]const u8) ?*struct_l_rtnl_route;
pub extern fn l_rtnl_route_new_prefix(ip: [*c]const u8, prefix_len: u8) ?*struct_l_rtnl_route;
pub extern fn l_rtnl_route_new_static(gw: [*c]const u8, ip: [*c]const u8, prefix_len: u8) ?*struct_l_rtnl_route;
pub extern fn l_rtnl_route_free(rt: ?*struct_l_rtnl_route) void;
pub inline fn l_rtnl_route_free_cleanup(arg_p: ?*anyopaque) void {
    var p = arg_p;
    l_rtnl_route_free(@ptrCast(?*struct_l_rtnl_route, @ptrCast([*c]?*anyopaque, @alignCast(@import("std").meta.alignment([*c]?*anyopaque), p)).*));
}
pub extern fn l_rtnl_route_get_family(rt: ?*const struct_l_rtnl_route) u8;
pub extern fn l_rtnl_route_get_gateway(rt: ?*const struct_l_rtnl_route, out_buf: [*c]u8) bool;
pub extern fn l_rtnl_route_get_gateway_in_addr(rt: ?*const struct_l_rtnl_route) ?*const anyopaque;
pub extern fn l_rtnl_route_get_dst(rt: ?*const struct_l_rtnl_route, out_buf: [*c]u8, out_prefix_len: [*c]u8) bool;
pub extern fn l_rtnl_route_get_dst_in_addr(rt: ?*const struct_l_rtnl_route, out_prefix_len: [*c]u8) ?*const anyopaque;
pub extern fn l_rtnl_route_get_lifetime(rt: ?*const struct_l_rtnl_route) u32;
pub extern fn l_rtnl_route_set_lifetime(rt: ?*struct_l_rtnl_route, lt: u32) bool;
pub extern fn l_rtnl_route_get_expiry(rt: ?*const struct_l_rtnl_route) u64;
pub extern fn l_rtnl_route_set_expiry(rt: ?*struct_l_rtnl_route, expiry_time: u64) bool;
pub extern fn l_rtnl_route_get_mtu(rt: ?*const struct_l_rtnl_route) u32;
pub extern fn l_rtnl_route_set_mtu(rt: ?*struct_l_rtnl_route, mtu: u32) bool;
pub extern fn l_rtnl_route_get_preference(rt: ?*const struct_l_rtnl_route) u8;
pub extern fn l_rtnl_route_set_preference(rt: ?*struct_l_rtnl_route, preference: u8) bool;
pub extern fn l_rtnl_route_get_prefsrc(rt: ?*const struct_l_rtnl_route, out_address: [*c]u8) bool;
pub extern fn l_rtnl_route_set_prefsrc(rt: ?*struct_l_rtnl_route, address: [*c]const u8) bool;
pub extern fn l_rtnl_route_get_priority(rt: ?*const struct_l_rtnl_route) u32;
pub extern fn l_rtnl_route_set_priority(rt: ?*struct_l_rtnl_route, priority: u32) bool;
pub extern fn l_rtnl_route_get_protocol(rt: ?*const struct_l_rtnl_route) u8;
pub extern fn l_rtnl_route_set_protocol(rt: ?*struct_l_rtnl_route, protocol: u8) bool;
pub extern fn l_rtnl_route_get_scope(rt: ?*const struct_l_rtnl_route) u8;
pub extern fn l_rtnl_route_set_scope(rt: ?*struct_l_rtnl_route, scope: u8) bool;
pub extern fn l_rtnl_set_linkmode_and_operstate(rtnl: ?*struct_l_netlink, ifindex: c_int, linkmode: u8, operstate: u8, cb: l_netlink_command_func_t, user_data: ?*anyopaque, destroy: l_netlink_destroy_func_t) u32;
pub extern fn l_rtnl_set_mac(rtnl: ?*struct_l_netlink, ifindex: c_int, addr: [*c]const u8, power_up: bool, cb: l_netlink_command_func_t, user_data: ?*anyopaque, destroy: l_netlink_destroy_func_t) u32;
pub extern fn l_rtnl_set_powered(rtnl: ?*struct_l_netlink, ifindex: c_int, powered: bool, cb: l_netlink_command_func_t, user_data: ?*anyopaque, destroy: l_netlink_destroy_func_t) u32;
pub extern fn l_rtnl_ifaddr4_extract(ifa: [*c]const struct_ifaddrmsg, bytes: c_int, label: [*c][*c]u8, ip: [*c][*c]u8, broadcast: [*c][*c]u8) void;
pub extern fn l_rtnl_ifaddr4_dump(rtnl: ?*struct_l_netlink, cb: l_netlink_command_func_t, user_data: ?*anyopaque, destroy: l_netlink_destroy_func_t) u32;
pub extern fn l_rtnl_ifaddr4_add(rtnl: ?*struct_l_netlink, ifindex: c_int, prefix_len: u8, ip: [*c]const u8, broadcast: [*c]const u8, cb: l_netlink_command_func_t, user_data: ?*anyopaque, destroy: l_netlink_destroy_func_t) u32;
pub extern fn l_rtnl_ifaddr4_delete(rtnl: ?*struct_l_netlink, ifindex: c_int, prefix_len: u8, ip: [*c]const u8, broadcast: [*c]const u8, cb: l_netlink_command_func_t, user_data: ?*anyopaque, destroy: l_netlink_destroy_func_t) u32;
pub extern fn l_rtnl_route4_extract(rtmsg: [*c]const struct_rtmsg, len: u32, table: [*c]u32, ifindex: [*c]u32, dst: [*c][*c]u8, gateway: [*c][*c]u8, src: [*c][*c]u8) void;
pub extern fn l_rtnl_route4_dump(rtnl: ?*struct_l_netlink, cb: l_netlink_command_func_t, user_data: ?*anyopaque, destroy: l_netlink_destroy_func_t) u32;
pub extern fn l_rtnl_route4_add_connected(rtnl: ?*struct_l_netlink, ifindex: c_int, dst_len: u8, dst: [*c]const u8, src: [*c]const u8, proto: u8, cb: l_netlink_command_func_t, user_data: ?*anyopaque, destroy: l_netlink_destroy_func_t) u32;
pub extern fn l_rtnl_route4_add_gateway(rtnl: ?*struct_l_netlink, ifindex: c_int, gateway: [*c]const u8, src: [*c]const u8, priority_offset: u32, proto: u8, cb: l_netlink_command_func_t, user_data: ?*anyopaque, destroy: l_netlink_destroy_func_t) u32;
pub extern fn l_rtnl_ifaddr6_extract(ifa: [*c]const struct_ifaddrmsg, len: c_int, ip: [*c][*c]u8) void;
pub extern fn l_rtnl_ifaddr6_dump(rtnl: ?*struct_l_netlink, cb: l_netlink_command_func_t, user_data: ?*anyopaque, destroy: l_netlink_destroy_func_t) u32;
pub extern fn l_rtnl_ifaddr6_add(rtnl: ?*struct_l_netlink, ifindex: c_int, prefix_len: u8, ip: [*c]const u8, cb: l_netlink_command_func_t, user_data: ?*anyopaque, destroy: l_netlink_destroy_func_t) u32;
pub extern fn l_rtnl_ifaddr6_delete(rtnl: ?*struct_l_netlink, ifindex: c_int, prefix_len: u8, ip: [*c]const u8, cb: l_netlink_command_func_t, user_data: ?*anyopaque, destroy: l_netlink_destroy_func_t) u32;
pub extern fn l_rtnl_route6_extract(rtmsg: [*c]const struct_rtmsg, len: u32, table: [*c]u32, ifindex: [*c]u32, dst: [*c][*c]u8, gateway: [*c][*c]u8, src: [*c][*c]u8) void;
pub extern fn l_rtnl_route6_dump(rtnl: ?*struct_l_netlink, cb: l_netlink_command_func_t, user_data: ?*anyopaque, destroy: l_netlink_destroy_func_t) u32;
pub extern fn l_rtnl_route6_add_gateway(rtnl: ?*struct_l_netlink, ifindex: c_int, gateway: [*c]const u8, priority_offset: u32, proto: u8, cb: l_netlink_command_func_t, user_data: ?*anyopaque, destroy: l_netlink_destroy_func_t) u32;
pub extern fn l_rtnl_route6_delete_gateway(rtnl: ?*struct_l_netlink, ifindex: c_int, gateway: [*c]const u8, priority_offset: u32, proto: u8, cb: l_netlink_command_func_t, user_data: ?*anyopaque, destroy: l_netlink_destroy_func_t) u32;
pub extern fn l_rtnl_ifaddr_extract(ifa: [*c]const struct_ifaddrmsg, bytes: c_int) ?*struct_l_rtnl_address;
pub extern fn l_rtnl_ifaddr_add(rtnl: ?*struct_l_netlink, ifindex: c_int, addr: ?*const struct_l_rtnl_address, cb: l_netlink_command_func_t, user_data: ?*anyopaque, destroy: l_netlink_destroy_func_t) u32;
pub extern fn l_rtnl_ifaddr_delete(rtnl: ?*struct_l_netlink, ifindex: c_int, addr: ?*const struct_l_rtnl_address, cb: l_netlink_command_func_t, user_data: ?*anyopaque, destroy: l_netlink_destroy_func_t) u32;
pub extern fn l_rtnl_route_add(rtnl: ?*struct_l_netlink, ifindex: c_int, rt: ?*const struct_l_rtnl_route, cb: l_netlink_command_func_t, user_data: ?*anyopaque, destroy: l_netlink_destroy_func_t) u32;
pub extern fn l_rtnl_route_delete(rtnl: ?*struct_l_netlink, ifindex: c_int, rt: ?*const struct_l_rtnl_route, cb: l_netlink_command_func_t, user_data: ?*anyopaque, destroy: l_netlink_destroy_func_t) u32;
pub extern fn l_rtnl_neighbor_get_hwaddr(rtnl: ?*struct_l_netlink, ifindex: c_int, family: c_int, ip: ?*const anyopaque, cb: l_rtnl_neighbor_get_cb_t, user_data: ?*anyopaque, destroy: l_netlink_destroy_func_t) u32;
pub extern fn l_rtnl_neighbor_set_hwaddr(rtnl: ?*struct_l_netlink, ifindex: c_int, family: c_int, ip: ?*const anyopaque, hwaddr: [*c]const u8, hwaddr_len: usize, cb: l_netlink_command_func_t, user_data: ?*anyopaque, destroy: l_netlink_destroy_func_t) u32;
pub extern fn l_rtnl_get(...) ?*struct_l_netlink;
pub const L_DBUS_SYSTEM_BUS: c_int = 0;
pub const L_DBUS_SESSION_BUS: c_int = 1;
pub const enum_l_dbus_bus = c_uint;
pub const L_DBUS_MATCH_NONE: c_int = 0;
pub const L_DBUS_MATCH_TYPE: c_int = 1;
pub const L_DBUS_MATCH_SENDER: c_int = 2;
pub const L_DBUS_MATCH_PATH: c_int = 3;
pub const L_DBUS_MATCH_INTERFACE: c_int = 4;
pub const L_DBUS_MATCH_MEMBER: c_int = 5;
pub const L_DBUS_MATCH_ARG0: c_int = 6;
pub const enum_l_dbus_match_type = c_uint;
pub const struct_l_dbus = opaque {};
pub const struct_l_dbus_interface = opaque {};
pub const struct_l_dbus_message_builder = opaque {};
pub const l_dbus_ready_func_t = ?*const fn (?*anyopaque) callconv(.C) void;
pub const l_dbus_disconnect_func_t = ?*const fn (?*anyopaque) callconv(.C) void;
pub const l_dbus_debug_func_t = ?*const fn ([*c]const u8, ?*anyopaque) callconv(.C) void;
pub const l_dbus_destroy_func_t = ?*const fn (?*anyopaque) callconv(.C) void;
pub const l_dbus_interface_setup_func_t = ?*const fn (?*struct_l_dbus_interface) callconv(.C) void;
pub const l_dbus_watch_func_t = ?*const fn (?*struct_l_dbus, ?*anyopaque) callconv(.C) void;
pub const l_dbus_name_acquire_func_t = ?*const fn (?*struct_l_dbus, bool, bool, ?*anyopaque) callconv(.C) void;
pub extern fn l_dbus_new(address: [*c]const u8) ?*struct_l_dbus;
pub extern fn l_dbus_new_default(bus: enum_l_dbus_bus) ?*struct_l_dbus;
pub extern fn l_dbus_destroy(dbus: ?*struct_l_dbus) void;
pub extern fn l_dbus_set_ready_handler(dbus: ?*struct_l_dbus, function: l_dbus_ready_func_t, user_data: ?*anyopaque, destroy: l_dbus_destroy_func_t) bool;
pub extern fn l_dbus_set_disconnect_handler(dbus: ?*struct_l_dbus, function: l_dbus_disconnect_func_t, user_data: ?*anyopaque, destroy: l_dbus_destroy_func_t) bool;
pub extern fn l_dbus_set_debug(dbus: ?*struct_l_dbus, function: l_dbus_debug_func_t, user_data: ?*anyopaque, destroy: l_dbus_destroy_func_t) bool;
pub const struct_l_dbus_message = opaque {};
pub const struct_l_dbus_message_iter = extern struct {
    message: ?*struct_l_dbus_message,
    sig_start: [*c]const u8,
    sig_len: u8,
    sig_pos: u8,
    data: ?*const anyopaque,
    len: usize,
    pos: usize,
    container_type: u8,
    offsets: ?*const anyopaque,
};
pub extern fn l_dbus_message_new_method_call(dbus: ?*struct_l_dbus, destination: [*c]const u8, path: [*c]const u8, interface: [*c]const u8, method: [*c]const u8) ?*struct_l_dbus_message;
pub extern fn l_dbus_message_new_signal(dbus: ?*struct_l_dbus, path: [*c]const u8, interface: [*c]const u8, name: [*c]const u8) ?*struct_l_dbus_message;
pub extern fn l_dbus_message_new_method_return(method_call: ?*struct_l_dbus_message) ?*struct_l_dbus_message;
pub extern fn l_dbus_message_new_error_valist(method_call: ?*struct_l_dbus_message, name: [*c]const u8, format: [*c]const u8, args: [*c]struct___va_list_tag) ?*struct_l_dbus_message;
pub extern fn l_dbus_message_new_error(method_call: ?*struct_l_dbus_message, name: [*c]const u8, format: [*c]const u8, ...) ?*struct_l_dbus_message;
pub extern fn l_dbus_message_ref(message: ?*struct_l_dbus_message) ?*struct_l_dbus_message;
pub extern fn l_dbus_message_unref(message: ?*struct_l_dbus_message) void;
pub extern fn l_dbus_message_get_path(message: ?*struct_l_dbus_message) [*c]const u8;
pub extern fn l_dbus_message_get_interface(message: ?*struct_l_dbus_message) [*c]const u8;
pub extern fn l_dbus_message_get_member(message: ?*struct_l_dbus_message) [*c]const u8;
pub extern fn l_dbus_message_get_destination(message: ?*struct_l_dbus_message) [*c]const u8;
pub extern fn l_dbus_message_get_sender(message: ?*struct_l_dbus_message) [*c]const u8;
pub extern fn l_dbus_message_get_signature(message: ?*struct_l_dbus_message) [*c]const u8;
pub extern fn l_dbus_message_set_no_reply(message: ?*struct_l_dbus_message, on: bool) bool;
pub extern fn l_dbus_message_get_no_reply(message: ?*struct_l_dbus_message) bool;
pub extern fn l_dbus_message_set_no_autostart(message: ?*struct_l_dbus_message, on: bool) bool;
pub extern fn l_dbus_message_get_no_autostart(message: ?*struct_l_dbus_message) bool;
pub const l_dbus_message_func_t = ?*const fn (?*struct_l_dbus_message, ?*anyopaque) callconv(.C) void;
pub extern fn l_dbus_send_with_reply(dbus: ?*struct_l_dbus, message: ?*struct_l_dbus_message, function: l_dbus_message_func_t, user_data: ?*anyopaque, destroy: l_dbus_destroy_func_t) u32;
pub extern fn l_dbus_send(dbus: ?*struct_l_dbus, message: ?*struct_l_dbus_message) u32;
pub extern fn l_dbus_cancel(dbus: ?*struct_l_dbus, serial: u32) bool;
pub extern fn l_dbus_register(dbus: ?*struct_l_dbus, function: l_dbus_message_func_t, user_data: ?*anyopaque, destroy: l_dbus_destroy_func_t) c_uint;
pub extern fn l_dbus_unregister(dbus: ?*struct_l_dbus, id: c_uint) bool;
pub extern fn l_dbus_method_call(dbus: ?*struct_l_dbus, destination: [*c]const u8, path: [*c]const u8, interface: [*c]const u8, method: [*c]const u8, setup: l_dbus_message_func_t, function: l_dbus_message_func_t, user_data: ?*anyopaque, destroy: l_dbus_destroy_func_t) u32;
pub extern fn l_dbus_message_is_error(message: ?*struct_l_dbus_message) bool;
pub extern fn l_dbus_message_get_error(message: ?*struct_l_dbus_message, name: [*c][*c]const u8, text: [*c][*c]const u8) bool;
pub extern fn l_dbus_message_get_arguments(message: ?*struct_l_dbus_message, signature: [*c]const u8, ...) bool;
pub extern fn l_dbus_message_get_arguments_valist(message: ?*struct_l_dbus_message, signature: [*c]const u8, args: [*c]struct___va_list_tag) bool;
pub extern fn l_dbus_message_iter_next_entry(iter: [*c]struct_l_dbus_message_iter, ...) bool;
pub extern fn l_dbus_message_iter_get_variant(iter: [*c]struct_l_dbus_message_iter, signature: [*c]const u8, ...) bool;
pub extern fn l_dbus_message_iter_get_fixed_array(iter: [*c]struct_l_dbus_message_iter, out: ?*anyopaque, n_elem: [*c]u32) bool;
pub extern fn l_dbus_message_set_arguments(message: ?*struct_l_dbus_message, signature: [*c]const u8, ...) bool;
pub extern fn l_dbus_message_set_arguments_valist(message: ?*struct_l_dbus_message, signature: [*c]const u8, args: [*c]struct___va_list_tag) bool;
pub extern fn l_dbus_message_builder_new(message: ?*struct_l_dbus_message) ?*struct_l_dbus_message_builder;
pub extern fn l_dbus_message_builder_destroy(builder: ?*struct_l_dbus_message_builder) void;
pub extern fn l_dbus_message_builder_append_basic(builder: ?*struct_l_dbus_message_builder, @"type": u8, value: ?*const anyopaque) bool;
pub extern fn l_dbus_message_builder_enter_container(builder: ?*struct_l_dbus_message_builder, container_type: u8, signature: [*c]const u8) bool;
pub extern fn l_dbus_message_builder_leave_container(builder: ?*struct_l_dbus_message_builder, container_type: u8) bool;
pub extern fn l_dbus_message_builder_enter_struct(builder: ?*struct_l_dbus_message_builder, signature: [*c]const u8) bool;
pub extern fn l_dbus_message_builder_leave_struct(builder: ?*struct_l_dbus_message_builder) bool;
pub extern fn l_dbus_message_builder_enter_dict(builder: ?*struct_l_dbus_message_builder, signature: [*c]const u8) bool;
pub extern fn l_dbus_message_builder_leave_dict(builder: ?*struct_l_dbus_message_builder) bool;
pub extern fn l_dbus_message_builder_enter_array(builder: ?*struct_l_dbus_message_builder, signature: [*c]const u8) bool;
pub extern fn l_dbus_message_builder_leave_array(builder: ?*struct_l_dbus_message_builder) bool;
pub extern fn l_dbus_message_builder_enter_variant(builder: ?*struct_l_dbus_message_builder, signature: [*c]const u8) bool;
pub extern fn l_dbus_message_builder_leave_variant(builder: ?*struct_l_dbus_message_builder) bool;
pub extern fn l_dbus_message_builder_append_from_iter(builder: ?*struct_l_dbus_message_builder, from: [*c]struct_l_dbus_message_iter) bool;
pub extern fn l_dbus_message_builder_append_from_valist(builder: ?*struct_l_dbus_message_builder, signature: [*c]const u8, args: [*c]struct___va_list_tag) bool;
pub extern fn l_dbus_message_builder_finalize(builder: ?*struct_l_dbus_message_builder) ?*struct_l_dbus_message;
pub extern fn l_dbus_register_interface(dbus: ?*struct_l_dbus, interface: [*c]const u8, setup_func: l_dbus_interface_setup_func_t, destroy: l_dbus_destroy_func_t, handle_old_style_properties: bool) bool;
pub extern fn l_dbus_unregister_interface(dbus: ?*struct_l_dbus, interface: [*c]const u8) bool;
pub extern fn l_dbus_register_object(dbus: ?*struct_l_dbus, path: [*c]const u8, user_data: ?*anyopaque, destroy: l_dbus_destroy_func_t, ...) bool;
pub extern fn l_dbus_unregister_object(dbus: ?*struct_l_dbus, object: [*c]const u8) bool;
pub extern fn l_dbus_object_add_interface(dbus: ?*struct_l_dbus, object: [*c]const u8, interface: [*c]const u8, user_data: ?*anyopaque) bool;
pub extern fn l_dbus_object_remove_interface(dbus: ?*struct_l_dbus, object: [*c]const u8, interface: [*c]const u8) bool;
pub extern fn l_dbus_object_get_data(dbus: ?*struct_l_dbus, object: [*c]const u8, interface: [*c]const u8) ?*anyopaque;
pub extern fn l_dbus_object_manager_enable(dbus: ?*struct_l_dbus, root: [*c]const u8) bool;
pub extern fn l_dbus_add_service_watch(dbus: ?*struct_l_dbus, name: [*c]const u8, connect_func: l_dbus_watch_func_t, disconnect_func: l_dbus_watch_func_t, user_data: ?*anyopaque, destroy: l_dbus_destroy_func_t) c_uint;
pub extern fn l_dbus_add_disconnect_watch(dbus: ?*struct_l_dbus, name: [*c]const u8, disconnect_func: l_dbus_watch_func_t, user_data: ?*anyopaque, destroy: l_dbus_destroy_func_t) c_uint;
pub extern fn l_dbus_remove_watch(dbus: ?*struct_l_dbus, id: c_uint) bool;
pub extern fn l_dbus_add_signal_watch(dbus: ?*struct_l_dbus, sender: [*c]const u8, path: [*c]const u8, interface: [*c]const u8, member: [*c]const u8, ...) c_uint;
pub extern fn l_dbus_remove_signal_watch(dbus: ?*struct_l_dbus, id: c_uint) bool;
pub extern fn l_dbus_name_acquire(dbus: ?*struct_l_dbus, name: [*c]const u8, allow_replacement: bool, replace_existing: bool, queue: bool, callback: l_dbus_name_acquire_func_t, user_data: ?*anyopaque) u32;
pub const L_DBUS_METHOD_FLAG_DEPRECATED: c_int = 1;
pub const L_DBUS_METHOD_FLAG_NOREPLY: c_int = 2;
pub const L_DBUS_METHOD_FLAG_ASYNC: c_int = 4;
pub const enum_l_dbus_method_flag = c_uint;
pub const L_DBUS_SIGNAL_FLAG_DEPRECATED: c_int = 1;
pub const enum_l_dbus_signal_flag = c_uint;
pub const L_DBUS_PROPERTY_FLAG_DEPRECATED: c_int = 1;
pub const L_DBUS_PROPERTY_FLAG_AUTO_EMIT: c_int = 2;
pub const enum_l_dbus_property_flag = c_uint;
pub const l_dbus_interface_method_cb_t = ?*const fn (?*struct_l_dbus, ?*struct_l_dbus_message, ?*anyopaque) callconv(.C) ?*struct_l_dbus_message;
pub const l_dbus_property_complete_cb_t = ?*const fn (?*struct_l_dbus, ?*struct_l_dbus_message, ?*struct_l_dbus_message) callconv(.C) void;
pub const l_dbus_property_set_cb_t = ?*const fn (?*struct_l_dbus, ?*struct_l_dbus_message, [*c]struct_l_dbus_message_iter, l_dbus_property_complete_cb_t, ?*anyopaque) callconv(.C) ?*struct_l_dbus_message;
pub const l_dbus_property_get_cb_t = ?*const fn (?*struct_l_dbus, ?*struct_l_dbus_message, ?*struct_l_dbus_message_builder, ?*anyopaque) callconv(.C) bool;
pub extern fn l_dbus_interface_method(interface: ?*struct_l_dbus_interface, name: [*c]const u8, flags: u32, cb: l_dbus_interface_method_cb_t, return_sig: [*c]const u8, param_sig: [*c]const u8, ...) bool;
pub extern fn l_dbus_interface_signal(interface: ?*struct_l_dbus_interface, name: [*c]const u8, flags: u32, signature: [*c]const u8, ...) bool;
pub extern fn l_dbus_interface_property(interface: ?*struct_l_dbus_interface, name: [*c]const u8, flags: u32, signature: [*c]const u8, getter: l_dbus_property_get_cb_t, setter: l_dbus_property_set_cb_t) bool;
pub extern fn l_dbus_property_changed(dbus: ?*struct_l_dbus, path: [*c]const u8, interface: [*c]const u8, property: [*c]const u8) bool;
pub const struct_l_dbus_client = opaque {};
pub const struct_l_dbus_proxy = opaque {};
pub const l_dbus_client_ready_func_t = ?*const fn (?*struct_l_dbus_client, ?*anyopaque) callconv(.C) void;
pub const l_dbus_client_proxy_func_t = ?*const fn (?*struct_l_dbus_proxy, ?*anyopaque) callconv(.C) void;
pub const l_dbus_client_proxy_result_func_t = ?*const fn (?*struct_l_dbus_proxy, ?*struct_l_dbus_message, ?*anyopaque) callconv(.C) void;
pub const l_dbus_client_property_function_t = ?*const fn (?*struct_l_dbus_proxy, [*c]const u8, ?*struct_l_dbus_message, ?*anyopaque) callconv(.C) void;
pub extern fn l_dbus_client_new(dbus: ?*struct_l_dbus, service: [*c]const u8, path: [*c]const u8) ?*struct_l_dbus_client;
pub extern fn l_dbus_client_destroy(client: ?*struct_l_dbus_client) void;
pub extern fn l_dbus_client_set_connect_handler(client: ?*struct_l_dbus_client, function: l_dbus_watch_func_t, user_data: ?*anyopaque, destroy: l_dbus_destroy_func_t) bool;
pub extern fn l_dbus_client_set_disconnect_handler(client: ?*struct_l_dbus_client, function: l_dbus_watch_func_t, user_data: ?*anyopaque, destroy: l_dbus_destroy_func_t) bool;
pub extern fn l_dbus_client_set_ready_handler(client: ?*struct_l_dbus_client, function: l_dbus_client_ready_func_t, user_data: ?*anyopaque, destroy: l_dbus_destroy_func_t) bool;
pub extern fn l_dbus_client_set_proxy_handlers(client: ?*struct_l_dbus_client, proxy_added: l_dbus_client_proxy_func_t, proxy_removed: l_dbus_client_proxy_func_t, property_changed: l_dbus_client_property_function_t, user_data: ?*anyopaque, destroy: l_dbus_destroy_func_t) bool;
pub extern fn l_dbus_proxy_get_path(proxy: ?*struct_l_dbus_proxy) [*c]const u8;
pub extern fn l_dbus_proxy_get_interface(proxy: ?*struct_l_dbus_proxy) [*c]const u8;
pub extern fn l_dbus_proxy_get_property(proxy: ?*struct_l_dbus_proxy, name: [*c]const u8, signature: [*c]const u8, ...) bool;
pub extern fn l_dbus_proxy_set_property(proxy: ?*struct_l_dbus_proxy, result: l_dbus_client_proxy_result_func_t, user_data: ?*anyopaque, destroy: l_dbus_destroy_func_t, name: [*c]const u8, signature: [*c]const u8, ...) bool;
pub extern fn l_dbus_proxy_method_call(proxy: ?*struct_l_dbus_proxy, method: [*c]const u8, setup: l_dbus_message_func_t, reply: l_dbus_client_proxy_result_func_t, user_data: ?*anyopaque, destroy: l_dbus_destroy_func_t) u32;
pub const struct_l_dhcp_client = opaque {};
pub const struct_l_dhcp_lease = opaque {};
pub const struct_l_dhcp_server = opaque {};
pub const L_DHCP_OPTION_SUBNET_MASK: c_int = 1;
pub const L_DHCP_OPTION_ROUTER: c_int = 3;
pub const L_DHCP_OPTION_DOMAIN_NAME_SERVER: c_int = 6;
pub const L_DHCP_OPTION_HOST_NAME: c_int = 12;
pub const L_DHCP_OPTION_DOMAIN_NAME: c_int = 15;
pub const L_DHCP_OPTION_BROADCAST_ADDRESS: c_int = 28;
pub const L_DHCP_OPTION_NTP_SERVERS: c_int = 42;
pub const L_DHCP_OPTION_REQUESTED_IP_ADDRESS: c_int = 50;
pub const L_DHCP_OPTION_IP_ADDRESS_LEASE_TIME: c_int = 51;
pub const L_DHCP_OPTION_RENEWAL_T1_TIME: c_int = 58;
pub const L_DHCP_OPTION_REBINDING_T2_TIME: c_int = 59;
pub const L_DHCP_OPTION_SERVER_IDENTIFIER: c_int = 54;
pub const enum_l_dhcp_option = c_uint;
pub const L_DHCP_CLIENT_EVENT_LEASE_OBTAINED: c_int = 0;
pub const L_DHCP_CLIENT_EVENT_IP_CHANGED: c_int = 1;
pub const L_DHCP_CLIENT_EVENT_LEASE_EXPIRED: c_int = 2;
pub const L_DHCP_CLIENT_EVENT_LEASE_RENEWED: c_int = 3;
pub const L_DHCP_CLIENT_EVENT_NO_LEASE: c_int = 4;
pub const enum_l_dhcp_client_event = c_uint;
pub const L_DHCP_SERVER_EVENT_NEW_LEASE: c_int = 0;
pub const L_DHCP_SERVER_EVENT_LEASE_EXPIRED: c_int = 1;
pub const enum_l_dhcp_server_event = c_uint;
pub const l_dhcp_client_event_cb_t = ?*const fn (?*struct_l_dhcp_client, enum_l_dhcp_client_event, ?*anyopaque) callconv(.C) void;
pub const l_dhcp_debug_cb_t = ?*const fn ([*c]const u8, ?*anyopaque) callconv(.C) void;
pub const l_dhcp_destroy_cb_t = ?*const fn (?*anyopaque) callconv(.C) void;
pub const l_dhcp_server_event_cb_t = ?*const fn (?*struct_l_dhcp_server, enum_l_dhcp_server_event, ?*anyopaque, ?*const struct_l_dhcp_lease) callconv(.C) void;
pub extern fn l_dhcp_client_new(ifindex: u32) ?*struct_l_dhcp_client;
pub extern fn l_dhcp_client_add_request_option(client: ?*struct_l_dhcp_client, option: u8) bool;
pub extern fn l_dhcp_client_destroy(client: ?*struct_l_dhcp_client) void;
pub extern fn l_dhcp_client_set_address(client: ?*struct_l_dhcp_client, @"type": u8, addr: [*c]const u8, addr_len: usize) bool;
pub extern fn l_dhcp_client_set_interface_name(client: ?*struct_l_dhcp_client, ifname: [*c]const u8) bool;
pub extern fn l_dhcp_client_set_hostname(client: ?*struct_l_dhcp_client, hostname: [*c]const u8) bool;
pub extern fn l_dhcp_client_set_rtnl(client: ?*struct_l_dhcp_client, rtnl: ?*struct_l_netlink) bool;
pub extern fn l_dhcp_client_get_lease(client: ?*const struct_l_dhcp_client) ?*const struct_l_dhcp_lease;
pub extern fn l_dhcp_client_start(client: ?*struct_l_dhcp_client) bool;
pub extern fn l_dhcp_client_stop(client: ?*struct_l_dhcp_client) bool;
pub extern fn l_dhcp_client_set_event_handler(client: ?*struct_l_dhcp_client, handler: l_dhcp_client_event_cb_t, userdata: ?*anyopaque, destroy: l_dhcp_destroy_cb_t) bool;
pub extern fn l_dhcp_client_set_debug(client: ?*struct_l_dhcp_client, function: l_dhcp_debug_cb_t, user_data: ?*anyopaque, destroy: l_dhcp_destroy_cb_t, priority: c_int) bool;
pub extern fn l_dhcp_lease_get_address(lease: ?*const struct_l_dhcp_lease) [*c]u8;
pub extern fn l_dhcp_lease_get_address_u32(lease: ?*const struct_l_dhcp_lease) u32;
pub extern fn l_dhcp_lease_get_gateway(lease: ?*const struct_l_dhcp_lease) [*c]u8;
pub extern fn l_dhcp_lease_get_gateway_u32(lease: ?*const struct_l_dhcp_lease) u32;
pub extern fn l_dhcp_lease_get_netmask(lease: ?*const struct_l_dhcp_lease) [*c]u8;
pub extern fn l_dhcp_lease_get_netmask_u32(lease: ?*const struct_l_dhcp_lease) u32;
pub extern fn l_dhcp_lease_get_prefix_length(lease: ?*const struct_l_dhcp_lease) u32;
pub extern fn l_dhcp_lease_get_broadcast(lease: ?*const struct_l_dhcp_lease) [*c]u8;
pub extern fn l_dhcp_lease_get_server_id(lease: ?*const struct_l_dhcp_lease) [*c]u8;
pub extern fn l_dhcp_lease_get_server_mac(lease: ?*const struct_l_dhcp_lease) [*c]const u8;
pub extern fn l_dhcp_lease_get_dns(lease: ?*const struct_l_dhcp_lease) [*c][*c]u8;
pub extern fn l_dhcp_lease_get_domain_name(lease: ?*const struct_l_dhcp_lease) [*c]u8;
pub extern fn l_dhcp_lease_get_mac(lease: ?*const struct_l_dhcp_lease) [*c]const u8;
pub extern fn l_dhcp_lease_get_t1(lease: ?*const struct_l_dhcp_lease) u32;
pub extern fn l_dhcp_lease_get_t2(lease: ?*const struct_l_dhcp_lease) u32;
pub extern fn l_dhcp_lease_get_lifetime(lease: ?*const struct_l_dhcp_lease) u32;
pub extern fn l_dhcp_lease_get_start_time(lease: ?*const struct_l_dhcp_lease) u64;
pub extern fn l_dhcp_server_new(ifindex: c_int) ?*struct_l_dhcp_server;
pub extern fn l_dhcp_server_destroy(server: ?*struct_l_dhcp_server) void;
pub extern fn l_dhcp_server_start(server: ?*struct_l_dhcp_server) bool;
pub extern fn l_dhcp_server_stop(server: ?*struct_l_dhcp_server) bool;
pub extern fn l_dhcp_server_set_ip_range(server: ?*struct_l_dhcp_server, start_ip: [*c]const u8, end_ip: [*c]const u8) bool;
pub extern fn l_dhcp_server_set_debug(server: ?*struct_l_dhcp_server, function: l_dhcp_debug_cb_t, user_data: ?*anyopaque, destory: l_dhcp_destroy_cb_t) bool;
pub extern fn l_dhcp_server_set_event_handler(server: ?*struct_l_dhcp_server, handler: l_dhcp_server_event_cb_t, user_data: ?*anyopaque, destroy: l_dhcp_destroy_cb_t) bool;
pub extern fn l_dhcp_server_set_lease_time(server: ?*struct_l_dhcp_server, lease_time: c_uint) bool;
pub extern fn l_dhcp_server_set_interface_name(server: ?*struct_l_dhcp_server, ifname: [*c]const u8) bool;
pub extern fn l_dhcp_server_set_ip_address(server: ?*struct_l_dhcp_server, ip: [*c]const u8) bool;
pub extern fn l_dhcp_server_set_netmask(server: ?*struct_l_dhcp_server, mask: [*c]const u8) bool;
pub extern fn l_dhcp_server_set_gateway(server: ?*struct_l_dhcp_server, ip: [*c]const u8) bool;
pub extern fn l_dhcp_server_set_dns(server: ?*struct_l_dhcp_server, dns: [*c][*c]u8) bool;
pub extern fn l_dhcp_server_set_authoritative(server: ?*struct_l_dhcp_server, authoritative: bool) void;
pub extern fn l_dhcp_server_set_enable_rapid_commit(server: ?*struct_l_dhcp_server, enable: bool) void;
pub extern fn l_dhcp_server_discover(server: ?*struct_l_dhcp_server, requested_ip_opt: u32, client_id: [*c]const u8, mac: [*c]const u8) ?*struct_l_dhcp_lease;
pub extern fn l_dhcp_server_request(server: ?*struct_l_dhcp_server, lease: ?*struct_l_dhcp_lease) bool;
pub extern fn l_dhcp_server_decline(server: ?*struct_l_dhcp_server, lease: ?*struct_l_dhcp_lease) bool;
pub extern fn l_dhcp_server_release(server: ?*struct_l_dhcp_server, lease: ?*struct_l_dhcp_lease) bool;
pub extern fn l_dhcp_server_lease_remove(server: ?*struct_l_dhcp_server, lease: ?*struct_l_dhcp_lease) bool;
pub extern fn l_dhcp_server_expire_by_mac(server: ?*struct_l_dhcp_server, mac: [*c]const u8) void;
pub const struct_l_dhcp6_client = opaque {};
pub const struct_l_dhcp6_lease = opaque {};
pub const struct_l_icmp6_client = opaque {};
pub const L_DHCP6_OPTION_DNS_SERVERS: c_int = 23;
pub const L_DHCP6_OPTION_DOMAIN_LIST: c_int = 24;
pub const L_DHCP6_OPTION_SNTP_SERVERS: c_int = 31;
pub const L_DHCP6_OPTION_NTP_SERVER: c_int = 56;
pub const enum_l_dhcp6_option = c_uint;
pub const L_DHCP6_CLIENT_EVENT_LEASE_OBTAINED: c_int = 0;
pub const L_DHCP6_CLIENT_EVENT_IP_CHANGED: c_int = 1;
pub const L_DHCP6_CLIENT_EVENT_LEASE_EXPIRED: c_int = 2;
pub const L_DHCP6_CLIENT_EVENT_LEASE_RENEWED: c_int = 3;
pub const L_DHCP6_CLIENT_EVENT_NO_LEASE: c_int = 4;
pub const enum_l_dhcp6_client_event = c_uint;
pub const l_dhcp6_client_event_cb_t = ?*const fn (?*struct_l_dhcp6_client, enum_l_dhcp6_client_event, ?*anyopaque) callconv(.C) void;
pub const l_dhcp6_debug_cb_t = ?*const fn ([*c]const u8, ?*anyopaque) callconv(.C) void;
pub const l_dhcp6_destroy_cb_t = ?*const fn (?*anyopaque) callconv(.C) void;
pub extern fn l_dhcp6_client_new(ifindex: u32) ?*struct_l_dhcp6_client;
pub extern fn l_dhcp6_client_destroy(client: ?*struct_l_dhcp6_client) void;
pub extern fn l_dhcp6_client_get_lease(client: ?*const struct_l_dhcp6_client) ?*const struct_l_dhcp6_lease;
pub extern fn l_dhcp6_client_set_address(client: ?*struct_l_dhcp6_client, @"type": u8, addr: [*c]const u8, addr_len: usize) bool;
pub extern fn l_dhcp6_client_set_link_local_address(client: ?*struct_l_dhcp6_client, ll: [*c]const u8) bool;
pub extern fn l_dhcp6_client_set_debug(client: ?*struct_l_dhcp6_client, function: l_dhcp6_debug_cb_t, user_data: ?*anyopaque, destroy: l_dhcp6_destroy_cb_t) bool;
pub extern fn l_dhcp6_client_set_event_handler(client: ?*struct_l_dhcp6_client, handler: l_dhcp6_client_event_cb_t, userdata: ?*anyopaque, destroy: l_dhcp6_destroy_cb_t) bool;
pub extern fn l_dhcp6_client_set_lla_randomized(client: ?*struct_l_dhcp6_client, randomized: bool) bool;
pub extern fn l_dhcp6_client_set_nodelay(client: ?*struct_l_dhcp6_client, nodelay: bool) bool;
pub extern fn l_dhcp6_client_set_nora(client: ?*struct_l_dhcp6_client, nora: bool) bool;
pub extern fn l_dhcp6_client_set_no_rapid_commit(client: ?*struct_l_dhcp6_client, no_rapid_commit: bool) bool;
pub extern fn l_dhcp6_client_set_rtnl(client: ?*struct_l_dhcp6_client, rtnl: ?*struct_l_netlink) bool;
pub extern fn l_dhcp6_client_set_stateless(client: ?*struct_l_dhcp6_client, stateless: bool) bool;
pub extern fn l_dhcp6_client_get_icmp6(client: ?*struct_l_dhcp6_client) ?*struct_l_icmp6_client;
pub extern fn l_dhcp6_client_add_request_option(client: ?*struct_l_dhcp6_client, option: enum_l_dhcp6_option) bool;
pub extern fn l_dhcp6_client_start(client: ?*struct_l_dhcp6_client) bool;
pub extern fn l_dhcp6_client_stop(client: ?*struct_l_dhcp6_client) bool;
pub extern fn l_dhcp6_lease_get_address(lease: ?*const struct_l_dhcp6_lease) [*c]u8;
pub extern fn l_dhcp6_lease_get_dns(lease: ?*const struct_l_dhcp6_lease) [*c][*c]u8;
pub extern fn l_dhcp6_lease_get_domains(lease: ?*const struct_l_dhcp6_lease) [*c][*c]u8;
pub extern fn l_dhcp6_lease_get_prefix_length(lease: ?*const struct_l_dhcp6_lease) u8;
pub extern fn l_dhcp6_lease_get_valid_lifetime(lease: ?*const struct_l_dhcp6_lease) u32;
pub extern fn l_dhcp6_lease_get_preferred_lifetime(lease: ?*const struct_l_dhcp6_lease) u32;
pub extern fn l_dhcp6_lease_get_start_time(lease: ?*const struct_l_dhcp6_lease) u64;
pub const struct_l_icmp6_router = opaque {};
pub const L_ICMP6_CLIENT_EVENT_ROUTER_FOUND: c_int = 0;
pub const enum_l_icmp6_client_event = c_uint;
pub const l_icmp6_debug_cb_t = ?*const fn ([*c]const u8, ?*anyopaque) callconv(.C) void;
pub const l_icmp6_destroy_cb_t = ?*const fn (?*anyopaque) callconv(.C) void;
pub const l_icmp6_client_event_cb_t = ?*const fn (?*struct_l_icmp6_client, enum_l_icmp6_client_event, ?*anyopaque, ?*anyopaque) callconv(.C) void;
pub extern fn l_icmp6_client_new(ifindex: u32) ?*struct_l_icmp6_client;
pub extern fn l_icmp6_client_free(client: ?*struct_l_icmp6_client) void;
pub extern fn l_icmp6_client_start(client: ?*struct_l_icmp6_client) bool;
pub extern fn l_icmp6_client_stop(client: ?*struct_l_icmp6_client) bool;
pub extern fn l_icmp6_client_get_router(client: ?*struct_l_icmp6_client) ?*const struct_l_icmp6_router;
pub extern fn l_icmp6_client_add_event_handler(client: ?*struct_l_icmp6_client, handler: l_icmp6_client_event_cb_t, user_data: ?*anyopaque, destroy: l_icmp6_destroy_cb_t) bool;
pub extern fn l_icmp6_client_set_debug(client: ?*struct_l_icmp6_client, function: l_icmp6_debug_cb_t, user_data: ?*anyopaque, destroy: l_icmp6_destroy_cb_t) bool;
pub extern fn l_icmp6_client_set_address(client: ?*struct_l_icmp6_client, addr: [*c]const u8) bool;
pub extern fn l_icmp6_client_set_nodelay(client: ?*struct_l_icmp6_client, nodelay: bool) bool;
pub extern fn l_icmp6_client_set_rtnl(client: ?*struct_l_icmp6_client, rtnl: ?*struct_l_netlink) bool;
pub extern fn l_icmp6_client_set_route_priority(client: ?*struct_l_icmp6_client, priority: u32) bool;
pub extern fn l_icmp6_client_set_link_local_address(client: ?*struct_l_icmp6_client, ll: [*c]const u8, optimistic: bool) bool;
pub extern fn l_icmp6_router_get_address(r: ?*const struct_l_icmp6_router) [*c]u8;
pub extern fn l_icmp6_router_get_managed(r: ?*const struct_l_icmp6_router) bool;
pub extern fn l_icmp6_router_get_other(r: ?*const struct_l_icmp6_router) bool;
pub extern fn l_icmp6_router_get_lifetime(r: ?*const struct_l_icmp6_router) u16;
pub extern fn l_icmp6_router_get_start_time(r: ?*const struct_l_icmp6_router) u64;
pub const L_CERT_KEY_RSA: c_int = 0;
pub const L_CERT_KEY_ECC: c_int = 1;
pub const L_CERT_KEY_UNKNOWN: c_int = 2;
pub const enum_l_cert_key_type = c_uint;
pub const l_cert_walk_cb_t = ?*const fn (?*struct_l_cert, ?*anyopaque) callconv(.C) bool;
pub extern fn l_cert_new_from_der(buf: [*c]const u8, buf_len: usize) ?*struct_l_cert;
pub extern fn l_cert_free(cert: ?*struct_l_cert) void;
pub inline fn l_cert_free_cleanup(arg_p: ?*anyopaque) void {
    var p = arg_p;
    l_cert_free(@ptrCast(?*struct_l_cert, @ptrCast([*c]?*anyopaque, @alignCast(@import("std").meta.alignment([*c]?*anyopaque), p)).*));
}
pub extern fn l_cert_get_der_data(cert: ?*struct_l_cert, out_len: [*c]usize) [*c]const u8;
pub extern fn l_cert_get_dn(cert: ?*struct_l_cert, out_len: [*c]usize) [*c]const u8;
pub extern fn l_cert_get_valid_times(cert: ?*struct_l_cert, out_not_before_time: [*c]u64, out_not_after_time: [*c]u64) bool;
pub extern fn l_cert_get_pubkey_type(cert: ?*struct_l_cert) enum_l_cert_key_type;
pub extern fn l_cert_get_pubkey(cert: ?*struct_l_cert) ?*struct_l_key;
pub extern fn l_certchain_free(chain: ?*struct_l_certchain) void;
pub inline fn l_certchain_free_cleanup(arg_p: ?*anyopaque) void {
    var p = arg_p;
    l_certchain_free(@ptrCast(?*struct_l_certchain, @ptrCast([*c]?*anyopaque, @alignCast(@import("std").meta.alignment([*c]?*anyopaque), p)).*));
}
pub extern fn l_certchain_get_leaf(chain: ?*struct_l_certchain) ?*struct_l_cert;
pub extern fn l_certchain_walk_from_leaf(chain: ?*struct_l_certchain, cb: l_cert_walk_cb_t, user_data: ?*anyopaque) void;
pub extern fn l_certchain_walk_from_ca(chain: ?*struct_l_certchain, cb: l_cert_walk_cb_t, user_data: ?*anyopaque) void;
pub extern fn l_certchain_verify(chain: ?*struct_l_certchain, ca_certs: ?*struct_l_queue, @"error": [*c][*c]const u8) bool;
pub extern fn l_cert_load_container_file(filename: [*c]const u8, password: [*c]const u8, out_certchain: [*c]?*struct_l_certchain, out_privkey: [*c]?*struct_l_key, out_encrypted: [*c]bool) bool;
pub extern fn l_cert_pkcs5_pbkdf1(@"type": enum_l_checksum_type, password: [*c]const u8, salt: [*c]const u8, salt_len: usize, iter_count: c_uint, out_dk: [*c]u8, dk_len: usize) bool;
pub extern fn l_cert_pkcs5_pbkdf2(@"type": enum_l_checksum_type, password: [*c]const u8, salt: [*c]const u8, salt_len: usize, iter_count: c_uint, out_dk: [*c]u8, dk_len: usize) bool;
pub const struct_l_ecc_curve = opaque {};
pub const struct_l_ecc_point = opaque {};
pub const struct_l_ecc_scalar = opaque {};
pub const L_ECC_POINT_TYPE_COMPLIANT: c_int = 1;
pub const L_ECC_POINT_TYPE_COMPRESSED_BIT0: c_int = 2;
pub const L_ECC_POINT_TYPE_COMPRESSED_BIT1: c_int = 3;
pub const L_ECC_POINT_TYPE_FULL: c_int = 4;
pub const enum_l_ecc_point_type = c_uint;
pub extern fn l_ecc_supported_ike_groups() [*c]const c_uint;
pub extern fn l_ecc_supported_tls_groups() [*c]const c_uint;
pub extern fn l_ecc_curve_from_name(name: [*c]const u8) ?*const struct_l_ecc_curve;
pub extern fn l_ecc_curve_from_ike_group(group: c_uint) ?*const struct_l_ecc_curve;
pub extern fn l_ecc_curve_from_tls_group(group: c_uint) ?*const struct_l_ecc_curve;
pub extern fn l_ecc_curve_get_name(curve: ?*const struct_l_ecc_curve) [*c]const u8;
pub extern fn l_ecc_curve_get_ike_group(curve: ?*const struct_l_ecc_curve) c_uint;
pub extern fn l_ecc_curve_get_tls_group(curve: ?*const struct_l_ecc_curve) c_uint;
pub extern fn l_ecc_curve_get_order(curve: ?*const struct_l_ecc_curve) ?*struct_l_ecc_scalar;
pub extern fn l_ecc_curve_get_prime(curve: ?*const struct_l_ecc_curve) ?*struct_l_ecc_scalar;
pub extern fn l_ecc_curve_get_scalar_bytes(curve: ?*const struct_l_ecc_curve) usize;
pub extern fn l_ecc_point_new(curve: ?*const struct_l_ecc_curve) ?*struct_l_ecc_point;
pub extern fn l_ecc_point_from_data(curve: ?*const struct_l_ecc_curve, @"type": enum_l_ecc_point_type, data: ?*const anyopaque, len: usize) ?*struct_l_ecc_point;
pub extern fn l_ecc_point_from_sswu(u: ?*const struct_l_ecc_scalar) ?*struct_l_ecc_point;
pub extern fn l_ecc_point_clone(p: ?*const struct_l_ecc_point) ?*struct_l_ecc_point;
pub extern fn l_ecc_point_get_curve(p: ?*const struct_l_ecc_point) ?*const struct_l_ecc_curve;
pub extern fn l_ecc_point_get_x(p: ?*const struct_l_ecc_point, x: ?*anyopaque, xlen: usize) isize;
pub extern fn l_ecc_point_get_y(p: ?*const struct_l_ecc_point, y: ?*anyopaque, ylen: usize) isize;
pub extern fn l_ecc_point_y_isodd(p: ?*const struct_l_ecc_point) bool;
pub extern fn l_ecc_point_get_data(p: ?*const struct_l_ecc_point, buf: ?*anyopaque, len: usize) isize;
pub extern fn l_ecc_point_free(p: ?*struct_l_ecc_point) void;
pub inline fn l_ecc_point_free_cleanup(arg_p: ?*anyopaque) void {
    var p = arg_p;
    l_ecc_point_free(@ptrCast(?*struct_l_ecc_point, @ptrCast([*c]?*anyopaque, @alignCast(@import("std").meta.alignment([*c]?*anyopaque), p)).*));
}
pub extern fn l_ecc_scalar_new(curve: ?*const struct_l_ecc_curve, buf: ?*const anyopaque, len: usize) ?*struct_l_ecc_scalar;
pub extern fn l_ecc_scalar_new_random(curve: ?*const struct_l_ecc_curve) ?*struct_l_ecc_scalar;
pub extern fn l_ecc_scalar_new_modp(curve: ?*const struct_l_ecc_curve, buf: ?*const anyopaque, len: usize) ?*struct_l_ecc_scalar;
pub extern fn l_ecc_scalar_new_reduced_1_to_n(curve: ?*const struct_l_ecc_curve, buf: ?*const anyopaque, len: usize) ?*struct_l_ecc_scalar;
pub extern fn l_ecc_scalar_get_data(c: ?*const struct_l_ecc_scalar, buf: ?*anyopaque, len: usize) isize;
pub extern fn l_ecc_scalar_free(c: ?*struct_l_ecc_scalar) void;
pub inline fn l_ecc_scalar_free_cleanup(arg_p: ?*anyopaque) void {
    var p = arg_p;
    l_ecc_scalar_free(@ptrCast(?*struct_l_ecc_scalar, @ptrCast([*c]?*anyopaque, @alignCast(@import("std").meta.alignment([*c]?*anyopaque), p)).*));
}
pub extern fn l_ecc_scalar_add(ret: ?*struct_l_ecc_scalar, a: ?*const struct_l_ecc_scalar, b: ?*const struct_l_ecc_scalar, mod: ?*const struct_l_ecc_scalar) bool;
pub extern fn l_ecc_point_multiply(ret: ?*struct_l_ecc_point, scalar: ?*const struct_l_ecc_scalar, point: ?*const struct_l_ecc_point) bool;
pub extern fn l_ecc_point_add(ret: ?*struct_l_ecc_point, a: ?*const struct_l_ecc_point, b: ?*const struct_l_ecc_point) bool;
pub extern fn l_ecc_point_inverse(p: ?*struct_l_ecc_point) bool;
pub extern fn l_ecc_scalar_multiply(ret: ?*struct_l_ecc_scalar, a: ?*const struct_l_ecc_scalar, b: ?*const struct_l_ecc_scalar) bool;
pub extern fn l_ecc_scalar_legendre(value: ?*struct_l_ecc_scalar) c_int;
pub extern fn l_ecc_scalar_sum_x(ret: ?*struct_l_ecc_scalar, x: ?*const struct_l_ecc_scalar) bool;
pub extern fn l_ecc_scalars_are_equal(a: ?*const struct_l_ecc_scalar, b: ?*const struct_l_ecc_scalar) bool;
pub extern fn l_ecc_points_are_equal(a: ?*const struct_l_ecc_point, b: ?*const struct_l_ecc_point) bool;
pub extern fn l_ecdh_generate_key_pair(curve: ?*const struct_l_ecc_curve, out_private: [*c]?*struct_l_ecc_scalar, out_public: [*c]?*struct_l_ecc_point) bool;
pub extern fn l_ecdh_generate_shared_secret(private_key: ?*const struct_l_ecc_scalar, other_public: ?*const struct_l_ecc_point, secret: [*c]?*struct_l_ecc_scalar) bool;
pub extern fn l_time_now() u64;
pub fn l_time_after(arg_a: u64, arg_b: u64) callconv(.C) bool {
    var a = arg_a;
    var b = arg_b;
    return a > b;
}
pub fn l_time_before(arg_a: u64, arg_b: u64) callconv(.C) bool {
    var a = arg_a;
    var b = arg_b;
    return l_time_after(b, a);
}
pub fn l_time_offset(arg_time: u64, arg_offset: u64) callconv(.C) u64 {
    var time = arg_time;
    var offset = arg_offset;
    if (offset > (@as(c_ulong, 18446744073709551615) -% time)) return @as(c_ulong, 18446744073709551615);
    return time +% offset;
}
pub fn l_time_diff(arg_a: u64, arg_b: u64) callconv(.C) u64 {
    var a = arg_a;
    var b = arg_b;
    return if (a < b) b -% a else a -% b;
}
pub fn l_time_to_secs(arg_time: u64) callconv(.C) u64 {
    var time = arg_time;
    return @bitCast(u64, @truncate(c_ulong, @bitCast(c_ulonglong, @as(c_ulonglong, time)) / @as(c_ulonglong, 1000000)));
}
pub fn l_time_to_msecs(arg_time: u64) callconv(.C) u64 {
    var time = arg_time;
    return @bitCast(u64, @truncate(c_ulong, @bitCast(c_ulonglong, @as(c_ulonglong, time)) / @as(c_ulonglong, 1000)));
}
pub const struct_l_gpio_chip = opaque {};
pub const struct_l_gpio_writer = opaque {};
pub const struct_l_gpio_reader = opaque {};
pub extern fn l_gpio_chips_with_line_label(line_label: [*c]const u8) [*c][*c]u8;
pub extern fn l_gpio_chip_new(chip_name: [*c]const u8) ?*struct_l_gpio_chip;
pub extern fn l_gpio_chip_free(chip: ?*struct_l_gpio_chip) void;
pub extern fn l_gpio_chip_get_label(chip: ?*struct_l_gpio_chip) [*c]const u8;
pub extern fn l_gpio_chip_get_name(chip: ?*struct_l_gpio_chip) [*c]const u8;
pub extern fn l_gpio_chip_get_num_lines(chip: ?*struct_l_gpio_chip) u32;
pub extern fn l_gpio_chip_find_line_offset(chip: ?*struct_l_gpio_chip, line_label: [*c]const u8, line_offset: [*c]u32) bool;
pub extern fn l_gpio_chip_get_line_label(chip: ?*struct_l_gpio_chip, offset: u32) [*c]u8;
pub extern fn l_gpio_chip_get_line_consumer(chip: ?*struct_l_gpio_chip, offset: u32) [*c]u8;
pub extern fn l_gpio_writer_new(chip: ?*struct_l_gpio_chip, consumer: [*c]const u8, n_offsets: u32, offsets: [*c]const u32, values: [*c]const u32) ?*struct_l_gpio_writer;
pub extern fn l_gpio_writer_free(writer: ?*struct_l_gpio_writer) void;
pub extern fn l_gpio_writer_set(writer: ?*struct_l_gpio_writer, n_values: u32, values: [*c]const u32) bool;
pub extern fn l_gpio_reader_new(chip: ?*struct_l_gpio_chip, consumer: [*c]const u8, n_offsets: u32, offsets: [*c]const u32) ?*struct_l_gpio_reader;
pub extern fn l_gpio_reader_free(reader: ?*struct_l_gpio_reader) void;
pub extern fn l_gpio_reader_get(reader: ?*struct_l_gpio_reader, n_values: u32, values: [*c]u32) bool;
pub extern fn l_path_next(path_str: [*c]const u8, ret: [*c][*c]u8) [*c]const u8;
pub extern fn l_path_find(basename: [*c]const u8, path_str: [*c]const u8, mode: c_int) [*c]u8;
pub extern fn l_path_get_mtime(path: [*c]const u8) u64;
pub extern fn l_path_touch(path: [*c]const u8) c_int;
pub const struct_l_acd = opaque {};
pub const L_ACD_EVENT_AVAILABLE: c_int = 0;
pub const L_ACD_EVENT_CONFLICT: c_int = 1;
pub const L_ACD_EVENT_LOST: c_int = 2;
pub const enum_l_acd_event = c_uint;
pub const L_ACD_DEFEND_POLICY_NONE: c_int = 0;
pub const L_ACD_DEFEND_POLICY_DEFEND: c_int = 1;
pub const L_ACD_DEFEND_POLICY_INFINITE: c_int = 2;
pub const enum_l_acd_defend_policy = c_uint;
pub const l_acd_event_func_t = ?*const fn (enum_l_acd_event, ?*anyopaque) callconv(.C) void;
pub const l_acd_destroy_func_t = ?*const fn (?*anyopaque) callconv(.C) void;
pub const l_acd_debug_cb_t = ?*const fn ([*c]const u8, ?*anyopaque) callconv(.C) void;
pub extern fn l_acd_new(ifindex: c_int) ?*struct_l_acd;
pub extern fn l_acd_start(acd: ?*struct_l_acd, ip: [*c]const u8) bool;
pub extern fn l_acd_set_event_handler(acd: ?*struct_l_acd, cb: l_acd_event_func_t, user_data: ?*anyopaque, destroy: l_acd_destroy_func_t) bool;
pub extern fn l_acd_stop(acd: ?*struct_l_acd) bool;
pub extern fn l_acd_destroy(acd: ?*struct_l_acd) void;
pub extern fn l_acd_set_debug(acd: ?*struct_l_acd, function: l_acd_debug_cb_t, user_data: ?*anyopaque, destory: l_acd_destroy_func_t) bool;
pub extern fn l_acd_set_skip_probes(acd: ?*struct_l_acd, skip: bool) bool;
pub extern fn l_acd_set_defend_policy(acd: ?*struct_l_acd, policy: enum_l_acd_defend_policy) bool;
pub const struct_l_tester = opaque {};
pub const L_TESTER_STAGE_INVALID: c_int = 0;
pub const L_TESTER_STAGE_PRE_SETUP: c_int = 1;
pub const L_TESTER_STAGE_SETUP: c_int = 2;
pub const L_TESTER_STAGE_RUN: c_int = 3;
pub const L_TESTER_STAGE_TEARDOWN: c_int = 4;
pub const L_TESTER_STAGE_POST_TEARDOWN: c_int = 5;
pub const enum_l_tester_stage = c_uint;
pub const l_tester_destroy_func_t = ?*const fn (?*anyopaque) callconv(.C) void;
pub const l_tester_data_func_t = ?*const fn (?*const anyopaque) callconv(.C) void;
pub const l_tester_finish_func_t = ?*const fn (?*struct_l_tester) callconv(.C) void;
pub const l_tester_wait_func_t = ?*const fn (?*anyopaque) callconv(.C) void;
pub extern fn l_tester_new(prefix: [*c]const u8, substring: [*c]const u8, list_cases: bool) ?*struct_l_tester;
pub extern fn l_tester_destroy(tester: ?*struct_l_tester) void;
pub extern fn l_tester_start(tester: ?*struct_l_tester, finish_func: l_tester_finish_func_t) void;
pub extern fn l_tester_summarize(tester: ?*struct_l_tester) bool;
pub extern fn l_tester_add_full(tester: ?*struct_l_tester, name: [*c]const u8, test_data: ?*const anyopaque, pre_setup_func: l_tester_data_func_t, setup_func: l_tester_data_func_t, test_func: l_tester_data_func_t, teardown_func: l_tester_data_func_t, post_teardown_func: l_tester_data_func_t, timeout: c_uint, user_data: ?*anyopaque, destroy: l_tester_destroy_func_t) void;
pub extern fn l_tester_add(tester: ?*struct_l_tester, name: [*c]const u8, test_data: ?*const anyopaque, setup_func: l_tester_data_func_t, test_func: l_tester_data_func_t, teardown_func: l_tester_data_func_t) void;
pub extern fn l_tester_pre_setup_complete(tester: ?*struct_l_tester) void;
pub extern fn l_tester_pre_setup_failed(tester: ?*struct_l_tester) void;
pub extern fn l_tester_setup_complete(tester: ?*struct_l_tester) void;
pub extern fn l_tester_setup_failed(tester: ?*struct_l_tester) void;
pub extern fn l_tester_test_passed(tester: ?*struct_l_tester) void;
pub extern fn l_tester_test_failed(tester: ?*struct_l_tester) void;
pub extern fn l_tester_test_abort(tester: ?*struct_l_tester) void;
pub extern fn l_tester_teardown_complete(tester: ?*struct_l_tester) void;
pub extern fn l_tester_teardown_failed(tester: ?*struct_l_tester) void;
pub extern fn l_tester_post_teardown_complete(tester: ?*struct_l_tester) void;
pub extern fn l_tester_post_teardown_failed(tester: ?*struct_l_tester) void;
pub extern fn l_tester_get_stage(tester: ?*struct_l_tester) enum_l_tester_stage;
pub extern fn l_tester_get_data(tester: ?*struct_l_tester) ?*anyopaque;
pub extern fn l_tester_wait(tester: ?*struct_l_tester, seconds: c_uint, func: l_tester_wait_func_t, user_data: ?*anyopaque) void;
pub const struct_l_netconfig = opaque {};
pub const L_NETCONFIG_EVENT_CONFIGURE: c_int = 0;
pub const L_NETCONFIG_EVENT_UPDATE: c_int = 1;
pub const L_NETCONFIG_EVENT_UNCONFIGURE: c_int = 2;
pub const L_NETCONFIG_EVENT_FAILED: c_int = 3;
pub const enum_l_netconfig_event = c_uint;
pub const l_netconfig_event_cb_t = ?*const fn (?*struct_l_netconfig, u8, enum_l_netconfig_event, ?*anyopaque) callconv(.C) void;
pub const l_netconfig_destroy_cb_t = ?*const fn (?*anyopaque) callconv(.C) void;
pub extern fn l_netconfig_new(ifindex: u32) ?*struct_l_netconfig;
pub extern fn l_netconfig_destroy(netconfig: ?*struct_l_netconfig) void;
pub extern fn l_netconfig_set_family_enabled(netconfig: ?*struct_l_netconfig, family: u8, enabled: bool) bool;
pub extern fn l_netconfig_set_hostname(netconfig: ?*struct_l_netconfig, hostname: [*c]const u8) bool;
pub extern fn l_netconfig_set_route_priority(netconfig: ?*struct_l_netconfig, priority: u32) bool;
pub extern fn l_netconfig_set_static_addr(netconfig: ?*struct_l_netconfig, family: u8, addr: ?*const struct_l_rtnl_address) bool;
pub extern fn l_netconfig_set_gateway_override(netconfig: ?*struct_l_netconfig, family: u8, gateway_str: [*c]const u8) bool;
pub extern fn l_netconfig_set_dns_override(netconfig: ?*struct_l_netconfig, family: u8, dns_list: [*c][*c]u8) bool;
pub extern fn l_netconfig_set_domain_names_override(netconfig: ?*struct_l_netconfig, family: u8, names: [*c][*c]u8) bool;
pub extern fn l_netconfig_set_acd_enabled(netconfig: ?*struct_l_netconfig, enabled: bool) bool;
pub extern fn l_netconfig_set_optimistic_dad_enabled(netconfig: ?*struct_l_netconfig, enabled: bool) bool;
pub extern fn l_netconfig_check_config(netconfig: ?*struct_l_netconfig) bool;
pub extern fn l_netconfig_reset_config(netconfig: ?*struct_l_netconfig) bool;
pub extern fn l_netconfig_start(netconfig: ?*struct_l_netconfig) bool;
pub extern fn l_netconfig_stop(netconfig: ?*struct_l_netconfig) void;
pub extern fn l_netconfig_unconfigure(netconfig: ?*struct_l_netconfig) void;
pub extern fn l_netconfig_get_dhcp_client(netconfig: ?*struct_l_netconfig) ?*struct_l_dhcp_client;
pub extern fn l_netconfig_get_dhcp6_client(netconfig: ?*struct_l_netconfig) ?*struct_l_dhcp6_client;
pub extern fn l_netconfig_get_icmp6_client(netconfig: ?*struct_l_netconfig) ?*struct_l_icmp6_client;
pub extern fn l_netconfig_set_event_handler(netconfig: ?*struct_l_netconfig, handler: l_netconfig_event_cb_t, user_data: ?*anyopaque, destroy: l_netconfig_destroy_cb_t) void;
pub extern fn l_netconfig_apply_rtnl(netconfig: ?*struct_l_netconfig) void;
pub extern fn l_netconfig_get_addresses(netconfig: ?*struct_l_netconfig, out_added: [*c][*c]const struct_l_queue_entry, out_updated: [*c][*c]const struct_l_queue_entry, out_removed: [*c][*c]const struct_l_queue_entry, out_expired: [*c][*c]const struct_l_queue_entry) [*c]const struct_l_queue_entry;
pub extern fn l_netconfig_get_routes(netconfig: ?*struct_l_netconfig, out_added: [*c][*c]const struct_l_queue_entry, out_updated: [*c][*c]const struct_l_queue_entry, out_removed: [*c][*c]const struct_l_queue_entry, out_expired: [*c][*c]const struct_l_queue_entry) [*c]const struct_l_queue_entry;
pub extern fn l_netconfig_get_dns_list(netconfig: ?*struct_l_netconfig) [*c][*c]u8;
pub extern fn l_netconfig_get_domain_names(netconfig: ?*struct_l_netconfig) [*c][*c]u8;
pub const __INTMAX_C_SUFFIX__ = @compileError("unable to translate macro: undefined identifier `L`"); // (no file):80:9
pub const __UINTMAX_C_SUFFIX__ = @compileError("unable to translate macro: undefined identifier `UL`"); // (no file):86:9
pub const __FLT16_DENORM_MIN__ = @compileError("unable to translate C expr: unexpected token 'IntegerLiteral'"); // (no file):109:9
pub const __FLT16_EPSILON__ = @compileError("unable to translate C expr: unexpected token 'IntegerLiteral'"); // (no file):113:9
pub const __FLT16_MAX__ = @compileError("unable to translate C expr: unexpected token 'IntegerLiteral'"); // (no file):119:9
pub const __FLT16_MIN__ = @compileError("unable to translate C expr: unexpected token 'IntegerLiteral'"); // (no file):122:9
pub const __INT64_C_SUFFIX__ = @compileError("unable to translate macro: undefined identifier `L`"); // (no file):183:9
pub const __UINT32_C_SUFFIX__ = @compileError("unable to translate macro: undefined identifier `U`"); // (no file):205:9
pub const __UINT64_C_SUFFIX__ = @compileError("unable to translate macro: undefined identifier `UL`"); // (no file):213:9
pub const __seg_gs = @compileError("unable to translate macro: undefined identifier `__attribute__`"); // (no file):342:9
pub const __seg_fs = @compileError("unable to translate macro: undefined identifier `__attribute__`"); // (no file):343:9
pub const __GLIBC_USE = @compileError("unable to translate macro: undefined identifier `__GLIBC_USE_`"); // /usr/include/features.h:186:9
pub const __glibc_has_attribute = @compileError("unable to translate macro: undefined identifier `__has_attribute`"); // /usr/include/sys/cdefs.h:45:10
pub const __glibc_has_extension = @compileError("unable to translate macro: undefined identifier `__has_extension`"); // /usr/include/sys/cdefs.h:55:10
pub const __THROW = @compileError("unable to translate macro: undefined identifier `__attribute__`"); // /usr/include/sys/cdefs.h:79:11
pub const __THROWNL = @compileError("unable to translate macro: undefined identifier `__attribute__`"); // /usr/include/sys/cdefs.h:80:11
pub const __NTH = @compileError("unable to translate macro: undefined identifier `__attribute__`"); // /usr/include/sys/cdefs.h:81:11
pub const __NTHNL = @compileError("unable to translate macro: undefined identifier `__attribute__`"); // /usr/include/sys/cdefs.h:82:11
pub const __CONCAT = @compileError("unable to translate C expr: unexpected token '##'"); // /usr/include/sys/cdefs.h:124:9
pub const __STRING = @compileError("unable to translate C expr: unexpected token '#'"); // /usr/include/sys/cdefs.h:125:9
pub const __glibc_unsigned_or_positive = @compileError("unable to translate macro: undefined identifier `__typeof`"); // /usr/include/sys/cdefs.h:160:9
pub const __glibc_fortify = @compileError("unable to translate C expr: expected ')' instead got '...'"); // /usr/include/sys/cdefs.h:185:9
pub const __glibc_fortify_n = @compileError("unable to translate C expr: expected ')' instead got '...'"); // /usr/include/sys/cdefs.h:195:9
pub const __warnattr = @compileError("unable to translate C expr: unexpected token 'Eof'"); // /usr/include/sys/cdefs.h:207:10
pub const __errordecl = @compileError("unable to translate C expr: unexpected token 'extern'"); // /usr/include/sys/cdefs.h:208:10
pub const __flexarr = @compileError("unable to translate C expr: unexpected token '['"); // /usr/include/sys/cdefs.h:216:10
pub const __REDIRECT = @compileError("unable to translate macro: undefined identifier `__asm__`"); // /usr/include/sys/cdefs.h:247:10
pub const __REDIRECT_NTH = @compileError("unable to translate macro: undefined identifier `__asm__`"); // /usr/include/sys/cdefs.h:254:11
pub const __REDIRECT_NTHNL = @compileError("unable to translate macro: undefined identifier `__asm__`"); // /usr/include/sys/cdefs.h:256:11
pub const __ASMNAME2 = @compileError("unable to translate C expr: unexpected token 'Identifier'"); // /usr/include/sys/cdefs.h:260:10
pub const __attribute_malloc__ = @compileError("unable to translate macro: undefined identifier `__attribute__`"); // /usr/include/sys/cdefs.h:281:10
pub const __attribute_alloc_size__ = @compileError("unable to translate C expr: unexpected token 'Eof'"); // /usr/include/sys/cdefs.h:292:10
pub const __attribute_alloc_align__ = @compileError("unable to translate macro: undefined identifier `__attribute__`"); // /usr/include/sys/cdefs.h:298:10
pub const __attribute_pure__ = @compileError("unable to translate macro: undefined identifier `__attribute__`"); // /usr/include/sys/cdefs.h:308:10
pub const __attribute_const__ = @compileError("unable to translate macro: undefined identifier `__attribute__`"); // /usr/include/sys/cdefs.h:315:10
pub const __attribute_maybe_unused__ = @compileError("unable to translate macro: undefined identifier `__attribute__`"); // /usr/include/sys/cdefs.h:321:10
pub const __attribute_used__ = @compileError("unable to translate macro: undefined identifier `__attribute__`"); // /usr/include/sys/cdefs.h:330:10
pub const __attribute_noinline__ = @compileError("unable to translate macro: undefined identifier `__attribute__`"); // /usr/include/sys/cdefs.h:331:10
pub const __attribute_deprecated__ = @compileError("unable to translate macro: undefined identifier `__attribute__`"); // /usr/include/sys/cdefs.h:339:10
pub const __attribute_deprecated_msg__ = @compileError("unable to translate macro: undefined identifier `__attribute__`"); // /usr/include/sys/cdefs.h:349:10
pub const __attribute_format_arg__ = @compileError("unable to translate macro: undefined identifier `__attribute__`"); // /usr/include/sys/cdefs.h:362:10
pub const __attribute_format_strfmon__ = @compileError("unable to translate macro: undefined identifier `__attribute__`"); // /usr/include/sys/cdefs.h:372:10
pub const __attribute_nonnull__ = @compileError("unable to translate macro: undefined identifier `__attribute__`"); // /usr/include/sys/cdefs.h:384:11
pub const __returns_nonnull = @compileError("unable to translate macro: undefined identifier `__attribute__`"); // /usr/include/sys/cdefs.h:397:10
pub const __attribute_warn_unused_result__ = @compileError("unable to translate macro: undefined identifier `__attribute__`"); // /usr/include/sys/cdefs.h:406:10
pub const __always_inline = @compileError("unable to translate macro: undefined identifier `__inline`"); // /usr/include/sys/cdefs.h:424:10
pub const __attribute_artificial__ = @compileError("unable to translate macro: undefined identifier `__attribute__`"); // /usr/include/sys/cdefs.h:433:10
pub const __extern_inline = @compileError("unable to translate macro: undefined identifier `__inline`"); // /usr/include/sys/cdefs.h:451:11
pub const __extern_always_inline = @compileError("unable to translate macro: undefined identifier `__attribute__`"); // /usr/include/sys/cdefs.h:452:11
pub const __restrict_arr = @compileError("unable to translate macro: undefined identifier `__restrict`"); // /usr/include/sys/cdefs.h:495:10
pub const __attribute_copy__ = @compileError("unable to translate C expr: unexpected token 'Eof'"); // /usr/include/sys/cdefs.h:544:10
pub const __LDBL_REDIR2_DECL = @compileError("unable to translate C expr: unexpected token 'Eof'"); // /usr/include/sys/cdefs.h:620:10
pub const __LDBL_REDIR_DECL = @compileError("unable to translate C expr: unexpected token 'Eof'"); // /usr/include/sys/cdefs.h:621:10
pub const __glibc_macro_warning1 = @compileError("unable to translate macro: undefined identifier `_Pragma`"); // /usr/include/sys/cdefs.h:635:10
pub const __glibc_macro_warning = @compileError("unable to translate macro: undefined identifier `GCC`"); // /usr/include/sys/cdefs.h:636:10
pub const __fortified_attr_access = @compileError("unable to translate C expr: unexpected token 'Eof'"); // /usr/include/sys/cdefs.h:681:11
pub const __attr_access = @compileError("unable to translate C expr: unexpected token 'Eof'"); // /usr/include/sys/cdefs.h:682:11
pub const __attr_access_none = @compileError("unable to translate C expr: unexpected token 'Eof'"); // /usr/include/sys/cdefs.h:683:11
pub const __attr_dealloc = @compileError("unable to translate C expr: unexpected token 'Eof'"); // /usr/include/sys/cdefs.h:693:10
pub const __attribute_returns_twice__ = @compileError("unable to translate macro: undefined identifier `__attribute__`"); // /usr/include/sys/cdefs.h:700:10
pub const va_start = @compileError("unable to translate macro: undefined identifier `__builtin_va_start`"); // /usr/lib/zig/lib/include/stdarg.h:17:9
pub const va_end = @compileError("unable to translate macro: undefined identifier `__builtin_va_end`"); // /usr/lib/zig/lib/include/stdarg.h:18:9
pub const va_arg = @compileError("unable to translate macro: undefined identifier `__builtin_va_arg`"); // /usr/lib/zig/lib/include/stdarg.h:19:9
pub const __va_copy = @compileError("unable to translate macro: undefined identifier `__builtin_va_copy`"); // /usr/lib/zig/lib/include/stdarg.h:24:9
pub const va_copy = @compileError("unable to translate macro: undefined identifier `__builtin_va_copy`"); // /usr/lib/zig/lib/include/stdarg.h:27:9
pub const __STD_TYPE = @compileError("unable to translate C expr: unexpected token 'typedef'"); // /usr/include/bits/types.h:137:10
pub const __FSID_T_TYPE = @compileError("unable to translate macro: undefined identifier `__val`"); // /usr/include/bits/typesizes.h:73:9
pub const __FD_ZERO = @compileError("unable to translate macro: undefined identifier `__i`"); // /usr/include/bits/select.h:25:9
pub const __FD_SET = @compileError("unable to translate C expr: expected ')' instead got '|='"); // /usr/include/bits/select.h:32:9
pub const __FD_CLR = @compileError("unable to translate C expr: expected ')' instead got '&='"); // /usr/include/bits/select.h:34:9
pub const __PTHREAD_MUTEX_INITIALIZER = @compileError("unable to translate C expr: unexpected token '{'"); // /usr/include/bits/struct_mutex.h:56:10
pub const __PTHREAD_RWLOCK_ELISION_EXTRA = @compileError("unable to translate C expr: unexpected token '{'"); // /usr/include/bits/struct_rwlock.h:40:11
pub const __ONCE_FLAG_INIT = @compileError("unable to translate C expr: unexpected token '{'"); // /usr/include/bits/thread-shared-types.h:113:9
pub const DEFINE_CLEANUP_FUNC = @compileError("unable to translate macro: undefined identifier `__attribute__`"); // ell/include/ell/cleanup.h:25:9
pub const l_container_of = @compileError("unable to translate macro: undefined identifier `_Pragma`"); // ell/include/ell/util.h:39:9
pub const L_STRINGIFY_ARG = @compileError("unable to translate C expr: unexpected token '#'"); // ell/include/ell/util.h:48:9
pub const L_WARN_ON = @compileError("unable to translate macro: undefined identifier `__extension__`"); // ell/include/ell/util.h:50:9
pub const L_GET_UNALIGNED = @compileError("unable to translate macro: undefined identifier `__extension__`"); // ell/include/ell/util.h:65:9
pub const L_PUT_UNALIGNED = @compileError("unable to translate macro: undefined identifier `__attribute__`"); // ell/include/ell/util.h:73:9
pub const L_AUTO_FREE_VAR = @compileError("unable to translate macro: undefined identifier `__attribute__`"); // ell/include/ell/util.h:230:9
pub const L_ARRAY_SIZE = @compileError("unable to translate C expr: unexpected token '('"); // ell/include/ell/util.h:233:9
pub const l_steal_ptr = @compileError("unable to translate macro: undefined identifier `__extension__`"); // ell/include/ell/util.h:250:9
pub const l_new = @compileError("unable to translate macro: undefined identifier `__extension__`"); // ell/include/ell/util.h:260:9
pub const L_TFR = @compileError("unable to translate macro: undefined identifier `__extension__`"); // ell/include/ell/util.h:303:9
pub const _L_IN_SET_CMP = @compileError("unable to translate C expr: expected ')' instead got '...'"); // ell/include/ell/util.h:310:9
pub const L_IN_SET = @compileError("unable to translate C expr: expected ')' instead got '...'"); // ell/include/ell/util.h:322:9
pub const L_IN_STRSET = @compileError("unable to translate C expr: expected ')' instead got '...'"); // ell/include/ell/util.h:325:9
pub const __CFLOAT32 = @compileError("unable to translate: TODO _Complex"); // /usr/include/bits/floatn-common.h:149:12
pub const __CFLOAT64 = @compileError("unable to translate: TODO _Complex"); // /usr/include/bits/floatn-common.h:160:13
pub const __CFLOAT32X = @compileError("unable to translate: TODO _Complex"); // /usr/include/bits/floatn-common.h:169:12
pub const __CFLOAT64X = @compileError("unable to translate: TODO _Complex"); // /usr/include/bits/floatn-common.h:178:13
pub const __builtin_nansf32 = @compileError("unable to translate macro: undefined identifier `__builtin_nansf`"); // /usr/include/bits/floatn-common.h:221:12
pub const __builtin_huge_valf64 = @compileError("unable to translate macro: undefined identifier `__builtin_huge_val`"); // /usr/include/bits/floatn-common.h:255:13
pub const __builtin_inff64 = @compileError("unable to translate macro: undefined identifier `__builtin_inf`"); // /usr/include/bits/floatn-common.h:256:13
pub const __builtin_nanf64 = @compileError("unable to translate macro: undefined identifier `__builtin_nan`"); // /usr/include/bits/floatn-common.h:257:13
pub const __builtin_nansf64 = @compileError("unable to translate macro: undefined identifier `__builtin_nans`"); // /usr/include/bits/floatn-common.h:258:13
pub const __builtin_huge_valf32x = @compileError("unable to translate macro: undefined identifier `__builtin_huge_val`"); // /usr/include/bits/floatn-common.h:272:12
pub const __builtin_inff32x = @compileError("unable to translate macro: undefined identifier `__builtin_inf`"); // /usr/include/bits/floatn-common.h:273:12
pub const __builtin_nanf32x = @compileError("unable to translate macro: undefined identifier `__builtin_nan`"); // /usr/include/bits/floatn-common.h:274:12
pub const __builtin_nansf32x = @compileError("unable to translate macro: undefined identifier `__builtin_nans`"); // /usr/include/bits/floatn-common.h:275:12
pub const __builtin_huge_valf64x = @compileError("unable to translate macro: undefined identifier `__builtin_huge_vall`"); // /usr/include/bits/floatn-common.h:289:13
pub const __builtin_inff64x = @compileError("unable to translate macro: undefined identifier `__builtin_infl`"); // /usr/include/bits/floatn-common.h:290:13
pub const __builtin_nanf64x = @compileError("unable to translate macro: undefined identifier `__builtin_nanl`"); // /usr/include/bits/floatn-common.h:291:13
pub const __builtin_nansf64x = @compileError("unable to translate macro: undefined identifier `__builtin_nansl`"); // /usr/include/bits/floatn-common.h:292:13
pub const l_log = @compileError("unable to translate C expr: expected ')' instead got '...'"); // ell/include/ell/log.h:52:9
pub const L_DEBUG_SYMBOL = @compileError("unable to translate C expr: expected ')' instead got '...'"); // ell/include/ell/log.h:69:9
pub const l_debug_enable = @compileError("unable to translate macro: undefined identifier `_Pragma`"); // ell/include/ell/log.h:89:9
pub const l_error = @compileError("unable to translate C expr: expected ')' instead got '...'"); // ell/include/ell/log.h:100:9
pub const l_warn = @compileError("unable to translate C expr: expected ')' instead got '...'"); // ell/include/ell/log.h:101:9
pub const l_info = @compileError("unable to translate C expr: expected ')' instead got '...'"); // ell/include/ell/log.h:102:9
pub const l_debug = @compileError("unable to translate C expr: expected ')' instead got '...'"); // ell/include/ell/log.h:103:9
pub const offsetof = @compileError("unable to translate macro: undefined identifier `__builtin_offsetof`"); // /usr/lib/zig/lib/include/stddef.h:104:9
pub const __struct_group = @compileError("unable to translate C expr: expected ')' instead got '...'"); // /usr/include/linux/stddef.h:26:9
pub const __DECLARE_FLEX_ARRAY = @compileError("unable to translate macro: undefined identifier `__empty_`"); // /usr/include/linux/stddef.h:42:9
pub const __aligned_u64 = @compileError("unable to translate macro: undefined identifier `__attribute__`"); // /usr/include/linux/types.h:46:9
pub const __aligned_be64 = @compileError("unable to translate macro: undefined identifier `__attribute__`"); // /usr/include/linux/types.h:47:9
pub const __aligned_le64 = @compileError("unable to translate macro: undefined identifier `__attribute__`"); // /usr/include/linux/types.h:48:9
pub const __AC = @compileError("unable to translate C expr: expected ')' instead got '##'"); // /usr/include/linux/const.h:20:9
pub const _UL = @compileError("unable to translate macro: undefined identifier `UL`"); // /usr/include/linux/const.h:25:9
pub const _ULL = @compileError("unable to translate macro: undefined identifier `ULL`"); // /usr/include/linux/const.h:26:9
pub const __ALIGN_KERNEL = @compileError("unable to translate macro: undefined identifier `typeof`"); // /usr/include/linux/const.h:31:9
pub const SOCK_TXREHASH_DEFAULT = @compileError("unable to translate macro: undefined identifier `u8`"); // /usr/include/linux/socket.h:34:9
pub const NLMSG_NEXT = @compileError("unable to translate C expr: expected ')' instead got '-='"); // /usr/include/linux/netlink.h:95:9
pub const RTA_NEXT = @compileError("unable to translate C expr: expected ')' instead got '-='"); // /usr/include/linux/rtnetlink.h:223:9
pub const tcm_block_index = @compileError("unable to translate macro: undefined identifier `tcm_parent`"); // /usr/include/linux/rtnetlink.h:611:9
pub const __llvm__ = @as(c_int, 1);
pub const __clang__ = @as(c_int, 1);
pub const __clang_major__ = @as(c_int, 15);
pub const __clang_minor__ = @as(c_int, 0);
pub const __clang_patchlevel__ = @as(c_int, 3);
pub const __clang_version__ = "15.0.3 (git@github.com:ziglang/zig-bootstrap.git 0ce789d0f7a4d89fdc4d9571209b6874d3e260c9)";
pub const __GNUC__ = @as(c_int, 4);
pub const __GNUC_MINOR__ = @as(c_int, 2);
pub const __GNUC_PATCHLEVEL__ = @as(c_int, 1);
pub const __GXX_ABI_VERSION = @as(c_int, 1002);
pub const __ATOMIC_RELAXED = @as(c_int, 0);
pub const __ATOMIC_CONSUME = @as(c_int, 1);
pub const __ATOMIC_ACQUIRE = @as(c_int, 2);
pub const __ATOMIC_RELEASE = @as(c_int, 3);
pub const __ATOMIC_ACQ_REL = @as(c_int, 4);
pub const __ATOMIC_SEQ_CST = @as(c_int, 5);
pub const __OPENCL_MEMORY_SCOPE_WORK_ITEM = @as(c_int, 0);
pub const __OPENCL_MEMORY_SCOPE_WORK_GROUP = @as(c_int, 1);
pub const __OPENCL_MEMORY_SCOPE_DEVICE = @as(c_int, 2);
pub const __OPENCL_MEMORY_SCOPE_ALL_SVM_DEVICES = @as(c_int, 3);
pub const __OPENCL_MEMORY_SCOPE_SUB_GROUP = @as(c_int, 4);
pub const __PRAGMA_REDEFINE_EXTNAME = @as(c_int, 1);
pub const __VERSION__ = "Clang 15.0.3 (git@github.com:ziglang/zig-bootstrap.git 0ce789d0f7a4d89fdc4d9571209b6874d3e260c9)";
pub const __OBJC_BOOL_IS_BOOL = @as(c_int, 0);
pub const __CONSTANT_CFSTRINGS__ = @as(c_int, 1);
pub const __clang_literal_encoding__ = "UTF-8";
pub const __clang_wide_literal_encoding__ = "UTF-32";
pub const __ORDER_LITTLE_ENDIAN__ = @as(c_int, 1234);
pub const __ORDER_BIG_ENDIAN__ = @as(c_int, 4321);
pub const __ORDER_PDP_ENDIAN__ = @as(c_int, 3412);
pub const __BYTE_ORDER__ = __ORDER_LITTLE_ENDIAN__;
pub const __LITTLE_ENDIAN__ = @as(c_int, 1);
pub const _LP64 = @as(c_int, 1);
pub const __LP64__ = @as(c_int, 1);
pub const __CHAR_BIT__ = @as(c_int, 8);
pub const __BOOL_WIDTH__ = @as(c_int, 8);
pub const __SHRT_WIDTH__ = @as(c_int, 16);
pub const __INT_WIDTH__ = @as(c_int, 32);
pub const __LONG_WIDTH__ = @as(c_int, 64);
pub const __LLONG_WIDTH__ = @as(c_int, 64);
pub const __BITINT_MAXWIDTH__ = @as(c_int, 128);
pub const __SCHAR_MAX__ = @as(c_int, 127);
pub const __SHRT_MAX__ = @as(c_int, 32767);
pub const __INT_MAX__ = @import("std").zig.c_translation.promoteIntLiteral(c_int, 2147483647, .decimal);
pub const __LONG_MAX__ = @import("std").zig.c_translation.promoteIntLiteral(c_long, 9223372036854775807, .decimal);
pub const __LONG_LONG_MAX__ = @as(c_longlong, 9223372036854775807);
pub const __WCHAR_MAX__ = @import("std").zig.c_translation.promoteIntLiteral(c_int, 2147483647, .decimal);
pub const __WCHAR_WIDTH__ = @as(c_int, 32);
pub const __WINT_MAX__ = @import("std").zig.c_translation.promoteIntLiteral(c_uint, 4294967295, .decimal);
pub const __WINT_WIDTH__ = @as(c_int, 32);
pub const __INTMAX_MAX__ = @import("std").zig.c_translation.promoteIntLiteral(c_long, 9223372036854775807, .decimal);
pub const __INTMAX_WIDTH__ = @as(c_int, 64);
pub const __SIZE_MAX__ = @import("std").zig.c_translation.promoteIntLiteral(c_ulong, 18446744073709551615, .decimal);
pub const __SIZE_WIDTH__ = @as(c_int, 64);
pub const __UINTMAX_MAX__ = @import("std").zig.c_translation.promoteIntLiteral(c_ulong, 18446744073709551615, .decimal);
pub const __UINTMAX_WIDTH__ = @as(c_int, 64);
pub const __PTRDIFF_MAX__ = @import("std").zig.c_translation.promoteIntLiteral(c_long, 9223372036854775807, .decimal);
pub const __PTRDIFF_WIDTH__ = @as(c_int, 64);
pub const __INTPTR_MAX__ = @import("std").zig.c_translation.promoteIntLiteral(c_long, 9223372036854775807, .decimal);
pub const __INTPTR_WIDTH__ = @as(c_int, 64);
pub const __UINTPTR_MAX__ = @import("std").zig.c_translation.promoteIntLiteral(c_ulong, 18446744073709551615, .decimal);
pub const __UINTPTR_WIDTH__ = @as(c_int, 64);
pub const __SIZEOF_DOUBLE__ = @as(c_int, 8);
pub const __SIZEOF_FLOAT__ = @as(c_int, 4);
pub const __SIZEOF_INT__ = @as(c_int, 4);
pub const __SIZEOF_LONG__ = @as(c_int, 8);
pub const __SIZEOF_LONG_DOUBLE__ = @as(c_int, 16);
pub const __SIZEOF_LONG_LONG__ = @as(c_int, 8);
pub const __SIZEOF_POINTER__ = @as(c_int, 8);
pub const __SIZEOF_SHORT__ = @as(c_int, 2);
pub const __SIZEOF_PTRDIFF_T__ = @as(c_int, 8);
pub const __SIZEOF_SIZE_T__ = @as(c_int, 8);
pub const __SIZEOF_WCHAR_T__ = @as(c_int, 4);
pub const __SIZEOF_WINT_T__ = @as(c_int, 4);
pub const __SIZEOF_INT128__ = @as(c_int, 16);
pub const __INTMAX_TYPE__ = c_long;
pub const __INTMAX_FMTd__ = "ld";
pub const __INTMAX_FMTi__ = "li";
pub const __UINTMAX_TYPE__ = c_ulong;
pub const __UINTMAX_FMTo__ = "lo";
pub const __UINTMAX_FMTu__ = "lu";
pub const __UINTMAX_FMTx__ = "lx";
pub const __UINTMAX_FMTX__ = "lX";
pub const __PTRDIFF_TYPE__ = c_long;
pub const __PTRDIFF_FMTd__ = "ld";
pub const __PTRDIFF_FMTi__ = "li";
pub const __INTPTR_TYPE__ = c_long;
pub const __INTPTR_FMTd__ = "ld";
pub const __INTPTR_FMTi__ = "li";
pub const __SIZE_TYPE__ = c_ulong;
pub const __SIZE_FMTo__ = "lo";
pub const __SIZE_FMTu__ = "lu";
pub const __SIZE_FMTx__ = "lx";
pub const __SIZE_FMTX__ = "lX";
pub const __WCHAR_TYPE__ = c_int;
pub const __WINT_TYPE__ = c_uint;
pub const __SIG_ATOMIC_MAX__ = @import("std").zig.c_translation.promoteIntLiteral(c_int, 2147483647, .decimal);
pub const __SIG_ATOMIC_WIDTH__ = @as(c_int, 32);
pub const __CHAR16_TYPE__ = c_ushort;
pub const __CHAR32_TYPE__ = c_uint;
pub const __UINTPTR_TYPE__ = c_ulong;
pub const __UINTPTR_FMTo__ = "lo";
pub const __UINTPTR_FMTu__ = "lu";
pub const __UINTPTR_FMTx__ = "lx";
pub const __UINTPTR_FMTX__ = "lX";
pub const __FLT16_HAS_DENORM__ = @as(c_int, 1);
pub const __FLT16_DIG__ = @as(c_int, 3);
pub const __FLT16_DECIMAL_DIG__ = @as(c_int, 5);
pub const __FLT16_HAS_INFINITY__ = @as(c_int, 1);
pub const __FLT16_HAS_QUIET_NAN__ = @as(c_int, 1);
pub const __FLT16_MANT_DIG__ = @as(c_int, 11);
pub const __FLT16_MAX_10_EXP__ = @as(c_int, 4);
pub const __FLT16_MAX_EXP__ = @as(c_int, 16);
pub const __FLT16_MIN_10_EXP__ = -@as(c_int, 4);
pub const __FLT16_MIN_EXP__ = -@as(c_int, 13);
pub const __FLT_DENORM_MIN__ = @as(f32, 1.40129846e-45);
pub const __FLT_HAS_DENORM__ = @as(c_int, 1);
pub const __FLT_DIG__ = @as(c_int, 6);
pub const __FLT_DECIMAL_DIG__ = @as(c_int, 9);
pub const __FLT_EPSILON__ = @as(f32, 1.19209290e-7);
pub const __FLT_HAS_INFINITY__ = @as(c_int, 1);
pub const __FLT_HAS_QUIET_NAN__ = @as(c_int, 1);
pub const __FLT_MANT_DIG__ = @as(c_int, 24);
pub const __FLT_MAX_10_EXP__ = @as(c_int, 38);
pub const __FLT_MAX_EXP__ = @as(c_int, 128);
pub const __FLT_MAX__ = @as(f32, 3.40282347e+38);
pub const __FLT_MIN_10_EXP__ = -@as(c_int, 37);
pub const __FLT_MIN_EXP__ = -@as(c_int, 125);
pub const __FLT_MIN__ = @as(f32, 1.17549435e-38);
pub const __DBL_DENORM_MIN__ = @as(f64, 4.9406564584124654e-324);
pub const __DBL_HAS_DENORM__ = @as(c_int, 1);
pub const __DBL_DIG__ = @as(c_int, 15);
pub const __DBL_DECIMAL_DIG__ = @as(c_int, 17);
pub const __DBL_EPSILON__ = @as(f64, 2.2204460492503131e-16);
pub const __DBL_HAS_INFINITY__ = @as(c_int, 1);
pub const __DBL_HAS_QUIET_NAN__ = @as(c_int, 1);
pub const __DBL_MANT_DIG__ = @as(c_int, 53);
pub const __DBL_MAX_10_EXP__ = @as(c_int, 308);
pub const __DBL_MAX_EXP__ = @as(c_int, 1024);
pub const __DBL_MAX__ = @as(f64, 1.7976931348623157e+308);
pub const __DBL_MIN_10_EXP__ = -@as(c_int, 307);
pub const __DBL_MIN_EXP__ = -@as(c_int, 1021);
pub const __DBL_MIN__ = @as(f64, 2.2250738585072014e-308);
pub const __LDBL_DENORM_MIN__ = @as(c_longdouble, 3.64519953188247460253e-4951);
pub const __LDBL_HAS_DENORM__ = @as(c_int, 1);
pub const __LDBL_DIG__ = @as(c_int, 18);
pub const __LDBL_DECIMAL_DIG__ = @as(c_int, 21);
pub const __LDBL_EPSILON__ = @as(c_longdouble, 1.08420217248550443401e-19);
pub const __LDBL_HAS_INFINITY__ = @as(c_int, 1);
pub const __LDBL_HAS_QUIET_NAN__ = @as(c_int, 1);
pub const __LDBL_MANT_DIG__ = @as(c_int, 64);
pub const __LDBL_MAX_10_EXP__ = @as(c_int, 4932);
pub const __LDBL_MAX_EXP__ = @as(c_int, 16384);
pub const __LDBL_MAX__ = @as(c_longdouble, 1.18973149535723176502e+4932);
pub const __LDBL_MIN_10_EXP__ = -@as(c_int, 4931);
pub const __LDBL_MIN_EXP__ = -@as(c_int, 16381);
pub const __LDBL_MIN__ = @as(c_longdouble, 3.36210314311209350626e-4932);
pub const __POINTER_WIDTH__ = @as(c_int, 64);
pub const __BIGGEST_ALIGNMENT__ = @as(c_int, 16);
pub const __WINT_UNSIGNED__ = @as(c_int, 1);
pub const __INT8_TYPE__ = i8;
pub const __INT8_FMTd__ = "hhd";
pub const __INT8_FMTi__ = "hhi";
pub const __INT8_C_SUFFIX__ = "";
pub const __INT16_TYPE__ = c_short;
pub const __INT16_FMTd__ = "hd";
pub const __INT16_FMTi__ = "hi";
pub const __INT16_C_SUFFIX__ = "";
pub const __INT32_TYPE__ = c_int;
pub const __INT32_FMTd__ = "d";
pub const __INT32_FMTi__ = "i";
pub const __INT32_C_SUFFIX__ = "";
pub const __INT64_TYPE__ = c_long;
pub const __INT64_FMTd__ = "ld";
pub const __INT64_FMTi__ = "li";
pub const __UINT8_TYPE__ = u8;
pub const __UINT8_FMTo__ = "hho";
pub const __UINT8_FMTu__ = "hhu";
pub const __UINT8_FMTx__ = "hhx";
pub const __UINT8_FMTX__ = "hhX";
pub const __UINT8_C_SUFFIX__ = "";
pub const __UINT8_MAX__ = @as(c_int, 255);
pub const __INT8_MAX__ = @as(c_int, 127);
pub const __UINT16_TYPE__ = c_ushort;
pub const __UINT16_FMTo__ = "ho";
pub const __UINT16_FMTu__ = "hu";
pub const __UINT16_FMTx__ = "hx";
pub const __UINT16_FMTX__ = "hX";
pub const __UINT16_C_SUFFIX__ = "";
pub const __UINT16_MAX__ = @import("std").zig.c_translation.promoteIntLiteral(c_int, 65535, .decimal);
pub const __INT16_MAX__ = @as(c_int, 32767);
pub const __UINT32_TYPE__ = c_uint;
pub const __UINT32_FMTo__ = "o";
pub const __UINT32_FMTu__ = "u";
pub const __UINT32_FMTx__ = "x";
pub const __UINT32_FMTX__ = "X";
pub const __UINT32_MAX__ = @import("std").zig.c_translation.promoteIntLiteral(c_uint, 4294967295, .decimal);
pub const __INT32_MAX__ = @import("std").zig.c_translation.promoteIntLiteral(c_int, 2147483647, .decimal);
pub const __UINT64_TYPE__ = c_ulong;
pub const __UINT64_FMTo__ = "lo";
pub const __UINT64_FMTu__ = "lu";
pub const __UINT64_FMTx__ = "lx";
pub const __UINT64_FMTX__ = "lX";
pub const __UINT64_MAX__ = @import("std").zig.c_translation.promoteIntLiteral(c_ulong, 18446744073709551615, .decimal);
pub const __INT64_MAX__ = @import("std").zig.c_translation.promoteIntLiteral(c_long, 9223372036854775807, .decimal);
pub const __INT_LEAST8_TYPE__ = i8;
pub const __INT_LEAST8_MAX__ = @as(c_int, 127);
pub const __INT_LEAST8_WIDTH__ = @as(c_int, 8);
pub const __INT_LEAST8_FMTd__ = "hhd";
pub const __INT_LEAST8_FMTi__ = "hhi";
pub const __UINT_LEAST8_TYPE__ = u8;
pub const __UINT_LEAST8_MAX__ = @as(c_int, 255);
pub const __UINT_LEAST8_FMTo__ = "hho";
pub const __UINT_LEAST8_FMTu__ = "hhu";
pub const __UINT_LEAST8_FMTx__ = "hhx";
pub const __UINT_LEAST8_FMTX__ = "hhX";
pub const __INT_LEAST16_TYPE__ = c_short;
pub const __INT_LEAST16_MAX__ = @as(c_int, 32767);
pub const __INT_LEAST16_WIDTH__ = @as(c_int, 16);
pub const __INT_LEAST16_FMTd__ = "hd";
pub const __INT_LEAST16_FMTi__ = "hi";
pub const __UINT_LEAST16_TYPE__ = c_ushort;
pub const __UINT_LEAST16_MAX__ = @import("std").zig.c_translation.promoteIntLiteral(c_int, 65535, .decimal);
pub const __UINT_LEAST16_FMTo__ = "ho";
pub const __UINT_LEAST16_FMTu__ = "hu";
pub const __UINT_LEAST16_FMTx__ = "hx";
pub const __UINT_LEAST16_FMTX__ = "hX";
pub const __INT_LEAST32_TYPE__ = c_int;
pub const __INT_LEAST32_MAX__ = @import("std").zig.c_translation.promoteIntLiteral(c_int, 2147483647, .decimal);
pub const __INT_LEAST32_WIDTH__ = @as(c_int, 32);
pub const __INT_LEAST32_FMTd__ = "d";
pub const __INT_LEAST32_FMTi__ = "i";
pub const __UINT_LEAST32_TYPE__ = c_uint;
pub const __UINT_LEAST32_MAX__ = @import("std").zig.c_translation.promoteIntLiteral(c_uint, 4294967295, .decimal);
pub const __UINT_LEAST32_FMTo__ = "o";
pub const __UINT_LEAST32_FMTu__ = "u";
pub const __UINT_LEAST32_FMTx__ = "x";
pub const __UINT_LEAST32_FMTX__ = "X";
pub const __INT_LEAST64_TYPE__ = c_long;
pub const __INT_LEAST64_MAX__ = @import("std").zig.c_translation.promoteIntLiteral(c_long, 9223372036854775807, .decimal);
pub const __INT_LEAST64_WIDTH__ = @as(c_int, 64);
pub const __INT_LEAST64_FMTd__ = "ld";
pub const __INT_LEAST64_FMTi__ = "li";
pub const __UINT_LEAST64_TYPE__ = c_ulong;
pub const __UINT_LEAST64_MAX__ = @import("std").zig.c_translation.promoteIntLiteral(c_ulong, 18446744073709551615, .decimal);
pub const __UINT_LEAST64_FMTo__ = "lo";
pub const __UINT_LEAST64_FMTu__ = "lu";
pub const __UINT_LEAST64_FMTx__ = "lx";
pub const __UINT_LEAST64_FMTX__ = "lX";
pub const __INT_FAST8_TYPE__ = i8;
pub const __INT_FAST8_MAX__ = @as(c_int, 127);
pub const __INT_FAST8_WIDTH__ = @as(c_int, 8);
pub const __INT_FAST8_FMTd__ = "hhd";
pub const __INT_FAST8_FMTi__ = "hhi";
pub const __UINT_FAST8_TYPE__ = u8;
pub const __UINT_FAST8_MAX__ = @as(c_int, 255);
pub const __UINT_FAST8_FMTo__ = "hho";
pub const __UINT_FAST8_FMTu__ = "hhu";
pub const __UINT_FAST8_FMTx__ = "hhx";
pub const __UINT_FAST8_FMTX__ = "hhX";
pub const __INT_FAST16_TYPE__ = c_short;
pub const __INT_FAST16_MAX__ = @as(c_int, 32767);
pub const __INT_FAST16_WIDTH__ = @as(c_int, 16);
pub const __INT_FAST16_FMTd__ = "hd";
pub const __INT_FAST16_FMTi__ = "hi";
pub const __UINT_FAST16_TYPE__ = c_ushort;
pub const __UINT_FAST16_MAX__ = @import("std").zig.c_translation.promoteIntLiteral(c_int, 65535, .decimal);
pub const __UINT_FAST16_FMTo__ = "ho";
pub const __UINT_FAST16_FMTu__ = "hu";
pub const __UINT_FAST16_FMTx__ = "hx";
pub const __UINT_FAST16_FMTX__ = "hX";
pub const __INT_FAST32_TYPE__ = c_int;
pub const __INT_FAST32_MAX__ = @import("std").zig.c_translation.promoteIntLiteral(c_int, 2147483647, .decimal);
pub const __INT_FAST32_WIDTH__ = @as(c_int, 32);
pub const __INT_FAST32_FMTd__ = "d";
pub const __INT_FAST32_FMTi__ = "i";
pub const __UINT_FAST32_TYPE__ = c_uint;
pub const __UINT_FAST32_MAX__ = @import("std").zig.c_translation.promoteIntLiteral(c_uint, 4294967295, .decimal);
pub const __UINT_FAST32_FMTo__ = "o";
pub const __UINT_FAST32_FMTu__ = "u";
pub const __UINT_FAST32_FMTx__ = "x";
pub const __UINT_FAST32_FMTX__ = "X";
pub const __INT_FAST64_TYPE__ = c_long;
pub const __INT_FAST64_MAX__ = @import("std").zig.c_translation.promoteIntLiteral(c_long, 9223372036854775807, .decimal);
pub const __INT_FAST64_WIDTH__ = @as(c_int, 64);
pub const __INT_FAST64_FMTd__ = "ld";
pub const __INT_FAST64_FMTi__ = "li";
pub const __UINT_FAST64_TYPE__ = c_ulong;
pub const __UINT_FAST64_MAX__ = @import("std").zig.c_translation.promoteIntLiteral(c_ulong, 18446744073709551615, .decimal);
pub const __UINT_FAST64_FMTo__ = "lo";
pub const __UINT_FAST64_FMTu__ = "lu";
pub const __UINT_FAST64_FMTx__ = "lx";
pub const __UINT_FAST64_FMTX__ = "lX";
pub const __USER_LABEL_PREFIX__ = "";
pub const __FINITE_MATH_ONLY__ = @as(c_int, 0);
pub const __GNUC_STDC_INLINE__ = @as(c_int, 1);
pub const __GCC_ATOMIC_TEST_AND_SET_TRUEVAL = @as(c_int, 1);
pub const __CLANG_ATOMIC_BOOL_LOCK_FREE = @as(c_int, 2);
pub const __CLANG_ATOMIC_CHAR_LOCK_FREE = @as(c_int, 2);
pub const __CLANG_ATOMIC_CHAR16_T_LOCK_FREE = @as(c_int, 2);
pub const __CLANG_ATOMIC_CHAR32_T_LOCK_FREE = @as(c_int, 2);
pub const __CLANG_ATOMIC_WCHAR_T_LOCK_FREE = @as(c_int, 2);
pub const __CLANG_ATOMIC_SHORT_LOCK_FREE = @as(c_int, 2);
pub const __CLANG_ATOMIC_INT_LOCK_FREE = @as(c_int, 2);
pub const __CLANG_ATOMIC_LONG_LOCK_FREE = @as(c_int, 2);
pub const __CLANG_ATOMIC_LLONG_LOCK_FREE = @as(c_int, 2);
pub const __CLANG_ATOMIC_POINTER_LOCK_FREE = @as(c_int, 2);
pub const __GCC_ATOMIC_BOOL_LOCK_FREE = @as(c_int, 2);
pub const __GCC_ATOMIC_CHAR_LOCK_FREE = @as(c_int, 2);
pub const __GCC_ATOMIC_CHAR16_T_LOCK_FREE = @as(c_int, 2);
pub const __GCC_ATOMIC_CHAR32_T_LOCK_FREE = @as(c_int, 2);
pub const __GCC_ATOMIC_WCHAR_T_LOCK_FREE = @as(c_int, 2);
pub const __GCC_ATOMIC_SHORT_LOCK_FREE = @as(c_int, 2);
pub const __GCC_ATOMIC_INT_LOCK_FREE = @as(c_int, 2);
pub const __GCC_ATOMIC_LONG_LOCK_FREE = @as(c_int, 2);
pub const __GCC_ATOMIC_LLONG_LOCK_FREE = @as(c_int, 2);
pub const __GCC_ATOMIC_POINTER_LOCK_FREE = @as(c_int, 2);
pub const __NO_INLINE__ = @as(c_int, 1);
pub const __PIC__ = @as(c_int, 2);
pub const __pic__ = @as(c_int, 2);
pub const __FLT_RADIX__ = @as(c_int, 2);
pub const __DECIMAL_DIG__ = __LDBL_DECIMAL_DIG__;
pub const __SSP_STRONG__ = @as(c_int, 2);
pub const __GCC_ASM_FLAG_OUTPUTS__ = @as(c_int, 1);
pub const __code_model_small__ = @as(c_int, 1);
pub const __amd64__ = @as(c_int, 1);
pub const __amd64 = @as(c_int, 1);
pub const __x86_64 = @as(c_int, 1);
pub const __x86_64__ = @as(c_int, 1);
pub const __SEG_GS = @as(c_int, 1);
pub const __SEG_FS = @as(c_int, 1);
pub const __k8 = @as(c_int, 1);
pub const __k8__ = @as(c_int, 1);
pub const __tune_k8__ = @as(c_int, 1);
pub const __REGISTER_PREFIX__ = "";
pub const __NO_MATH_INLINES = @as(c_int, 1);
pub const __AES__ = @as(c_int, 1);
pub const __VAES__ = @as(c_int, 1);
pub const __PCLMUL__ = @as(c_int, 1);
pub const __VPCLMULQDQ__ = @as(c_int, 1);
pub const __LAHF_SAHF__ = @as(c_int, 1);
pub const __LZCNT__ = @as(c_int, 1);
pub const __RDRND__ = @as(c_int, 1);
pub const __FSGSBASE__ = @as(c_int, 1);
pub const __BMI__ = @as(c_int, 1);
pub const __BMI2__ = @as(c_int, 1);
pub const __POPCNT__ = @as(c_int, 1);
pub const __PRFCHW__ = @as(c_int, 1);
pub const __RDSEED__ = @as(c_int, 1);
pub const __ADX__ = @as(c_int, 1);
pub const __MOVBE__ = @as(c_int, 1);
pub const __FMA__ = @as(c_int, 1);
pub const __F16C__ = @as(c_int, 1);
pub const __GFNI__ = @as(c_int, 1);
pub const __AVX512CD__ = @as(c_int, 1);
pub const __AVX512VPOPCNTDQ__ = @as(c_int, 1);
pub const __AVX512VNNI__ = @as(c_int, 1);
pub const __AVX512DQ__ = @as(c_int, 1);
pub const __AVX512BITALG__ = @as(c_int, 1);
pub const __AVX512BW__ = @as(c_int, 1);
pub const __AVX512VL__ = @as(c_int, 1);
pub const __AVX512VBMI__ = @as(c_int, 1);
pub const __AVX512VBMI2__ = @as(c_int, 1);
pub const __AVX512IFMA__ = @as(c_int, 1);
pub const __AVX512VP2INTERSECT__ = @as(c_int, 1);
pub const __SHA__ = @as(c_int, 1);
pub const __FXSR__ = @as(c_int, 1);
pub const __XSAVE__ = @as(c_int, 1);
pub const __XSAVEOPT__ = @as(c_int, 1);
pub const __XSAVEC__ = @as(c_int, 1);
pub const __XSAVES__ = @as(c_int, 1);
pub const __CLFLUSHOPT__ = @as(c_int, 1);
pub const __CLWB__ = @as(c_int, 1);
pub const __RDPID__ = @as(c_int, 1);
pub const __INVPCID__ = @as(c_int, 1);
pub const __AVX512F__ = @as(c_int, 1);
pub const __AVX2__ = @as(c_int, 1);
pub const __AVX__ = @as(c_int, 1);
pub const __SSE4_2__ = @as(c_int, 1);
pub const __SSE4_1__ = @as(c_int, 1);
pub const __SSSE3__ = @as(c_int, 1);
pub const __SSE3__ = @as(c_int, 1);
pub const __SSE2__ = @as(c_int, 1);
pub const __SSE2_MATH__ = @as(c_int, 1);
pub const __SSE__ = @as(c_int, 1);
pub const __SSE_MATH__ = @as(c_int, 1);
pub const __MMX__ = @as(c_int, 1);
pub const __GCC_HAVE_SYNC_COMPARE_AND_SWAP_1 = @as(c_int, 1);
pub const __GCC_HAVE_SYNC_COMPARE_AND_SWAP_2 = @as(c_int, 1);
pub const __GCC_HAVE_SYNC_COMPARE_AND_SWAP_4 = @as(c_int, 1);
pub const __GCC_HAVE_SYNC_COMPARE_AND_SWAP_8 = @as(c_int, 1);
pub const __GCC_HAVE_SYNC_COMPARE_AND_SWAP_16 = @as(c_int, 1);
pub const __SIZEOF_FLOAT128__ = @as(c_int, 16);
pub const unix = @as(c_int, 1);
pub const __unix = @as(c_int, 1);
pub const __unix__ = @as(c_int, 1);
pub const linux = @as(c_int, 1);
pub const __linux = @as(c_int, 1);
pub const __linux__ = @as(c_int, 1);
pub const __ELF__ = @as(c_int, 1);
pub const __gnu_linux__ = @as(c_int, 1);
pub const __FLOAT128__ = @as(c_int, 1);
pub const __STDC__ = @as(c_int, 1);
pub const __STDC_HOSTED__ = @as(c_int, 1);
pub const __STDC_VERSION__ = @as(c_long, 201710);
pub const __STDC_UTF_16__ = @as(c_int, 1);
pub const __STDC_UTF_32__ = @as(c_int, 1);
pub const __GLIBC_MINOR__ = @as(c_int, 36);
pub const _DEBUG = @as(c_int, 1);
pub const __GCC_HAVE_DWARF2_CFI_ASM = @as(c_int, 1);
pub const __ELL_UTIL_H = "";
pub const _STRING_H = @as(c_int, 1);
pub const __GLIBC_INTERNAL_STARTING_HEADER_IMPLEMENTATION = "";
pub const _FEATURES_H = @as(c_int, 1);
pub const __KERNEL_STRICT_NAMES = "";
pub inline fn __GNUC_PREREQ(maj: anytype, min: anytype) @TypeOf(((__GNUC__ << @as(c_int, 16)) + __GNUC_MINOR__) >= ((maj << @as(c_int, 16)) + min)) {
    return ((__GNUC__ << @as(c_int, 16)) + __GNUC_MINOR__) >= ((maj << @as(c_int, 16)) + min);
}
pub inline fn __glibc_clang_prereq(maj: anytype, min: anytype) @TypeOf(((__clang_major__ << @as(c_int, 16)) + __clang_minor__) >= ((maj << @as(c_int, 16)) + min)) {
    return ((__clang_major__ << @as(c_int, 16)) + __clang_minor__) >= ((maj << @as(c_int, 16)) + min);
}
pub const _DEFAULT_SOURCE = @as(c_int, 1);
pub const __GLIBC_USE_ISOC2X = @as(c_int, 0);
pub const __USE_ISOC11 = @as(c_int, 1);
pub const __USE_ISOC99 = @as(c_int, 1);
pub const __USE_ISOC95 = @as(c_int, 1);
pub const __USE_POSIX_IMPLICITLY = @as(c_int, 1);
pub const _POSIX_SOURCE = @as(c_int, 1);
pub const _POSIX_C_SOURCE = @as(c_long, 200809);
pub const __USE_POSIX = @as(c_int, 1);
pub const __USE_POSIX2 = @as(c_int, 1);
pub const __USE_POSIX199309 = @as(c_int, 1);
pub const __USE_POSIX199506 = @as(c_int, 1);
pub const __USE_XOPEN2K = @as(c_int, 1);
pub const __USE_XOPEN2K8 = @as(c_int, 1);
pub const _ATFILE_SOURCE = @as(c_int, 1);
pub const __WORDSIZE = @as(c_int, 64);
pub const __WORDSIZE_TIME64_COMPAT32 = @as(c_int, 1);
pub const __SYSCALL_WORDSIZE = @as(c_int, 64);
pub const __TIMESIZE = __WORDSIZE;
pub const __USE_MISC = @as(c_int, 1);
pub const __USE_ATFILE = @as(c_int, 1);
pub const __USE_FORTIFY_LEVEL = @as(c_int, 0);
pub const __GLIBC_USE_DEPRECATED_GETS = @as(c_int, 0);
pub const __GLIBC_USE_DEPRECATED_SCANF = @as(c_int, 0);
pub const _STDC_PREDEF_H = @as(c_int, 1);
pub const __STDC_IEC_559__ = @as(c_int, 1);
pub const __STDC_IEC_60559_BFP__ = @as(c_long, 201404);
pub const __STDC_IEC_559_COMPLEX__ = @as(c_int, 1);
pub const __STDC_IEC_60559_COMPLEX__ = @as(c_long, 201404);
pub const __STDC_ISO_10646__ = @as(c_long, 201706);
pub const __GNU_LIBRARY__ = @as(c_int, 6);
pub const __GLIBC__ = @as(c_int, 2);
pub inline fn __GLIBC_PREREQ(maj: anytype, min: anytype) @TypeOf(((__GLIBC__ << @as(c_int, 16)) + __GLIBC_MINOR__) >= ((maj << @as(c_int, 16)) + min)) {
    return ((__GLIBC__ << @as(c_int, 16)) + __GLIBC_MINOR__) >= ((maj << @as(c_int, 16)) + min);
}
pub const _SYS_CDEFS_H = @as(c_int, 1);
pub inline fn __glibc_has_builtin(name: anytype) @TypeOf(__has_builtin(name)) {
    return __has_builtin(name);
}
pub const __LEAF = "";
pub const __LEAF_ATTR = "";
pub inline fn __P(args: anytype) @TypeOf(args) {
    return args;
}
pub inline fn __PMT(args: anytype) @TypeOf(args) {
    return args;
}
pub const __ptr_t = ?*anyopaque;
pub const __BEGIN_DECLS = "";
pub const __END_DECLS = "";
pub inline fn __bos(ptr: anytype) @TypeOf(__builtin_object_size(ptr, __USE_FORTIFY_LEVEL > @as(c_int, 1))) {
    return __builtin_object_size(ptr, __USE_FORTIFY_LEVEL > @as(c_int, 1));
}
pub inline fn __bos0(ptr: anytype) @TypeOf(__builtin_object_size(ptr, @as(c_int, 0))) {
    return __builtin_object_size(ptr, @as(c_int, 0));
}
pub inline fn __glibc_objsize0(__o: anytype) @TypeOf(__bos0(__o)) {
    return __bos0(__o);
}
pub inline fn __glibc_objsize(__o: anytype) @TypeOf(__bos(__o)) {
    return __bos(__o);
}
pub inline fn __glibc_safe_len_cond(__l: anytype, __s: anytype, __osz: anytype) @TypeOf(__l <= @import("std").zig.c_translation.MacroArithmetic.div(__osz, __s)) {
    return __l <= @import("std").zig.c_translation.MacroArithmetic.div(__osz, __s);
}
pub inline fn __glibc_safe_or_unknown_len(__l: anytype, __s: anytype, __osz: anytype) @TypeOf(((__builtin_constant_p(__osz) != 0) and (__osz == (__SIZE_TYPE__ - @as(c_int, 1)))) or (((__glibc_unsigned_or_positive(__l) != 0) and (__builtin_constant_p(__glibc_safe_len_cond(__SIZE_TYPE__(__l), __s, __osz)) != 0)) and (__glibc_safe_len_cond(__SIZE_TYPE__(__l), __s, __osz) != 0))) {
    return ((__builtin_constant_p(__osz) != 0) and (__osz == (__SIZE_TYPE__ - @as(c_int, 1)))) or (((__glibc_unsigned_or_positive(__l) != 0) and (__builtin_constant_p(__glibc_safe_len_cond(__SIZE_TYPE__(__l), __s, __osz)) != 0)) and (__glibc_safe_len_cond(__SIZE_TYPE__(__l), __s, __osz) != 0));
}
pub inline fn __glibc_unsafe_len(__l: anytype, __s: anytype, __osz: anytype) @TypeOf(((__glibc_unsigned_or_positive(__l) != 0) and (__builtin_constant_p(__glibc_safe_len_cond(__SIZE_TYPE__(__l), __s, __osz)) != 0)) and !(__glibc_safe_len_cond(__SIZE_TYPE__(__l), __s, __osz) != 0)) {
    return ((__glibc_unsigned_or_positive(__l) != 0) and (__builtin_constant_p(__glibc_safe_len_cond(__SIZE_TYPE__(__l), __s, __osz)) != 0)) and !(__glibc_safe_len_cond(__SIZE_TYPE__(__l), __s, __osz) != 0);
}
pub const __glibc_c99_flexarr_available = @as(c_int, 1);
pub inline fn __ASMNAME(cname: anytype) @TypeOf(__ASMNAME2(__USER_LABEL_PREFIX__, cname)) {
    return __ASMNAME2(__USER_LABEL_PREFIX__, cname);
}
pub inline fn __nonnull(params: anytype) @TypeOf(__attribute_nonnull__(params)) {
    return __attribute_nonnull__(params);
}
pub const __wur = "";
pub const __fortify_function = __extern_always_inline ++ __attribute_artificial__;
pub inline fn __glibc_unlikely(cond: anytype) @TypeOf(__builtin_expect(cond, @as(c_int, 0))) {
    return __builtin_expect(cond, @as(c_int, 0));
}
pub inline fn __glibc_likely(cond: anytype) @TypeOf(__builtin_expect(cond, @as(c_int, 1))) {
    return __builtin_expect(cond, @as(c_int, 1));
}
pub const __attribute_nonstring__ = "";
pub const __LDOUBLE_REDIRECTS_TO_FLOAT128_ABI = @as(c_int, 0);
pub inline fn __LDBL_REDIR1(name: anytype, proto: anytype, alias: anytype) @TypeOf(name ++ proto) {
    _ = @TypeOf(alias);
    return name ++ proto;
}
pub inline fn __LDBL_REDIR(name: anytype, proto: anytype) @TypeOf(name ++ proto) {
    return name ++ proto;
}
pub inline fn __LDBL_REDIR1_NTH(name: anytype, proto: anytype, alias: anytype) @TypeOf(name ++ proto ++ __THROW) {
    _ = @TypeOf(alias);
    return name ++ proto ++ __THROW;
}
pub inline fn __LDBL_REDIR_NTH(name: anytype, proto: anytype) @TypeOf(name ++ proto ++ __THROW) {
    return name ++ proto ++ __THROW;
}
pub inline fn __REDIRECT_LDBL(name: anytype, proto: anytype, alias: anytype) @TypeOf(__REDIRECT(name, proto, alias)) {
    return __REDIRECT(name, proto, alias);
}
pub inline fn __REDIRECT_NTH_LDBL(name: anytype, proto: anytype, alias: anytype) @TypeOf(__REDIRECT_NTH(name, proto, alias)) {
    return __REDIRECT_NTH(name, proto, alias);
}
pub const __HAVE_GENERIC_SELECTION = @as(c_int, 1);
pub const __attr_dealloc_free = "";
pub const __stub___compat_bdflush = "";
pub const __stub_chflags = "";
pub const __stub_fchflags = "";
pub const __stub_gtty = "";
pub const __stub_revoke = "";
pub const __stub_setlogin = "";
pub const __stub_sigreturn = "";
pub const __stub_stty = "";
pub const __GLIBC_USE_LIB_EXT2 = @as(c_int, 0);
pub const __GLIBC_USE_IEC_60559_BFP_EXT = @as(c_int, 0);
pub const __GLIBC_USE_IEC_60559_BFP_EXT_C2X = @as(c_int, 0);
pub const __GLIBC_USE_IEC_60559_EXT = @as(c_int, 0);
pub const __GLIBC_USE_IEC_60559_FUNCS_EXT = @as(c_int, 0);
pub const __GLIBC_USE_IEC_60559_FUNCS_EXT_C2X = @as(c_int, 0);
pub const __GLIBC_USE_IEC_60559_TYPES_EXT = @as(c_int, 0);
pub const __need_size_t = "";
pub const __need_NULL = "";
pub const _SIZE_T = "";
pub const NULL = @import("std").zig.c_translation.cast(?*anyopaque, @as(c_int, 0));
pub const _BITS_TYPES_LOCALE_T_H = @as(c_int, 1);
pub const _BITS_TYPES___LOCALE_T_H = @as(c_int, 1);
pub const _STRINGS_H = @as(c_int, 1);
pub const __STDBOOL_H = "";
pub const __bool_true_false_are_defined = @as(c_int, 1);
pub const @"bool" = bool;
pub const @"true" = @as(c_int, 1);
pub const @"false" = @as(c_int, 0);
pub const __STDARG_H = "";
pub const _VA_LIST = "";
pub const __GNUC_VA_LIST = @as(c_int, 1);
pub const __CLANG_INTTYPES_H = "";
pub const _INTTYPES_H = @as(c_int, 1);
pub const __CLANG_STDINT_H = "";
pub const _STDINT_H = @as(c_int, 1);
pub const _BITS_TYPES_H = @as(c_int, 1);
pub const __S16_TYPE = c_short;
pub const __U16_TYPE = c_ushort;
pub const __S32_TYPE = c_int;
pub const __U32_TYPE = c_uint;
pub const __SLONGWORD_TYPE = c_long;
pub const __ULONGWORD_TYPE = c_ulong;
pub const __SQUAD_TYPE = c_long;
pub const __UQUAD_TYPE = c_ulong;
pub const __SWORD_TYPE = c_long;
pub const __UWORD_TYPE = c_ulong;
pub const __SLONG32_TYPE = c_int;
pub const __ULONG32_TYPE = c_uint;
pub const __S64_TYPE = c_long;
pub const __U64_TYPE = c_ulong;
pub const _BITS_TYPESIZES_H = @as(c_int, 1);
pub const __SYSCALL_SLONG_TYPE = __SLONGWORD_TYPE;
pub const __SYSCALL_ULONG_TYPE = __ULONGWORD_TYPE;
pub const __DEV_T_TYPE = __UQUAD_TYPE;
pub const __UID_T_TYPE = __U32_TYPE;
pub const __GID_T_TYPE = __U32_TYPE;
pub const __INO_T_TYPE = __SYSCALL_ULONG_TYPE;
pub const __INO64_T_TYPE = __UQUAD_TYPE;
pub const __MODE_T_TYPE = __U32_TYPE;
pub const __NLINK_T_TYPE = __SYSCALL_ULONG_TYPE;
pub const __FSWORD_T_TYPE = __SYSCALL_SLONG_TYPE;
pub const __OFF_T_TYPE = __SYSCALL_SLONG_TYPE;
pub const __OFF64_T_TYPE = __SQUAD_TYPE;
pub const __PID_T_TYPE = __S32_TYPE;
pub const __RLIM_T_TYPE = __SYSCALL_ULONG_TYPE;
pub const __RLIM64_T_TYPE = __UQUAD_TYPE;
pub const __BLKCNT_T_TYPE = __SYSCALL_SLONG_TYPE;
pub const __BLKCNT64_T_TYPE = __SQUAD_TYPE;
pub const __FSBLKCNT_T_TYPE = __SYSCALL_ULONG_TYPE;
pub const __FSBLKCNT64_T_TYPE = __UQUAD_TYPE;
pub const __FSFILCNT_T_TYPE = __SYSCALL_ULONG_TYPE;
pub const __FSFILCNT64_T_TYPE = __UQUAD_TYPE;
pub const __ID_T_TYPE = __U32_TYPE;
pub const __CLOCK_T_TYPE = __SYSCALL_SLONG_TYPE;
pub const __TIME_T_TYPE = __SYSCALL_SLONG_TYPE;
pub const __USECONDS_T_TYPE = __U32_TYPE;
pub const __SUSECONDS_T_TYPE = __SYSCALL_SLONG_TYPE;
pub const __SUSECONDS64_T_TYPE = __SQUAD_TYPE;
pub const __DADDR_T_TYPE = __S32_TYPE;
pub const __KEY_T_TYPE = __S32_TYPE;
pub const __CLOCKID_T_TYPE = __S32_TYPE;
pub const __TIMER_T_TYPE = ?*anyopaque;
pub const __BLKSIZE_T_TYPE = __SYSCALL_SLONG_TYPE;
pub const __SSIZE_T_TYPE = __SWORD_TYPE;
pub const __CPU_MASK_TYPE = __SYSCALL_ULONG_TYPE;
pub const __OFF_T_MATCHES_OFF64_T = @as(c_int, 1);
pub const __INO_T_MATCHES_INO64_T = @as(c_int, 1);
pub const __RLIM_T_MATCHES_RLIM64_T = @as(c_int, 1);
pub const __STATFS_MATCHES_STATFS64 = @as(c_int, 1);
pub const __KERNEL_OLD_TIMEVAL_MATCHES_TIMEVAL64 = @as(c_int, 1);
pub const __FD_SETSIZE = @as(c_int, 1024);
pub const _BITS_TIME64_H = @as(c_int, 1);
pub const __TIME64_T_TYPE = __TIME_T_TYPE;
pub const _BITS_WCHAR_H = @as(c_int, 1);
pub const __WCHAR_MAX = __WCHAR_MAX__;
pub const __WCHAR_MIN = -__WCHAR_MAX - @as(c_int, 1);
pub const _BITS_STDINT_INTN_H = @as(c_int, 1);
pub const _BITS_STDINT_UINTN_H = @as(c_int, 1);
pub const __intptr_t_defined = "";
pub const __INT64_C = @import("std").zig.c_translation.Macros.L_SUFFIX;
pub const __UINT64_C = @import("std").zig.c_translation.Macros.UL_SUFFIX;
pub const INT8_MIN = -@as(c_int, 128);
pub const INT16_MIN = -@as(c_int, 32767) - @as(c_int, 1);
pub const INT32_MIN = -@import("std").zig.c_translation.promoteIntLiteral(c_int, 2147483647, .decimal) - @as(c_int, 1);
pub const INT64_MIN = -__INT64_C(@import("std").zig.c_translation.promoteIntLiteral(c_int, 9223372036854775807, .decimal)) - @as(c_int, 1);
pub const INT8_MAX = @as(c_int, 127);
pub const INT16_MAX = @as(c_int, 32767);
pub const INT32_MAX = @import("std").zig.c_translation.promoteIntLiteral(c_int, 2147483647, .decimal);
pub const INT64_MAX = __INT64_C(@import("std").zig.c_translation.promoteIntLiteral(c_int, 9223372036854775807, .decimal));
pub const UINT8_MAX = @as(c_int, 255);
pub const UINT16_MAX = @import("std").zig.c_translation.promoteIntLiteral(c_int, 65535, .decimal);
pub const UINT32_MAX = @import("std").zig.c_translation.promoteIntLiteral(c_uint, 4294967295, .decimal);
pub const UINT64_MAX = __UINT64_C(@import("std").zig.c_translation.promoteIntLiteral(c_int, 18446744073709551615, .decimal));
pub const INT_LEAST8_MIN = -@as(c_int, 128);
pub const INT_LEAST16_MIN = -@as(c_int, 32767) - @as(c_int, 1);
pub const INT_LEAST32_MIN = -@import("std").zig.c_translation.promoteIntLiteral(c_int, 2147483647, .decimal) - @as(c_int, 1);
pub const INT_LEAST64_MIN = -__INT64_C(@import("std").zig.c_translation.promoteIntLiteral(c_int, 9223372036854775807, .decimal)) - @as(c_int, 1);
pub const INT_LEAST8_MAX = @as(c_int, 127);
pub const INT_LEAST16_MAX = @as(c_int, 32767);
pub const INT_LEAST32_MAX = @import("std").zig.c_translation.promoteIntLiteral(c_int, 2147483647, .decimal);
pub const INT_LEAST64_MAX = __INT64_C(@import("std").zig.c_translation.promoteIntLiteral(c_int, 9223372036854775807, .decimal));
pub const UINT_LEAST8_MAX = @as(c_int, 255);
pub const UINT_LEAST16_MAX = @import("std").zig.c_translation.promoteIntLiteral(c_int, 65535, .decimal);
pub const UINT_LEAST32_MAX = @import("std").zig.c_translation.promoteIntLiteral(c_uint, 4294967295, .decimal);
pub const UINT_LEAST64_MAX = __UINT64_C(@import("std").zig.c_translation.promoteIntLiteral(c_int, 18446744073709551615, .decimal));
pub const INT_FAST8_MIN = -@as(c_int, 128);
pub const INT_FAST16_MIN = -@import("std").zig.c_translation.promoteIntLiteral(c_long, 9223372036854775807, .decimal) - @as(c_int, 1);
pub const INT_FAST32_MIN = -@import("std").zig.c_translation.promoteIntLiteral(c_long, 9223372036854775807, .decimal) - @as(c_int, 1);
pub const INT_FAST64_MIN = -__INT64_C(@import("std").zig.c_translation.promoteIntLiteral(c_int, 9223372036854775807, .decimal)) - @as(c_int, 1);
pub const INT_FAST8_MAX = @as(c_int, 127);
pub const INT_FAST16_MAX = @import("std").zig.c_translation.promoteIntLiteral(c_long, 9223372036854775807, .decimal);
pub const INT_FAST32_MAX = @import("std").zig.c_translation.promoteIntLiteral(c_long, 9223372036854775807, .decimal);
pub const INT_FAST64_MAX = __INT64_C(@import("std").zig.c_translation.promoteIntLiteral(c_int, 9223372036854775807, .decimal));
pub const UINT_FAST8_MAX = @as(c_int, 255);
pub const UINT_FAST16_MAX = @import("std").zig.c_translation.promoteIntLiteral(c_ulong, 18446744073709551615, .decimal);
pub const UINT_FAST32_MAX = @import("std").zig.c_translation.promoteIntLiteral(c_ulong, 18446744073709551615, .decimal);
pub const UINT_FAST64_MAX = __UINT64_C(@import("std").zig.c_translation.promoteIntLiteral(c_int, 18446744073709551615, .decimal));
pub const INTPTR_MIN = -@import("std").zig.c_translation.promoteIntLiteral(c_long, 9223372036854775807, .decimal) - @as(c_int, 1);
pub const INTPTR_MAX = @import("std").zig.c_translation.promoteIntLiteral(c_long, 9223372036854775807, .decimal);
pub const UINTPTR_MAX = @import("std").zig.c_translation.promoteIntLiteral(c_ulong, 18446744073709551615, .decimal);
pub const INTMAX_MIN = -__INT64_C(@import("std").zig.c_translation.promoteIntLiteral(c_int, 9223372036854775807, .decimal)) - @as(c_int, 1);
pub const INTMAX_MAX = __INT64_C(@import("std").zig.c_translation.promoteIntLiteral(c_int, 9223372036854775807, .decimal));
pub const UINTMAX_MAX = __UINT64_C(@import("std").zig.c_translation.promoteIntLiteral(c_int, 18446744073709551615, .decimal));
pub const PTRDIFF_MIN = -@import("std").zig.c_translation.promoteIntLiteral(c_long, 9223372036854775807, .decimal) - @as(c_int, 1);
pub const PTRDIFF_MAX = @import("std").zig.c_translation.promoteIntLiteral(c_long, 9223372036854775807, .decimal);
pub const SIG_ATOMIC_MIN = -@import("std").zig.c_translation.promoteIntLiteral(c_int, 2147483647, .decimal) - @as(c_int, 1);
pub const SIG_ATOMIC_MAX = @import("std").zig.c_translation.promoteIntLiteral(c_int, 2147483647, .decimal);
pub const SIZE_MAX = @import("std").zig.c_translation.promoteIntLiteral(c_ulong, 18446744073709551615, .decimal);
pub const WCHAR_MIN = __WCHAR_MIN;
pub const WCHAR_MAX = __WCHAR_MAX;
pub const WINT_MIN = @as(c_uint, 0);
pub const WINT_MAX = @import("std").zig.c_translation.promoteIntLiteral(c_uint, 4294967295, .decimal);
pub inline fn INT8_C(c: anytype) @TypeOf(c) {
    return c;
}
pub inline fn INT16_C(c: anytype) @TypeOf(c) {
    return c;
}
pub inline fn INT32_C(c: anytype) @TypeOf(c) {
    return c;
}
pub const INT64_C = @import("std").zig.c_translation.Macros.L_SUFFIX;
pub inline fn UINT8_C(c: anytype) @TypeOf(c) {
    return c;
}
pub inline fn UINT16_C(c: anytype) @TypeOf(c) {
    return c;
}
pub const UINT32_C = @import("std").zig.c_translation.Macros.U_SUFFIX;
pub const UINT64_C = @import("std").zig.c_translation.Macros.UL_SUFFIX;
pub const INTMAX_C = @import("std").zig.c_translation.Macros.L_SUFFIX;
pub const UINTMAX_C = @import("std").zig.c_translation.Macros.UL_SUFFIX;
pub const ____gwchar_t_defined = @as(c_int, 1);
pub const __PRI64_PREFIX = "l";
pub const __PRIPTR_PREFIX = "l";
pub const PRId8 = "d";
pub const PRId16 = "d";
pub const PRId32 = "d";
pub const PRId64 = __PRI64_PREFIX ++ "d";
pub const PRIdLEAST8 = "d";
pub const PRIdLEAST16 = "d";
pub const PRIdLEAST32 = "d";
pub const PRIdLEAST64 = __PRI64_PREFIX ++ "d";
pub const PRIdFAST8 = "d";
pub const PRIdFAST16 = __PRIPTR_PREFIX ++ "d";
pub const PRIdFAST32 = __PRIPTR_PREFIX ++ "d";
pub const PRIdFAST64 = __PRI64_PREFIX ++ "d";
pub const PRIi8 = "i";
pub const PRIi16 = "i";
pub const PRIi32 = "i";
pub const PRIi64 = __PRI64_PREFIX ++ "i";
pub const PRIiLEAST8 = "i";
pub const PRIiLEAST16 = "i";
pub const PRIiLEAST32 = "i";
pub const PRIiLEAST64 = __PRI64_PREFIX ++ "i";
pub const PRIiFAST8 = "i";
pub const PRIiFAST16 = __PRIPTR_PREFIX ++ "i";
pub const PRIiFAST32 = __PRIPTR_PREFIX ++ "i";
pub const PRIiFAST64 = __PRI64_PREFIX ++ "i";
pub const PRIo8 = "o";
pub const PRIo16 = "o";
pub const PRIo32 = "o";
pub const PRIo64 = __PRI64_PREFIX ++ "o";
pub const PRIoLEAST8 = "o";
pub const PRIoLEAST16 = "o";
pub const PRIoLEAST32 = "o";
pub const PRIoLEAST64 = __PRI64_PREFIX ++ "o";
pub const PRIoFAST8 = "o";
pub const PRIoFAST16 = __PRIPTR_PREFIX ++ "o";
pub const PRIoFAST32 = __PRIPTR_PREFIX ++ "o";
pub const PRIoFAST64 = __PRI64_PREFIX ++ "o";
pub const PRIu8 = "u";
pub const PRIu16 = "u";
pub const PRIu32 = "u";
pub const PRIu64 = __PRI64_PREFIX ++ "u";
pub const PRIuLEAST8 = "u";
pub const PRIuLEAST16 = "u";
pub const PRIuLEAST32 = "u";
pub const PRIuLEAST64 = __PRI64_PREFIX ++ "u";
pub const PRIuFAST8 = "u";
pub const PRIuFAST16 = __PRIPTR_PREFIX ++ "u";
pub const PRIuFAST32 = __PRIPTR_PREFIX ++ "u";
pub const PRIuFAST64 = __PRI64_PREFIX ++ "u";
pub const PRIx8 = "x";
pub const PRIx16 = "x";
pub const PRIx32 = "x";
pub const PRIx64 = __PRI64_PREFIX ++ "x";
pub const PRIxLEAST8 = "x";
pub const PRIxLEAST16 = "x";
pub const PRIxLEAST32 = "x";
pub const PRIxLEAST64 = __PRI64_PREFIX ++ "x";
pub const PRIxFAST8 = "x";
pub const PRIxFAST16 = __PRIPTR_PREFIX ++ "x";
pub const PRIxFAST32 = __PRIPTR_PREFIX ++ "x";
pub const PRIxFAST64 = __PRI64_PREFIX ++ "x";
pub const PRIX8 = "X";
pub const PRIX16 = "X";
pub const PRIX32 = "X";
pub const PRIX64 = __PRI64_PREFIX ++ "X";
pub const PRIXLEAST8 = "X";
pub const PRIXLEAST16 = "X";
pub const PRIXLEAST32 = "X";
pub const PRIXLEAST64 = __PRI64_PREFIX ++ "X";
pub const PRIXFAST8 = "X";
pub const PRIXFAST16 = __PRIPTR_PREFIX ++ "X";
pub const PRIXFAST32 = __PRIPTR_PREFIX ++ "X";
pub const PRIXFAST64 = __PRI64_PREFIX ++ "X";
pub const PRIdMAX = __PRI64_PREFIX ++ "d";
pub const PRIiMAX = __PRI64_PREFIX ++ "i";
pub const PRIoMAX = __PRI64_PREFIX ++ "o";
pub const PRIuMAX = __PRI64_PREFIX ++ "u";
pub const PRIxMAX = __PRI64_PREFIX ++ "x";
pub const PRIXMAX = __PRI64_PREFIX ++ "X";
pub const PRIdPTR = __PRIPTR_PREFIX ++ "d";
pub const PRIiPTR = __PRIPTR_PREFIX ++ "i";
pub const PRIoPTR = __PRIPTR_PREFIX ++ "o";
pub const PRIuPTR = __PRIPTR_PREFIX ++ "u";
pub const PRIxPTR = __PRIPTR_PREFIX ++ "x";
pub const PRIXPTR = __PRIPTR_PREFIX ++ "X";
pub const SCNd8 = "hhd";
pub const SCNd16 = "hd";
pub const SCNd32 = "d";
pub const SCNd64 = __PRI64_PREFIX ++ "d";
pub const SCNdLEAST8 = "hhd";
pub const SCNdLEAST16 = "hd";
pub const SCNdLEAST32 = "d";
pub const SCNdLEAST64 = __PRI64_PREFIX ++ "d";
pub const SCNdFAST8 = "hhd";
pub const SCNdFAST16 = __PRIPTR_PREFIX ++ "d";
pub const SCNdFAST32 = __PRIPTR_PREFIX ++ "d";
pub const SCNdFAST64 = __PRI64_PREFIX ++ "d";
pub const SCNi8 = "hhi";
pub const SCNi16 = "hi";
pub const SCNi32 = "i";
pub const SCNi64 = __PRI64_PREFIX ++ "i";
pub const SCNiLEAST8 = "hhi";
pub const SCNiLEAST16 = "hi";
pub const SCNiLEAST32 = "i";
pub const SCNiLEAST64 = __PRI64_PREFIX ++ "i";
pub const SCNiFAST8 = "hhi";
pub const SCNiFAST16 = __PRIPTR_PREFIX ++ "i";
pub const SCNiFAST32 = __PRIPTR_PREFIX ++ "i";
pub const SCNiFAST64 = __PRI64_PREFIX ++ "i";
pub const SCNu8 = "hhu";
pub const SCNu16 = "hu";
pub const SCNu32 = "u";
pub const SCNu64 = __PRI64_PREFIX ++ "u";
pub const SCNuLEAST8 = "hhu";
pub const SCNuLEAST16 = "hu";
pub const SCNuLEAST32 = "u";
pub const SCNuLEAST64 = __PRI64_PREFIX ++ "u";
pub const SCNuFAST8 = "hhu";
pub const SCNuFAST16 = __PRIPTR_PREFIX ++ "u";
pub const SCNuFAST32 = __PRIPTR_PREFIX ++ "u";
pub const SCNuFAST64 = __PRI64_PREFIX ++ "u";
pub const SCNo8 = "hho";
pub const SCNo16 = "ho";
pub const SCNo32 = "o";
pub const SCNo64 = __PRI64_PREFIX ++ "o";
pub const SCNoLEAST8 = "hho";
pub const SCNoLEAST16 = "ho";
pub const SCNoLEAST32 = "o";
pub const SCNoLEAST64 = __PRI64_PREFIX ++ "o";
pub const SCNoFAST8 = "hho";
pub const SCNoFAST16 = __PRIPTR_PREFIX ++ "o";
pub const SCNoFAST32 = __PRIPTR_PREFIX ++ "o";
pub const SCNoFAST64 = __PRI64_PREFIX ++ "o";
pub const SCNx8 = "hhx";
pub const SCNx16 = "hx";
pub const SCNx32 = "x";
pub const SCNx64 = __PRI64_PREFIX ++ "x";
pub const SCNxLEAST8 = "hhx";
pub const SCNxLEAST16 = "hx";
pub const SCNxLEAST32 = "x";
pub const SCNxLEAST64 = __PRI64_PREFIX ++ "x";
pub const SCNxFAST8 = "hhx";
pub const SCNxFAST16 = __PRIPTR_PREFIX ++ "x";
pub const SCNxFAST32 = __PRIPTR_PREFIX ++ "x";
pub const SCNxFAST64 = __PRI64_PREFIX ++ "x";
pub const SCNdMAX = __PRI64_PREFIX ++ "d";
pub const SCNiMAX = __PRI64_PREFIX ++ "i";
pub const SCNoMAX = __PRI64_PREFIX ++ "o";
pub const SCNuMAX = __PRI64_PREFIX ++ "u";
pub const SCNxMAX = __PRI64_PREFIX ++ "x";
pub const SCNdPTR = __PRIPTR_PREFIX ++ "d";
pub const SCNiPTR = __PRIPTR_PREFIX ++ "i";
pub const SCNoPTR = __PRIPTR_PREFIX ++ "o";
pub const SCNuPTR = __PRIPTR_PREFIX ++ "u";
pub const SCNxPTR = __PRIPTR_PREFIX ++ "x";
pub const _ENDIAN_H = @as(c_int, 1);
pub const _BITS_ENDIAN_H = @as(c_int, 1);
pub const __LITTLE_ENDIAN = @as(c_int, 1234);
pub const __BIG_ENDIAN = @as(c_int, 4321);
pub const __PDP_ENDIAN = @as(c_int, 3412);
pub const _BITS_ENDIANNESS_H = @as(c_int, 1);
pub const __BYTE_ORDER = __LITTLE_ENDIAN;
pub const __FLOAT_WORD_ORDER = __BYTE_ORDER;
pub inline fn __LONG_LONG_PAIR(HI: anytype, LO: anytype) @TypeOf(HI) {
    return blk: {
        _ = @TypeOf(LO);
        break :blk HI;
    };
}
pub const LITTLE_ENDIAN = __LITTLE_ENDIAN;
pub const BIG_ENDIAN = __BIG_ENDIAN;
pub const PDP_ENDIAN = __PDP_ENDIAN;
pub const BYTE_ORDER = __BYTE_ORDER;
pub const _BITS_BYTESWAP_H = @as(c_int, 1);
pub inline fn __bswap_constant_16(x: anytype) __uint16_t {
    return @import("std").zig.c_translation.cast(__uint16_t, ((x >> @as(c_int, 8)) & @as(c_int, 0xff)) | ((x & @as(c_int, 0xff)) << @as(c_int, 8)));
}
pub inline fn __bswap_constant_32(x: anytype) @TypeOf(((((x & @import("std").zig.c_translation.promoteIntLiteral(c_uint, 0xff000000, .hexadecimal)) >> @as(c_int, 24)) | ((x & @import("std").zig.c_translation.promoteIntLiteral(c_uint, 0x00ff0000, .hexadecimal)) >> @as(c_int, 8))) | ((x & @as(c_uint, 0x0000ff00)) << @as(c_int, 8))) | ((x & @as(c_uint, 0x000000ff)) << @as(c_int, 24))) {
    return ((((x & @import("std").zig.c_translation.promoteIntLiteral(c_uint, 0xff000000, .hexadecimal)) >> @as(c_int, 24)) | ((x & @import("std").zig.c_translation.promoteIntLiteral(c_uint, 0x00ff0000, .hexadecimal)) >> @as(c_int, 8))) | ((x & @as(c_uint, 0x0000ff00)) << @as(c_int, 8))) | ((x & @as(c_uint, 0x000000ff)) << @as(c_int, 24));
}
pub inline fn __bswap_constant_64(x: anytype) @TypeOf(((((((((x & @as(c_ulonglong, 0xff00000000000000)) >> @as(c_int, 56)) | ((x & @as(c_ulonglong, 0x00ff000000000000)) >> @as(c_int, 40))) | ((x & @as(c_ulonglong, 0x0000ff0000000000)) >> @as(c_int, 24))) | ((x & @as(c_ulonglong, 0x000000ff00000000)) >> @as(c_int, 8))) | ((x & @as(c_ulonglong, 0x00000000ff000000)) << @as(c_int, 8))) | ((x & @as(c_ulonglong, 0x0000000000ff0000)) << @as(c_int, 24))) | ((x & @as(c_ulonglong, 0x000000000000ff00)) << @as(c_int, 40))) | ((x & @as(c_ulonglong, 0x00000000000000ff)) << @as(c_int, 56))) {
    return ((((((((x & @as(c_ulonglong, 0xff00000000000000)) >> @as(c_int, 56)) | ((x & @as(c_ulonglong, 0x00ff000000000000)) >> @as(c_int, 40))) | ((x & @as(c_ulonglong, 0x0000ff0000000000)) >> @as(c_int, 24))) | ((x & @as(c_ulonglong, 0x000000ff00000000)) >> @as(c_int, 8))) | ((x & @as(c_ulonglong, 0x00000000ff000000)) << @as(c_int, 8))) | ((x & @as(c_ulonglong, 0x0000000000ff0000)) << @as(c_int, 24))) | ((x & @as(c_ulonglong, 0x000000000000ff00)) << @as(c_int, 40))) | ((x & @as(c_ulonglong, 0x00000000000000ff)) << @as(c_int, 56));
}
pub const _BITS_UINTN_IDENTITY_H = @as(c_int, 1);
pub inline fn htobe16(x: anytype) @TypeOf(__bswap_16(x)) {
    return __bswap_16(x);
}
pub inline fn htole16(x: anytype) @TypeOf(__uint16_identity(x)) {
    return __uint16_identity(x);
}
pub inline fn be16toh(x: anytype) @TypeOf(__bswap_16(x)) {
    return __bswap_16(x);
}
pub inline fn le16toh(x: anytype) @TypeOf(__uint16_identity(x)) {
    return __uint16_identity(x);
}
pub inline fn htobe32(x: anytype) @TypeOf(__bswap_32(x)) {
    return __bswap_32(x);
}
pub inline fn htole32(x: anytype) @TypeOf(__uint32_identity(x)) {
    return __uint32_identity(x);
}
pub inline fn be32toh(x: anytype) @TypeOf(__bswap_32(x)) {
    return __bswap_32(x);
}
pub inline fn le32toh(x: anytype) @TypeOf(__uint32_identity(x)) {
    return __uint32_identity(x);
}
pub inline fn htobe64(x: anytype) @TypeOf(__bswap_64(x)) {
    return __bswap_64(x);
}
pub inline fn htole64(x: anytype) @TypeOf(__uint64_identity(x)) {
    return __uint64_identity(x);
}
pub inline fn be64toh(x: anytype) @TypeOf(__bswap_64(x)) {
    return __bswap_64(x);
}
pub inline fn le64toh(x: anytype) @TypeOf(__uint64_identity(x)) {
    return __uint64_identity(x);
}
pub const _BYTESWAP_H = @as(c_int, 1);
pub inline fn bswap_16(x: anytype) @TypeOf(__bswap_16(x)) {
    return __bswap_16(x);
}
pub inline fn bswap_32(x: anytype) @TypeOf(__bswap_32(x)) {
    return __bswap_32(x);
}
pub inline fn bswap_64(x: anytype) @TypeOf(__bswap_64(x)) {
    return __bswap_64(x);
}
pub const _SYS_UIO_H = @as(c_int, 1);
pub const _SYS_TYPES_H = @as(c_int, 1);
pub const __u_char_defined = "";
pub const __ino_t_defined = "";
pub const __dev_t_defined = "";
pub const __gid_t_defined = "";
pub const __mode_t_defined = "";
pub const __nlink_t_defined = "";
pub const __uid_t_defined = "";
pub const __off_t_defined = "";
pub const __pid_t_defined = "";
pub const __id_t_defined = "";
pub const __ssize_t_defined = "";
pub const __daddr_t_defined = "";
pub const __key_t_defined = "";
pub const __clock_t_defined = @as(c_int, 1);
pub const __clockid_t_defined = @as(c_int, 1);
pub const __time_t_defined = @as(c_int, 1);
pub const __timer_t_defined = @as(c_int, 1);
pub const __BIT_TYPES_DEFINED__ = @as(c_int, 1);
pub const _SYS_SELECT_H = @as(c_int, 1);
pub inline fn __FD_ISSET(d: anytype, s: anytype) @TypeOf((__FDS_BITS(s)[@intCast(usize, __FD_ELT(d))] & __FD_MASK(d)) != @as(c_int, 0)) {
    return (__FDS_BITS(s)[@intCast(usize, __FD_ELT(d))] & __FD_MASK(d)) != @as(c_int, 0);
}
pub const __sigset_t_defined = @as(c_int, 1);
pub const ____sigset_t_defined = "";
pub const _SIGSET_NWORDS = @import("std").zig.c_translation.MacroArithmetic.div(@as(c_int, 1024), @as(c_int, 8) * @import("std").zig.c_translation.sizeof(c_ulong));
pub const __timeval_defined = @as(c_int, 1);
pub const _STRUCT_TIMESPEC = @as(c_int, 1);
pub const __suseconds_t_defined = "";
pub const __NFDBITS = @as(c_int, 8) * @import("std").zig.c_translation.cast(c_int, @import("std").zig.c_translation.sizeof(__fd_mask));
pub inline fn __FD_ELT(d: anytype) @TypeOf(@import("std").zig.c_translation.MacroArithmetic.div(d, __NFDBITS)) {
    return @import("std").zig.c_translation.MacroArithmetic.div(d, __NFDBITS);
}
pub inline fn __FD_MASK(d: anytype) __fd_mask {
    return @import("std").zig.c_translation.cast(__fd_mask, @as(c_ulong, 1) << @import("std").zig.c_translation.MacroArithmetic.rem(d, __NFDBITS));
}
pub inline fn __FDS_BITS(set: anytype) @TypeOf(set.*.__fds_bits) {
    return set.*.__fds_bits;
}
pub const FD_SETSIZE = __FD_SETSIZE;
pub const NFDBITS = __NFDBITS;
pub inline fn FD_SET(fd: anytype, fdsetp: anytype) @TypeOf(__FD_SET(fd, fdsetp)) {
    return __FD_SET(fd, fdsetp);
}
pub inline fn FD_CLR(fd: anytype, fdsetp: anytype) @TypeOf(__FD_CLR(fd, fdsetp)) {
    return __FD_CLR(fd, fdsetp);
}
pub inline fn FD_ISSET(fd: anytype, fdsetp: anytype) @TypeOf(__FD_ISSET(fd, fdsetp)) {
    return __FD_ISSET(fd, fdsetp);
}
pub inline fn FD_ZERO(fdsetp: anytype) @TypeOf(__FD_ZERO(fdsetp)) {
    return __FD_ZERO(fdsetp);
}
pub const __blksize_t_defined = "";
pub const __blkcnt_t_defined = "";
pub const __fsblkcnt_t_defined = "";
pub const __fsfilcnt_t_defined = "";
pub const _BITS_PTHREADTYPES_COMMON_H = @as(c_int, 1);
pub const _THREAD_SHARED_TYPES_H = @as(c_int, 1);
pub const _BITS_PTHREADTYPES_ARCH_H = @as(c_int, 1);
pub const __SIZEOF_PTHREAD_MUTEX_T = @as(c_int, 40);
pub const __SIZEOF_PTHREAD_ATTR_T = @as(c_int, 56);
pub const __SIZEOF_PTHREAD_RWLOCK_T = @as(c_int, 56);
pub const __SIZEOF_PTHREAD_BARRIER_T = @as(c_int, 32);
pub const __SIZEOF_PTHREAD_MUTEXATTR_T = @as(c_int, 4);
pub const __SIZEOF_PTHREAD_COND_T = @as(c_int, 48);
pub const __SIZEOF_PTHREAD_CONDATTR_T = @as(c_int, 4);
pub const __SIZEOF_PTHREAD_RWLOCKATTR_T = @as(c_int, 8);
pub const __SIZEOF_PTHREAD_BARRIERATTR_T = @as(c_int, 4);
pub const __LOCK_ALIGNMENT = "";
pub const __ONCE_ALIGNMENT = "";
pub const _BITS_ATOMIC_WIDE_COUNTER_H = "";
pub const _THREAD_MUTEX_INTERNAL_H = @as(c_int, 1);
pub const __PTHREAD_MUTEX_HAVE_PREV = @as(c_int, 1);
pub const _RWLOCK_INTERNAL_H = "";
pub inline fn __PTHREAD_RWLOCK_INITIALIZER(__flags: anytype) @TypeOf(__flags) {
    return blk: {
        _ = @as(c_int, 0);
        _ = @as(c_int, 0);
        _ = @as(c_int, 0);
        _ = @as(c_int, 0);
        _ = @as(c_int, 0);
        _ = @as(c_int, 0);
        _ = @as(c_int, 0);
        _ = @as(c_int, 0);
        _ = @TypeOf(__PTHREAD_RWLOCK_ELISION_EXTRA);
        _ = @as(c_int, 0);
        break :blk __flags;
    };
}
pub const __have_pthread_attr_t = @as(c_int, 1);
pub const __iovec_defined = @as(c_int, 1);
pub const _BITS_UIO_LIM_H = @as(c_int, 1);
pub const __IOV_MAX = @as(c_int, 1024);
pub const UIO_MAXIOV = __IOV_MAX;
pub inline fn L_STRINGIFY(val: anytype) @TypeOf(L_STRINGIFY_ARG(val)) {
    return L_STRINGIFY_ARG(val);
}
pub inline fn L_PTR_TO_UINT(p: anytype) c_uint {
    return @import("std").zig.c_translation.cast(c_uint, @import("std").zig.c_translation.cast(usize, p));
}
pub inline fn L_UINT_TO_PTR(u: anytype) ?*anyopaque {
    return @import("std").zig.c_translation.cast(?*anyopaque, @import("std").zig.c_translation.cast(usize, u));
}
pub inline fn L_PTR_TO_INT(p: anytype) c_int {
    return @import("std").zig.c_translation.cast(c_int, @import("std").zig.c_translation.cast(isize, p));
}
pub inline fn L_INT_TO_PTR(u: anytype) ?*anyopaque {
    return @import("std").zig.c_translation.cast(?*anyopaque, @import("std").zig.c_translation.cast(isize, u));
}
pub inline fn L_LE16_TO_CPU(val: anytype) @TypeOf(val) {
    return val;
}
pub inline fn L_LE32_TO_CPU(val: anytype) @TypeOf(val) {
    return val;
}
pub inline fn L_LE64_TO_CPU(val: anytype) @TypeOf(val) {
    return val;
}
pub inline fn L_CPU_TO_LE16(val: anytype) @TypeOf(val) {
    return val;
}
pub inline fn L_CPU_TO_LE32(val: anytype) @TypeOf(val) {
    return val;
}
pub inline fn L_CPU_TO_LE64(val: anytype) @TypeOf(val) {
    return val;
}
pub inline fn L_BE16_TO_CPU(val: anytype) @TypeOf(bswap_16(val)) {
    return bswap_16(val);
}
pub inline fn L_BE32_TO_CPU(val: anytype) @TypeOf(bswap_32(val)) {
    return bswap_32(val);
}
pub inline fn L_BE64_TO_CPU(val: anytype) @TypeOf(bswap_64(val)) {
    return bswap_64(val);
}
pub inline fn L_CPU_TO_BE16(val: anytype) @TypeOf(bswap_16(val)) {
    return bswap_16(val);
}
pub inline fn L_CPU_TO_BE32(val: anytype) @TypeOf(bswap_32(val)) {
    return bswap_32(val);
}
pub inline fn L_CPU_TO_BE64(val: anytype) @TypeOf(bswap_64(val)) {
    return bswap_64(val);
}
pub const __ELL_TEST_H = "";
pub const __ELL_STRV_H = "";
pub const __ELL_UTF8_H = "";
pub const _WCHAR_H = @as(c_int, 1);
pub const _BITS_FLOATN_H = "";
pub const __HAVE_FLOAT128 = @as(c_int, 0);
pub const __HAVE_DISTINCT_FLOAT128 = @as(c_int, 0);
pub const __HAVE_FLOAT64X = @as(c_int, 1);
pub const __HAVE_FLOAT64X_LONG_DOUBLE = @as(c_int, 1);
pub const _BITS_FLOATN_COMMON_H = "";
pub const __HAVE_FLOAT16 = @as(c_int, 0);
pub const __HAVE_FLOAT32 = @as(c_int, 1);
pub const __HAVE_FLOAT64 = @as(c_int, 1);
pub const __HAVE_FLOAT32X = @as(c_int, 1);
pub const __HAVE_FLOAT128X = @as(c_int, 0);
pub const __HAVE_DISTINCT_FLOAT16 = __HAVE_FLOAT16;
pub const __HAVE_DISTINCT_FLOAT32 = @as(c_int, 0);
pub const __HAVE_DISTINCT_FLOAT64 = @as(c_int, 0);
pub const __HAVE_DISTINCT_FLOAT32X = @as(c_int, 0);
pub const __HAVE_DISTINCT_FLOAT64X = @as(c_int, 0);
pub const __HAVE_DISTINCT_FLOAT128X = __HAVE_FLOAT128X;
pub const __HAVE_FLOAT128_UNLIKE_LDBL = (__HAVE_DISTINCT_FLOAT128 != 0) and (__LDBL_MANT_DIG__ != @as(c_int, 113));
pub const __HAVE_FLOATN_NOT_TYPEDEF = @as(c_int, 0);
pub const __f32 = @import("std").zig.c_translation.Macros.F_SUFFIX;
pub inline fn __f64(x: anytype) @TypeOf(x) {
    return x;
}
pub inline fn __f32x(x: anytype) @TypeOf(x) {
    return x;
}
pub const __f64x = @import("std").zig.c_translation.Macros.L_SUFFIX;
pub inline fn __builtin_huge_valf32() @TypeOf(__builtin_huge_valf()) {
    return __builtin_huge_valf();
}
pub inline fn __builtin_inff32() @TypeOf(__builtin_inff()) {
    return __builtin_inff();
}
pub inline fn __builtin_nanf32(x: anytype) @TypeOf(__builtin_nanf(x)) {
    return __builtin_nanf(x);
}
pub const __need_wchar_t = "";
pub const _WCHAR_T = "";
pub const __need___va_list = "";
pub const __wint_t_defined = @as(c_int, 1);
pub const _WINT_T = @as(c_int, 1);
pub const __mbstate_t_defined = @as(c_int, 1);
pub const ____mbstate_t_defined = @as(c_int, 1);
pub const ____FILE_defined = @as(c_int, 1);
pub const __FILE_defined = @as(c_int, 1);
pub const WEOF = @import("std").zig.c_translation.promoteIntLiteral(c_uint, 0xffffffff, .hexadecimal);
pub const __attr_dealloc_fclose = "";
pub inline fn l_ascii_isalnum(c: anytype) @TypeOf((l_ascii_table[@intCast(usize, @import("std").zig.c_translation.cast(u8, c))] & L_ASCII_ALNUM) != @as(c_int, 0)) {
    return (l_ascii_table[@intCast(usize, @import("std").zig.c_translation.cast(u8, c))] & L_ASCII_ALNUM) != @as(c_int, 0);
}
pub inline fn l_ascii_isalpha(c: anytype) @TypeOf((l_ascii_table[@intCast(usize, @import("std").zig.c_translation.cast(u8, c))] & L_ASCII_ALPHA) != @as(c_int, 0)) {
    return (l_ascii_table[@intCast(usize, @import("std").zig.c_translation.cast(u8, c))] & L_ASCII_ALPHA) != @as(c_int, 0);
}
pub inline fn l_ascii_iscntrl(c: anytype) @TypeOf((l_ascii_table[@intCast(usize, @import("std").zig.c_translation.cast(u8, c))] & L_ASCII_CNTRL) != @as(c_int, 0)) {
    return (l_ascii_table[@intCast(usize, @import("std").zig.c_translation.cast(u8, c))] & L_ASCII_CNTRL) != @as(c_int, 0);
}
pub inline fn l_ascii_isdigit(c: anytype) @TypeOf((l_ascii_table[@intCast(usize, @import("std").zig.c_translation.cast(u8, c))] & L_ASCII_DIGIT) != @as(c_int, 0)) {
    return (l_ascii_table[@intCast(usize, @import("std").zig.c_translation.cast(u8, c))] & L_ASCII_DIGIT) != @as(c_int, 0);
}
pub inline fn l_ascii_isgraph(c: anytype) @TypeOf((l_ascii_table[@intCast(usize, @import("std").zig.c_translation.cast(u8, c))] & L_ASCII_GRAPH) != @as(c_int, 0)) {
    return (l_ascii_table[@intCast(usize, @import("std").zig.c_translation.cast(u8, c))] & L_ASCII_GRAPH) != @as(c_int, 0);
}
pub inline fn l_ascii_islower(c: anytype) @TypeOf((l_ascii_table[@intCast(usize, @import("std").zig.c_translation.cast(u8, c))] & L_ASCII_LOWER) != @as(c_int, 0)) {
    return (l_ascii_table[@intCast(usize, @import("std").zig.c_translation.cast(u8, c))] & L_ASCII_LOWER) != @as(c_int, 0);
}
pub inline fn l_ascii_isprint(c: anytype) @TypeOf((l_ascii_table[@intCast(usize, @import("std").zig.c_translation.cast(u8, c))] & L_ASCII_PRINT) != @as(c_int, 0)) {
    return (l_ascii_table[@intCast(usize, @import("std").zig.c_translation.cast(u8, c))] & L_ASCII_PRINT) != @as(c_int, 0);
}
pub inline fn l_ascii_ispunct(c: anytype) @TypeOf((l_ascii_table[@intCast(usize, @import("std").zig.c_translation.cast(u8, c))] & L_ASCII_PUNCT) != @as(c_int, 0)) {
    return (l_ascii_table[@intCast(usize, @import("std").zig.c_translation.cast(u8, c))] & L_ASCII_PUNCT) != @as(c_int, 0);
}
pub inline fn l_ascii_isspace(c: anytype) @TypeOf((l_ascii_table[@intCast(usize, @import("std").zig.c_translation.cast(u8, c))] & L_ASCII_SPACE) != @as(c_int, 0)) {
    return (l_ascii_table[@intCast(usize, @import("std").zig.c_translation.cast(u8, c))] & L_ASCII_SPACE) != @as(c_int, 0);
}
pub inline fn l_ascii_isupper(c: anytype) @TypeOf((l_ascii_table[@intCast(usize, @import("std").zig.c_translation.cast(u8, c))] & L_ASCII_UPPER) != @as(c_int, 0)) {
    return (l_ascii_table[@intCast(usize, @import("std").zig.c_translation.cast(u8, c))] & L_ASCII_UPPER) != @as(c_int, 0);
}
pub inline fn l_ascii_isxdigit(c: anytype) @TypeOf((l_ascii_table[@intCast(usize, @import("std").zig.c_translation.cast(u8, c))] & L_ASCII_XDIGIT) != @as(c_int, 0)) {
    return (l_ascii_table[@intCast(usize, @import("std").zig.c_translation.cast(u8, c))] & L_ASCII_XDIGIT) != @as(c_int, 0);
}
pub const __ELL_QUEUE_H = "";
pub const __ELL_HASHMAP_H = "";
pub const __ELL_STRING_H = "";
pub const __ELL_MAIN_H = "";
pub const __ELL_IDLE_H = "";
pub const __ELL_SIGNAL_H = "";
pub const __ELL_TIMEOUT_H = "";
pub const __ELL_IO_H = "";
pub const __ELL_RINGBUF_H = "";
pub const _STDLIB_H = @as(c_int, 1);
pub const WNOHANG = @as(c_int, 1);
pub const WUNTRACED = @as(c_int, 2);
pub const WSTOPPED = @as(c_int, 2);
pub const WEXITED = @as(c_int, 4);
pub const WCONTINUED = @as(c_int, 8);
pub const WNOWAIT = @import("std").zig.c_translation.promoteIntLiteral(c_int, 0x01000000, .hexadecimal);
pub const __WNOTHREAD = @import("std").zig.c_translation.promoteIntLiteral(c_int, 0x20000000, .hexadecimal);
pub const __WALL = @import("std").zig.c_translation.promoteIntLiteral(c_int, 0x40000000, .hexadecimal);
pub const __WCLONE = @import("std").zig.c_translation.promoteIntLiteral(c_int, 0x80000000, .hexadecimal);
pub inline fn __WEXITSTATUS(status: anytype) @TypeOf((status & @import("std").zig.c_translation.promoteIntLiteral(c_int, 0xff00, .hexadecimal)) >> @as(c_int, 8)) {
    return (status & @import("std").zig.c_translation.promoteIntLiteral(c_int, 0xff00, .hexadecimal)) >> @as(c_int, 8);
}
pub inline fn __WTERMSIG(status: anytype) @TypeOf(status & @as(c_int, 0x7f)) {
    return status & @as(c_int, 0x7f);
}
pub inline fn __WSTOPSIG(status: anytype) @TypeOf(__WEXITSTATUS(status)) {
    return __WEXITSTATUS(status);
}
pub inline fn __WIFEXITED(status: anytype) @TypeOf(__WTERMSIG(status) == @as(c_int, 0)) {
    return __WTERMSIG(status) == @as(c_int, 0);
}
pub inline fn __WIFSIGNALED(status: anytype) @TypeOf((@import("std").zig.c_translation.cast(i8, (status & @as(c_int, 0x7f)) + @as(c_int, 1)) >> @as(c_int, 1)) > @as(c_int, 0)) {
    return (@import("std").zig.c_translation.cast(i8, (status & @as(c_int, 0x7f)) + @as(c_int, 1)) >> @as(c_int, 1)) > @as(c_int, 0);
}
pub inline fn __WIFSTOPPED(status: anytype) @TypeOf((status & @as(c_int, 0xff)) == @as(c_int, 0x7f)) {
    return (status & @as(c_int, 0xff)) == @as(c_int, 0x7f);
}
pub inline fn __WIFCONTINUED(status: anytype) @TypeOf(status == __W_CONTINUED) {
    return status == __W_CONTINUED;
}
pub inline fn __WCOREDUMP(status: anytype) @TypeOf(status & __WCOREFLAG) {
    return status & __WCOREFLAG;
}
pub inline fn __W_EXITCODE(ret: anytype, sig: anytype) @TypeOf((ret << @as(c_int, 8)) | sig) {
    return (ret << @as(c_int, 8)) | sig;
}
pub inline fn __W_STOPCODE(sig: anytype) @TypeOf((sig << @as(c_int, 8)) | @as(c_int, 0x7f)) {
    return (sig << @as(c_int, 8)) | @as(c_int, 0x7f);
}
pub const __W_CONTINUED = @import("std").zig.c_translation.promoteIntLiteral(c_int, 0xffff, .hexadecimal);
pub const __WCOREFLAG = @as(c_int, 0x80);
pub inline fn WEXITSTATUS(status: anytype) @TypeOf(__WEXITSTATUS(status)) {
    return __WEXITSTATUS(status);
}
pub inline fn WTERMSIG(status: anytype) @TypeOf(__WTERMSIG(status)) {
    return __WTERMSIG(status);
}
pub inline fn WSTOPSIG(status: anytype) @TypeOf(__WSTOPSIG(status)) {
    return __WSTOPSIG(status);
}
pub inline fn WIFEXITED(status: anytype) @TypeOf(__WIFEXITED(status)) {
    return __WIFEXITED(status);
}
pub inline fn WIFSIGNALED(status: anytype) @TypeOf(__WIFSIGNALED(status)) {
    return __WIFSIGNALED(status);
}
pub inline fn WIFSTOPPED(status: anytype) @TypeOf(__WIFSTOPPED(status)) {
    return __WIFSTOPPED(status);
}
pub inline fn WIFCONTINUED(status: anytype) @TypeOf(__WIFCONTINUED(status)) {
    return __WIFCONTINUED(status);
}
pub const __ldiv_t_defined = @as(c_int, 1);
pub const __lldiv_t_defined = @as(c_int, 1);
pub const RAND_MAX = @import("std").zig.c_translation.promoteIntLiteral(c_int, 2147483647, .decimal);
pub const EXIT_FAILURE = @as(c_int, 1);
pub const EXIT_SUCCESS = @as(c_int, 0);
pub const MB_CUR_MAX = __ctype_get_mb_cur_max();
pub const _ALLOCA_H = @as(c_int, 1);
pub const __COMPAR_FN_T = "";
pub const __ELL_LOG_H = "";
pub const L_LOG_ERR = @as(c_int, 3);
pub const L_LOG_WARNING = @as(c_int, 4);
pub const L_LOG_INFO = @as(c_int, 6);
pub const L_LOG_DEBUG = @as(c_int, 7);
pub const L_DEBUG_FLAG_DEFAULT = @as(c_int, 0);
pub const L_DEBUG_FLAG_PRINT = @as(c_int, 1) << @as(c_int, 0);
pub const __ELL_CHECKSUM_H = "";
pub const __ELL_SETTINGS_H = "";
pub const __STDDEF_H = "";
pub const __need_ptrdiff_t = "";
pub const __need_STDDEF_H_misc = "";
pub const _PTRDIFF_T = "";
pub const __CLANG_MAX_ALIGN_T_DEFINED = "";
pub const __ELL_HWDB_H = "";
pub const __ELL_CIPHER_H = "";
pub const __ELL_RANDOM_H = "";
pub const __ELL_UINTSET_H = "";
pub const __ELL_BASE64_H = "";
pub const __ELL_PEM_H = "";
pub const __ELL_TLS_H = "";
pub const __ELL_UUID_H = "";
pub const __ELL_KEY_H = "";
pub const __ELL_FILE_H = "";
pub const __ELL_DIR_H = "";
pub const __ELL_NET_H = "";
pub const __ELL_NETLINK_H = "";
pub const __ELL_GENL_H = "";
pub const __ELL_RTNL_H = "";
pub const __LINUX_RTNETLINK_H = "";
pub const _LINUX_TYPES_H = "";
pub const _ASM_GENERIC_TYPES_H = "";
pub const _ASM_GENERIC_INT_LL64_H = "";
pub const __ASM_X86_BITSPERLONG_H = "";
pub const __BITS_PER_LONG = @as(c_int, 64);
pub const __ASM_GENERIC_BITS_PER_LONG = "";
pub const _LINUX_POSIX_TYPES_H = "";
pub const _LINUX_STDDEF_H = "";
pub const _ASM_X86_POSIX_TYPES_64_H = "";
pub const __ASM_GENERIC_POSIX_TYPES_H = "";
pub const __bitwise = "";
pub const __bitwise__ = __bitwise;
pub const __LINUX_NETLINK_H = "";
pub const _LINUX_CONST_H = "";
pub inline fn _AC(X: anytype, Y: anytype) @TypeOf(__AC(X, Y)) {
    return __AC(X, Y);
}
pub const _AT = @import("std").zig.c_translation.Macros.CAST_OR_CALL;
pub inline fn _BITUL(x: anytype) @TypeOf(_UL(@as(c_int, 1)) << x) {
    return _UL(@as(c_int, 1)) << x;
}
pub inline fn _BITULL(x: anytype) @TypeOf(_ULL(@as(c_int, 1)) << x) {
    return _ULL(@as(c_int, 1)) << x;
}
pub inline fn __ALIGN_KERNEL_MASK(x: anytype, mask: anytype) @TypeOf((x + mask) & ~mask) {
    return (x + mask) & ~mask;
}
pub inline fn __KERNEL_DIV_ROUND_UP(n: anytype, d: anytype) @TypeOf(@import("std").zig.c_translation.MacroArithmetic.div((n + d) - @as(c_int, 1), d)) {
    return @import("std").zig.c_translation.MacroArithmetic.div((n + d) - @as(c_int, 1), d);
}
pub const _LINUX_SOCKET_H = "";
pub const _K_SS_MAXSIZE = @as(c_int, 128);
pub const SOCK_SNDBUF_LOCK = @as(c_int, 1);
pub const SOCK_RCVBUF_LOCK = @as(c_int, 2);
pub const SOCK_BUF_LOCK_MASK = SOCK_SNDBUF_LOCK | SOCK_RCVBUF_LOCK;
pub const SOCK_TXREHASH_DISABLED = @as(c_int, 0);
pub const SOCK_TXREHASH_ENABLED = @as(c_int, 1);
pub const NETLINK_ROUTE = @as(c_int, 0);
pub const NETLINK_UNUSED = @as(c_int, 1);
pub const NETLINK_USERSOCK = @as(c_int, 2);
pub const NETLINK_FIREWALL = @as(c_int, 3);
pub const NETLINK_SOCK_DIAG = @as(c_int, 4);
pub const NETLINK_NFLOG = @as(c_int, 5);
pub const NETLINK_XFRM = @as(c_int, 6);
pub const NETLINK_SELINUX = @as(c_int, 7);
pub const NETLINK_ISCSI = @as(c_int, 8);
pub const NETLINK_AUDIT = @as(c_int, 9);
pub const NETLINK_FIB_LOOKUP = @as(c_int, 10);
pub const NETLINK_CONNECTOR = @as(c_int, 11);
pub const NETLINK_NETFILTER = @as(c_int, 12);
pub const NETLINK_IP6_FW = @as(c_int, 13);
pub const NETLINK_DNRTMSG = @as(c_int, 14);
pub const NETLINK_KOBJECT_UEVENT = @as(c_int, 15);
pub const NETLINK_GENERIC = @as(c_int, 16);
pub const NETLINK_SCSITRANSPORT = @as(c_int, 18);
pub const NETLINK_ECRYPTFS = @as(c_int, 19);
pub const NETLINK_RDMA = @as(c_int, 20);
pub const NETLINK_CRYPTO = @as(c_int, 21);
pub const NETLINK_SMC = @as(c_int, 22);
pub const NETLINK_INET_DIAG = NETLINK_SOCK_DIAG;
pub const MAX_LINKS = @as(c_int, 32);
pub const NLM_F_REQUEST = @as(c_int, 0x01);
pub const NLM_F_MULTI = @as(c_int, 0x02);
pub const NLM_F_ACK = @as(c_int, 0x04);
pub const NLM_F_ECHO = @as(c_int, 0x08);
pub const NLM_F_DUMP_INTR = @as(c_int, 0x10);
pub const NLM_F_DUMP_FILTERED = @as(c_int, 0x20);
pub const NLM_F_ROOT = @as(c_int, 0x100);
pub const NLM_F_MATCH = @as(c_int, 0x200);
pub const NLM_F_ATOMIC = @as(c_int, 0x400);
pub const NLM_F_DUMP = NLM_F_ROOT | NLM_F_MATCH;
pub const NLM_F_REPLACE = @as(c_int, 0x100);
pub const NLM_F_EXCL = @as(c_int, 0x200);
pub const NLM_F_CREATE = @as(c_int, 0x400);
pub const NLM_F_APPEND = @as(c_int, 0x800);
pub const NLM_F_NONREC = @as(c_int, 0x100);
pub const NLM_F_CAPPED = @as(c_int, 0x100);
pub const NLM_F_ACK_TLVS = @as(c_int, 0x200);
pub const NLMSG_ALIGNTO = @as(c_uint, 4);
pub inline fn NLMSG_ALIGN(len: anytype) @TypeOf(((len + NLMSG_ALIGNTO) - @as(c_int, 1)) & ~(NLMSG_ALIGNTO - @as(c_int, 1))) {
    return ((len + NLMSG_ALIGNTO) - @as(c_int, 1)) & ~(NLMSG_ALIGNTO - @as(c_int, 1));
}
pub const NLMSG_HDRLEN = @import("std").zig.c_translation.cast(c_int, NLMSG_ALIGN(@import("std").zig.c_translation.sizeof(struct_nlmsghdr)));
pub inline fn NLMSG_LENGTH(len: anytype) @TypeOf(len + NLMSG_HDRLEN) {
    return len + NLMSG_HDRLEN;
}
pub inline fn NLMSG_SPACE(len: anytype) @TypeOf(NLMSG_ALIGN(NLMSG_LENGTH(len))) {
    return NLMSG_ALIGN(NLMSG_LENGTH(len));
}
pub inline fn NLMSG_DATA(nlh: anytype) ?*anyopaque {
    return @import("std").zig.c_translation.cast(?*anyopaque, @import("std").zig.c_translation.cast([*c]u8, nlh) + NLMSG_HDRLEN);
}
pub inline fn NLMSG_OK(nlh: anytype, len: anytype) @TypeOf(((len >= @import("std").zig.c_translation.cast(c_int, @import("std").zig.c_translation.sizeof(struct_nlmsghdr))) and (nlh.*.nlmsg_len >= @import("std").zig.c_translation.sizeof(struct_nlmsghdr))) and (nlh.*.nlmsg_len <= len)) {
    return ((len >= @import("std").zig.c_translation.cast(c_int, @import("std").zig.c_translation.sizeof(struct_nlmsghdr))) and (nlh.*.nlmsg_len >= @import("std").zig.c_translation.sizeof(struct_nlmsghdr))) and (nlh.*.nlmsg_len <= len);
}
pub inline fn NLMSG_PAYLOAD(nlh: anytype, len: anytype) @TypeOf(nlh.*.nlmsg_len - NLMSG_SPACE(len)) {
    return nlh.*.nlmsg_len - NLMSG_SPACE(len);
}
pub const NLMSG_NOOP = @as(c_int, 0x1);
pub const NLMSG_ERROR = @as(c_int, 0x2);
pub const NLMSG_DONE = @as(c_int, 0x3);
pub const NLMSG_OVERRUN = @as(c_int, 0x4);
pub const NLMSG_MIN_TYPE = @as(c_int, 0x10);
pub const NETLINK_ADD_MEMBERSHIP = @as(c_int, 1);
pub const NETLINK_DROP_MEMBERSHIP = @as(c_int, 2);
pub const NETLINK_PKTINFO = @as(c_int, 3);
pub const NETLINK_BROADCAST_ERROR = @as(c_int, 4);
pub const NETLINK_NO_ENOBUFS = @as(c_int, 5);
pub const NETLINK_RX_RING = @as(c_int, 6);
pub const NETLINK_TX_RING = @as(c_int, 7);
pub const NETLINK_LISTEN_ALL_NSID = @as(c_int, 8);
pub const NETLINK_LIST_MEMBERSHIPS = @as(c_int, 9);
pub const NETLINK_CAP_ACK = @as(c_int, 10);
pub const NETLINK_EXT_ACK = @as(c_int, 11);
pub const NETLINK_GET_STRICT_CHK = @as(c_int, 12);
pub const NL_MMAP_MSG_ALIGNMENT = NLMSG_ALIGNTO;
pub inline fn NL_MMAP_MSG_ALIGN(sz: anytype) @TypeOf(__ALIGN_KERNEL(sz, NL_MMAP_MSG_ALIGNMENT)) {
    return __ALIGN_KERNEL(sz, NL_MMAP_MSG_ALIGNMENT);
}
pub const NL_MMAP_HDRLEN = NL_MMAP_MSG_ALIGN(@import("std").zig.c_translation.sizeof(struct_nl_mmap_hdr));
pub const NET_MAJOR = @as(c_int, 36);
pub const NLA_F_NESTED = @as(c_int, 1) << @as(c_int, 15);
pub const NLA_F_NET_BYTEORDER = @as(c_int, 1) << @as(c_int, 14);
pub const NLA_TYPE_MASK = ~(NLA_F_NESTED | NLA_F_NET_BYTEORDER);
pub const NLA_ALIGNTO = @as(c_int, 4);
pub inline fn NLA_ALIGN(len: anytype) @TypeOf(((len + NLA_ALIGNTO) - @as(c_int, 1)) & ~(NLA_ALIGNTO - @as(c_int, 1))) {
    return ((len + NLA_ALIGNTO) - @as(c_int, 1)) & ~(NLA_ALIGNTO - @as(c_int, 1));
}
pub const NLA_HDRLEN = @import("std").zig.c_translation.cast(c_int, NLA_ALIGN(@import("std").zig.c_translation.sizeof(struct_nlattr)));
pub const _LINUX_IF_LINK_H = "";
pub const IFLA_MAX = __IFLA_MAX - @as(c_int, 1);
pub inline fn IFLA_RTA(r: anytype) [*c]struct_rtattr {
    return @import("std").zig.c_translation.cast([*c]struct_rtattr, @import("std").zig.c_translation.cast([*c]u8, r) + NLMSG_ALIGN(@import("std").zig.c_translation.sizeof(struct_ifinfomsg)));
}
pub inline fn IFLA_PAYLOAD(n: anytype) @TypeOf(NLMSG_PAYLOAD(n, @import("std").zig.c_translation.sizeof(struct_ifinfomsg))) {
    return NLMSG_PAYLOAD(n, @import("std").zig.c_translation.sizeof(struct_ifinfomsg));
}
pub const IFLA_INET_MAX = __IFLA_INET_MAX - @as(c_int, 1);
pub const IFLA_INET6_MAX = __IFLA_INET6_MAX - @as(c_int, 1);
pub const IFLA_BR_MAX = __IFLA_BR_MAX - @as(c_int, 1);
pub const IFLA_BRPORT_MAX = __IFLA_BRPORT_MAX - @as(c_int, 1);
pub const IFLA_INFO_MAX = __IFLA_INFO_MAX - @as(c_int, 1);
pub const IFLA_VLAN_MAX = __IFLA_VLAN_MAX - @as(c_int, 1);
pub const IFLA_VLAN_QOS_MAX = __IFLA_VLAN_QOS_MAX - @as(c_int, 1);
pub const IFLA_MACVLAN_MAX = __IFLA_MACVLAN_MAX - @as(c_int, 1);
pub const MACVLAN_FLAG_NOPROMISC = @as(c_int, 1);
pub const MACVLAN_FLAG_NODST = @as(c_int, 2);
pub const IFLA_VRF_MAX = __IFLA_VRF_MAX - @as(c_int, 1);
pub const IFLA_VRF_PORT_MAX = __IFLA_VRF_PORT_MAX - @as(c_int, 1);
pub const IFLA_MACSEC_MAX = __IFLA_MACSEC_MAX - @as(c_int, 1);
pub const IFLA_XFRM_MAX = __IFLA_XFRM_MAX - @as(c_int, 1);
pub const IFLA_IPVLAN_MAX = __IFLA_IPVLAN_MAX - @as(c_int, 1);
pub const IPVLAN_F_PRIVATE = @as(c_int, 0x01);
pub const IPVLAN_F_VEPA = @as(c_int, 0x02);
pub const TUNNEL_MSG_FLAG_STATS = @as(c_int, 0x01);
pub const TUNNEL_MSG_VALID_USER_FLAGS = TUNNEL_MSG_FLAG_STATS;
pub const VNIFILTER_ENTRY_STATS_MAX = __VNIFILTER_ENTRY_STATS_MAX - @as(c_int, 1);
pub const VXLAN_VNIFILTER_ENTRY_MAX = __VXLAN_VNIFILTER_ENTRY_MAX - @as(c_int, 1);
pub const VXLAN_VNIFILTER_MAX = __VXLAN_VNIFILTER_MAX - @as(c_int, 1);
pub const IFLA_VXLAN_MAX = __IFLA_VXLAN_MAX - @as(c_int, 1);
pub const IFLA_GENEVE_MAX = __IFLA_GENEVE_MAX - @as(c_int, 1);
pub const IFLA_BAREUDP_MAX = __IFLA_BAREUDP_MAX - @as(c_int, 1);
pub const IFLA_PPP_MAX = __IFLA_PPP_MAX - @as(c_int, 1);
pub const IFLA_GTP_MAX = __IFLA_GTP_MAX - @as(c_int, 1);
pub const IFLA_BOND_MAX = __IFLA_BOND_MAX - @as(c_int, 1);
pub const IFLA_BOND_AD_INFO_MAX = __IFLA_BOND_AD_INFO_MAX - @as(c_int, 1);
pub const IFLA_BOND_SLAVE_MAX = __IFLA_BOND_SLAVE_MAX - @as(c_int, 1);
pub const IFLA_VF_INFO_MAX = __IFLA_VF_INFO_MAX - @as(c_int, 1);
pub const IFLA_VF_MAX = __IFLA_VF_MAX - @as(c_int, 1);
pub const IFLA_VF_VLAN_INFO_MAX = __IFLA_VF_VLAN_INFO_MAX - @as(c_int, 1);
pub const MAX_VLAN_LIST_LEN = @as(c_int, 1);
pub const IFLA_VF_STATS_MAX = __IFLA_VF_STATS_MAX - @as(c_int, 1);
pub const IFLA_VF_PORT_MAX = __IFLA_VF_PORT_MAX - @as(c_int, 1);
pub const IFLA_PORT_MAX = __IFLA_PORT_MAX - @as(c_int, 1);
pub const PORT_PROFILE_MAX = @as(c_int, 40);
pub const PORT_UUID_MAX = @as(c_int, 16);
pub const PORT_SELF_VF = -@as(c_int, 1);
pub const IFLA_IPOIB_MAX = __IFLA_IPOIB_MAX - @as(c_int, 1);
pub const IFLA_HSR_MAX = __IFLA_HSR_MAX - @as(c_int, 1);
pub const IFLA_STATS_MAX = __IFLA_STATS_MAX - @as(c_int, 1);
pub inline fn IFLA_STATS_FILTER_BIT(ATTR: anytype) @TypeOf(@as(c_int, 1) << (ATTR - @as(c_int, 1))) {
    return @as(c_int, 1) << (ATTR - @as(c_int, 1));
}
pub const IFLA_STATS_GETSET_MAX = __IFLA_STATS_GETSET_MAX - @as(c_int, 1);
pub const LINK_XSTATS_TYPE_MAX = __LINK_XSTATS_TYPE_MAX - @as(c_int, 1);
pub const IFLA_OFFLOAD_XSTATS_MAX = __IFLA_OFFLOAD_XSTATS_MAX - @as(c_int, 1);
pub const IFLA_OFFLOAD_XSTATS_HW_S_INFO_MAX = __IFLA_OFFLOAD_XSTATS_HW_S_INFO_MAX - @as(c_int, 1);
pub const XDP_FLAGS_UPDATE_IF_NOEXIST = @as(c_uint, 1) << @as(c_int, 0);
pub const XDP_FLAGS_SKB_MODE = @as(c_uint, 1) << @as(c_int, 1);
pub const XDP_FLAGS_DRV_MODE = @as(c_uint, 1) << @as(c_int, 2);
pub const XDP_FLAGS_HW_MODE = @as(c_uint, 1) << @as(c_int, 3);
pub const XDP_FLAGS_REPLACE = @as(c_uint, 1) << @as(c_int, 4);
pub const XDP_FLAGS_MODES = (XDP_FLAGS_SKB_MODE | XDP_FLAGS_DRV_MODE) | XDP_FLAGS_HW_MODE;
pub const XDP_FLAGS_MASK = (XDP_FLAGS_UPDATE_IF_NOEXIST | XDP_FLAGS_MODES) | XDP_FLAGS_REPLACE;
pub const IFLA_XDP_MAX = __IFLA_XDP_MAX - @as(c_int, 1);
pub const IFLA_TUN_MAX = __IFLA_TUN_MAX - @as(c_int, 1);
pub const RMNET_FLAGS_INGRESS_DEAGGREGATION = @as(c_uint, 1) << @as(c_int, 0);
pub const RMNET_FLAGS_INGRESS_MAP_COMMANDS = @as(c_uint, 1) << @as(c_int, 1);
pub const RMNET_FLAGS_INGRESS_MAP_CKSUMV4 = @as(c_uint, 1) << @as(c_int, 2);
pub const RMNET_FLAGS_EGRESS_MAP_CKSUMV4 = @as(c_uint, 1) << @as(c_int, 3);
pub const RMNET_FLAGS_INGRESS_MAP_CKSUMV5 = @as(c_uint, 1) << @as(c_int, 4);
pub const RMNET_FLAGS_EGRESS_MAP_CKSUMV5 = @as(c_uint, 1) << @as(c_int, 5);
pub const IFLA_RMNET_MAX = __IFLA_RMNET_MAX - @as(c_int, 1);
pub const IFLA_MCTP_MAX = __IFLA_MCTP_MAX - @as(c_int, 1);
pub const __LINUX_IF_ADDR_H = "";
pub const IFA_MAX = __IFA_MAX - @as(c_int, 1);
pub const IFA_F_SECONDARY = @as(c_int, 0x01);
pub const IFA_F_TEMPORARY = IFA_F_SECONDARY;
pub const IFA_F_NODAD = @as(c_int, 0x02);
pub const IFA_F_OPTIMISTIC = @as(c_int, 0x04);
pub const IFA_F_DADFAILED = @as(c_int, 0x08);
pub const IFA_F_HOMEADDRESS = @as(c_int, 0x10);
pub const IFA_F_DEPRECATED = @as(c_int, 0x20);
pub const IFA_F_TENTATIVE = @as(c_int, 0x40);
pub const IFA_F_PERMANENT = @as(c_int, 0x80);
pub const IFA_F_MANAGETEMPADDR = @as(c_int, 0x100);
pub const IFA_F_NOPREFIXROUTE = @as(c_int, 0x200);
pub const IFA_F_MCAUTOJOIN = @as(c_int, 0x400);
pub const IFA_F_STABLE_PRIVACY = @as(c_int, 0x800);
pub inline fn IFA_RTA(r: anytype) [*c]struct_rtattr {
    return @import("std").zig.c_translation.cast([*c]struct_rtattr, @import("std").zig.c_translation.cast([*c]u8, r) + NLMSG_ALIGN(@import("std").zig.c_translation.sizeof(struct_ifaddrmsg)));
}
pub inline fn IFA_PAYLOAD(n: anytype) @TypeOf(NLMSG_PAYLOAD(n, @import("std").zig.c_translation.sizeof(struct_ifaddrmsg))) {
    return NLMSG_PAYLOAD(n, @import("std").zig.c_translation.sizeof(struct_ifaddrmsg));
}
pub const IFAPROT_UNSPEC = @as(c_int, 0);
pub const IFAPROT_KERNEL_LO = @as(c_int, 1);
pub const IFAPROT_KERNEL_RA = @as(c_int, 2);
pub const IFAPROT_KERNEL_LL = @as(c_int, 3);
pub const __LINUX_NEIGHBOUR_H = "";
pub const NDA_MAX = __NDA_MAX - @as(c_int, 1);
pub const NTF_USE = @as(c_int, 1) << @as(c_int, 0);
pub const NTF_SELF = @as(c_int, 1) << @as(c_int, 1);
pub const NTF_MASTER = @as(c_int, 1) << @as(c_int, 2);
pub const NTF_PROXY = @as(c_int, 1) << @as(c_int, 3);
pub const NTF_EXT_LEARNED = @as(c_int, 1) << @as(c_int, 4);
pub const NTF_OFFLOADED = @as(c_int, 1) << @as(c_int, 5);
pub const NTF_STICKY = @as(c_int, 1) << @as(c_int, 6);
pub const NTF_ROUTER = @as(c_int, 1) << @as(c_int, 7);
pub const NTF_EXT_MANAGED = @as(c_int, 1) << @as(c_int, 0);
pub const NUD_INCOMPLETE = @as(c_int, 0x01);
pub const NUD_REACHABLE = @as(c_int, 0x02);
pub const NUD_STALE = @as(c_int, 0x04);
pub const NUD_DELAY = @as(c_int, 0x08);
pub const NUD_PROBE = @as(c_int, 0x10);
pub const NUD_FAILED = @as(c_int, 0x20);
pub const NUD_NOARP = @as(c_int, 0x40);
pub const NUD_PERMANENT = @as(c_int, 0x80);
pub const NUD_NONE = @as(c_int, 0x00);
pub const NDTPA_MAX = __NDTPA_MAX - @as(c_int, 1);
pub const NDTA_MAX = __NDTA_MAX - @as(c_int, 1);
pub const NFEA_MAX = __NFEA_MAX - @as(c_int, 1);
pub const RTNL_FAMILY_IPMR = @as(c_int, 128);
pub const RTNL_FAMILY_IP6MR = @as(c_int, 129);
pub const RTNL_FAMILY_MAX = @as(c_int, 129);
pub const RTM_NEWNVLAN = RTM_NEWVLAN;
pub const RTM_MAX = ((__RTM_MAX + @as(c_int, 3)) & ~@as(c_int, 3)) - @as(c_int, 1);
pub const RTM_NR_MSGTYPES = (RTM_MAX + @as(c_int, 1)) - RTM_BASE;
pub const RTM_NR_FAMILIES = RTM_NR_MSGTYPES >> @as(c_int, 2);
pub inline fn RTM_FAM(cmd: anytype) @TypeOf((cmd - RTM_BASE) >> @as(c_int, 2)) {
    return (cmd - RTM_BASE) >> @as(c_int, 2);
}
pub const RTA_ALIGNTO = @as(c_uint, 4);
pub inline fn RTA_ALIGN(len: anytype) @TypeOf(((len + RTA_ALIGNTO) - @as(c_int, 1)) & ~(RTA_ALIGNTO - @as(c_int, 1))) {
    return ((len + RTA_ALIGNTO) - @as(c_int, 1)) & ~(RTA_ALIGNTO - @as(c_int, 1));
}
pub inline fn RTA_OK(rta: anytype, len: anytype) @TypeOf(((len >= @import("std").zig.c_translation.cast(c_int, @import("std").zig.c_translation.sizeof(struct_rtattr))) and (rta.*.rta_len >= @import("std").zig.c_translation.sizeof(struct_rtattr))) and (rta.*.rta_len <= len)) {
    return ((len >= @import("std").zig.c_translation.cast(c_int, @import("std").zig.c_translation.sizeof(struct_rtattr))) and (rta.*.rta_len >= @import("std").zig.c_translation.sizeof(struct_rtattr))) and (rta.*.rta_len <= len);
}
pub inline fn RTA_LENGTH(len: anytype) @TypeOf(RTA_ALIGN(@import("std").zig.c_translation.sizeof(struct_rtattr)) + len) {
    return RTA_ALIGN(@import("std").zig.c_translation.sizeof(struct_rtattr)) + len;
}
pub inline fn RTA_SPACE(len: anytype) @TypeOf(RTA_ALIGN(RTA_LENGTH(len))) {
    return RTA_ALIGN(RTA_LENGTH(len));
}
pub inline fn RTA_DATA(rta: anytype) ?*anyopaque {
    return @import("std").zig.c_translation.cast(?*anyopaque, @import("std").zig.c_translation.cast([*c]u8, rta) + RTA_LENGTH(@as(c_int, 0)));
}
pub inline fn RTA_PAYLOAD(rta: anytype) @TypeOf(@import("std").zig.c_translation.cast(c_int, rta.*.rta_len) - RTA_LENGTH(@as(c_int, 0))) {
    return @import("std").zig.c_translation.cast(c_int, rta.*.rta_len) - RTA_LENGTH(@as(c_int, 0));
}
pub const RTN_MAX = __RTN_MAX - @as(c_int, 1);
pub const RTPROT_UNSPEC = @as(c_int, 0);
pub const RTPROT_REDIRECT = @as(c_int, 1);
pub const RTPROT_KERNEL = @as(c_int, 2);
pub const RTPROT_BOOT = @as(c_int, 3);
pub const RTPROT_STATIC = @as(c_int, 4);
pub const RTPROT_GATED = @as(c_int, 8);
pub const RTPROT_RA = @as(c_int, 9);
pub const RTPROT_MRT = @as(c_int, 10);
pub const RTPROT_ZEBRA = @as(c_int, 11);
pub const RTPROT_BIRD = @as(c_int, 12);
pub const RTPROT_DNROUTED = @as(c_int, 13);
pub const RTPROT_XORP = @as(c_int, 14);
pub const RTPROT_NTK = @as(c_int, 15);
pub const RTPROT_DHCP = @as(c_int, 16);
pub const RTPROT_MROUTED = @as(c_int, 17);
pub const RTPROT_KEEPALIVED = @as(c_int, 18);
pub const RTPROT_BABEL = @as(c_int, 42);
pub const RTPROT_OPENR = @as(c_int, 99);
pub const RTPROT_BGP = @as(c_int, 186);
pub const RTPROT_ISIS = @as(c_int, 187);
pub const RTPROT_OSPF = @as(c_int, 188);
pub const RTPROT_RIP = @as(c_int, 189);
pub const RTPROT_EIGRP = @as(c_int, 192);
pub const RTM_F_NOTIFY = @as(c_int, 0x100);
pub const RTM_F_CLONED = @as(c_int, 0x200);
pub const RTM_F_EQUALIZE = @as(c_int, 0x400);
pub const RTM_F_PREFIX = @as(c_int, 0x800);
pub const RTM_F_LOOKUP_TABLE = @as(c_int, 0x1000);
pub const RTM_F_FIB_MATCH = @as(c_int, 0x2000);
pub const RTM_F_OFFLOAD = @as(c_int, 0x4000);
pub const RTM_F_TRAP = @import("std").zig.c_translation.promoteIntLiteral(c_int, 0x8000, .hexadecimal);
pub const RTM_F_OFFLOAD_FAILED = @import("std").zig.c_translation.promoteIntLiteral(c_int, 0x20000000, .hexadecimal);
pub const RTA_MAX = __RTA_MAX - @as(c_int, 1);
pub inline fn RTM_RTA(r: anytype) [*c]struct_rtattr {
    return @import("std").zig.c_translation.cast([*c]struct_rtattr, @import("std").zig.c_translation.cast([*c]u8, r) + NLMSG_ALIGN(@import("std").zig.c_translation.sizeof(struct_rtmsg)));
}
pub inline fn RTM_PAYLOAD(n: anytype) @TypeOf(NLMSG_PAYLOAD(n, @import("std").zig.c_translation.sizeof(struct_rtmsg))) {
    return NLMSG_PAYLOAD(n, @import("std").zig.c_translation.sizeof(struct_rtmsg));
}
pub const RTNH_F_DEAD = @as(c_int, 1);
pub const RTNH_F_PERVASIVE = @as(c_int, 2);
pub const RTNH_F_ONLINK = @as(c_int, 4);
pub const RTNH_F_OFFLOAD = @as(c_int, 8);
pub const RTNH_F_LINKDOWN = @as(c_int, 16);
pub const RTNH_F_UNRESOLVED = @as(c_int, 32);
pub const RTNH_F_TRAP = @as(c_int, 64);
pub const RTNH_COMPARE_MASK = ((RTNH_F_DEAD | RTNH_F_LINKDOWN) | RTNH_F_OFFLOAD) | RTNH_F_TRAP;
pub const RTNH_ALIGNTO = @as(c_int, 4);
pub inline fn RTNH_ALIGN(len: anytype) @TypeOf(((len + RTNH_ALIGNTO) - @as(c_int, 1)) & ~(RTNH_ALIGNTO - @as(c_int, 1))) {
    return ((len + RTNH_ALIGNTO) - @as(c_int, 1)) & ~(RTNH_ALIGNTO - @as(c_int, 1));
}
pub inline fn RTNH_OK(rtnh: anytype, len: anytype) @TypeOf((rtnh.*.rtnh_len >= @import("std").zig.c_translation.sizeof(struct_rtnexthop)) and (@import("std").zig.c_translation.cast(c_int, rtnh.*.rtnh_len) <= len)) {
    return (rtnh.*.rtnh_len >= @import("std").zig.c_translation.sizeof(struct_rtnexthop)) and (@import("std").zig.c_translation.cast(c_int, rtnh.*.rtnh_len) <= len);
}
pub inline fn RTNH_NEXT(rtnh: anytype) [*c]struct_rtnexthop {
    return @import("std").zig.c_translation.cast([*c]struct_rtnexthop, @import("std").zig.c_translation.cast([*c]u8, rtnh) + RTNH_ALIGN(rtnh.*.rtnh_len));
}
pub inline fn RTNH_LENGTH(len: anytype) @TypeOf(RTNH_ALIGN(@import("std").zig.c_translation.sizeof(struct_rtnexthop)) + len) {
    return RTNH_ALIGN(@import("std").zig.c_translation.sizeof(struct_rtnexthop)) + len;
}
pub inline fn RTNH_SPACE(len: anytype) @TypeOf(RTNH_ALIGN(RTNH_LENGTH(len))) {
    return RTNH_ALIGN(RTNH_LENGTH(len));
}
pub inline fn RTNH_DATA(rtnh: anytype) [*c]struct_rtattr {
    return @import("std").zig.c_translation.cast([*c]struct_rtattr, @import("std").zig.c_translation.cast([*c]u8, rtnh) + RTNH_LENGTH(@as(c_int, 0)));
}
pub const RTNETLINK_HAVE_PEERINFO = @as(c_int, 1);
pub const RTAX_MAX = __RTAX_MAX - @as(c_int, 1);
pub const RTAX_FEATURE_ECN = @as(c_int, 1) << @as(c_int, 0);
pub const RTAX_FEATURE_SACK = @as(c_int, 1) << @as(c_int, 1);
pub const RTAX_FEATURE_TIMESTAMP = @as(c_int, 1) << @as(c_int, 2);
pub const RTAX_FEATURE_ALLFRAG = @as(c_int, 1) << @as(c_int, 3);
pub const RTAX_FEATURE_MASK = ((RTAX_FEATURE_ECN | RTAX_FEATURE_SACK) | RTAX_FEATURE_TIMESTAMP) | RTAX_FEATURE_ALLFRAG;
pub const PREFIX_MAX = __PREFIX_MAX - @as(c_int, 1);
pub const TCM_IFINDEX_MAGIC_BLOCK = @import("std").zig.c_translation.promoteIntLiteral(c_uint, 0xFFFFFFFF, .hexadecimal);
pub const TCA_MAX = __TCA_MAX - @as(c_int, 1);
pub const TCA_DUMP_FLAGS_TERSE = @as(c_int, 1) << @as(c_int, 0);
pub inline fn TCA_RTA(r: anytype) [*c]struct_rtattr {
    return @import("std").zig.c_translation.cast([*c]struct_rtattr, @import("std").zig.c_translation.cast([*c]u8, r) + NLMSG_ALIGN(@import("std").zig.c_translation.sizeof(struct_tcmsg)));
}
pub inline fn TCA_PAYLOAD(n: anytype) @TypeOf(NLMSG_PAYLOAD(n, @import("std").zig.c_translation.sizeof(struct_tcmsg))) {
    return NLMSG_PAYLOAD(n, @import("std").zig.c_translation.sizeof(struct_tcmsg));
}
pub const NDUSEROPT_MAX = __NDUSEROPT_MAX - @as(c_int, 1);
pub const RTMGRP_LINK = @as(c_int, 1);
pub const RTMGRP_NOTIFY = @as(c_int, 2);
pub const RTMGRP_NEIGH = @as(c_int, 4);
pub const RTMGRP_TC = @as(c_int, 8);
pub const RTMGRP_IPV4_IFADDR = @as(c_int, 0x10);
pub const RTMGRP_IPV4_MROUTE = @as(c_int, 0x20);
pub const RTMGRP_IPV4_ROUTE = @as(c_int, 0x40);
pub const RTMGRP_IPV4_RULE = @as(c_int, 0x80);
pub const RTMGRP_IPV6_IFADDR = @as(c_int, 0x100);
pub const RTMGRP_IPV6_MROUTE = @as(c_int, 0x200);
pub const RTMGRP_IPV6_ROUTE = @as(c_int, 0x400);
pub const RTMGRP_IPV6_IFINFO = @as(c_int, 0x800);
pub const RTMGRP_DECnet_IFADDR = @as(c_int, 0x1000);
pub const RTMGRP_DECnet_ROUTE = @as(c_int, 0x4000);
pub const RTMGRP_IPV6_PREFIX = @import("std").zig.c_translation.promoteIntLiteral(c_int, 0x20000, .hexadecimal);
pub const RTNLGRP_MAX = __RTNLGRP_MAX - @as(c_int, 1);
pub const TCA_ACT_TAB = TCA_ROOT_TAB;
pub const TCAA_MAX = TCA_ROOT_TAB;
pub const TCA_ROOT_MAX = __TCA_ROOT_MAX - @as(c_int, 1);
pub inline fn TA_RTA(r: anytype) [*c]struct_rtattr {
    return @import("std").zig.c_translation.cast([*c]struct_rtattr, @import("std").zig.c_translation.cast([*c]u8, r) + NLMSG_ALIGN(@import("std").zig.c_translation.sizeof(struct_tcamsg)));
}
pub inline fn TA_PAYLOAD(n: anytype) @TypeOf(NLMSG_PAYLOAD(n, @import("std").zig.c_translation.sizeof(struct_tcamsg))) {
    return NLMSG_PAYLOAD(n, @import("std").zig.c_translation.sizeof(struct_tcamsg));
}
pub const TCA_FLAG_LARGE_DUMP_ON = @as(c_int, 1) << @as(c_int, 0);
pub const TCA_ACT_FLAG_LARGE_DUMP_ON = TCA_FLAG_LARGE_DUMP_ON;
pub const TCA_ACT_FLAG_TERSE_DUMP = @as(c_int, 1) << @as(c_int, 1);
pub const RTEXT_FILTER_VF = @as(c_int, 1) << @as(c_int, 0);
pub const RTEXT_FILTER_BRVLAN = @as(c_int, 1) << @as(c_int, 1);
pub const RTEXT_FILTER_BRVLAN_COMPRESSED = @as(c_int, 1) << @as(c_int, 2);
pub const RTEXT_FILTER_SKIP_STATS = @as(c_int, 1) << @as(c_int, 3);
pub const RTEXT_FILTER_MRP = @as(c_int, 1) << @as(c_int, 4);
pub const RTEXT_FILTER_CFM_CONFIG = @as(c_int, 1) << @as(c_int, 5);
pub const RTEXT_FILTER_CFM_STATUS = @as(c_int, 1) << @as(c_int, 6);
pub const RTEXT_FILTER_MST = @as(c_int, 1) << @as(c_int, 7);
pub const __ELL_DBUS_H = "";
pub const L_DBUS_INTERFACE_DBUS = "org.freedesktop.DBus";
pub const L_DBUS_INTERFACE_INTROSPECTABLE = "org.freedesktop.DBus.Introspectable";
pub const L_DBUS_INTERFACE_PROPERTIES = "org.freedesktop.DBus.Properties";
pub const L_DBUS_INTERFACE_OBJECT_MANAGER = "org.freedesktop.DBus.ObjectManager";
pub inline fn L_DBUS_MATCH_ARGUMENT(i: anytype) @TypeOf(L_DBUS_MATCH_ARG0 + i) {
    return L_DBUS_MATCH_ARG0 + i;
}
pub const __ELL_SERVICE_H = "";
pub const __ELL_DBUS_CLIENT_H = "";
pub const __ELL_DHCP_H = "";
pub const __ELL_DHCP6_H = "";
pub const __ELL_ICMP6_H = "";
pub const __ELL_CERT_H = "";
pub const __ELL_ECC_H = "";
pub const L_ECC_MAX_DIGITS = @as(c_int, 6);
pub const L_ECC_SCALAR_MAX_BYTES = L_ECC_MAX_DIGITS * @as(c_int, 8);
pub const L_ECC_POINT_MAX_BYTES = L_ECC_SCALAR_MAX_BYTES * @as(c_int, 2);
pub const __ELL_ECDH_H = "";
pub const __ELL_TIME_H = "";
pub const L_USEC_PER_SEC = @as(c_ulonglong, 1000000);
pub const L_MSEC_PER_SEC = @as(c_ulonglong, 1000);
pub const L_USEC_PER_MSEC = @as(c_ulonglong, 1000);
pub const L_NSEC_PER_SEC = @as(c_ulonglong, 1000000000);
pub const L_NSEC_PER_MSEC = @as(c_ulonglong, 1000000);
pub const L_NSEC_PER_USEC = @as(c_ulonglong, 1000);
pub const L_TIME_INVALID = @import("std").zig.c_translation.cast(u64, -@as(c_int, 1));
pub const __ELL_GPIO_H = "";
pub const __ELL_PATH_H = "";
pub const __ELL_ACD_H = "";
pub const __ELL_TESTER_H = "";
pub const __ELL_NETCONFIG_H = "";
pub const __locale_data = struct___locale_data;
pub const __locale_struct = struct___locale_struct;
pub const __va_list_tag = struct___va_list_tag;
pub const timeval = struct_timeval;
pub const timespec = struct_timespec;
pub const __pthread_internal_list = struct___pthread_internal_list;
pub const __pthread_internal_slist = struct___pthread_internal_slist;
pub const __pthread_mutex_s = struct___pthread_mutex_s;
pub const __pthread_rwlock_arch_t = struct___pthread_rwlock_arch_t;
pub const __pthread_cond_s = struct___pthread_cond_s;
pub const iovec = struct_iovec;
pub const _IO_FILE = struct__IO_FILE;
pub const tm = struct_tm;
pub const l_ascii = enum_l_ascii;
pub const l_queue = struct_l_queue;
pub const l_queue_entry = struct_l_queue_entry;
pub const l_hashmap = struct_l_hashmap;
pub const l_string = struct_l_string;
pub const l_idle = struct_l_idle;
pub const l_signal = struct_l_signal;
pub const l_timeout = struct_l_timeout;
pub const l_io = struct_l_io;
pub const random_data = struct_random_data;
pub const drand48_data = struct_drand48_data;
pub const l_ringbuf = struct_l_ringbuf;
pub const l_debug_desc = struct_l_debug_desc;
pub const l_checksum = struct_l_checksum;
pub const l_checksum_type = enum_l_checksum_type;
pub const l_settings = struct_l_settings;
pub const l_hwdb = struct_l_hwdb;
pub const l_hwdb_entry = struct_l_hwdb_entry;
pub const l_cipher = struct_l_cipher;
pub const l_cipher_type = enum_l_cipher_type;
pub const l_aead_cipher = struct_l_aead_cipher;
pub const l_aead_cipher_type = enum_l_aead_cipher_type;
pub const l_uintset = struct_l_uintset;
pub const l_key = struct_l_key;
pub const l_cert = struct_l_cert;
pub const l_certchain = struct_l_certchain;
pub const l_tls_version = enum_l_tls_version;
pub const l_tls = struct_l_tls;
pub const l_tls_alert_desc = enum_l_tls_alert_desc;
pub const l_uuid_version = enum_l_uuid_version;
pub const l_keyring = struct_l_keyring;
pub const l_key_feature = enum_l_key_feature;
pub const l_key_type = enum_l_key_type;
pub const l_keyring_restriction = enum_l_keyring_restriction;
pub const l_key_cipher_type = enum_l_key_cipher_type;
pub const l_dir_watch = struct_l_dir_watch;
pub const l_dir_watch_event = enum_l_dir_watch_event;
pub const in_addr = struct_in_addr;
pub const in6_addr = struct_in6_addr;
pub const l_netlink = struct_l_netlink;
pub const l_genl = struct_l_genl;
pub const l_genl_family_info = struct_l_genl_family_info;
pub const l_genl_family = struct_l_genl_family;
pub const l_genl_msg = struct_l_genl_msg;
pub const l_genl_attr = struct_l_genl_attr;
pub const __kernel_sockaddr_storage = struct___kernel_sockaddr_storage;
pub const sockaddr_nl = struct_sockaddr_nl;
pub const nlmsghdr = struct_nlmsghdr;
pub const nlmsgerr = struct_nlmsgerr;
pub const nlmsgerr_attrs = enum_nlmsgerr_attrs;
pub const nl_pktinfo = struct_nl_pktinfo;
pub const nl_mmap_req = struct_nl_mmap_req;
pub const nl_mmap_hdr = struct_nl_mmap_hdr;
pub const nl_mmap_status = enum_nl_mmap_status;
pub const nlattr = struct_nlattr;
pub const nla_bitfield32 = struct_nla_bitfield32;
pub const netlink_attribute_type = enum_netlink_attribute_type;
pub const netlink_policy_type_attr = enum_netlink_policy_type_attr;
pub const rtnl_link_stats = struct_rtnl_link_stats;
pub const rtnl_link_stats64 = struct_rtnl_link_stats64;
pub const rtnl_hw_stats64 = struct_rtnl_hw_stats64;
pub const rtnl_link_ifmap = struct_rtnl_link_ifmap;
pub const in6_addr_gen_mode = enum_in6_addr_gen_mode;
pub const ifla_bridge_id = struct_ifla_bridge_id;
pub const ifla_cacheinfo = struct_ifla_cacheinfo;
pub const ifla_vlan_flags = struct_ifla_vlan_flags;
pub const ifla_vlan_qos_mapping = struct_ifla_vlan_qos_mapping;
pub const macvlan_mode = enum_macvlan_mode;
pub const macvlan_macaddr_mode = enum_macvlan_macaddr_mode;
pub const macsec_validation_type = enum_macsec_validation_type;
pub const macsec_offload = enum_macsec_offload;
pub const ipvlan_mode = enum_ipvlan_mode;
pub const tunnel_msg = struct_tunnel_msg;
pub const ifla_vxlan_port_range = struct_ifla_vxlan_port_range;
pub const ifla_vxlan_df = enum_ifla_vxlan_df;
pub const ifla_geneve_df = enum_ifla_geneve_df;
pub const ifla_gtp_role = enum_ifla_gtp_role;
pub const ifla_vf_mac = struct_ifla_vf_mac;
pub const ifla_vf_broadcast = struct_ifla_vf_broadcast;
pub const ifla_vf_vlan = struct_ifla_vf_vlan;
pub const ifla_vf_vlan_info = struct_ifla_vf_vlan_info;
pub const ifla_vf_tx_rate = struct_ifla_vf_tx_rate;
pub const ifla_vf_rate = struct_ifla_vf_rate;
pub const ifla_vf_spoofchk = struct_ifla_vf_spoofchk;
pub const ifla_vf_guid = struct_ifla_vf_guid;
pub const ifla_vf_link_state = struct_ifla_vf_link_state;
pub const ifla_vf_rss_query_en = struct_ifla_vf_rss_query_en;
pub const ifla_vf_trust = struct_ifla_vf_trust;
pub const ifla_port_vsi = struct_ifla_port_vsi;
pub const if_stats_msg = struct_if_stats_msg;
pub const ifla_rmnet_flags = struct_ifla_rmnet_flags;
pub const ifaddrmsg = struct_ifaddrmsg;
pub const ifa_cacheinfo = struct_ifa_cacheinfo;
pub const ndmsg = struct_ndmsg;
pub const nda_cacheinfo = struct_nda_cacheinfo;
pub const ndt_stats = struct_ndt_stats;
pub const ndtmsg = struct_ndtmsg;
pub const ndt_config = struct_ndt_config;
pub const rtattr = struct_rtattr;
pub const rtmsg = struct_rtmsg;
pub const rt_scope_t = enum_rt_scope_t;
pub const rt_class_t = enum_rt_class_t;
pub const rtattr_type_t = enum_rtattr_type_t;
pub const rtnexthop = struct_rtnexthop;
pub const rtvia = struct_rtvia;
pub const rta_cacheinfo = struct_rta_cacheinfo;
pub const rta_session = struct_rta_session;
pub const rta_mfc_stats = struct_rta_mfc_stats;
pub const rtgenmsg = struct_rtgenmsg;
pub const ifinfomsg = struct_ifinfomsg;
pub const prefixmsg = struct_prefixmsg;
pub const prefix_cacheinfo = struct_prefix_cacheinfo;
pub const tcmsg = struct_tcmsg;
pub const nduseroptmsg = struct_nduseroptmsg;
pub const rtnetlink_groups = enum_rtnetlink_groups;
pub const tcamsg = struct_tcamsg;
pub const l_rtnl_address = struct_l_rtnl_address;
pub const l_rtnl_route = struct_l_rtnl_route;
pub const l_dbus_bus = enum_l_dbus_bus;
pub const l_dbus_match_type = enum_l_dbus_match_type;
pub const l_dbus = struct_l_dbus;
pub const l_dbus_interface = struct_l_dbus_interface;
pub const l_dbus_message_builder = struct_l_dbus_message_builder;
pub const l_dbus_message = struct_l_dbus_message;
pub const l_dbus_message_iter = struct_l_dbus_message_iter;
pub const l_dbus_method_flag = enum_l_dbus_method_flag;
pub const l_dbus_signal_flag = enum_l_dbus_signal_flag;
pub const l_dbus_property_flag = enum_l_dbus_property_flag;
pub const l_dbus_client = struct_l_dbus_client;
pub const l_dbus_proxy = struct_l_dbus_proxy;
pub const l_dhcp_client = struct_l_dhcp_client;
pub const l_dhcp_lease = struct_l_dhcp_lease;
pub const l_dhcp_server = struct_l_dhcp_server;
pub const l_dhcp_option = enum_l_dhcp_option;
pub const l_dhcp_client_event = enum_l_dhcp_client_event;
pub const l_dhcp_server_event = enum_l_dhcp_server_event;
pub const l_dhcp6_client = struct_l_dhcp6_client;
pub const l_dhcp6_lease = struct_l_dhcp6_lease;
pub const l_icmp6_client = struct_l_icmp6_client;
pub const l_dhcp6_option = enum_l_dhcp6_option;
pub const l_dhcp6_client_event = enum_l_dhcp6_client_event;
pub const l_icmp6_router = struct_l_icmp6_router;
pub const l_icmp6_client_event = enum_l_icmp6_client_event;
pub const l_cert_key_type = enum_l_cert_key_type;
pub const l_ecc_curve = struct_l_ecc_curve;
pub const l_ecc_point = struct_l_ecc_point;
pub const l_ecc_scalar = struct_l_ecc_scalar;
pub const l_ecc_point_type = enum_l_ecc_point_type;
pub const l_gpio_chip = struct_l_gpio_chip;
pub const l_gpio_writer = struct_l_gpio_writer;
pub const l_gpio_reader = struct_l_gpio_reader;
pub const l_acd = struct_l_acd;
pub const l_acd_event = enum_l_acd_event;
pub const l_acd_defend_policy = enum_l_acd_defend_policy;
pub const l_tester = struct_l_tester;
pub const l_tester_stage = enum_l_tester_stage;
pub const l_netconfig = struct_l_netconfig;
pub const l_netconfig_event = enum_l_netconfig_event;
