const std = @import("std");
const ell_builder = @import("libs/ell/build.zig");

pub fn build(b: *std.build.Builder) !void {
    // Standard target options allows the person running `zig build` to choose
    // what target to build for. Here we do not override the defaults, which
    // means any target is allowed, and the default is native. Other options
    // for restricting supported target set are available.
    const target = b.standardTargetOptions(.{});

    // Standard release options allow the person running `zig build` to select
    // between Debug, ReleaseSafe, ReleaseFast, and ReleaseSmall.
    const mode = b.standardReleaseOptions();

    const ell = try ell_builder.build(b, mode, target);

    const dhcp_client = b.addExecutable("dhcp-client", "src/dhcp_client.zig");
    dhcp_client.linkLibC();
    dhcp_client.addIncludePath("zig-out/lib/ell/include");
    dhcp_client.linkLibrary(ell);
    dhcp_client.setTarget(target);
    dhcp_client.setBuildMode(mode);
    dhcp_client.install();
}
